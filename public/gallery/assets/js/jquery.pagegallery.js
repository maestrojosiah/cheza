/* 
	Author: http://codecanyon.net/user/sike
*/      

;(function($){      
	function openImage(_n, thumbArr, _this){
			thumbArr[_n].addClass('selected').siblings().removeClass('selected');
			var _thumb = thumbArr[_n].find('img'),
				largesrc = _thumb.data('large'),
				title = _thumb.data('caption'),
				type = _thumb.data('type');
			var _container = $('div#' + _this.data('settings').largeContainerID);
			// _container.find('div.closeButton').css('opacity', 0);
			_container.find('div.closeButton').hide();
			clearInterval(_this.data('slideShowInt'));
			if(type!='video'){
				$('<img/>').load( function() {	
					 _container.find('img').remove();
					 _container.find('span').remove();
					 _container.find('iframe').remove();					
					 _container.append('<img src="' + largesrc + '"/>');
					 var _img = _container.find('img');
					 _img.addClass('animated '+ _this.data('settings').largeImgEase);
					imgRealHeight = this.height;
					if(_this.data('settings').closeButtonPosition=="left"){
						_container.find('div.closeButton').css({
							marginTop: -14,
							marginLeft: (_this.width() - this.width)*.5 - 14
						});											
					}else{
						_container.find('div.closeButton').css({
							marginTop: -14,
							marginLeft: this.width + (_this.width() - this.width)*.5 - 14
						});											
					}
					_container.find('div.closeButton').delay(600).fadeIn()//stop(true).animate({opacity: 1}, 300);
					_this.data('imgRealHeight', imgRealHeight);
					_container.css('marginTop', ($(window).height() - imgRealHeight)*.5 + _this.data('settings').largeContainerOffset + 'px');

					if(title!=""&&title){
						_container.append('<span class="caption">' + title + '</span>');				
						_container.find('span').addClass('animate1 '+ _this.data('settings').largeImgEase);
					}
					
					_this.data('arrowButton').show();
					clearInterval(_this.data('slideShowInt'));	
					if(_this.data('slideShow')&&_this.data('settings').slideShow){
						var slideShowInt = setInterval(function() {
							var _n = _this.data('currentImgNum');
							_n++;
							if(_n>thumbArr.length-1) _n = 0; 
							nextImage(_n, thumbArr, _this);				
							_this.data('currentImgNum', _n);
						}, _this.data('settings').slideShowDelay);
						_this.data('slideShowInt', slideShowInt);				
						
					}				
					
					
					
				}).attr( 'src', largesrc);
				
				
			}else{
				var _width = _thumb.data('width');
				var _height = _thumb.data('height');
				_this.data('imgRealHeight', _height);				
				 _container.find('img').remove();
				 _container.find('span').remove();
				 _container.find('iframe').remove();					
				
				if(_this.data('settings').closeButtonPosition=="left"){
					_container.find('div.closeButton').css({
						marginTop: -14,
						marginLeft: (_this.width() - _width)*.5 - 14
					});											
				}else{
					_container.find('div.closeButton').css({
						marginTop: -14,
						marginLeft: _width + (_this.width() - _width)*.5 - 14
					});											
				}
				_container.find('div.closeButton').delay(600).fadeIn();
				
	 			_container.append('<iframe src="' + largesrc + ' + "width="'+_width+' + "height="'+_height +' "frameborder="0" webkitAllowFullScreen allowFullScreen />');				
				_container.css('marginTop', ($(window).height() - _height)*.5 + _this.data('settings').largeContainerOffset + 'px')					
				
				if(title!=""&&title){
					_container.append('<span class="caption">' + title + '</span>');				
					_container.find('span').addClass('animate1 '+ _this.data('settings').largeImgEase);
				}
				
	
			}
			
					
	}
	
	function nextImage(_n, thumbArr, _this){
		if(thumbArr[_this.data('currentImgNum')].find('img').data('type')!="video"){
			/*
			var _n = currentImgNum;
			_n++;
			if(_n>thumbArr.length-1) _n = 0;
			*/
			openImage(_n, thumbArr, _this);
			$("div.holder").jPages(Math.floor(_n/_this.data('settings').perPage)+1);
		}
	}


	$.fn.extend({
		pageGallery: function(options) {
	      	// plugin default options, it's extendable
			var settings = { 
	            thumbContainerID: 'thumbnails',
				largeContainerID: 'largeImage',
	            perPage: 5,
				gridThumbEase: false,
				listThumbEase: false,
				largeImgEase: 'fadeInUp',
				largeContainerOffset: -80,
				largeImgClickable: true,
				closeButtonPosition : 'right',  // left | right
				slideShow: true,
				slideShowDelay: 5000
			}; 
			
  			// extends settings with options provided
	        if (options) {
				$.extend(settings, options);
			} 
			
			var _this = this;    
			
			var _largeContainer = $('div#' + settings.largeContainerID);       
			
			// use jPage to generate the thumbnails navigation
			$('div.holder').jPages({
	            containerID : settings.thumbContainerID,
	            perPage     : 5,
				/*
	            previous    : ".prev",
	            next        : ".next",		
	            links       : 'blank',
				*/
	            direction   : 'auto',
	            animation   : settings.listThumbEase
        	});		

			var imgRealHeight;
			var thumbArr = [];
			var _perPage = settings.perPage;
			_this.data('settings', settings);
			
			$('ul#'+settings.thumbContainerID+' li').each(function(index) {
				thumbArr[index] = $(this);
				$(this).attr('rel', index);					
			});
			
			
			$('ul#'+settings.thumbContainerID+' li').on('click', function(event) {
				var _n = $(this).attr('rel');									
				if(isGrid){
					 _largeContainer.find('img').remove();
					 _largeContainer.find('span').remove();
					 _largeContainer.find('iframe').remove();	
					 _largeContainer.fadeIn();
					$('div.holder').jPages("destroy").jPages({
			            containerID : settings.thumbContainerID,
			            perPage     : 5,
						/*
			            previous    : ".prev",
			            next        : ".next",			
			            links       : 'blank',
						*/			
			            direction   : 'auto',
			            animation   : settings.listThumbEase
		        	});	
					_perPage = settings.perPage;
					$("div.holder").jPages(Math.floor(_n/_perPage)+1);	
					closeButton.hide();		
					$('div.thumbContainer').animate({marginBottom: 0}, 600, function(){
						_largeContainer.show();
						openImage(_n, thumbArr, _this);
						currentImgNum = _n;
						_this.data('currentImgNum');
					});	
					
					isGrid = false;						
					
				}else{

					if(_this.data('currentImgNum')!=_n){
						arrowButton.fadeIn();
						openImage(_n, thumbArr, _this);
						currentImgNum = _n;
						_this.data('currentImgNum', currentImgNum);
					}
				}
				

				
			});
			
			
			// add the close button
			var closeButton = $('<div class="' + settings.closeButtonClass + ' closeButton"></div>');					
			// closeButton.hide();			
			_largeContainer.append(closeButton);
			closeButton.css({
			  /*
			  	opacity: 0,
			  */
			  cursor: 'pointer'
			});
            closeButton.on('click', function(event) {
				// closeButton.stop(true).animate({opacity: 0}, 300);
				_largeContainer.fadeOut();
				// $('ul#'+settings.thumbContainerID+' li').css('marginRight', 20);					
    		 	$('div.holder').jPages('destroy').jPages({
		            containerID   : settings.thumbContainerID,
		            perPage       : 15,			            
		            direction   : 'auto',
		            animation   : settings.gridThumbEase,
					callback    : function(){
						$('div.thumbContainer').css({
							'marginBottom': ($(window).height()-$('div.thumbContainer').height())*.5
						});											
					}
	
		        });		
				_perPage = 12;
				$('div.thumbContainer').animate({marginBottom: ($(window).height()-$('div.thumbContainer').height())*.5}, 600);				
				arrowButton.fadeOut();
			  	clearInterval(_this.data('slideShowInt'));	
				_this.data('slideShow', false);					
				isGrid = true;
				
            });     			
			
			// add the arrow buttons
			var arrowButton = $('<div id="arrowButton"><div class="prevArrow"></div><div class="nextArrow"></div></div>');	   		 			
			_this.data('arrowButton', arrowButton);
			var _nextBtn, _prevBtn, arrowHeight;
			if($('#arrowButton')[0]==undefined){
				$(document.body).append(arrowButton); 				   	 					
				arrowButton.hide();
	            _nextBtn = $('.nextArrow', arrowButton).on('click', function(event) {
					var _n = currentImgNum;
					_n++;
					if(_n>thumbArr.length-1) _n = 0;
					openImage(_n, thumbArr, _this);
					$("div.holder").jPages(Math.floor(_n/settings.perPage)+1);
					currentImgNum = _n;
					_this.data('currentImgNum', currentImgNum);
	            });;
	            _prevBtn = $('.prevArrow', arrowButton).on('click', function(event) {
					var _n = currentImgNum;
					_n--;
					if(_n<0) _n = thumbArr.length-1;
					openImage(_n, thumbArr, _this);
					$("div.holder").jPages(Math.floor(_n/settings.perPage)+1);
					currentImgNum = _n;
					_this.data('currentImgNum', currentImgNum);
	            });
				arrowHeight = _nextBtn.height();
				// var ph = arrowButton.parent().height();
				var mh = Math.ceil(($(window).height()-arrowHeight)*.5 + settings.largeContainerOffset);
				arrowButton.css('top', mh);	                               
				$(window).resize(function(event){
					var mh = Math.ceil(($(window).height()-arrowHeight)*.5 + settings.largeContainerOffset);
					arrowButton.css('top', mh);	 					
				})
				
			}
			
			
	
			// load the first image by default
			openImage(0, thumbArr, _this);
			var currentImgNum = 0;
			_this.data('currentImgNum', currentImgNum);
			var isGrid = false;
			
			
			_largeContainer.on('mouseover', function(event) {
			  	clearInterval(_this.data('slideShowInt'));	
				_this.data('slideShow', false);
			});
			_largeContainer.on('mouseout', function(event) {
				if(settings.slideShow&&!isGrid){
					_this.data('slideShow', true);
					// _this.data('slideShow', settings.slideShow);
					if(thumbArr[currentImgNum].find('img').data('type')!="video"){				
						var slideShowInt = setInterval(function() {
							var _n = currentImgNum;
							_n++;
							if(_n>thumbArr.length-1) _n = 0; 
							nextImage(_n, thumbArr, _this);				
							currentImgNum = _n;
							_this.data('currentImgNum', currentImgNum);
						}, _this.data('settings').slideShowDelay);
						_this.data('slideShowInt', slideShowInt);				
					}
					
				}
			  	
			});
			if(settings.largeImgClickable){
				_largeContainer.on('click', function(event) {
					if(thumbArr[currentImgNum].find('img').data('type')!="video"&&event.target.nodeName.toLowerCase()=="img"){
						var _n = currentImgNum;
						_n++;
						if(_n>thumbArr.length-1) _n = 0;
						openImage(_n, thumbArr, _this);
						$("div.holder").jPages(Math.floor(_n/settings.perPage)+1);
						currentImgNum = _n;
						_this.data('currentImgNum', currentImgNum);
					}
				
				});				
			}
			
			/*
			if(settings.slideShow){
				var slideShowInt = setInterval(function() {
					var _n = currentImgNum;
					_n++;
					if(_n>thumbArr.length-1) _n = 0; 
					nextImage(_n, thumbArr, _this);				
					currentImgNum = _n;
					_this.data('currentImgNum', currentImgNum);
				}, _this.data('settings').slideShowDelay);
				_this.data('slideShowInt', slideShowInt);								
			}
			*/
			
			_this.data('slideShow', settings.slideShow);
			
			


			// resize handler
			$(window).resize(function() {
				imgRealHeight = _this.data('imgRealHeight');
				_largeContainer.css('marginTop', ($(window).height() - imgRealHeight)*.5 + settings.largeContainerOffset + 'px');					
				if(isGrid){
					$('div.thumbContainer').css('marginBottom', ($(window).height()-$('div.thumbContainer').height())*.5);				
				}
			});			
			
			// the public function
			_this.destroy = function(){
				$('div.holder').jPages("destroy");
				$('ul#'+settings.thumbContainerID+' li').off();				
				closeButton.remove();
				arrowButton.remove();				
				_largeContainer.off();
				currentImgNum = 0;
				$('div.thumbContainer').animate({marginBottom: 0}, 600, function(){
					_largeContainer.fadeIn();
					// openImage(currentImgNum, thumbArr, _this);
				});				
				clearInterval(_this.data('slideShowInt'));
				_this.data('slideShow', false);
				_this.removeData('settings');
				isGrid = false;	
				
			}			
		
			return this;
		}

	});
		
})(jQuery);