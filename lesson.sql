-- MySQL dump 10.13  Distrib 8.0.37, for Linux (x86_64)
--
-- Host: localhost    Database: chezamus_db
-- ------------------------------------------------------
-- Server version	8.0.37-0ubuntu0.24.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lesson` (
  `id` int NOT NULL AUTO_INCREMENT,
  `presentation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_required` tinyint(1) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson`
--

LOCK TABLES `lesson` WRITE;
/*!40000 ALTER TABLE `lesson` DISABLE KEYS */;
INSERT INTO `lesson` VALUES (1,'Music-theory-grade-1-lesson-1-630489c21e2c3.pptx','Music-Theory-Grade-1-lesson-1-630489c21f19f.pdf','Rhythm',0,'rhythm','grade-1'),(2,'Music-Theory-Grade-1-lesson-2-630487e1a8b41.pptx','Music-Theory-Grade-1-lesson-2-630487e1a93af.pdf','Rhythm Part 2',0,'rhythm-2','grade-1'),(3,'Music-Theory-Grade-1-lesson-3-631966d6952b2.pptx','Music-Theory-Grade-1-lesson-3-631966d69ae3d.pdf','Rhythm Part 3',0,'rhythm-3','grade-1'),(4,'Music-Theory-Grade-1-lesson-4-63234c6a51957.pptx','Music-Theory-Grade-1-lesson-4-63234c6a58387.pdf','Keys and Key Signatures',0,'keys-and-key-signatures','grade-1'),(5,'Music-Theory-Grade-1-Terms-and-Signs-633120f739e23.pptx','Music-Theory-Grade-1-Terms-and-Signs-633120f73a9b0.pdf','Terms and Signs',0,'terms-signs','grade-1'),(6,'Music-theory-Grade-2-Lesson-1-6335cc44b56c7.pptx','Music-theory-Grade-2-Lesson-1-6335cc44bc2ac.pdf','Pitch',0,'pitch','grade-2'),(7,'Music-Theory-Grade-2-Lesson-2-6348d8c27c802.pptx','Music-Theory-Grade-2-Lesson-2-6348d8c282831.pdf','Rhythm Part 2',0,'rhythm-2','grade-2'),(8,'Music-Theory-Grade-2-Lesson-3-63689c87a0e37.pptx','Music-Theory-Grade-2-Lesson-3-63689c87a6e82.pdf','Keys and Scales Part 1',0,'keys-scales-1','grade-2'),(9,'Music-Theory-Grade-2-Lesson-4-63873a989d83c.pptx','Music-Theory-Grade-2-Lesson-4-63873a98a3565.pdf','Keys and Scales Part 2',0,'keys-scales-2','grade-2'),(10,'Music-Theory-Grade-2-Lesson-5-647577cbf41f8.pptx','Music-Theory-Grade-2-Lesson-5-647577cc020f3.pdf','Intervals',0,'intervals','grade-2'),(11,'Music-Theory-Grade-3-Lesson-1-6486fb7631c94.pptx','Music-Theory-Grade-3-Lesson-1-6486fb76526d0.pdf','Rhythm Part 1',0,'rhythm-1','grade-3'),(12,'Music-Theory-Grade-3-Lesson-2-648adfbeae9b7.pptx','Music-Theory-Grade-3-Lesson-2-648adfbecfea2.pdf','Rhythm Part 2',0,'rhythm-2','grade-3'),(13,'Music-theory-grade-1-lesson-1-630489c21e2c3-66824ed315a23.pptx','Music-Theory-Grade-1-lesson-1-630489c21f19f-66824ed316169.pdf','Pitch',0,'pitch','grade-1'),(14,'Music-Theory-Grade-1-lesson-2-630487e1a8b41-6682560c4c4fa.pptx','Music-Theory-Grade-1-lesson-2-630487e1a93af-6682560c4cebe.pdf','Pitch Part 2',0,'pitch-2','grade-1'),(15,'Music-Theory-Grade-1-lesson-3-631966d6952b2-66825d44b0991.pptx','Music-Theory-Grade-1-lesson-3-631966d69ae3d-66825d44b0eb0.pdf','Scales',0,'scales','grade-1'),(16,'Music-Theory-Grade-1-lesson-4-63234c6a51957-66825ea71124d.pptx','Music-Theory-Grade-1-lesson-4-63234c6a58387-66825ea711620.pdf','Intervals',0,'intervals','grade-1'),(17,'Music-Theory-Grade-1-lesson-4-63234c6a51957-668260792c5e1.pptx','Music-Theory-Grade-1-lesson-4-63234c6a58387-668260792c9d1.pdf','Tonic Triads',0,'tonic-triads','grade-1'),(18,'Music-theory-Grade-2-Lesson-1-6335cc44b56c7-668264370a951.pptx','Music-theory-Grade-2-Lesson-1-6335cc44bc2ac-668264370acc0.pdf','Rhythm Part 1',0,'rhythm-1','grade-2'),(19,'Music-Theory-Grade-2-Lesson-2-6348d8c27c802-6682665a8680f.pptx','Music-Theory-Grade-2-Lesson-2-6348d8c282831-6682665a86b44.pdf','Rhythm Part 3',0,'rhythm-3','grade-2'),(20,'Music-Theory-Grade-2-Lesson-5-647577cbf41f8-668268443ea1c.pptx','Music-Theory-Grade-2-Lesson-5-647577cc020f3-668268443ef06.pdf','Tonic Triads',0,'tonic-triads','grade-2'),(21,NULL,NULL,'Keys and Scales Part 1',0,'keys-scales-1','grade-3'),(22,NULL,NULL,'Pitch',0,'pitch','grade-3');
/*!40000 ALTER TABLE `lesson` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-01 12:09:49
