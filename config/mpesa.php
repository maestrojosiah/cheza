<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Account
    |--------------------------------------------------------------------------
    |
    | This is the default account to be used when none is specified.
    */

    'default' => 'production',

    /*
    |--------------------------------------------------------------------------
    | Native File Cache Location
    |--------------------------------------------------------------------------
    |
    | When using the Native Cache driver, this will be the relative directory
    | where the cache information will be stored.
    */

    'cache_location' => 'var/cache',

    /*
    |--------------------------------------------------------------------------
    | Accounts
    |--------------------------------------------------------------------------
    |
    | These are the accounts that can be used with the package. You can configure
    | as many as needed. Two have been setup for you.
    |
    | Sandbox: Determines whether to use the sandbox, Possible values: sandbox | production
    | Initiator: This is the username used to authenticate the transaction request
    | LNMO:
    |    paybill: Your paybill number
    |    shortcode: Your business shortcode
    |    passkey: The passkey for the paybill number
    |    callback: Endpoint that will be be queried on completion or failure of the transaction.
    |
    */

    'accounts' => [
        'staging' => [
            'sandbox' => true,
            'key' => 'IiSvzrhQ5yrDxlLgub1MkVPBV39MPA80',
            'secret' => 'uchLyChLtkDGyiHU',
            'initiator' => 'testapi',
            'id_validation_callback' => 'https://chezamusicschool.co.ke/touch/script/pmt',
            'lnmo' => [
                'paybill' => 174379,
                'shortcode' => 174379,
                'passkey' => 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
                'callback' => 'https://chezamusicschool.co.ke/touch/script/pmt',
            ]
        ],

        'production' => [
            'sandbox' => false,
            'key' => 'KZ9AbGcfYRbTOfWqIfi4E4kGrIYZbB2d',
            'secret' => 'WK67rhnOP4ArFikl',
            'initiator' => 'MUSKE MUSICIANS PORTAL LIMITED',
            'id_validation_callback' => 'https://chezamusicschool.co.ke/touch/script/pmt',
            'lnmo' => [
                'paybill' => 9810765,
                'shortcode' => 7927542,
                'passkey' => '7d5f5f458c9950cce146bf342b934afef5f78cf5248d86a81c31b8270ed3ae06',
                'callback' => 'https://chezamusicschool.co.ke/touch/script/pmt',
            ]
        ],
    ],
];