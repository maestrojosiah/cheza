<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\NavigationRepository;
use App\Repository\UserSettingsRepository;

class GlobalVars extends AbstractExtension
{
    public function __construct(private readonly UserRepository $usr, private readonly ConfigurationRepository $config, private readonly NavigationRepository $nav)
    {
    }

    public function getFunctions(): ?array
    {
        return [new TwigFunction('global_vars', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {
        $configData = $this->config->findAll();
        $navData = $this->nav->findAll();
        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['configData'] = $configData ? $configData[0] : null;
        $glVars['navData'] = $navData;
        $glVars['thisUrl'] = $url['path'];

        return $glVars[$select];

    }
}
