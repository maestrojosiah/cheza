<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ExternalLinkFilter extends AbstractExtension
{
    public function getFilters(): ?array
    {
        return [new TwigFilter('external_link', $this->externalLinkFilter(...))];
    }

    public function externalLinkFilter($url): array
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", (string) $url)) {
            $url = "http://" . $url;
        }

        return [$url];
    }

    public function getName()
    {
        return 'external_link_filter';
    }
}
