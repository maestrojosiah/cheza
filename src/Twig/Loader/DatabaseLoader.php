<?php

namespace App\Twig\Loader;

use App\Entity\Section;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Loader\LoaderInterface;
use Twig\Error\LoaderError;
use Twig\Source;

class DatabaseLoader implements LoaderInterface
{
    protected $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Section::class);
    }

    public function getSourceContext(string $name): Source
    {
        if (false === $template = $this->getTemplate($name)) {
            throw new LoaderError(sprintf('Template "%s" does not exist.', $name));
        }

        return new Source($template->getTypography(), $name);
    }

    public function exists($name): ?bool
    {
        return (bool)$this->getTemplate($name);
    }


    public function getCacheKey(string $name): string
    {
        if (null === $path = $this->getTemplate($name)) {
            return '';
        }
        return $name;
    }

    public function isFresh($name, $time): bool
    {
        if (false === $template = $this->getTemplate($name)) {
            return false;
        }

        return $template->getLastUpdated()->getTimestamp() < $time;
    }

    /**
     * @param $name
     * @return Template|null
     */
    protected function getTemplate($name)
    {
        return $this->repo->findOneBy(['name' => $name]);
    }
}
