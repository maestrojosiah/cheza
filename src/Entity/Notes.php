<?php

namespace App\Entity;

use App\Repository\NotesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NotesRepository::class)]
class Notes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'notes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\User $user = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createdOn = null;

    #[ORM\Column(type: 'text')]
    private ?string $message = null;

    #[ORM\ManyToOne(targetEntity: UserInstrumentGrade::class, inversedBy: 'notes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\UserInstrumentGrade $uig = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->createdOn;
    }

    public function setCreatedOn(\DateTimeInterface $createdOn): self
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getUig(): ?UserInstrumentGrade
    {
        return $this->uig;
    }

    public function setUig(?UserInstrumentGrade $uig): self
    {
        $this->uig = $uig;

        return $this;
    }
}
