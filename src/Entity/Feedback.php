<?php

namespace App\Entity;

use App\Repository\FeedbackRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FeedbackRepository::class)]
class Feedback
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private ?string $comment = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $improvement = null;

    #[ORM\ManyToOne(targetEntity: Workshop::class, inversedBy: 'feedback')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Workshop $contact = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $subscription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getImprovement(): ?string
    {
        return $this->improvement;
    }

    public function setImprovement(?string $improvement): self
    {
        $this->improvement = $improvement;

        return $this;
    }

    public function getContact(): ?Workshop
    {
        return $this->contact;
    }

    public function setContact(?Workshop $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getSubscription(): ?string
    {
        return $this->subscription;
    }

    public function setSubscription(?string $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }
}
