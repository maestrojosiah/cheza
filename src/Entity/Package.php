<?php

namespace App\Entity;

use App\Repository\PackageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PackageRepository::class)]
class Package implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $price = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $duration = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $noOfSessions = null;

    #[ORM\OneToMany(targetEntity: Session::class, mappedBy: 'package')]
    private Collection $sessions;


    #[ORM\OneToMany(targetEntity: Course::class, mappedBy: 'package')]
    private Collection $courses;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $costPerLesson = null;

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNoOfSessions(): ?string
    {
        return $this->noOfSessions;
    }

    public function setNoOfSessions(string $noOfSessions): self
    {
        $this->noOfSessions = $noOfSessions;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setPackage($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getPackage() === $this) {
                $session->setPackage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setPackage($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getPackage() === $this) {
                $course->setPackage(null);
            }
        }

        return $this;
    }

    public function getCostPerLesson(): ?string
    {
        return $this->costPerLesson;
    }

    public function setCostPerLesson(?string $costPerLesson): self
    {
        $this->costPerLesson = $costPerLesson;

        return $this;
    }

}
