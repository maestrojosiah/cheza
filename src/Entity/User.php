<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: '`user`')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements PasswordAuthenticatedUserInterface, UserInterface, \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180)]
    private ?string $email = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    private ?string $password = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $active = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $fullname = null;

    #[ORM\Column(type: 'boolean')]
    private bool $isVerified = false;

    #[ORM\OneToMany(targetEntity: Session::class, mappedBy: 'teacher')]
    private Collection $teachersessions;

    #[ORM\OneToMany(targetEntity: Session::class, mappedBy: 'student')]
    private Collection $studentsessions;

    #[ORM\OneToOne(targetEntity: TeacherUserData::class, inversedBy: 'teacher', cascade: ['persist', 'remove'])]
    private ?\App\Entity\TeacherUserData $teacherdata = null;

    #[ORM\OneToOne(targetEntity: StudentUserData::class, inversedBy: 'student', cascade: ['persist', 'remove'])]
    private ?\App\Entity\StudentUserData $studentdata = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $usertype = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\File(mimeTypes: ['image/png', 'image/jpg', 'image/jpeg'])]
    private ?string $photo = null;

    #[ORM\OneToMany(targetEntity: SchoolTime::class, mappedBy: 'teacher')]
    private Collection $schoolTimes;

    #[ORM\OneToMany(targetEntity: Payment::class, mappedBy: 'user')]
    private Collection $payments;

    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'user')]
    private Collection $posts;

    #[ORM\OneToMany(targetEntity: UserSettings::class, mappedBy: 'user')]
    private Collection $userSettings;

    #[ORM\OneToMany(targetEntity: Communication::class, mappedBy: 'admin')]
    private Collection $communications;

    #[ORM\OneToMany(targetEntity: DeductionPayment::class, mappedBy: 'teacher')]
    private Collection $deductionPayments;

    #[ORM\OneToMany(targetEntity: Notes::class, mappedBy: 'user', orphanRemoval: true)]
    private Collection $notes;

    #[ORM\OneToMany(targetEntity: Assignment::class, mappedBy: 'student', orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'DESC'])]
    private Collection $assignments;

    #[ORM\OneToMany(targetEntity: PracticeSessions::class, mappedBy: 'student', orphanRemoval: true)]
    private Collection $practiceSessions;

    #[ORM\OneToMany(targetEntity: Assignment::class, mappedBy: 'teacher')]
    private Collection $teacher_assignments;

    #[ORM\OneToMany(targetEntity: LessonPlan::class, mappedBy: 'student')]
    private Collection $lessonPlans;


    #[ORM\OneToMany(targetEntity: Blog::class, mappedBy: 'user', orphanRemoval: true)]
    private Collection $blogs;

    #[ORM\OneToMany(targetEntity: Comment::class, mappedBy: 'user', orphanRemoval: true)]
    private Collection $comments;

    #[ORM\OneToMany(targetEntity: CommentReply::class, mappedBy: 'user', orphanRemoval: true)]
    private Collection $commentReplies;

    #[ORM\ManyToMany(targetEntity: Course::class, mappedBy: 'teachers')]
    private Collection $courses;

    #[ORM\OneToMany(targetEntity: PhotoUpload::class, mappedBy: 'user', orphanRemoval: true)]
    private Collection $photoUploads;

    #[ORM\OneToMany(targetEntity: ShortUrl::class, mappedBy: 'user')]
    private Collection $shortUrls;

    #[ORM\OneToMany(targetEntity: UigGroup::class, mappedBy: 'user')]
    private Collection $uigGroups;

    #[ORM\OneToMany(targetEntity: Reporting::class, mappedBy: 'user')]
    private Collection $reportings;

    #[ORM\OneToMany(targetEntity: Reporting::class, mappedBy: 'teacher')]
    private Collection $teacher_reports;

    #[ORM\OneToMany(mappedBy: 'student', targetEntity: SimpleReport::class)]
    private Collection $simpleReports;

    #[ORM\OneToMany(mappedBy: 'teacher', targetEntity: SimpleReport::class)]
    private Collection $trReports;

    #[ORM\OneToMany(mappedBy: 'teacher', targetEntity: TeacherInvoice::class)]
    private Collection $teacherInvoices;

    /**
     * @var Collection<int, UserInstrumentGrade>
     */
    #[ORM\OneToMany(mappedBy: 'teacher', targetEntity: UserInstrumentGrade::class)]
    private Collection $userInstrumentGrades;

    public function __toString(): string
    {
        return (string) $this->getFullName();
    }

    public function __construct()
    {
        $this->teachersessions = new ArrayCollection();
        $this->studentsessions = new ArrayCollection();
        $this->schoolTimes = new ArrayCollection();
        $this->payments = new ArrayCollection();
        // $this->settings = new ArrayCollection();
        $this->userSettings = new ArrayCollection();
        $this->communications = new ArrayCollection();
        $this->deductionPayments = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->assignments = new ArrayCollection();
        $this->practiceSessions = new ArrayCollection();
        $this->teacher_assignments = new ArrayCollection();
        $this->lessonPlans = new ArrayCollection();
        $this->blogs = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->commentReplies = new ArrayCollection();
        $this->courses = new ArrayCollection();
        $this->photoUploads = new ArrayCollection();
        $this->shortUrls = new ArrayCollection();
        $this->uigGroups = new ArrayCollection();
        $this->reportings = new ArrayCollection();
        $this->teacher_reports = new ArrayCollection();
        $this->simpleReports = new ArrayCollection();
        $this->trReports = new ArrayCollection();
        $this->teacherInvoices = new ArrayCollection();
        $this->userInstrumentGrades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    // /**
    //  * A visual identifier that represents this user.
    //  *
    //  * @see UserInterface
    //  */
    // public function getUsername(): string
    // {
    //     return (string) $this->email;
    // }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function hasRole(array $roles, $role)
    {
        return in_array($role, $roles);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    // /**
    //  * @see UserInterface
    //  */
    // public function getSalt(): ?string
    // {
    //     // not needed when using the "bcrypt" algorithm in security.yaml
    //     return null;
    // }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getTeacherSessions(): Collection
    {
        return $this->teachersessions;
    }

    public function addTeacherSession(Session $teachersession): self
    {
        if (!$this->teachersessions->contains($teachersession)) {
            $this->teachersessions[] = $teachersession;
            $teachersession->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherSession(Session $teachersession): self
    {
        if ($this->teachersessions->removeElement($teachersession)) {
            // set the owning side to null (unless already changed)`
            if ($teachersession->getTeacher() === $this) {
                $teachersession->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getStudentsessions(): Collection
    {
        return $this->studentsessions;
    }

    public function addStudentsession(Session $studentsession): self
    {
        if (!$this->studentsessions->contains($studentsession)) {
            $this->studentsessions[] = $studentsession;
            $studentsession->setStudent($this);
        }

        return $this;
    }

    public function removeStudentsession(Session $studentsession): self
    {
        if ($this->studentsessions->removeElement($studentsession)) {
            // set the owning side to null (unless already changed)
            if ($studentsession->getStudent() === $this) {
                $studentsession->setStudent(null);
            }
        }

        return $this;
    }

    public function getTeacherdata(): ?TeacherUserData
    {
        return $this->teacherdata;
    }

    public function setTeacherdata(?TeacherUserData $teacherdata): self
    {
        $this->teacherdata = $teacherdata;

        return $this;
    }

    public function getStudentdata(): ?StudentUserData
    {
        return $this->studentdata;
    }

    public function setStudentdata(?StudentUserData $studentdata): self
    {
        $this->studentdata = $studentdata;

        return $this;
    }

    public function getUsertype(): ?string
    {
        return $this->usertype;
    }

    public function setUsertype(string $usertype): self
    {
        $this->usertype = $usertype;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|SchoolTime[]
     */
    public function getSchoolTimes(): Collection
    {
        return $this->schoolTimes;
    }

    public function addSchoolTime(SchoolTime $schoolTime): self
    {
        if (!$this->schoolTimes->contains($schoolTime)) {
            $this->schoolTimes[] = $schoolTime;
            $schoolTime->setTeacher($this);
        }

        return $this;
    }

    public function removeSchoolTime(SchoolTime $schoolTime): self
    {
        if ($this->schoolTimes->removeElement($schoolTime)) {
            // set the owning side to null (unless already changed)
            if ($schoolTime->getTeacher() === $this) {
                $schoolTime->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setUser($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getUser() === $this) {
                $payment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserSettings[]
     */
    public function getUserSettings(): Collection
    {
        return $this->userSettings;
    }

    public function addUserSetting(UserSettings $userSetting): self
    {
        if (!$this->userSettings->contains($userSetting)) {
            $this->userSettings[] = $userSetting;
            $userSetting->setUser($this);
        }

        return $this;
    }

    public function removeUserSetting(UserSettings $userSetting): self
    {
        if ($this->userSettings->removeElement($userSetting)) {
            // set the owning side to null (unless already changed)
            if ($userSetting->getUser() === $this) {
                $userSetting->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Communication[]
     */
    public function getCommunications(): Collection
    {
        return $this->communications;
    }

    public function addCommunication(Communication $communication): self
    {
        if (!$this->communications->contains($communication)) {
            $this->communications[] = $communication;
            $communication->setAdmin($this);
        }

        return $this;
    }

    public function removeCommunication(Communication $communication): self
    {
        if ($this->communications->removeElement($communication)) {
            // set the owning side to null (unless already changed)
            if ($communication->getAdmin() === $this) {
                $communication->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DeductionPayment[]
     */
    public function getDeductionPayments(): Collection
    {
        return $this->deductionPayments;
    }

    public function addDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if (!$this->deductionPayments->contains($deductionPayment)) {
            $this->deductionPayments[] = $deductionPayment;
            $deductionPayment->setTeacher($this);
        }

        return $this;
    }

    public function removeDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if ($this->deductionPayments->removeElement($deductionPayment)) {
            // set the owning side to null (unless already changed)
            if ($deductionPayment->getTeacher() === $this) {
                $deductionPayment->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notes[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setUser($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getUser() === $this) {
                $note->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assignment[]
     */
    public function getAssignments(): Collection
    {
        return $this->assignments;
    }

    public function addAssignment(Assignment $assignment): self
    {
        if (!$this->assignments->contains($assignment)) {
            $this->assignments[] = $assignment;
            $assignment->setStudent($this);
        }

        return $this;
    }

    public function removeAssignment(Assignment $assignment): self
    {
        if ($this->assignments->removeElement($assignment)) {
            // set the owning side to null (unless already changed)
            if ($assignment->getStudent() === $this) {
                $assignment->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PracticeSessions[]
     */
    public function getPracticeSessions(): Collection
    {
        return $this->practiceSessions;
    }

    public function addPracticeSession(PracticeSessions $practiceSession): self
    {
        if (!$this->practiceSessions->contains($practiceSession)) {
            $this->practiceSessions[] = $practiceSession;
            $practiceSession->setStudent($this);
        }

        return $this;
    }

    public function removePracticeSession(PracticeSessions $practiceSession): self
    {
        if ($this->practiceSessions->removeElement($practiceSession)) {
            // set the owning side to null (unless already changed)
            if ($practiceSession->getStudent() === $this) {
                $practiceSession->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assignment[]
     */
    public function getTeacherAssignments(): Collection
    {
        return $this->teacher_assignments;
    }

    public function addTeacherAssignment(Assignment $teacherAssignment): self
    {
        if (!$this->teacher_assignments->contains($teacherAssignment)) {
            $this->teacher_assignments[] = $teacherAssignment;
            $teacherAssignment->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherAssignment(Assignment $teacherAssignment): self
    {
        if ($this->teacher_assignments->removeElement($teacherAssignment)) {
            // set the owning side to null (unless already changed)
            if ($teacherAssignment->getTeacher() === $this) {
                $teacherAssignment->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LessonPlan[]
     */
    public function getLessonPlans(): Collection
    {
        return $this->lessonPlans;
    }

    public function addLessonPlan(LessonPlan $lessonPlan): self
    {
        if (!$this->lessonPlans->contains($lessonPlan)) {
            $this->lessonPlans[] = $lessonPlan;
            $lessonPlan->setStudent($this);
        }

        return $this;
    }

    public function removeLessonPlan(LessonPlan $lessonPlan): self
    {
        if ($this->lessonPlans->removeElement($lessonPlan)) {
            // set the owning side to null (unless already changed)
            if ($lessonPlan->getStudent() === $this) {
                $lessonPlan->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Blog[]
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): self
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs[] = $blog;
            $blog->setUser($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): self
    {
        if ($this->blogs->removeElement($blog)) {
            // set the owning side to null (unless already changed)
            if ($blog->getUser() === $this) {
                $blog->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommentReply[]
     */
    public function getCommentReplies(): Collection
    {
        return $this->commentReplies;
    }

    public function addCommentReply(CommentReply $commentReply): self
    {
        if (!$this->commentReplies->contains($commentReply)) {
            $this->commentReplies[] = $commentReply;
            $commentReply->setUser($this);
        }

        return $this;
    }

    public function removeCommentReply(CommentReply $commentReply): self
    {
        if ($this->commentReplies->removeElement($commentReply)) {
            // set the owning side to null (unless already changed)
            if ($commentReply->getUser() === $this) {
                $commentReply->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->addTeacher($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            $course->removeTeacher($this);
        }

        return $this;
    }

    /**
     * @return Collection|PhotoUpload[]
     */
    public function getPhotoUploads(): Collection
    {
        return $this->photoUploads;
    }

    public function addPhotoUpload(PhotoUpload $photoUpload): self
    {
        if (!$this->photoUploads->contains($photoUpload)) {
            $this->photoUploads[] = $photoUpload;
            $photoUpload->setUser($this);
        }

        return $this;
    }

    public function removePhotoUpload(PhotoUpload $photoUpload): self
    {
        if ($this->photoUploads->removeElement($photoUpload)) {
            // set the owning side to null (unless already changed)
            if ($photoUpload->getUser() === $this) {
                $photoUpload->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ShortUrl[]
     */
    public function getShortUrls(): Collection
    {
        return $this->shortUrls;
    }

    public function addShortUrl(ShortUrl $shortUrl): self
    {
        if (!$this->shortUrls->contains($shortUrl)) {
            $this->shortUrls[] = $shortUrl;
            $shortUrl->setUser($this);
        }

        return $this;
    }

    public function removeShortUrl(ShortUrl $shortUrl): self
    {
        if ($this->shortUrls->removeElement($shortUrl)) {
            // set the owning side to null (unless already changed)
            if ($shortUrl->getUser() === $this) {
                $shortUrl->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UigGroup[]
     */
    public function getUigGroups(): Collection
    {
        return $this->uigGroups;
    }

    public function addUigGroup(UigGroup $uigGroup): self
    {
        if (!$this->uigGroups->contains($uigGroup)) {
            $this->uigGroups[] = $uigGroup;
            $uigGroup->setUser($this);
        }

        return $this;
    }

    public function removeUigGroup(UigGroup $uigGroup): self
    {
        if ($this->uigGroups->removeElement($uigGroup)) {
            // set the owning side to null (unless already changed)
            if ($uigGroup->getUser() === $this) {
                $uigGroup->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reporting[]
     */
    public function getReportings(): Collection
    {
        return $this->reportings;
    }

    public function addReporting(Reporting $reporting): self
    {
        if (!$this->reportings->contains($reporting)) {
            $this->reportings[] = $reporting;
            $reporting->setUser($this);
        }

        return $this;
    }

    public function removeReporting(Reporting $reporting): self
    {
        if ($this->reportings->removeElement($reporting)) {
            // set the owning side to null (unless already changed)
            if ($reporting->getUser() === $this) {
                $reporting->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reporting[]
     */
    public function getTeacherReports(): Collection
    {
        return $this->teacher_reports;
    }

    public function addTeacherReport(Reporting $teacherReport): self
    {
        if (!$this->teacher_reports->contains($teacherReport)) {
            $this->teacher_reports[] = $teacherReport;
            $teacherReport->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherReport(Reporting $teacherReport): self
    {
        if ($this->teacher_reports->removeElement($teacherReport)) {
            // set the owning side to null (unless already changed)
            if ($teacherReport->getTeacher() === $this) {
                $teacherReport->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SimpleReport>
     */
    public function getSimpleReports(): Collection
    {
        return $this->simpleReports;
    }

    public function addSimpleReport(SimpleReport $simpleReport): static
    {
        if (!$this->simpleReports->contains($simpleReport)) {
            $this->simpleReports->add($simpleReport);
            $simpleReport->setStudent($this);
        }

        return $this;
    }

    public function removeSimpleReport(SimpleReport $simpleReport): static
    {
        if ($this->simpleReports->removeElement($simpleReport)) {
            // set the owning side to null (unless already changed)
            if ($simpleReport->getStudent() === $this) {
                $simpleReport->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SimpleReport>
     */
    public function getTrReports(): Collection
    {
        return $this->trReports;
    }

    public function addTrReport(SimpleReport $trReport): static
    {
        if (!$this->trReports->contains($trReport)) {
            $this->trReports->add($trReport);
            $trReport->setTeacher($this);
        }

        return $this;
    }

    public function removeTrReport(SimpleReport $trReport): static
    {
        if ($this->trReports->removeElement($trReport)) {
            // set the owning side to null (unless already changed)
            if ($trReport->getTeacher() === $this) {
                $trReport->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TeacherInvoice>
     */
    public function getTeacherInvoices(): Collection
    {
        return $this->teacherInvoices;
    }

    public function addTeacherInvoice(TeacherInvoice $teacherInvoice): static
    {
        if (!$this->teacherInvoices->contains($teacherInvoice)) {
            $this->teacherInvoices->add($teacherInvoice);
            $teacherInvoice->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherInvoice(TeacherInvoice $teacherInvoice): static
    {
        if ($this->teacherInvoices->removeElement($teacherInvoice)) {
            // set the owning side to null (unless already changed)
            if ($teacherInvoice->getTeacher() === $this) {
                $teacherInvoice->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserInstrumentGrade>
     */
    public function getUserInstrumentGrades(): Collection
    {
        return $this->userInstrumentGrades;
    }

    public function addUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): static
    {
        if (!$this->userInstrumentGrades->contains($userInstrumentGrade)) {
            $this->userInstrumentGrades->add($userInstrumentGrade);
            $userInstrumentGrade->setTeacher($this);
        }

        return $this;
    }

    public function removeUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): static
    {
        if ($this->userInstrumentGrades->removeElement($userInstrumentGrade)) {
            // set the owning side to null (unless already changed)
            if ($userInstrumentGrade->getTeacher() === $this) {
                $userInstrumentGrade->setTeacher(null);
            }
        }

        return $this;
    }

}
