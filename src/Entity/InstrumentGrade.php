<?php

namespace App\Entity;

use App\Repository\InstrumentGradeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InstrumentGradeRepository::class)]
class InstrumentGrade implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: UserInstrumentGrade::class, mappedBy: 'grade')]
    private Collection $userInstrumentGrades;

    public function __construct()
    {
        $this->instruments = new ArrayCollection();
        $this->userInstrumentGrades = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserInstrumentGrades(): Collection
    {
        return $this->userInstrumentGrades;
    }

    public function addUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if (!$this->userInstrumentGrades->contains($userInstrumentGrade)) {
            $this->userInstrumentGrades[] = $userInstrumentGrade;
            $userInstrumentGrade->setGrade($this);
        }

        return $this;
    }

    public function removeUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if ($this->userInstrumentGrades->removeElement($userInstrumentGrade)) {
            // set the owning side to null (unless already changed)
            if ($userInstrumentGrade->getGrade() === $this) {
                $userInstrumentGrade->setGrade(null);
            }
        }

        return $this;
    }

}
