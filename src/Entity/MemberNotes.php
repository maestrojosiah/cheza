<?php

namespace App\Entity;

use App\Repository\MemberNotesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MemberNotesRepository::class)]
class MemberNotes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: GroupMember::class, inversedBy: 'memberNotes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\GroupMember $member = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $addedOn = null;

    #[ORM\Column(type: 'text')]
    private ?string $message = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMember(): ?GroupMember
    {
        return $this->member;
    }

    public function setMember(?GroupMember $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getAddedOn(): ?\DateTimeInterface
    {
        return $this->addedOn;
    }

    public function setAddedOn(\DateTimeInterface $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
