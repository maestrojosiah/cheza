<?php

namespace App\Entity;

use App\Repository\ReportingRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReportingRepository::class)]
class Reporting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'reportings')]
    private ?\App\Entity\User $user = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $term = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $instrument = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $age = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $performed = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $learning_goals = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $progress_assessment = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $strengths = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $weaknesses = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $recommendations = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $practice_habits = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $learning_level = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $sex = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $addedOn = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'teacher_reports')]
    private ?\App\Entity\User $teacher = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $sent = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $summary = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTerm(): ?string
    {
        return $this->term;
    }

    public function setTerm(?string $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(?string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getPerformed(): ?string
    {
        return $this->performed;
    }

    public function setPerformed(?string $performed): self
    {
        $this->performed = $performed;

        return $this;
    }

    public function getLearningGoals(): ?string
    {
        return $this->learning_goals;
    }

    public function setLearningGoals(?string $learning_goals): self
    {
        $this->learning_goals = $learning_goals;

        return $this;
    }

    public function getProgressAssessment(): ?string
    {
        return $this->progress_assessment;
    }

    public function setProgressAssessment(?string $progress_assessment): self
    {
        $this->progress_assessment = $progress_assessment;

        return $this;
    }

    public function getStrengths(): ?string
    {
        return $this->strengths;
    }

    public function setStrengths(?string $strengths): self
    {
        $this->strengths = $strengths;

        return $this;
    }

    public function getWeaknesses(): ?string
    {
        return $this->weaknesses;
    }

    public function setWeaknesses(?string $weaknesses): self
    {
        $this->weaknesses = $weaknesses;

        return $this;
    }

    public function getRecommendations(): ?string
    {
        return $this->recommendations;
    }

    public function setRecommendations(?string $recommendations): self
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    public function getPracticeHabits(): ?string
    {
        return $this->practice_habits;
    }

    public function setPracticeHabits(?string $practice_habits): self
    {
        $this->practice_habits = $practice_habits;

        return $this;
    }

    public function getLearningLevel(): ?string
    {
        return $this->learning_level;
    }

    public function setLearningLevel(?string $learning_level): self
    {
        $this->learning_level = $learning_level;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getAddedOn(): ?\DateTimeImmutable
    {
        return $this->addedOn;
    }

    public function setAddedOn(?\DateTimeImmutable $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getSent(): ?bool
    {
        return $this->sent;
    }

    public function setSent(?bool $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

}
