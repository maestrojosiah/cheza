<?php

namespace App\Entity;

use App\Repository\LessonPlanRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LessonPlanRepository::class)]
class LessonPlan
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $addedOn = null;

    #[ORM\OneToOne(targetEntity: Session::class, inversedBy: 'lessonPlan', cascade: ['persist', 'remove'])]
    private ?\App\Entity\Session $session = null;

    #[ORM\Column(type: 'text')]
    private ?string $work = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'lessonPlans')]
    private ?\App\Entity\User $student = null;

    #[ORM\Column(type: 'text')]
    private ?string $activities = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $assessment = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $nextlesson = null;

    // public function __toString()
    // {
    //     return $this->name;
    // }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddedOn(): ?\DateTimeInterface
    {
        return $this->addedOn;
    }

    public function setAddedOn(\DateTimeInterface $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getWork(): ?string
    {
        return $this->work;
    }

    public function setWork(string $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getActivities(): ?string
    {
        return $this->activities;
    }

    public function setActivities(string $activities): self
    {
        $this->activities = $activities;

        return $this;
    }

    public function getAssessment(): ?string
    {
        return $this->assessment;
    }

    public function setAssessment(string $assessment): self
    {
        $this->assessment = $assessment;

        return $this;
    }

    public function getNextlesson(): ?string
    {
        return $this->nextlesson;
    }

    public function setNextlesson(?string $nextlesson): self
    {
        $this->nextlesson = $nextlesson;

        return $this;
    }
}
