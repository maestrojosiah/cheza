<?php

namespace App\Entity;

use App\Repository\ExerciseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExerciseRepository::class)]
class Exercise implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $type = null;

    #[ORM\OneToMany(targetEntity: FlashCard::class, mappedBy: 'exercise', orphanRemoval: true)]
    private Collection $flashCards;

    #[ORM\ManyToOne(targetEntity: Instrument::class, inversedBy: 'exercises')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Instrument $instrument = null;

    public function __toString(): string
    {
        return (string) $this->type;
    }

    public function __construct()
    {
        $this->flashCards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|FlashCard[]
     */
    public function getFlashCards(): Collection
    {
        return $this->flashCards;
    }

    public function addFlashCard(FlashCard $flashCard): self
    {
        if (!$this->flashCards->contains($flashCard)) {
            $this->flashCards[] = $flashCard;
            $flashCard->setExercise($this);
        }

        return $this;
    }

    public function removeFlashCard(FlashCard $flashCard): self
    {
        if ($this->flashCards->removeElement($flashCard)) {
            // set the owning side to null (unless already changed)
            if ($flashCard->getExercise() === $this) {
                $flashCard->setExercise(null);
            }
        }

        return $this;
    }

    public function getInstrument(): ?Instrument
    {
        return $this->instrument;
    }

    public function setInstrument(?Instrument $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }
}
