<?php

namespace App\Entity;

use App\Repository\MyKeyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MyKeyRepository::class)]
class MyKey implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: Mode::class, inversedBy: 'myKeys')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Mode $mode = null;

    #[ORM\OneToMany(targetEntity: FlashCard::class, mappedBy: 'mykey', orphanRemoval: true)]
    private Collection $flashCards;

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function __construct()
    {
        $this->flashCards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMode(): ?Mode
    {
        return $this->mode;
    }

    public function setMode(?Mode $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * @return Collection|FlashCard[]
     */
    public function getFlashCards(): Collection
    {
        return $this->flashCards;
    }

    public function addFlashCard(FlashCard $flashCard): self
    {
        if (!$this->flashCards->contains($flashCard)) {
            $this->flashCards[] = $flashCard;
            $flashCard->setMykey($this);
        }

        return $this;
    }

    public function removeFlashCard(FlashCard $flashCard): self
    {
        if ($this->flashCards->removeElement($flashCard)) {
            // set the owning side to null (unless already changed)
            if ($flashCard->getMykey() === $this) {
                $flashCard->setMykey(null);
            }
        }

        return $this;
    }
}
