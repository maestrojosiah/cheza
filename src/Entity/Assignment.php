<?php

namespace App\Entity;

use App\Repository\AssignmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AssignmentRepository::class)]
class Assignment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'assignments')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\User $student = null;

    #[ORM\Column(type: 'text')]
    private ?string $assignment = null;

    #[ORM\ManyToOne(targetEntity: Attachment::class, inversedBy: 'assignments')]
    private ?\App\Entity\Attachment $attachment = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $assessment = null;

    #[ORM\OneToMany(targetEntity: PracticeSessions::class, mappedBy: 'assignment', orphanRemoval: true)]
    private Collection $practiceSessions;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $addedOn = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'teacher_assignments')]
    private ?\App\Entity\User $teacher = null;

    #[ORM\Column(type: 'time')]
    private ?\DateTimeInterface $reminder = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $knob = null;

    public function __construct()
    {
        $this->practiceSessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getAssignment(): ?string
    {
        return $this->assignment;
    }

    public function setAssignment(string $assignment): self
    {
        $this->assignment = $assignment;

        return $this;
    }

    public function getAttachment(): ?Attachment
    {
        return $this->attachment;
    }

    public function setAttachment(?Attachment $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }

    public function getAssessment(): ?string
    {
        return $this->assessment;
    }

    public function setAssessment(?string $assessment): self
    {
        $this->assessment = $assessment;

        return $this;
    }

    /**
     * @return Collection|PracticeSessions[]
     */
    public function getPracticeSessions(): Collection
    {
        return $this->practiceSessions;
    }

    public function addPracticeSession(PracticeSessions $practiceSession): self
    {
        if (!$this->practiceSessions->contains($practiceSession)) {
            $this->practiceSessions[] = $practiceSession;
            $practiceSession->setAssignment($this);
        }

        return $this;
    }

    public function removePracticeSession(PracticeSessions $practiceSession): self
    {
        if ($this->practiceSessions->removeElement($practiceSession)) {
            // set the owning side to null (unless already changed)
            if ($practiceSession->getAssignment() === $this) {
                $practiceSession->setAssignment(null);
            }
        }

        return $this;
    }

    public function getAddedOn(): ?\DateTimeInterface
    {
        return $this->addedOn;
    }

    public function setAddedOn(?\DateTimeInterface $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getReminder(): ?\DateTimeInterface
    {
        return $this->reminder;
    }

    public function setReminder(\DateTimeInterface $reminder): self
    {
        $this->reminder = $reminder;

        return $this;
    }

    public function getKnob(): ?bool
    {
        return $this->knob;
    }

    public function setKnob(?bool $knob): self
    {
        $this->knob = $knob;

        return $this;
    }

}
