<?php

namespace App\Entity;

use App\Repository\UserInstrumentGradeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserInstrumentGradeRepository::class)]
class UserInstrumentGrade implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: StudentUserData::class, inversedBy: 'userInstruments')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?\App\Entity\StudentUserData $studentUserData = null;

    #[ORM\ManyToOne(targetEntity: TeacherUserData::class, inversedBy: 'userinstruments')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?\App\Entity\TeacherUserData $teacherUserData = null;

    #[ORM\ManyToOne(targetEntity: InstrumentGrade::class, inversedBy: 'userInstrumentGrades')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?\App\Entity\InstrumentGrade $grade = null;

    #[ORM\ManyToOne(targetEntity: Instrument::class, inversedBy: 'userInstrumentGrades')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?\App\Entity\Instrument $instrument = null;

    #[ORM\OneToMany(targetEntity: Session::class, mappedBy: 'userInstrumentGrade')]
    private Collection $sessions;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $payment = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $active = null;

    #[ORM\OneToMany(targetEntity: Payment::class, mappedBy: 'uig')]
    private Collection $payments;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $siblingname = null;

    #[ORM\OneToMany(targetEntity: Report::class, mappedBy: 'uig')]
    private Collection $reports;

    #[ORM\OneToMany(targetEntity: Notes::class, mappedBy: 'uig', orphanRemoval: true)]
    private Collection $notes;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $percentage = null;

    #[ORM\ManyToOne(inversedBy: 'userInstrumentGrades')]
    private ?User $teacher = null;

    #[ORM\ManyToOne(inversedBy: 'userInstrumentGrades')]
    private ?Term $term = null;


    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    public function __toString(): string
    {
        if($this->studentUserData !== null && $this->getStudentUserData()->getStudent() !== null && ($this->siblingname == null || $this->siblingname == "")) {
            return $this->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $this->getInstrument()->getName();//' . ' ( ' . $this->getGrade()->getName() . ' ) ';
        } elseif($this->siblingname != null && $this->siblingname != "") {
            return $this->siblingname . ' - ' .  $this->getInstrument()->getName();// . ' ( ' . $this->getGrade()->getName() . ' ) ';
        } else {
            return (string) $this->getInstrument()->getName();
        }

    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudentUserData(): ?StudentUserData
    {
        return $this->studentUserData;
    }

    public function setStudentUserData(?StudentUserData $studentUserData): self
    {
        $this->studentUserData = $studentUserData;

        return $this;
    }

    public function getTeacherUserData(): ?TeacherUserData
    {
        return $this->teacherUserData;
    }

    public function setTeacherUserData(?TeacherUserData $teacherUserData): self
    {
        $this->teacherUserData = $teacherUserData;

        return $this;
    }

    public function getGrade(): ?InstrumentGrade
    {
        return $this->grade;
    }

    public function setGrade(?InstrumentGrade $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getInstrument(): ?Instrument
    {
        return $this->instrument;
    }

    public function setInstrument(?Instrument $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setUserInstrumentGrade($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getUserInstrumentGrade() === $this) {
                $session->setUserInstrumentGrade(null);
            }
        }

        return $this;
    }

    public function getPayment(): ?string
    {
        return $this->payment;
    }

    public function setPayment(?string $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setUig($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getUig() === $this) {
                $payment->setUig(null);
            }
        }

        return $this;
    }

    public function getSiblingname(): ?string
    {
        return $this->siblingname;
    }

    public function setSiblingname(?string $siblingname): self
    {
        $this->siblingname = $siblingname;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setUig($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getUig() === $this) {
                $report->setUig(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notes[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setUig($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getUig() === $this) {
                $note->setUig(null);
            }
        }

        return $this;
    }

    public function getPercentage(): ?string
    {
        return $this->percentage;
    }

    public function setPercentage(?string $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): static
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getTerm(): ?Term
    {
        return $this->term;
    }

    public function setTerm(?Term $term): static
    {
        $this->term = $term;

        return $this;
    }

}
