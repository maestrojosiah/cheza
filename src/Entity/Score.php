<?php

namespace App\Entity;

use App\Repository\ScoreRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ScoreRepository::class)]
class Score
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Quiz::class, inversedBy: 'scores')]
    private ?\App\Entity\Quiz $quiz = null;

    // #[ORM\ManyToOne(targetEntity: Choice::class, inversedBy: 'scores')]
    // private ?\App\Entity\Choice $choice = null;

    #[ORM\ManyToOne(targetEntity: Test::class, inversedBy: 'scores')]
    private ?\App\Entity\Test $test = null;

    #[ORM\ManyToOne(inversedBy: 'scores')]
    private ?Choice $choice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    // public function getChoice(): ?Choice
    // {
    //     return $this->choice;
    // }

    // public function setChoice(?Choice $choice): self
    // {
    //     $this->choice = $choice;

    //     return $this;
    // }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getChoice(): ?Choice
    {
        return $this->choice;
    }

    public function setChoice(?Choice $choice): static
    {
        $this->choice = $choice;

        return $this;
    }
}
