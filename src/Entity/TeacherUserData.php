<?php

namespace App\Entity;

use App\Repository\TeacherUserDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeacherUserDataRepository::class)]
class TeacherUserData implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $resumelnk = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $phone = null;

    #[ORM\OneToOne(targetEntity: User::class, mappedBy: 'teacherdata', cascade: ['persist', 'remove'])]
    private ?\App\Entity\User $teacher = null;

    #[ORM\OneToMany(targetEntity: UserInstrumentGrade::class, mappedBy: 'teacherUserData')]
    private Collection $userinstruments;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $lessonmode = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $branch = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $sex = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $nextofkinname = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $nextofkinphonenum = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $nextofkinemail = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $nextofkinrship = null;

    #[ORM\OneToMany(targetEntity: Deductions::class, mappedBy: 'teacher')]
    private Collection $deductions;

    #[ORM\OneToMany(targetEntity: Report::class, mappedBy: 'teacher')]
    private Collection $reports;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $about = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $conditions = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $percentage = null;

    public function __construct()
    {
        $this->userinstruments = new ArrayCollection();
        $this->deductions = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    public function __toString(): string
    {
        if(null !== $this->teacher) {
            return (string) $this->teacher->getFullname();
        } else {
            return (string)$this->id;
        }

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResumelnk(): ?string
    {
        return $this->resumelnk;
    }

    public function setResumelnk(string $resumelnk): self
    {
        $this->resumelnk = $resumelnk;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->teacher;
    }

    public function setUser(?User $teacher): self
    {
        // unset the owning side of the relation if necessary
        if ($teacher === null && $this->teacher !== null) {
            $this->teacher->setTeacherdata(null);
        }

        // set the owning side of the relation if necessary
        if ($teacher !== null && $teacher->getTeacherdata() !== $this) {
            $teacher->setTeacherdata($this);
        }

        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserinstruments(): Collection
    {
        return $this->userinstruments;
    }

    public function addUserinstruments(UserInstrumentGrade $userinstruments): self
    {
        if (!$this->userinstruments->contains($userinstruments)) {
            $this->userinstruments[] = $userinstruments;
            $userinstruments->setTeacherUserData($this);
        }

        return $this;
    }

    public function removeUserinstruments(UserInstrumentGrade $userinstruments): self
    {
        if ($this->userinstruments->removeElement($userinstruments)) {
            // set the owning side to null (unless already changed)
            if ($userinstruments->getTeacherUserData() === $this) {
                $userinstruments->setTeacherUserData(null);
            }
        }

        return $this;
    }

    public function getLessonmode(): ?string
    {
        return $this->lessonmode;
    }

    public function setLessonmode(?string $lessonmode): self
    {
        $this->lessonmode = $lessonmode;

        return $this;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(?string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getNextofkinname(): ?string
    {
        return $this->nextofkinname;
    }

    public function setNextofkinname(?string $nextofkinname): self
    {
        $this->nextofkinname = $nextofkinname;

        return $this;
    }

    public function getNextofkinphonenum(): ?string
    {
        return $this->nextofkinphonenum;
    }

    public function setNextofkinphonenum(?string $nextofkinphonenum): self
    {
        $this->nextofkinphonenum = $nextofkinphonenum;

        return $this;
    }

    public function getNextofkinemail(): ?string
    {
        return $this->nextofkinemail;
    }

    public function setNextofkinemail(?string $nextofkinemail): self
    {
        $this->nextofkinemail = $nextofkinemail;

        return $this;
    }

    public function getNextofkinrship(): ?string
    {
        return $this->nextofkinrship;
    }

    public function setNextofkinrship(?string $nextofkinrship): self
    {
        $this->nextofkinrship = $nextofkinrship;

        return $this;
    }

    /**
     * @return Collection|Deductions[]
     */
    public function getDeductions(): Collection
    {
        return $this->deductions;
    }

    public function addDeduction(Deductions $deduction): self
    {
        if (!$this->deductions->contains($deduction)) {
            $this->deductions[] = $deduction;
            $deduction->setTeacher($this);
        }

        return $this;
    }

    public function removeDeduction(Deductions $deduction): self
    {
        if ($this->deductions->removeElement($deduction)) {
            // set the owning side to null (unless already changed)
            if ($deduction->getTeacher() === $this) {
                $deduction->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setTeacher($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getTeacher() === $this) {
                $report->setTeacher(null);
            }
        }

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getConditions(): ?string
    {
        return $this->conditions;
    }

    public function setConditions(?string $conditions): self
    {
        $this->conditions = $conditions;

        return $this;
    }

    public function getPercentage(): ?string
    {
        return $this->percentage;
    }

    public function setPercentage(?string $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

}
