<?php

namespace App\Entity;

use App\Repository\TeacherInvoiceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeacherInvoiceRepository::class)]
class TeacherInvoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'teacherInvoices')]
    private ?User $teacher = null;

    #[ORM\Column]
    private ?int $amount = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $dateSubmitted = null;

    #[ORM\Column(length: 255)]
    private ?string $url = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $uigs = null;

    #[ORM\Column]
    private ?int $deduction = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): static
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDateSubmitted(): ?\DateTimeImmutable
    {
        return $this->dateSubmitted;
    }

    public function setDateSubmitted(\DateTimeImmutable $dateSubmitted): static
    {
        $this->dateSubmitted = $dateSubmitted;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getUigs(): ?string
    {
        return $this->uigs;
    }

    public function setUigs(string $uigs): static
    {
        $this->uigs = $uigs;

        return $this;
    }

    public function getDeduction(): ?int
    {
        return $this->deduction;
    }

    public function setDeduction(int $deduction): static
    {
        $this->deduction = $deduction;

        return $this;
    }
}
