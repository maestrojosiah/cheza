<?php

namespace App\Entity;

use App\Repository\CommunicationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommunicationRepository::class)]
class Communication
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $sentAt = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $sentTo = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $subject = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $sendToContact = null;

    #[ORM\Column(type: 'text')]
    private ?string $message = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $sentStatus = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $deliveredStatus = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $openedStatus = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $deliveredAt = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $type = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'communications')]
    private ?\App\Entity\User $admin = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $messageId = null;

    #[ORM\ManyToOne(targetEntity: Attachment::class, inversedBy: 'communications')]
    private ?\App\Entity\Attachment $attachment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getSentTo(): ?string
    {
        return $this->sentTo;
    }

    public function setSentTo(string $sentTo): self
    {
        $this->sentTo = $sentTo;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getSendToContact(): ?string
    {
        return $this->sendToContact;
    }

    public function setSendToContact(string $sendToContact): self
    {
        $this->sendToContact = $sendToContact;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getSentStatus(): ?string
    {
        return $this->sentStatus;
    }

    public function setSentStatus(string $sentStatus): self
    {
        $this->sentStatus = $sentStatus;

        return $this;
    }

    public function getDeliveredStatus(): ?string
    {
        return $this->deliveredStatus;
    }

    public function setDeliveredStatus(string $deliveredStatus): self
    {
        $this->deliveredStatus = $deliveredStatus;

        return $this;
    }

    public function getOpenedStatus(): ?string
    {
        return $this->openedStatus;
    }

    public function setOpenedStatus(string $openedStatus): self
    {
        $this->openedStatus = $openedStatus;

        return $this;
    }

    public function getDeliveredAt(): ?\DateTimeInterface
    {
        return $this->deliveredAt;
    }

    public function setDeliveredAt(\DateTimeInterface $deliveredAt): self
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getMessageId(): ?string
    {
        return $this->messageId;
    }

    public function setMessageId(?string $messageId): self
    {
        $this->messageId = $messageId;

        return $this;
    }

    public function getAttachment(): ?Attachment
    {
        return $this->attachment;
    }

    public function setAttachment(?Attachment $attachment): self
    {
        $this->attachment = $attachment;

        return $this;
    }
}
