<?php

namespace App\Entity;

use App\Repository\FlashCardRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FlashCardRepository::class)]
class FlashCard
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Exercise::class, inversedBy: 'flashCards')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Exercise $exercise = null;

    #[ORM\ManyToOne(targetEntity: MyKey::class, inversedBy: 'flashCards')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\MyKey $mykey = null;

    #[ORM\ManyToOne(targetEntity: Mode::class, inversedBy: 'flashCards')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Mode $mode = null;

    #[ORM\ManyToOne(targetEntity: Level::class, inversedBy: 'flashCards')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Level $level = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $image = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExercise(): ?Exercise
    {
        return $this->exercise;
    }

    public function setExercise(?Exercise $exercise): self
    {
        $this->exercise = $exercise;

        return $this;
    }

    public function getMykey(): ?MyKey
    {
        return $this->mykey;
    }

    public function setMykey(?MyKey $mykey): self
    {
        $this->mykey = $mykey;

        return $this;
    }

    public function getMode(): ?Mode
    {
        return $this->mode;
    }

    public function setMode(?Mode $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
