<?php

namespace App\Entity;

use App\Repository\NavigationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NavigationRepository::class)]
class Navigation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $link = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $hasChildren = null;

    #[ORM\OneToMany(targetEntity: NavChild::class, mappedBy: 'navigation', orphanRemoval: true)]
    private Collection $navChildren;

    #[ORM\Column(type: 'integer')]
    private ?int $position = null;

    public function __construct()
    {
        $this->navChildren = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getHasChildren(): ?bool
    {
        return $this->hasChildren;
    }

    public function setHasChildren(bool $hasChildren): self
    {
        $this->hasChildren = $hasChildren;

        return $this;
    }

    /**
     * @return Collection|NavChild[]
     */
    public function getNavChildren(): Collection
    {
        return $this->navChildren;
    }

    public function addNavChild(NavChild $navChild): self
    {
        if (!$this->navChildren->contains($navChild)) {
            $this->navChildren[] = $navChild;
            $navChild->setNavigation($this);
        }

        return $this;
    }

    public function removeNavChild(NavChild $navChild): self
    {
        if ($this->navChildren->removeElement($navChild)) {
            // set the owning side to null (unless already changed)
            if ($navChild->getNavigation() === $this) {
                $navChild->setNavigation(null);
            }
        }

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
