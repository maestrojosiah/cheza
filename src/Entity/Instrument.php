<?php

namespace App\Entity;

use App\Repository\InstrumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InstrumentRepository::class)]
class Instrument implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $availability = null;

    #[ORM\Column(type: 'text', length: 255)]
    private ?string $category = null;

    #[ORM\Column(type: 'text')]
    private ?string $description = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'Please, upload the product image as a png or jpg image.')]
    #[Assert\File(mimeTypes: ['image/png', 'image/jpg', 'image/jpeg'])]
    private ?string $image = null;

    #[ORM\OneToMany(targetEntity: UserInstrumentGrade::class, mappedBy: 'instrument')]
    private Collection $userInstrumentGrades;

    #[ORM\OneToMany(targetEntity: Exercise::class, mappedBy: 'instrument', orphanRemoval: true)]
    private Collection $exercises;

    #[ORM\OneToMany(targetEntity: Course::class, mappedBy: 'instrument', orphanRemoval: true)]
    private Collection $courses;

    public function __construct()
    {
        $this->instrumentGrades = new ArrayCollection();
        $this->userInstrumentGrades = new ArrayCollection();
        $this->exercises = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAvailability(): ?string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserInstrumentGrades(): Collection
    {
        return $this->userInstrumentGrades;
    }

    public function addUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if (!$this->userInstrumentGrades->contains($userInstrumentGrade)) {
            $this->userInstrumentGrades[] = $userInstrumentGrade;
            $userInstrumentGrade->setInstrument($this);
        }

        return $this;
    }

    public function removeUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if ($this->userInstrumentGrades->removeElement($userInstrumentGrade)) {
            // set the owning side to null (unless already changed)
            if ($userInstrumentGrade->getInstrument() === $this) {
                $userInstrumentGrade->setInstrument(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exercise[]
     */
    public function getExercises(): Collection
    {
        return $this->exercises;
    }

    public function addExercise(Exercise $exercise): self
    {
        if (!$this->exercises->contains($exercise)) {
            $this->exercises[] = $exercise;
            $exercise->setInstrument($this);
        }

        return $this;
    }

    public function removeExercise(Exercise $exercise): self
    {
        if ($this->exercises->removeElement($exercise)) {
            // set the owning side to null (unless already changed)
            if ($exercise->getInstrument() === $this) {
                $exercise->setInstrument(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setInstrument($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            // set the owning side to null (unless already changed)
            if ($course->getInstrument() === $this) {
                $course->setInstrument(null);
            }
        }

        return $this;
    }


}
