<?php

namespace App\Entity;

use App\Repository\PracticeSessionsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PracticeSessionsRepository::class)]
class PracticeSessions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'practiceSessions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\User $student = null;

    #[ORM\ManyToOne(targetEntity: Assignment::class, inversedBy: 'practiceSessions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Assignment $assignment = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $doneOn = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getAssignment(): ?Assignment
    {
        return $this->assignment;
    }

    public function setAssignment(?Assignment $assignment): self
    {
        $this->assignment = $assignment;

        return $this;
    }

    public function getDoneOn(): ?\DateTimeInterface
    {
        return $this->doneOn;
    }

    public function setDoneOn(\DateTimeInterface $doneOn): self
    {
        $this->doneOn = $doneOn;

        return $this;
    }
}
