<?php

namespace App\Entity;

use App\Repository\GroupMemberRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupMemberRepository::class)]
class GroupMember implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $fullname = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $gender = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $age = null;

    #[ORM\OneToMany(targetEntity: MemberNotes::class, mappedBy: 'member', orphanRemoval: true)]
    private Collection $memberNotes;

    #[ORM\ManyToOne(targetEntity: UigGroup::class, inversedBy: 'groupMembers')]
    private ?\App\Entity\UigGroup $uigGroup = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $school_id = null;

    public function __construct()
    {
        $this->memberNotes = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getFullname();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(string $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return Collection|MemberNotes[]
     */
    public function getMemberNotes(): Collection
    {
        return $this->memberNotes;
    }

    public function addMemberNote(MemberNotes $memberNote): self
    {
        if (!$this->memberNotes->contains($memberNote)) {
            $this->memberNotes[] = $memberNote;
            $memberNote->setMember($this);
        }

        return $this;
    }

    public function removeMemberNote(MemberNotes $memberNote): self
    {
        if ($this->memberNotes->removeElement($memberNote)) {
            // set the owning side to null (unless already changed)
            if ($memberNote->getMember() === $this) {
                $memberNote->setMember(null);
            }
        }

        return $this;
    }

    public function getUigGroup(): ?UigGroup
    {
        return $this->uigGroup;
    }

    public function setUigGroup(?UigGroup $uigGroup): self
    {
        $this->uigGroup = $uigGroup;

        return $this;
    }

    public function getSchoolId(): ?string
    {
        return $this->school_id;
    }

    public function setSchoolId(?string $school_id): static
    {
        $this->school_id = $school_id;

        return $this;
    }

}
