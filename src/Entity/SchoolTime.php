<?php

namespace App\Entity;

use App\Repository\SchoolTimeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SchoolTimeRepository::class)]
class SchoolTime implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'schoolTimes')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?\App\Entity\User $teacher = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $beginAt = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $endAt = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $day = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $jsdaycode = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $title = null;

    public function __toString(): string
    {
        return $this->teacher->getFullname() . '\'s ' . $this->getDay() . ' Schedule' ;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(?\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getJsdaycode(): ?int
    {
        return $this->jsdaycode;
    }

    public function setJsdaycode(?int $jsdaycode): self
    {
        $this->jsdaycode = $jsdaycode;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

}
