<?php

namespace App\Entity;

use App\Repository\TermRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TermRepository::class)]
class Term implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $termnumber = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $startingOn = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $endingOn = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $halftermstart = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $halftermend = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $holidaystart = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $holidayend = null;

    /**
     * @var Collection<int, UserInstrumentGrade>
     */
    #[ORM\OneToMany(mappedBy: 'term', targetEntity: UserInstrumentGrade::class)]
    private Collection $userInstrumentGrades;

    public function __construct()
    {
        $this->userInstrumentGrades = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->termnumber;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTermnumber(): ?string
    {
        return $this->termnumber;
    }

    public function setTermnumber(string $termnumber): self
    {
        $this->termnumber = $termnumber;

        return $this;
    }

    public function getStartingOn(): ?\DateTimeInterface
    {
        return $this->startingOn;
    }

    public function setStartingOn(\DateTimeInterface $startingOn): self
    {
        $this->startingOn = $startingOn;

        return $this;
    }

    public function getEndingOn(): ?\DateTimeInterface
    {
        return $this->endingOn;
    }

    public function setEndingOn(\DateTimeInterface $endingOn): self
    {
        $this->endingOn = $endingOn;

        return $this;
    }

    public function getHalftermstart(): ?\DateTimeInterface
    {
        return $this->halftermstart;
    }

    public function setHalftermstart(\DateTimeInterface $halftermstart): self
    {
        $this->halftermstart = $halftermstart;

        return $this;
    }

    public function getHalftermend(): ?\DateTimeInterface
    {
        return $this->halftermend;
    }

    public function setHalftermend(\DateTimeInterface $halftermend): self
    {
        $this->halftermend = $halftermend;

        return $this;
    }

    public function getHolidaystart(): ?\DateTimeInterface
    {
        return $this->holidaystart;
    }

    public function setHolidaystart(\DateTimeInterface $holidaystart): self
    {
        $this->holidaystart = $holidaystart;

        return $this;
    }

    public function getHolidayend(): ?\DateTimeInterface
    {
        return $this->holidayend;
    }

    public function setHolidayend(\DateTimeInterface $holidayend): self
    {
        $this->holidayend = $holidayend;

        return $this;
    }

    /**
     * @return Collection<int, UserInstrumentGrade>
     */
    public function getUserInstrumentGrades(): Collection
    {
        return $this->userInstrumentGrades;
    }

    public function addUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): static
    {
        if (!$this->userInstrumentGrades->contains($userInstrumentGrade)) {
            $this->userInstrumentGrades->add($userInstrumentGrade);
            $userInstrumentGrade->setTerm($this);
        }

        return $this;
    }

    public function removeUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): static
    {
        if ($this->userInstrumentGrades->removeElement($userInstrumentGrade)) {
            // set the owning side to null (unless already changed)
            if ($userInstrumentGrade->getTerm() === $this) {
                $userInstrumentGrade->setTerm(null);
            }
        }

        return $this;
    }
}
