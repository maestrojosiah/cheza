<?php

namespace App\Entity;

use App\Repository\ModeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ModeRepository::class)]
class Mode implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: MyKey::class, mappedBy: 'mode', orphanRemoval: true)]
    private Collection $myKeys;

    #[ORM\OneToMany(targetEntity: FlashCard::class, mappedBy: 'mode', orphanRemoval: true)]
    private Collection $flashCards;

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function __construct()
    {
        $this->myKeys = new ArrayCollection();
        $this->flashCards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|MyKey[]
     */
    public function getMyKeys(): Collection
    {
        return $this->myKeys;
    }

    public function addMyKey(MyKey $myKey): self
    {
        if (!$this->myKeys->contains($myKey)) {
            $this->myKeys[] = $myKey;
            $myKey->setMode($this);
        }

        return $this;
    }

    public function removeMyKey(MyKey $myKey): self
    {
        if ($this->myKeys->removeElement($myKey)) {
            // set the owning side to null (unless already changed)
            if ($myKey->getMode() === $this) {
                $myKey->setMode(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FlashCard[]
     */
    public function getFlashCards(): Collection
    {
        return $this->flashCards;
    }

    public function addFlashCard(FlashCard $flashCard): self
    {
        if (!$this->flashCards->contains($flashCard)) {
            $this->flashCards[] = $flashCard;
            $flashCard->setMode($this);
        }

        return $this;
    }

    public function removeFlashCard(FlashCard $flashCard): self
    {
        if ($this->flashCards->removeElement($flashCard)) {
            // set the owning side to null (unless already changed)
            if ($flashCard->getMode() === $this) {
                $flashCard->setMode(null);
            }
        }

        return $this;
    }
}
