<?php

namespace App\Entity;

use App\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SectionRepository::class)]
class Section implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: Page::class, inversedBy: 'sections')]
    #[ORM\JoinColumn(nullable: false)]
    private ?\App\Entity\Page $page = null;

    #[ORM\OneToMany(targetEntity: WebPhotos::class, mappedBy: 'section', orphanRemoval: true)]
    private Collection $webPhotos;

    #[ORM\Column(type: 'text')]
    private ?string $typography = null;

    #[ORM\Column(type: 'integer')]
    private ?int $position = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $visible = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $lastUpdated = null;

    public function __construct()
    {
        $this->webPhotos = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name . " - " . $this->page->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return Collection|WebPhotos[]
     */
    public function getWebPhotos(): Collection
    {
        return $this->webPhotos;
    }

    public function addWebPhoto(WebPhotos $webPhoto): self
    {
        if (!$this->webPhotos->contains($webPhoto)) {
            $this->webPhotos[] = $webPhoto;
            $webPhoto->setSection($this);
        }

        return $this;
    }

    public function removeWebPhoto(WebPhotos $webPhoto): self
    {
        if ($this->webPhotos->removeElement($webPhoto)) {
            // set the owning side to null (unless already changed)
            if ($webPhoto->getSection() === $this) {
                $webPhoto->setSection(null);
            }
        }

        return $this;
    }

    public function getTypography(): ?string
    {
        return $this->typography;
    }

    public function setTypography(string $typography): self
    {
        $this->typography = $typography;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeInterface
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(\DateTimeInterface $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

}
