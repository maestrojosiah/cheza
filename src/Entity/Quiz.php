<?php

namespace App\Entity;

use App\Repository\QuizRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuizRepository::class)]
class Quiz implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private ?string $content = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $image = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $answerMode = null;

    // #[ORM\OneToMany(targetEntity: Choice::class, mappedBy: 'quiz')]
    // private Collection $choices;

    #[ORM\ManyToOne(targetEntity: Lesson::class, inversedBy: 'quizzes')]
    private ?\App\Entity\Lesson $lesson = null;

    #[ORM\OneToMany(targetEntity: Score::class, mappedBy: 'quiz')]
    private Collection $scores;

    #[ORM\OneToMany(mappedBy: 'quiz', targetEntity: Choice::class)]
    private Collection $choices;

    public function __construct()
    {
        $this->choices = new ArrayCollection();
        $this->scores = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->lesson->getName() . ' Lesson - Quiz ' . $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getAnswerMode(): ?string
    {
        return $this->answerMode;
    }

    public function setAnswerMode(string $answerMode): self
    {
        $this->answerMode = $answerMode;

        return $this;
    }

    // /**
    //  * @return Collection|Choice[]
    //  */
    // public function getChoices(): Collection
    // {
    //     return $this->choices;
    // }

    // public function addChoice(Choice $choice): self
    // {
    //     if (!$this->choices->contains($choice)) {
    //         $this->choices[] = $choice;
    //         $choice->setQuiz($this);
    //     }

    //     return $this;
    // }

    // public function removeChoice(Choice $choice): self
    // {
    //     if ($this->choices->removeElement($choice)) {
    //         // set the owning side to null (unless already changed)
    //         if ($choice->getQuiz() === $this) {
    //             $choice->setQuiz(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * @return Collection|Score[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setQuiz($this);
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        if ($this->scores->removeElement($score)) {
            // set the owning side to null (unless already changed)
            if ($score->getQuiz() === $this) {
                $score->setQuiz(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Choice>
     */
    public function getChoices(): Collection
    {
        return $this->choices;
    }

    public function addChoice(Choice $choice): static
    {
        if (!$this->choices->contains($choice)) {
            $this->choices->add($choice);
            $choice->setQuiz($this);
        }

        return $this;
    }

    public function removeChoice(Choice $choice): static
    {
        if ($this->choices->removeElement($choice)) {
            // set the owning side to null (unless already changed)
            if ($choice->getQuiz() === $this) {
                $choice->setQuiz(null);
            }
        }

        return $this;
    }
}
