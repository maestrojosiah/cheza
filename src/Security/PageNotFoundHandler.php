<?php


// src/Security/AccessDeniedHandler.php

namespace App\Security;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ShortUrlRepository;

class PageNotFoundHandler extends AbstractController
{
    public $dictionary = "abcdeklmnfghjpqrstzABCuvwxyDEFGHNPQRJKLMSTUVW23456XYZ789";
    final public const ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
    final public const BASE = 51; // strlen(self::ALPHABET)

    public function __construct()
    {
        $this->dictionary = str_split((string) $this->dictionary);
    }

    #[Route(path: '/shownotfound/error', name: '404notfound', methods: ['GET'])]
    public function show(ShortUrlRepository $shortRepo): Response
    {
        $query = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $shortcode = explode('/', parse_url($query)['path'])[1];
        $decodedId = $this->decode($shortcode);
        $shortUrl = $shortRepo->findOneByDigit($decodedId);
    
        if (null === $shortUrl) {
            return $this->render('default/404.html.twig', [
                'test' => null,
            ], new Response(null, Response::HTTP_NOT_FOUND));
        }
    
        return $this->redirect($shortUrl->getUrl(), Response::HTTP_MOVED_PERMANENTLY);
    }
    
    // public function show(ShortUrlRepository $shortRepo): Response
    // {
    //     $query = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    //     $shortcode = explode('/', parse_url($query)['path'])[1];
    //     $decodedId = $this->decode($shortcode);
    //     $shortUrl = $shortRepo->findOneByDigit($decodedId);

    //     // return $this->render('default/err.html.twig', [
    //     //     'test' => $shortUrl->getUrl(),
    //     // ]);
    //     if(null == $shortUrl) {
    //         return new Response('page not found. Call 0711 832 933 and inform us of this error', \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND);
    //     } else {
    //         return $this->redirect($shortUrl->getUrl(), \Symfony\Component\HttpFoundation\Response::HTTP_MOVED_PERMANENTLY);
    //     }
    //     // return new Response($content, 404);
    // }

    
    public function encode($i)
    {
        if ($i == 0) {
            return $this->dictionary[0];
        }

        $result = [];
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        while ($i > 0) {
            $result[] = $this->dictionary[($i % $base)];
            $i = floor($i / $base);
        }

        $result = array_reverse($result);

        return join("", $result);
    }

    public function decode($input)
    {
        $i = 0;
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        $input = str_split((string) $input);

        foreach($input as $char) {
            $pos = array_search($char, $this->dictionary);

            $i = $i * $base + $pos;
        }

        return $i;
    }
    public static function encode2($num)
    {
        $str = '';

        while ($num > 0) {
            $str = self::ALPHABET[($num % self::BASE)] . $str;
            $num = (int) ($num / self::BASE);
        }

        return $str;
    }

    public static function decode2($str)
    {
        $num = 0;
        $len = strlen((string) $str);

        for ($i = 0; $i < $len; $i++) {
            $num = $num * self::BASE + strpos(self::ALPHABET, (string) $str[$i]);
        }

        return $num;
    }

    /* use this to generate id from desired short string
        public function decodee($input)
        {
            $dictionary = str_split("abcdeklmnfghjpqrstzABCuvwxyDEFGHNPQRJKLMSTUVW23456XYZ789");
            $ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
            $BASE = 51; // strlen(self::ALPHABET)
            $i = 0;
            $base = count($dictionary);

            $input = str_split($input);

            foreach($input as $char)
            {
                $pos = array_search($char, $dictionary);

                $i = $i * $base + $pos;
            }

            return $i;
        }

    */
}
