<?php


// src/Security/AccessDeniedHandler.php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException): ?Response
    {
        $content = "You are trying to access a page that you don't have permissions to access. <br /><br />You do not have permission to access the information that you have requested. 
        Kindly ask the admin to assign you the priviledges.<br /><br />
        For help, call 0711476249.<br /><br /> <a href='/'>Homepage</a> <a href='/logout'>Logout</a>";

        return new Response($content, \Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN);
    }
}
