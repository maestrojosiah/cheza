<?php 

namespace App\Form;

use App\Entity\GroupMember;
use App\Entity\UigGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GroupMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullname')
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Male' => 'male',
                    'Female' => 'female',
                ],
            ])
            ->add('age')
            ->add('school_id')
            ->add('uigGroup', EntityType::class, [
                'class' => UigGroup::class,
                'choice_label' => 'title', // Will display the title of the group in the dropdown
                'placeholder' => 'Select Group', // Optional: placeholder for the select field
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GroupMember::class,
        ]);
    }
}
