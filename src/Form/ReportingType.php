<?php

namespace App\Form;

use App\Entity\Reporting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('term')
            ->add('instrument')
            ->add('age')
            ->add('performed')
            ->add('learning_goals')
            ->add('progress_assessment')
            ->add('strengths')
            ->add('weaknesses')
            ->add('recommendations')
            ->add('practice_habits')
            ->add('learning_level')
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reporting::class,
        ]);
    }
}
