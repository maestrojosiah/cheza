<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('description', CKEditorType::class, ['config' => ['uiColor' => '#ffffff']])
            ->add('image', FileType::class, [
                'label' => 'png and jpeg',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])
            ->add('name')
            ->add('instrument')
            ->add('package')
            ->add('teachers', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'query_builder' => fn (UserRepository $er) => $er->createQueryBuilder('u')
                    ->andWhere('u.usertype = ?1')
                    ->orderBy('u.id', 'ASC')
                    ->setParameter(1, 'teacher'),
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Group Lessons' => 'group',
                    'One On One' => 'single',
                    'Advanced' => 'advanced',
                ]
            ])

            // ->add('teachers', EntityType::class, [
            //     'class' => User::class,
            //     'choices' => $options['teachers'],
            //     'mapped' => false,
            //     'multiple' => true,
            //     'choice_label' => function (User $user) {
            //         return $user->getFullname();
            //     }
            // ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
