<?php

namespace App\Form;

use App\Entity\Quiz;
use App\Entity\Lesson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class QuizType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'content',
                CKEditorType::class,
                ['config' => ['uiColor' => '#ffffff']]
            )
            ->add('image', FileType::class, [
                'label' => 'Attachment: png and jpeg',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])
            ->add('answerMode', ChoiceType::class, [
                'choices' => [
                    'Text' => 'text',
                    'Image' => 'image',
                    'Image and text' => 'imagetext',
                ]
            ])
            ->add('lesson', EntityType::class, [
                'class' => Lesson::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('a')
                    ->orderBy('a.id', 'desc'),
                'choice_label' => function (Lesson $lesson) {
                    return $lesson->getName() . ' - ' . $lesson->getGrade();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Quiz::class,
        ]);
    }
}
