<?php

namespace App\Form;

use App\Entity\WebPhotos;
use App\Entity\Section;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class WebPhotosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('url')
            ->add('placement')
            ->add('alt')
            ->add('thumbnail')
            ->add('section', EntityType::class, [
                'class' => Section::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                    ->orderBy('u.id', 'DESC')
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WebPhotos::class,
        ]);
    }
}
