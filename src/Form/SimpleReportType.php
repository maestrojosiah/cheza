<?php

namespace App\Form;

use App\Entity\SimpleReport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimpleReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullreport')
            ->add('background')
            ->add('addedOn')
            ->add('sent')
            ->add('student')
            ->add('teacher')
        ;
    }

    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults([
            'data_class' => SimpleReport::class,
        ]);
    }
}
