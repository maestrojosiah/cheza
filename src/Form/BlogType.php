<?php

namespace App\Form;

use App\Entity\Blog;
use App\Entity\BlogCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('slug')
            ->add('excerpt')
            ->add('content', CKEditorType::class, ['config' => ['uiColor' => '#ffffff']])
            ->add('published_at', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'html5' => false,
                // 'date_format' => 'dd.MM.yyyy',
                'attr' => ['class' => 'js-datepicker'],
                'with_seconds' => false,
                'required' => true,
            ])
            ->add('category', EntityType::class, [
                'class' => BlogCategory::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('a')
                    ->orderBy('a.id', 'asc'),
            ])
            ->add('published')
            ->add('image', FileType::class, [
                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
