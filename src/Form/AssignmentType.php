<?php

namespace App\Form;

use App\Entity\Attachment;
use App\Entity\Assignment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AssignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'assignment',
                CKEditorType::class,
                ['config' => ['uiColor' => '#ffffff']]
            )
            ->add('assessment', ChoiceType::class, [
                'choices' => [
                    'Excellent' => 'excellent',
                    'Good' => 'good',
                    'Fair' => 'fair',
                    'Poor' => 'poor',
                    'Will Assess Later' => 'later',
                ]
            ])
            ->add('attachment', EntityType::class, [
                'class' => Attachment::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('a')
                    ->orderBy('a.id', 'asc'),
                'choice_label' => 'filename',
            ])
            ->add('reminder', TimeType::class, [
                'input'  => 'datetime',
                'widget' => 'choice',
                'hours' => [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22],
                'minutes' => [0,30]
            ])
            ->add('knob', CheckboxType::class, [
                'label'    => 'Reminder Switch',
                'required' => false,
            ])
        ;
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $newChoice = new ChoiceView([], '', 'No Attachment'); // <- new option
        $view->children['attachment']->vars['choices'][] = $newChoice;//<- adding the new option
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Assignment::class,
        ]);
    }
}
