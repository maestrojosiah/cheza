<?php

namespace App\Form;

use App\Entity\Package;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PackageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('price')
            ->add('duration', ChoiceType::class, [
                'choices' => [
                    '60 Minutes' => '60 Minutes',
                    '45 Minutes' => '45 Minutes',
                    '30 Minutes' => '30 Minutes',
                    '25 Minutes' => '25 Minutes',
                    '20 Minutes' => '20 Minutes',
                    '2 Hrs Group' => '120 Minutes',
                    '1 Hr Group' => '60 Minutes',
                    'Weekly' => '1 Weekly',
                    'Fortnight' => '1 Fortnight',
                    'Monthly' => '1 Monthly',
                ]
            ])
            ->add('name')
            ->add('noOfSessions')
            ->add('costPerLesson')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Package::class,
        ]);
    }
}
