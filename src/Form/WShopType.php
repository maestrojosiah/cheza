<?php

namespace App\Form;

use App\Entity\WShop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('eventType')
            ->add('description')
            ->add('deadline')
            ->add('happensOn')
            ->add('title')
            ->add('slogan')
            ->add('pricing')
            ->add('photo')
            ->add('document')
            ->add('link')
            ->add('comment')
            ->add('extrahtml')
            ->add('extrainfo')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WShop::class,
        ]);
    }
}
