<?php

namespace App\Form;

use App\Entity\Instrument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class InstrumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            // ->add('description', TextareaType::class, [
            //     'attr' => ['class' => 'tinymce'],
            // ])
            ->add('description', CKEditorType::class, ['config' => ['uiColor' => '#ffffff']])
            ->add('image', FileType::class, ['label' => 'Image (PNG/JPG file)', 'data_class' => null])
            ->add('category', ChoiceType::class, [
                'choices' => [
                    'Instrument' => 'instrument',
                    'Course' => 'course',
                    'Group classes' => 'traditional',
                ]
            ])
            ->add('availability', ChoiceType::class, [
                'choices' => [
                    'Available' => 'available',
                    'Online mode only' => 'online-only',
                    'Physical lessons only' => 'offline-only',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Instrument::class,
        ]);
    }
}
