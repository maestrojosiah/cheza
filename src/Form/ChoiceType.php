<?php

namespace App\Form;

use App\Entity\Choice;
use App\Entity\Quiz;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ChoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'content',
                CKEditorType::class,
                ['config' => ['uiColor' => '#ffffff']]
            )
            ->add('refLetter')
            ->add('quiz', EntityType::class, [
                'class' => Quiz::class,
                'choice_label' => function (Quiz $quiz) {
                    return (string) $quiz; // This uses the __toString() method
                },
                'data' => $options['quiz'], // Preselect the quiz
                // 'disabled' => true, // Make it non-editable if necessary
            ])
            ->add('isCorrect')
            ->add('image', FileType::class, [
                'label' => 'Attachment: png and jpeg',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Choice::class,
            'quiz' => null, // Add quiz as an option to the form
        ]);
    }
    
    public function getBlockPrefix()
    {
        return 'question_choice';
    }
    
}
