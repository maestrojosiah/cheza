<?php

namespace App\Service;

use App\Service\Mailer;
use App\Repository\UserRepository;
use Twig\Environment;

class FeesNotify
{

    public function __construct(private readonly Mailer $mailer, private readonly Environment $twig, private readonly UserRepository $userRepo)
    {
    }

    public function notifyIfPaidFor($current_lesson, $fees_per_lesson, $number_of_sessions, $lessons_done, $paid_fees, $teacher, $admins, $student, $uig, $action): String
    {
        // notify when teacher prepares lesson plan
        // notify when teacher marks as attended

        $msg = "before_anything";
                
        // Calculate paid for lessons
        $paid_for_lessons = floor($paid_fees / $fees_per_lesson);
        
        // Notify if it is the last lesson that has been paid for
        $remaining_lessons = $number_of_sessions - $lessons_done;
        $last_paid_for_lesson = $paid_for_lessons == $current_lesson;
        
        if ($last_paid_for_lesson) {
            // echo "This is the last lesson that has been paid for.\n";
            // notify this is the last lesson that has been paid for
            $msg = "last_lesson";
            $this->notifyFeesUpdate($teacher, $msg, $admins, $student, $uig, $action);
        }
        
        // Notify if the current lesson is paid for
        if ($current_lesson <= $paid_for_lessons) {
            // echo "The current lesson is paid for.\n";
            // do nothing, everything is ok
            $msg = "all_is_well";
        } else {
            // echo "The current lesson is not paid for.\n";
            // cry out.. this lesson hasn't been paid for
            $msg = "not_paid_for";
            $this->notifyFeesUpdate($teacher, $msg, $admins, $student, $uig, $action);
        }
        
                
        return $msg;
    }

    public function notifyFeesUpdate($teacher, $msg, $admins, $student, $uig, $action){
        // send message to teacher to stop lessons
        // send message to admin to notify client
        $subject = "";
        $studentName = $student->getFullname();
        $studentId = $student->getId();
        $uigName = explode(' - ', $uig)[0];
        $uigId = $uig->getId();
        $teacherName = $teacher->getFullname();
        if($msg == 'last_lesson'){
            $subject = "Last Paid lesson for $uigName";
            $messageToAdmin = "$teacher has $action the last paid session for $uigName. Please make the necessary communications to the teacher and the client. <a href='https://chezamusicschool.co.ke/admin/remind/parent/prepay/$studentId/$uigId/last_paid_for'>Send Reminder Now</a>";
            $messageToTeacher = "The system is sending this message to notify you that $uigName will exhaust their paid-for lessons after you do that lesson that you've $action. Please contact an admin to know the best way forward.";
        }
        if ($msg == 'not_paid_for'){
            $subject = "Lesson for $uigName not paid for";
            $messageToAdmin = "$teacher has $action $uigName's session that has not been paid for. Please make the necessary communications to the teacher and the client. <a href='https://chezamusicschool.co.ke/admin/remind/parent/prepay/$studentId/$uigId/not_paid_for'>Send Reminder Now</a>";
            $messageToTeacher = "The system is sending this message to notify you that $uigName has not paid for the lesson you have $action. Please contact an admin to know the best way forward.";
        }
        
        $this->mailer->sendEmail(['user' => $teacher, 'msg' => $messageToTeacher], $teacher->getEmail(), $subject , "communication.html.twig", "necessary", "personal_email_from_office");
        foreach ($admins as $admin) {
            $this->mailer->sendEmail(['user' => $admin, 'msg' => $messageToAdmin], $admin->getEmail(), $subject , "communication.html.twig", "necessary", "personal_email_from_office");
        }
        

    }


}