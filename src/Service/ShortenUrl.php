<?php

namespace App\Service;

use App\Repository\UserRepository;
use App\Repository\AttachmentRepository;
use App\Repository\ShortUrlRepository;
use App\Entity\ShortUrl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Mailer;

class ShortenUrl extends AbstractController
{
    public $dictionary = "abcdeklmnfghjpqrstzABCuvwxyDEFGHNPQRJKLMSTUVW23456XYZ789";
    public $shortUrlRepo;
    final public const ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
    final public const BASE = 51; // strlen(self::ALPHABET)

    public function __construct(ShortUrlRepository $shortUrlRepo, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
        $this->dictionary = str_split((string) $this->dictionary);
        $this->shortUrlRepo = $shortUrlRepo;
    }

    public function new($path, $id, $purpose)
    {
        $shortUrl = new ShortUrl();

        $entityManager = $this->managerRegistry->getManager();
        $lastShortUrl = $this->shortUrlRepo->findLastOne();

        if(null == $lastShortUrl) {
            $newId = 1;
        } else {
            $newId = $lastShortUrl->getId() + 1;
        }
        $short = $this->encode($newId);
        $shortUrl->setShort($short);
        $shortUrl->setPurpose($purpose);
        $shortUrl->setUrl($path.$id);
        $shortUrl->setDigit($newId);
        $newUrl = 'https://chez.co.ke/'.$short;

        $entityManager->persist($shortUrl);
        $entityManager->flush();

        return $newUrl;

    }

    public function encode($i)
    {
        if ($i == 0) {
            return $this->dictionary[0];
        }

        $result = [];
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        while ($i > 0) {
            $result[] = $this->dictionary[($i % $base)];
            $i = floor($i / $base);
        }

        $result = array_reverse($result);

        return join("", $result);
    }

    public function decode($input)
    {
        $i = 0;
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        $input = str_split((string) $input);

        foreach($input as $char) {
            $pos = array_search($char, $this->dictionary);

            $i = $i * $base + $pos;
        }

        return $i;
    }
    public static function encode2($num)
    {
        $str = '';

        while ($num > 0) {
            $str = self::ALPHABET[($num % self::BASE)] . $str;
            $num = (int) ($num / self::BASE);
        }

        return $str;
    }

    public static function decode2($str)
    {
        $num = 0;
        $len = strlen((string) $str);

        for ($i = 0; $i < $len; $i++) {
            $num = $num * self::BASE + strpos(self::ALPHABET, (string) $str[$i]);
        }

        return $num;
    }

}
