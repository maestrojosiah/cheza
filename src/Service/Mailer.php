<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\User;
use App\Repository\SettingsRepository;
use App\Repository\UserRepository;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;

class Mailer
{
    public function __construct(private readonly MailerInterface $mailer, private readonly Pdf $pdf, private readonly Environment $twig, private readonly UserRepository $userRepo, private readonly SettingsRepository $settingsRepo)
    {
    }

    public function sendEmailMessage($data, $sendto, $subject, $template, $category, $emailtype)
    {
        $user = $this->userRepo->findOneByEmail($sendto);
        $nokemail = $this->getNOKEmail($user);
        $usersettings = $user->getUserSettings();
        $settings = $this->settingsRepo->findOneByDescription($emailtype);

        $emailSend = $this->emailSendSettings($usersettings, $settings, $emailtype, $category);

        if($emailSend == true) {
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)

            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)

            // pass variables (name => value) to the template
            ->context($data);

            if (null !== $nokemail) {
                $email->addCc($nokemail);
            }


            $this->mailer->send($email);
            // echo "sending email<br>";
            return true;

        } else {
            if($emailtype == 'todays_lessons_sms' || $emailtype == 'todays_lessons_email') {
                // do nothing
            } else {
                $email = (new TemplatedEmail())
                ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                ->to(new Address('chezamusicschool@gmail.com'))
                ->subject('Couldn\'t send message')

                // path of the Twig template to render
                ->htmlTemplate('emails/notsent.html.twig')

                // pass variables (name => value) to the template
                ->context(['notsentto' => $sendto]);

                $this->mailer->send($email);
            }
            return false;
        }

    }

    public function sendEmail($data, $sendto, $subject, $template, $category, $emailtype)
    {

        $emailSend = true;
        $user = $this->userRepo->findOneByEmail($sendto);
        if($user == null) {
            $nokemail = null;
        } else {
            $nokemail = $this->getNOKEmail($user);
        }

        if($emailSend == true) {
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)

            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)

            // pass variables (name => value) to the template
            ->context($data);

            if (null !== $nokemail) {
                $email->addCc($nokemail);
            }

            $this->mailer->send($email);
            // echo "sending email<br>";
            return true;

        } else {
            if($emailtype == 'todays_lessons_sms' || $emailtype == 'todays_lessons_email') {
                // do nothing
            } else {
                $email = (new TemplatedEmail())
                ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                ->to(new Address('chezamusicschool@gmail.com'))
                ->subject('Couldn\'t send message')

                // path of the Twig template to render
                ->htmlTemplate('emails/notsent.html.twig')

                // pass variables (name => value) to the template
                ->context(['notsentto' => $sendto]);

                $this->mailer->send($email);
            }
            return false;
        }

    }

    public function sendEmailWithLinkedAttachment($data, $sendto, $subject, $template, $category, $emailtype, $attachmentPath, $attachmentName)
    {
        $user = $this->userRepo->findOneByEmail($sendto);

        if($user == null) {
            $nokemail = null;
        } else {
            $nokemail = $this->getNOKEmail($user);
        }

        $usersettings = $user->getUserSettings();
        $settings = $this->settingsRepo->findOneByDescription($emailtype);

        $emailSend = $this->emailSendSettings($usersettings, $settings, $emailtype, $category);
        if($emailSend == true) {
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)
            ->attachFromPath($attachmentPath, $attachmentName)

            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)

            // pass variables (name => value) to the template
            ->context($data);

            if (null !== $nokemail) {
                $email->addCc($nokemail);
            }

            $this->mailer->send($email);
            return true;

        } else {
            if($emailtype == 'todays_lessons_sms' || $emailtype == 'todays_lessons_email') {
                // do nothing
            } else {
                $email = (new TemplatedEmail())
                ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                ->to(new Address('chezamusicschool@gmail.com'))
                ->subject('Couldn\'t send message')

                // path of the Twig template to render
                ->htmlTemplate('emails/notsent.html.twig')

                // pass variables (name => value) to the template
                ->context(['notsentto' => $sendto]);

                $this->mailer->send($email);
            }
            return false;
        }

    }

    public function sendEmailWithAttachment($data, $sendto, $subject, $html, $template, $docname, $category, $emailtype)
    {
        $user = $this->userRepo->findOneByEmail($sendto);

        if($user == null) {
            $nokemail = null;
        } else {
            $nokemail = $this->getNOKEmail($user);
        }

        $usersettings = $user->getUserSettings();
        $settings = $this->settingsRepo->findOneByDescription($emailtype);

        $emailSend = $this->emailSendSettings($usersettings, $settings, $emailtype, $category);

        // $html = $this->twig->render('emails/author-weekly-report-pdf.html.twig', [
        //     'articles' => $articles,
        // ]);
        $pdf = $this->mailPdfCreate($html);

        if($emailSend == true) {
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)
            ->htmlTemplate('emails/'.$template)
            ->context($data)
            ->attach($pdf, sprintf($docname.'-%s.pdf', date('Y-m-d')));

            if (null !== $nokemail) {
                $email->addCc($nokemail);
            }

            $this->mailer->send($email);
            return true;
        } else {
            if($emailtype == 'todays_lessons_sms' || $emailtype == 'todays_lessons_email') {
                // do nothing
            } else {
                $email = (new TemplatedEmail())
                ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                ->to(new Address('chezamusicschool@gmail.com'))
                ->subject('Couldn\'t send message')

                // path of the Twig template to render
                ->htmlTemplate('emails/notsent.html.twig')

                // pass variables (name => value) to the template
                ->context(['notsentto' => $sendto]);

                $this->mailer->send($email);
            }
            return false;
        }

    }

    public function mailPdfCreate($html)
    {

        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $pdf = $dompdf->output();
        return $pdf;

    }

    public function getNOKEmail($user){
        $usertype = $user->getUsertype();

        if($usertype == 'admin'){
            return null;
        }
        if($usertype == 'teacher')
        {
            $userdata = $user->getTeacherData();
        } else {
            $userdata = $user->getStudentData();
        }
        $nokemail = $userdata->getNextofkinemail();

        if($nokemail == $user->getEmail()){ 
            return null;
        }

        return $nokemail;
    }

    public function sendEmailWithAttachmentToNonUser($data, $sendto, $subject, $html, $template, $docname, $category, $emailtype)
    {
        $pdf = $this->mailPdfCreate($html);
        // $pdf = $this->mailPdfCreate($html);

        $email = (new TemplatedEmail())
        ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
        ->to(new Address($sendto))
        ->subject($subject)
        ->htmlTemplate('emails/'.$template)
        ->context($data)
        ->attach($pdf, sprintf($docname.'-%s.pdf', date('Y-m-d')));

        $this->mailer->send($email);
        return true;

    }

    public function emailSendSettings($usersettings, $setting, $settingsdesc, $category)
    {

        $u_settings = [];
        $emailtypes = [];
        $emailSend = false;

        foreach ($usersettings as $key => $u_setting) {
            $u_settings[$u_setting->getSetting()->getDescription()] = $u_setting->getSetting()->getDescription()."-".$u_setting->getValue();
            $emailtypes[$u_setting->getSetting()->getId()] = $u_setting->getSetting()->getDescription();
        }

        // die();
        // echo "<pre>";
        // var_dump($settings->getDescription());
        // var_dump($u_settings);
        // var_dump($emailtypes);
        $u_settingsKeys = array_keys($u_settings);
        // var_dump($u_settingsKeys);

        // if settings for reminders explicitly done
        if(in_array($settingsdesc, $u_settingsKeys)) {
            // echo 'exists '. $settingsdesc;
            if(in_array($settingsdesc . '-on', $u_settings)) {
                $emailSend = true;
            }
        } else {
            // echo 'doesnt exist '. $settingsdesc;
        }
        // echo "setting " . $setting->getDescription();
        // echo "<br>so far - " . $emailSend;
        // if no settings but default value is on
        if(empty($u_settings) && $setting->getDefaultVal() == 'true') {
            $emailSend = true;
        }
        // if no settings have been done OR the category for this email is 'necessary', send
        if($category == "necessary") {
            $emailSend = true;
        }
        // echo '<br>';
        // echo 'emailSend: ' . $emailSend;

        return $emailSend;

    }



}
