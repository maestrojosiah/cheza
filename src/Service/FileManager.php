<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\User;
use App\Repository\UserSettingsRepository;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FileManager extends AbstractController
{
    public function __construct(private readonly MailerInterface $mailer, private readonly Pdf $pdf, private readonly Environment $twig, private readonly UserSettingsRepository $usr)
    {
    }

    public function deleteIfFileExists($file, $entry, $path)
    {

        if(@is_array(getimagesize($file))) {
            $image = true;
        } else {
            $image = false;
        }
        if($image == true) {
            //delete the current photo if available
            if($entry != null) {
                $current_image_path = $this->getParameter("$path")."/".$entry;
                $current_image_thumb_path = $this->getParameter("$path")."/thumbs/".$entry;
                if(file_exists($current_image_path)) {
                    unlink($current_image_path);
                }
                if(file_exists($current_image_thumb_path)) {
                    unlink($current_image_thumb_path);
                }
            }
        }

        return true;
    }

}
