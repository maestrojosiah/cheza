<?php 

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class MusicApiService
{
    private $client;
    private $clientId;
    private $clientSecret;
    private $tokenUrl;

    public function __construct(HttpClientInterface $client, string $clientId, string $clientSecret, string $tokenUrl)
    {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->tokenUrl = $tokenUrl;
    }

    public function getAccessToken(): string
    {
        $response = $this->client->request('POST', $this->tokenUrl, [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);

        $data = $response->toArray();

        return $data['access_token'];
    }

    public function submitAssessment(array $data): array
    {
        $accessToken = $this->getAccessToken();

        $response = $this->client->request('POST', 'https://<yourschool>.smartshule.com/api/add_records', [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json',
            ],
            'json' => $data,
        ]);

        return $response->toArray();
    }

}
