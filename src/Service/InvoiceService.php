<?php

namespace App\Service;

use App\Entity\Session;

class InvoiceService
{
    private const DEFAULT_COMMISSION_RATE = 0.7; // 70% commission
    private const DEFAULT_MAX_COMMISSION = 1500; // Cap applies only to default commission

    /**
     * Calculate the commission for a session.
     *
     * @param Session $session
     * @return float The calculated commission.
     */
    public function calculateCommission(Session $session): float
    {
        // Default values
        $percentage = self::DEFAULT_COMMISSION_RATE;
        $cap = self::DEFAULT_MAX_COMMISSION;
        
        // Check for UserInstrumentGrade overrides
        $userInstrumentGrade = $session->getUserInstrumentGrade();
        if (!empty($userInstrumentGrade->getPercentage())) {
            $percentage = (float)$userInstrumentGrade->getPercentage() ?? $percentage;
            // Apply the cap only if the Teacherdata percentage matches the default 70%
            if ($percentage !== self::DEFAULT_COMMISSION_RATE) {
                $cap = null; // No cap for other percentages
            }
        }

        // Check for Teacherdata overrides
        $teacherData = $session->getTeacher()->getTeacherdata();
        if ($teacherData) {
            if ($teacherData && $teacherData->getPercentage() !== null) {
                $percentage = (float)$teacherData->getPercentage();
            }
            
            // $percentage = (float)$teacherData->getPercentage() ?? $percentage;

            // Apply the cap only if the Teacherdata percentage matches the default 70%
            if ($percentage !== self::DEFAULT_COMMISSION_RATE) {
                $cap = null; // No cap for other percentages
            }

        }

        // Calculate the commission
        $package = $session->getPackage();
        $pricePerSession = $package->getPrice() / $package->getNoOfSessions();
        $commission = $pricePerSession * $percentage;

        // Apply the cap if applicable
        if ($cap !== null) {
            return min($commission, $cap);
        }

        return $commission;
    }
}
