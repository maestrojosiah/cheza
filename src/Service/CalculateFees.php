<?php

namespace App\Service;

class CalculateFees
{

    public function calculateBalances($sessions, $uig, $pricePerSession)
    {
        // Calculate done lessons
        $doneLessons = 0;
        $doneSessions = [];
        
        foreach ($sessions as $s) {
            if ($s->getDone() == true) {
                $doneLessons++;
                $doneSessions[] = $s; // Use indexed array for easier manipulation
            } 
        }
        
        $current_lesson = $doneLessons + 1;

        $payments = $uig->getPayments();
        $cashPaid = 0;
        foreach ($payments as $payment) {
            if ($payment->getType() == 'pmt') {
                $cashPaid += $payment->getAmount();
            }
        }

        // Calculate fully paid lessons and the remaining amount
        $paidForLessons = floor($cashPaid / $pricePerSession);
        $remainingAmount = $cashPaid - ($paidForLessons * $pricePerSession);

        $unpaid_lessons = $doneLessons - $paidForLessons;
        $int = $remainingAmount > 0 ? 1 : 0;
        $ul = $remainingAmount > 0 ? $unpaid_lessons - 1 : $unpaid_lessons;
        
        $unpaid_sessions = [];


        // Select the last unpaid lessons
        if ($unpaid_lessons > 0) {
            $start_index = max(0, count($doneSessions) - $unpaid_lessons);
            $unpaid_sessions = array_slice($doneSessions, $start_index, $unpaid_lessons);
        }
    
        // Adjust the unpaid sessions to reflect the partial payment
        if ($remainingAmount > 0 && !empty($unpaid_sessions)) {
            $partial_session = $unpaid_sessions[0];
            $partial_payment_info = [
                'lesson' => $partial_session,
                'remaining_amount' => $pricePerSession - $remainingAmount
            ];
            
            $unpaid_sessions[0] = [
                'session' => $partial_session,
                'paid_amount' => $remainingAmount,
                'remaining_amount' => $pricePerSession - $remainingAmount
            ];
            
            $total_amount = $ul * $pricePerSession + ($pricePerSession - $remainingAmount);
        } else {
            $partial_payment_info = null;
            $total_amount = $ul * $pricePerSession;
        }
    
        // Format unpaid sessions with payment details
        $formatted_unpaid_sessions = [];
        foreach ($unpaid_sessions as $index => $sess) {
            if ($index == 0 && isset($sess['paid_amount'])) {
                $formatted_unpaid_sessions[] = [
                    'date' => $sess['session']->getStartingOn(),
                    'paid_amount' => $sess['paid_amount'],
                    'remaining_amount' => $sess['remaining_amount']
                ];
            } else {
                $formatted_unpaid_sessions[] = [
                    'date' => $sess->getStartingOn(),
                    'paid_amount' => 0,
                    'remaining_amount' => $pricePerSession
                ];
            }
        }
    
        return [$cashPaid, $unpaid_lessons, $formatted_unpaid_sessions, $total_amount, $partial_payment_info];
    }
    
}