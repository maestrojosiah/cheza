<?php

namespace App\Repository;

use App\Entity\DeductionPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DeductionPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeductionPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeductionPayment[]    findAll()
 * @method DeductionPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeductionPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeductionPayment::class);
    }

    // /**
    //  * @return DeductionPayment[] Returns an array of DeductionPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeductionPayment
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
