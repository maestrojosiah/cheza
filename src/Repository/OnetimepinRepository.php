<?php

namespace App\Repository;

use App\Entity\Onetimepin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Onetimepin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Onetimepin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Onetimepin[]    findAll()
 * @method Onetimepin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OnetimepinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Onetimepin::class);
    }

    // /**
    //  * @return Onetimepin[] Returns an array of Onetimepin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Onetimepin
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
