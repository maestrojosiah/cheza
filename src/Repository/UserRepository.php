<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findEntitiesByString($str)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e
                FROM App:User e
                WHERE e.fullname LIKE :str'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }

    /**
     * @return User[] Returns an array of User objects
     */

    public function findFirstByEmail($emailAddress, $usertype = 'student')
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :emailAddress')
            ->andWhere('u.usertype = :usertype')
            ->setParameter('emailAddress', $emailAddress)
            ->setParameter('usertype', $usertype)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }


    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findTeachers()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.usertype = :val')
            ->setParameter('val', 'teacher')
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findTeachersByRand($limit = 4)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.usertype = :val')
            ->andWhere('u.active = :activ')
            ->setParameter('val', 'teacher')
            ->setParameter('activ', 1)
            ->orderBy('RAND()')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    public function countUsersOfType($typestring)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.usertype = :type')
            ->setParameter('type', $typestring)
            ->select('count(u.usertype)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActive($activestatus)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.active = :activestatus')
            ->setParameter('activestatus', $activestatus)
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
