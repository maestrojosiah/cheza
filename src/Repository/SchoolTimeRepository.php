<?php

namespace App\Repository;

use App\Entity\SchoolTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolTime[]    findAll()
 * @method SchoolTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolTimeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SchoolTime::class);
    }

    // /**
    //  * @return SchoolTime[] Returns an array of SchoolTime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SchoolTime
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
