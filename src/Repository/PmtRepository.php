<?php

namespace App\Repository;

use App\Entity\Pmt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pmt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pmt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pmt[]    findAll()
 * @method Pmt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PmtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pmt::class);
    }

    // /**
    //  * @return Pmt[] Returns an array of Pmt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pmt
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
