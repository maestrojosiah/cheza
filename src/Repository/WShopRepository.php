<?php

namespace App\Repository;

use App\Entity\WShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method WShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method WShop[]    findAll()
 * @method WShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WShop::class);
    }

    // /**
    //  * @return WShop[] Returns an array of WShop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WShop
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
