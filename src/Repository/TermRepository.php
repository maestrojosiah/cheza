<?php

namespace App\Repository;

use App\Entity\Term;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Term|null find($id, $lockMode = null, $lockVersion = null)
 * @method Term|null findOneBy(array $criteria, array $orderBy = null)
 * @method Term[]    findAll()
 * @method Term[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Term::class);
    }

    // /**
    //  * @return Term[] Returns an array of Term objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findCurrentTerm($date)
    {
        $term = "";
        $month = $date->format('n');
        if($month <= 4) {
            $term = 'Term 1';
        } elseif ($month >= 5 and $month <= 8) {
            $term = 'Term 2';
        } elseif ($month >= 9 and $month <= 12) {
            $term = 'Term 3';
        }
        return $term;

    }

    public function findNextTerm($termNumber)
    {
        $nextTerm = null;
        if($termNumber == 'Term 1') {
            $nextTerm = 'Term 2';
        } elseif ($termNumber == 'Term 2') {
            $nextTerm = 'Term 3';
        } elseif ($termNumber == 'Term 3') {
            $nextTerm = 'Term 1';
        }
        return $nextTerm;

    }

    public function getTermInt($date)
    {
        $int = "";
        $month = $date->format('z');
        $year = $date->format('Y');
        if($month <= 121) {
            $int = 1 . $year;
        } elseif ($month >= 122 and $month <= 242) {
            $int = 2 . $year;
        } elseif ($month >= 243 and $month <= 365) {
            $int = 3 . $year;
        }

        return $int;

    }

    public function findCurrentTermArr($date)
    {
        $term = "";
        $month = $date->format('z');
        if($month <= 121) {
            $term = 'Term 1';
        } elseif ($month >= 122 and $month <= 242) {
            $term = 'Term 2';
        } elseif ($month >= 243 and $month <= 365) {
            $term = 'Term 3';
        }
        return $term;
    }

    /*
    public function findOneBySomeField($value): ?Term
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
