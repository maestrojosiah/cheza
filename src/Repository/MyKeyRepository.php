<?php

namespace App\Repository;

use App\Entity\MyKey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MyKey|null find($id, $lockMode = null, $lockVersion = null)
 * @method MyKey|null findOneBy(array $criteria, array $orderBy = null)
 * @method MyKey[]    findAll()
 * @method MyKey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MyKeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MyKey::class);
    }

    // /**
    //  * @return MyKey[] Returns an array of MyKey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MyKey
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
