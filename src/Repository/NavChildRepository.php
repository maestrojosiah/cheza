<?php

namespace App\Repository;

use App\Entity\NavChild;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NavChild|null find($id, $lockMode = null, $lockVersion = null)
 * @method NavChild|null findOneBy(array $criteria, array $orderBy = null)
 * @method NavChild[]    findAll()
 * @method NavChild[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NavChildRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NavChild::class);
    }

    // /**
    //  * @return NavChild[] Returns an array of NavChild objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NavChild
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
