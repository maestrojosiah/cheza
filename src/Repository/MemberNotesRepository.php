<?php

namespace App\Repository;

use App\Entity\MemberNotes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MemberNotes|null find($id, $lockMode = null, $lockVersion = null)
 * @method MemberNotes|null findOneBy(array $criteria, array $orderBy = null)
 * @method MemberNotes[]    findAll()
 * @method MemberNotes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberNotesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MemberNotes::class);
    }

    // /**
    //  * @return MemberNotes[] Returns an array of MemberNotes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MemberNotes
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
