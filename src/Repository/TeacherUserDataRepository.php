<?php

namespace App\Repository;

use App\Entity\TeacherUserData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TeacherUserData|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherUserData|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherUserData[]    findAll()
 * @method TeacherUserData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherUserDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeacherUserData::class);
    }

    // /**
    //  * @return TeacherUserData[] Returns an array of TeacherUserData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeacherUserData
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
