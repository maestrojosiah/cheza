<?php

namespace App\Repository;

use App\Entity\StudentUserData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StudentUserData|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentUserData|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentUserData[]    findAll()
 * @method StudentUserData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentUserDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentUserData::class);
    }

    // /**
    //  * @return StudentUserData[] Returns an array of StudentUserData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentUserData
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
