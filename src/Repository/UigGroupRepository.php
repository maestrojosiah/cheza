<?php

namespace App\Repository;

use App\Entity\UigGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UigGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method UigGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method UigGroup[]    findAll()
 * @method UigGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UigGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UigGroup::class);
    }

    // /**
    //  * @return UigGroup[] Returns an array of UigGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UigGroup
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
