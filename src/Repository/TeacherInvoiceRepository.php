<?php

namespace App\Repository;

use App\Entity\TeacherInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TeacherInvoice>
 *
 * @method TeacherInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherInvoice[]    findAll()
 * @method TeacherInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeacherInvoice::class);
    }

//    /**
//     * @return TeacherInvoice[] Returns an array of TeacherInvoice objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TeacherInvoice
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
