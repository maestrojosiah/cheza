<?php

namespace App\Repository;

use App\Entity\PracticeSessions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PracticeSessions|null find($id, $lockMode = null, $lockVersion = null)
 * @method PracticeSessions|null findOneBy(array $criteria, array $orderBy = null)
 * @method PracticeSessions[]    findAll()
 * @method PracticeSessions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PracticeSessionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PracticeSessions::class);
    }

    /**
     * @return Session[] Returns an array of Session objects
     */

    public function practicedToday($todayStart, $todayEnd)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.doneOn BETWEEN :from AND :to')
            ->setParameter('from', $todayStart)
            ->setParameter('to', $todayEnd)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // /**
    //  * @return PracticeSessions[] Returns an array of PracticeSessions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PracticeSessions
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
