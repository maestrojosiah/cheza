<?php

namespace App\Repository;

use App\Entity\ContactGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactGroup[]    findAll()
 * @method ContactGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactGroup::class);
    }

    // /**
    //  * @return ContactGroup[] Returns an array of ContactGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactGroup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
