<?php

namespace App\Extensions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;
use Doctrine\ORM\Query\Lexer;

class Rand extends FunctionNode
{
    private \Doctrine\ORM\Query\AST\ArithmeticTerm|\Doctrine\ORM\Query\AST\SimpleArithmeticExpression|null $expression = null;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker): string
    {
        if ($this->expression) {
            return 'RAND(' . $this->expression->dispatch($sqlWalker) . ')';
        }

        return 'RAND()';
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser): void
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        // var_dump($lexer->lookahead->type);
        // die();
        if (Lexer::T_CLOSE_PARENTHESIS !== $lexer->lookahead->type) {
            $this->expression = $parser->SimpleArithmeticExpression();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
