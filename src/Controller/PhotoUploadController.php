<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PhotoUpload;
use App\Repository\PhotoUploadRepository;

class PhotoUploadController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/photo/upload', name: 'photo_upload')]
    public function index(PhotoUploadRepository $photoUploadRepository): Response
    {
        return $this->render('file_upload/cropper.html.twig', [
            'photos' => $photoUploadRepository->findBy(
                [],
                ['id'=> 'DESC'],
                20
            ),
        ]);
    }

    #[Route(path: 'photo/upload/delete/{photo_id}', name: 'photo_delete')]
    public function delete(PhotoUploadRepository $photoRepo, $photo_id): Response
    {
        $photo = $photoRepo->findOneById($photo_id);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($photo);
        $entityManager->flush();

        return $this->redirectToRoute('photo_upload');
    }


}
