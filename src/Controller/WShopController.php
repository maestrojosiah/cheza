<?php

namespace App\Controller;

use App\Entity\WShop;
use App\Form\WShopType;
use App\Repository\WShopRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/w/shop')]
class WShopController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'w_shop_index', methods: ['GET'])]
    public function index(WShopRepository $wShopRepository): Response
    {
        return $this->render('w_shop/index.html.twig', [
            'w_shops' => $wShopRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'w_shop_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $wShop = new WShop();
        $form = $this->createForm(WShopType::class, $wShop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($wShop);
            $entityManager->flush();

            return $this->redirectToRoute('w_shop_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('w_shop/new.html.twig', [
            'w_shop' => $wShop,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'w_shop_show', methods: ['GET'])]
    public function show(WShop $wShop): Response
    {
        return $this->render('w_shop/show.html.twig', [
            'w_shop' => $wShop,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'w_shop_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, WShop $wShop): Response
    {
        $form = $this->createForm(WShopType::class, $wShop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('w_shop_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('w_shop/edit.html.twig', [
            'w_shop' => $wShop,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'w_shop_delete', methods: ['POST'])]
    public function delete(Request $request, WShop $wShop): Response
    {
        if ($this->isCsrfTokenValid('delete'.$wShop->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($wShop);
            $entityManager->flush();
        }

        return $this->redirectToRoute('w_shop_index', [], Response::HTTP_SEE_OTHER);
    }
}
