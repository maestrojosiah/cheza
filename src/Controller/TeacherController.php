<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PackageRepository;
use App\Repository\PaymentRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\SessionRepository;
use App\Repository\TermRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\UserRepository;
use App\Service\InvoiceService;
use Doctrine\ORM\EntityManager;

class TeacherController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/teacher/calendar', name: 'teacher_session_calendar', methods: ['GET'])]
    public function calendar(SchoolTimeRepository $schoolTimeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $schedules = $schoolTimeRepo->findByTeacher($this->getUser());
        return $this->render('teacher/calendar.html.twig', [
            'schedules' => $schedules,
            'teacher' => $this->getUser(),
        ]);
    }

    #[Route(path: '/teacher/attendance', name: 'teacher_student_attendance', methods: ['GET'])]
    public function attendance(UserInstrumentGradeRepository $userInstrumentGradeRepo, InvoiceService $invoiceService): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        $em = $this->managerRegistry->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';
        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }
        $teacher = $this->getUser();
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('thisteacher', $teacher->getId());
        $result = $statement->executeQuery();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $result->fetchAllAssociative();

        foreach ($userInstrumentGradesIds as $uigId) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session) {
                if ($session->getDone() && !$session->getInvoiced()) {
                    $commission = $invoiceService->calculateCommission($session);
                    $inv += $commission;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)) {
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }

        $days = [];
        foreach ($userInstrumentGrades as $uig) {

            $uigsessions = $uig->getSessions();
            foreach ($uigsessions as $key => $sess) {
                $beginAt = $sess->getBeginAt();
                $endAt = $sess->getEndAt();
                $days[$uig->getId().'-'.$beginAt->format('l')] = $beginAt->format('l').' '.$beginAt->format('H:i').' - '.$endAt->format('H:i');
            }

        }

        return $this->render('teacher/attendance.html.twig', [
            'teacher' => $this->getUser(),
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'days' => $days,
            'inv' => $inv,
        ]);
    }

    #[Route(path: '/teacher/attendance/completed', name: 'teacher_student_attendance_completed', methods: ['GET'])]
    public function attendanceDone(InvoiceService $invoiceService, UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        $em = $this->managerRegistry->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';
        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }
        $teacher = $this->getUser();
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('thisteacher', $teacher->getId());
        $result = $statement->executeQuery();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $result->fetchAllAssociative();

        foreach ($userInstrumentGradesIds as $uigId) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session) {
                if($session->getDone() == true && $session->getInvoiced() == false) {
                    $commission = $invoiceService->calculateCommission($session);
                    $inv += $commission;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)) {
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }
        $days = [];
        foreach ($userInstrumentGrades as $uig) {

            $uigsessions = $uig->getSessions();
            foreach ($uigsessions as $key => $sess) {
                $beginAt = $sess->getBeginAt();
                $endAt = $sess->getEndAt();
                $days[$uig->getId().'-'.$beginAt->format('l')] = $beginAt->format('l').' '.$beginAt->format('H:i').' - '.$endAt->format('H:i');
            }

        }


        return $this->render('teacher/attendance_done.html.twig', [
            'teacher' => $this->getUser(),
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'days' => $days,
            'inv' => $inv,
        ]);
    }

    #[Route(path: '/teacher/invoice/confirm', name: 'teacher_send_invoice_confirm', methods: ['GET'])]
    public function sendInvoiceConfirm(
        InvoiceService $invoiceService, 
        UserInstrumentGradeRepository $userInstrumentGradeRepo
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $teacher = $this->getUser();
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }

        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }
        // Fetch user instrument grade IDs
        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session WHERE session.teacher_id = :thisteacher;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->bindValue('thisteacher', $teacher->getId());
        $result = $statement->executeQuery();
        $userInstrumentGradesIds = $result->fetchAllAssociative();
    
        // Fetch UserInstrumentGrades and their sessions
        $userInstrumentGrades = [];
        foreach ($userInstrumentGradesIds as $uigId) {
            $uig = $userInstrumentGradeRepo->findOneById($uigId['user_instrument_grade_id']);
            if ($uig) {
                $userInstrumentGrades[] = $uig;
            }
        }
    
        $teacherData = $teacher->getTeacherdata();
        if ($teacherData && $teacherData->getPercentage() !== null) {
            $percentage = (float)$teacherData->getPercentage();
        } else {
            $percentage = 0.7;
        }
        // $percentage = $teacherData->getPercentage() ?? 0.7; // Default percentage if not set
        $deductions = array_reduce($teacherData->getDeductions()->toArray(), fn($sum, $d) => $sum + $d->getToDeduct(), 0);
    
        $invoiceData = [];
        $totalInvoiceAmount = 0;
        $nonEmptyUigs = []; // This is where we will store the grouped session data
    
        foreach ($userInstrumentGrades as $uig) {
            $sessionGroups = [];
            foreach ($uig->getSessions() as $session) {
                if ($session->getDone() && !$session->getInvoiced()) {
                    $package = $session->getPackage();
                    $pricePerSession = $package->getPrice() / $package->getNoOfSessions();
                    // $uigPercentage = $uig->getPercentage() ?? $percentage;
                    if ($uig && $uig->getPercentage() !== null && !empty($uig->getPercentage())) {
                        $uigPercentage = (float)$uigPercentage = $uig->getPercentage();
                    } else {
                        $uigPercentage = $percentage;
                    }
                    $sessionCommission = $invoiceService->calculateCommission($session);
    
                    $totalInvoiceAmount += $sessionCommission;
                    // Group sessions by userInstrumentGradeId and packageId
                    $sessionGroups[] = [
                        'packageId' => $package->getId(),
                        'pricePerSession' => $pricePerSession,
                        'uigPercentage' => $uigPercentage,
                        'sessionCommission' => $sessionCommission,
                    ];
    
                    // Building nonEmptyUigs
                    $nonEmptyUigs[$uig->getId()][] = $uig->getId() . '-' . $package->getId(); // same as in the original code
                }
            }
            if (!empty($sessionGroups)) {
                $invoiceData[] = [
                    'userInstrumentGradeId' => $uig->getId(),
                    'studentName' => $uig->getStudentUserData()->getStudent()->getFullName(), 
                    'noOfSessions' => count($sessionGroups),
                    'sessions' => $sessionGroups,
                ];
            }
        }
    
        return $this->render('teacher/send_invoice.html.twig', [
            'teacher' => $teacher,
            'invoiceData' => $invoiceData,
            'totalInvoiceAmount' => $totalInvoiceAmount,
            'ded' => $deductions,
            'nonEmptyUigs' => $nonEmptyUigs,  
            'inv' => $totalInvoiceAmount,
        ]);
    }
    

    #[Route(path: '/teacher', name: 'teacher')]
    public function index(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('teacher/profile.html.twig', [
            'teacher' => $this->getUser(),
            'data' => $data,
        ]);
    }
    #[Route(path: '/teacher/dashboard', name: 'teacher-dashboard')]
    public function dashboard(SessionRepository $sessionRepo, UserRepository $userRepo, TermRepository $termRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $students = [];
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }
        $teacher = $this->getUser();
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }
        $RAW_QUERY = 'select COUNT(*) from session where teacher_id = ' . $teacher->getId(). ' and invoiced is NULL group by user_instrument_grade_id;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $result1 = $statement->executeQuery();
        $today = new \Datetime(date('Y-m-d'));
        $currentTerm = $termRepo->findCurrentTerm($today);
        $ct = $termRepo->findOneByTermnumber($currentTerm);

        $studentscount = $result1->fetchAllAssociative();
        $todayslessons = $sessionRepo->findTodaysLessons($teacher);

        $RAW_QUERY2 = 'select user_id, term, added_on from reporting where teacher_id = ' . $teacher->getTeacherdata()->getId() . ';';
        $statement2 = $em->getConnection()->prepare($RAW_QUERY2);
        $result2 = $statement2->executeQuery();

        $reports = $result2->fetchAllAssociative();

        $RAW_QUERY3 = 'select student_id from session where teacher_id = '. $teacher->getId() .' and invoiced is NULL group by student_id;';
        $statement3 = $em->getConnection()->prepare($RAW_QUERY3);
        $result3 = $statement3->executeQuery();

        $student_ids = $result3->fetchAllAssociative();

        $RAW_QUERY4 = 'select user_instrument_grade_id from session where invoiced is null and teacher_id = '. $teacher->getId() .' group by user_instrument_grade_id;';
        $statement4 = $em->getConnection()->prepare($RAW_QUERY4);
        $result4 = $statement4->executeQuery();

        $all_students = $result4->fetchAllAssociative();
        $uigs = [];

        $this_term_reports = [];
        foreach($reports as $key => $report) {
            $this_term_int = $termRepo->getTermInt($today);
            $report_term_int = explode(' ', (string) $report['term'])[1].date('Y');
            // echo $this_term_int . '-' . $report_term_int . "<br/>";
            if($this_term_int == $report_term_int) {
                $this_term_reports[] = $report;
            }
        }

        foreach ($all_students as $entry) {
            $uigs[] = $userInstrumentGradeRepo->findOneById($entry);
        }

        foreach ($student_ids as $key => $student_id) {
            $students[] = $userRepo->findOneById($student_id);
        }

        $totalfees = 0;
        $paidfees = 0;
        $schooltimes = [];
        $studenttimes = [];

        foreach ($students as $student) {
            $totalfees += $student->getStudentsessions()[0]->getPackage()->getPrice();

            $RAW_QUERY4 = 'SELECT SUM(amount) AS totalamount FROM payment where payment.user_id = '. $student->getId() .' and payment.type = "pmt" ;';
            $statement4 = $em->getConnection()->prepare($RAW_QUERY4);
            $result = $statement4->executeQuery();
            $totalpaid = $result->fetchAllAssociative();
            $paidfees += $totalpaid[0]['totalamount'];


        }

        foreach ($uigs as $uig) {
            $sess = explode(' ', (string) $uig->getSessions()[0]->getPackage()->getDuration())[0];

            $init = (int)$sess * 60;
            $hours = floor($init / 3600);
            $minutes = floor(($init / 60) % 60);
            $seconds = $init % 60;
            if ($hours < 10) {
                $hours = sprintf("%02d", $hours);
            }
            if ($minutes < 10) {
                $minutes = sprintf("%02d", $minutes);
            }
            if ($seconds < 10) {
                $seconds = sprintf("%02d", $seconds);
            }

            $studenttimes[] = "$hours:$minutes:$seconds";
        }
        // calculate total schooltime for this teacher
        foreach ($teacher->getSchoolTimes() as $schedule) {
            $a = $schedule->getBeginAt();
            $b = $schedule->getEndAt();
            $interval = $b->diff($a);

            // echo $interval->format("%H");
            $schooltimes[] = $interval->format('%H:%I:%S');
        }

        $hour = 0;
        $min = 0;
        $sec = 0;
        $schtime = strtotime("00:00:00"); // school times
        foreach($schooltimes as $a) {
            $schtime += strtotime((string) $a) - strtotime('00:00:00');
            $time = explode(':', (string) $a);
            $sec += $time[2];
            $min += $time[1];
            $hour += $time[0];
        }
        $totalschooltime = (int)$hour + ($min / 60);

        $hr = 0;
        $mn = 0;
        $sc = 0;
        $stutime = strtotime("00:00:00"); // studenttimes
        foreach($studenttimes as $a) {
            $stutime += strtotime($a) - strtotime('00:00:00');
            $time = explode(':', $a);
            $sc += $time[2];
            $mn += $time[1];
            $hr += $time[0];
        }
        $totalstudenttime = (int)$hr + ($mn / 60);

        $sessions = $sessionRepo->findBy(
            ['invoiced' => null, 'teacher' => $teacher],
            ['id' => 'ASC']
        );
        $donesessions = $sessionRepo->findBy(
            ['invoiced' => null, 'teacher' => $teacher, 'done' => true],
            ['id' => 'ASC']
        );

        $date1 = new \DateTime(date('Y-m-d H:i:s', $schtime));
        $date2 = new \DateTime(date('Y-m-d H:i:s', $stutime));
        $interval = $date1->diff($date2);

        // echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
        $data = [];
        $teachersessions = $teacher->getTeachersessions();
        $schedules = $teacher->getSchoolTimes();
        $data['all_students'] = $all_students;
        $data['teachersessions'] = $sessions;
        $data['studentscount'] = $studentscount;
        $data['schedules'] = $schedules;
        $data['todayslessonscount'] = is_countable($todayslessons) ? count($todayslessons) : 0;
        $data['todayslessons'] = $todayslessons;
        $data['reports'] = $this_term_reports;
        $data['sessions'] = $sessions;
        $data['donesessions'] = $donesessions;
        $data['donesessions'] = $donesessions;
        $data['students'] = $students;
        $data['totalfees'] = $totalfees;
        $data['paidfees'] = $paidfees;
        $data['schooltimes'] = $totalschooltime;
        $data['studenttime'] = $totalstudenttime;
        $data['currentTerm'] = $currentTerm;
        $data['teacher'] = $teacher;
        $data['schooltimess'] = $schooltimes;
        $data['studenttimes'] = $studenttimes;
        $data['interval'] = "difference " . $interval->h . " hours ";
        $data['schtime'] = date('H:i:s', $schtime);
        $data['stutime'] = date('H:i:s', $stutime);
        $data['strtimediff'] = date('H:i:s', $schtime - $stutime - strtotime("00:00:00"));

        return $this->render('teacher/dashboard.html.twig', [
            'data' => $data,
        ]);
    }

    #[Route(path: '/teacher/students/list', name: 'teacher-students-list')]
    public function studentsList(SessionRepository $sessionRepo, TermRepository $termRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $teacher = $this->getUser();
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }

        $sessionsuniq = [];
        $term1 = [];
        $term2 = [];
        $term3 = [];

        $r = [];
        foreach ($teacher->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $reports[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }
            
        }

        $currentTerm = $termRepo->findCurrentTermArr(new \Datetime());
        $ct = $termRepo->findOneByTermnumber($currentTerm);
        $terms = $termRepo->findAll();
        $uigsForThisTeacher = $uigRepo->findBy(
            array('teacher' => $teacher),
        );

        $termStart = $ct->getStartingOn();
        $termEnd = $ct->getEndingOn();
        $sessions = $teacher->getTeachersessions();


        foreach ($sessions as $key => $sess) {
            $uig = $sess->getUserInstrumentGrade();

            $firstSession = $sessionRepo->getOneWithThisYearDate($uig->getId());
            if (!empty($firstSession)) {
                $id = $firstSession[0]['id'];
                $s = $sessionRepo->find($id);
                $firstSessionDate = $s->getStartingOn();
                $prevailingTerm = $termRepo->findCurrentTerm($firstSessionDate);
                if($prevailingTerm == "Term 1" || $s->getUserInstrumentGrade()->getTerm() == "Term 1") {
                    $term1[$uig->getId()] = $s;
                } 
                
                if ($prevailingTerm == "Term 2" || $s->getUserInstrumentGrade()->getTerm() == "Term 2") {
                    $term2[$uig->getId()] = $s;
                } 
                
                if ($prevailingTerm == "Term 3" || $s->getUserInstrumentGrade()->getTerm() == "Term 3") {
                    $term3[$uig->getId()] = $s;
                }
                $sessionsuniq[$s->getUserInstrumentGrade()->getId().'-'.$prevailingTerm] = $s;
            }
            
        }

      
        return $this->render('teacher/studentslist.html.twig', [
            'sessions' => $sessionsuniq,
            'currentTerm' => $currentTerm,
            'r' => $r,
            'terms' => ['Term 1' => $term1, 'Term 2' => $term2, 'Term 3' => $term3],
            'uigsForThisTeacher' => $uigsForThisTeacher,

        ]);
        
    }



}
