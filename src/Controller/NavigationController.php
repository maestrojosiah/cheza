<?php

namespace App\Controller;

use App\Entity\Navigation;
use App\Form\NavigationType;
use App\Repository\NavigationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/navigation')]
class NavigationController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'navigation_index', methods: ['GET'])]
    public function index(NavigationRepository $navigationRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('navigation/index.html.twig', [
            'navigations' => $navigationRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'navigation_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $navigation = new Navigation();
        $form = $this->createForm(NavigationType::class, $navigation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($navigation);
            $entityManager->flush();

            return $this->redirectToRoute('navigation_index');
        }

        return $this->render('navigation/new.html.twig', [
            'navigation' => $navigation,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'navigation_show', methods: ['GET'])]
    public function show(Navigation $navigation): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('navigation/show.html.twig', [
            'navigation' => $navigation,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'navigation_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Navigation $navigation): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(NavigationType::class, $navigation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('navigation_index');
        }

        return $this->render('navigation/edit.html.twig', [
            'navigation' => $navigation,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'navigation_delete', methods: ['DELETE'])]
    public function delete(Request $request, Navigation $navigation): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$navigation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($navigation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('navigation_index');
    }
}
