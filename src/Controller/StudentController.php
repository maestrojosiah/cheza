<?php

namespace App\Controller;

use App\Entity\InstrumentGrade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\StudentUserDataRepository;
use App\Repository\PracticeSessionsRepository;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\SessionRepository;
use App\Repository\AssignmentRepository;
use App\Repository\TermRepository;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Service\InvoiceService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class StudentController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/student/calendar', name: 'student_session_calendar', methods: ['GET'])]
    public function calendar(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        return $this->render('student/calendar.html.twig', [
            'student' => $this->getUser(),
        ]);
    }

    #[Route(path: '/student/attendance/{student_id}', name: 'student_attendance', methods: ['GET'])]
    public function attendance(InvoiceService $invoiceService, UserInstrumentGradeRepository $userInstrumentGradeRepo, $student_id, UserRepository $userRepo): Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $student = $userRepo->findOneById($student_id);

        $em = $this->managerRegistry->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.student_id = :thisstudent;';

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('thisstudent', $student->getId());
        $result = $statement->executeQuery();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $result->fetchAllAssociative();

        foreach ($userInstrumentGradesIds as $uigId) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session) {
                if($session->getDone() == true && $session->getInvoiced() == false) {
                    $commission = $invoiceService->calculateCommission($session);
                    $inv += $commission;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)) {
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }

        $days = [];
        foreach ($userInstrumentGrades as $uig) {

            $uigsessions = $uig->getSessions();
            foreach ($uigsessions as $key => $sess) {
                $beginAt = $sess->getBeginAt();
                $endAt = $sess->getEndAt();
                $days[$uig->getId().'-'.$beginAt->format('l')] = $beginAt->format('l').' '.$beginAt->format('H:i').' - '.$endAt->format('H:i');
            }

        }


        return $this->render('student/attendance.html.twig', [
            'teacher' => $this->getUser(),
            'student' => $student,
            'days' => $days,
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'inv' => $inv,
        ]);
    }

    #[Route(path: '/student', name: 'student')]
    public function index(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('student/profile.html.twig', [
            'student' => $this->getUser(),
            'data' => $data,
        ]);
    }
    #[Route(path: '/student/dashboard', name: 'student-dashboard')]
    public function dashboard(UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo, PracticeSessionsRepository $practiceRepo, AssignmentRepository $assignmentRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');

        $assignments = $assignmentRepo->findBy(
            ['student' => $this->getUser()],
            ['id' => 'DESC'],
            2
        );
        $studentData = $this->getUser()->getStudentdata();

        $student = $this->getUser();
        $payments = $student->getPayments();

        $monday = new \DateTimeImmutable('Monday this week');
        $clonedMonday = $monday;
        $sunday = $clonedMonday->modify('+6 days');
        $lessonsThisWeek = $sessionRepo->findLessonsThisWeek($student, $monday, $sunday);

        $todayStart = new \DateTimeImmutable('first minute today');
        $todayEnd = new \DateTimeImmutable('last minute tomorrow');

        $practicedToday = $practiceRepo->practicedToday($todayStart, $todayEnd);

        $mysessions = $sessionRepo->findBy(
            ['student' => $this->getUser()],
            ['id' => 'ASC']
        );
        $uniqSessions = [];
        foreach ($mysessions as $key => $s) {
            $uniqSessions[$s->getTeacher()->getId()] = $s;
        }
        $uigsForThisStudent = $uigRepo->findBy(
            ['studentUserData' => $studentData],
            ['id' => 'DESC']
        );
        return $this->render('student/dashboard.html.twig', [
            'uigs' => $uigsForThisStudent,
            'lessonsThisWeek' => $lessonsThisWeek,
            'practicedToday' => $practicedToday,
            'payments' => $payments,
            'student' => $student,
            'assignments' => $assignments,
            'mysessions' => $mysessions,
            'uniqSessions' => $uniqSessions,
        ]);
    }

    #[Route(path: '/report/view/{session_id}', name: 'report_view', methods: ['GET', 'POST'])]
    public function report(UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo, TermRepository $termRepo, ReportRepository $reportRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $past = [];
        $sentreports = [];
        $term1 = [];
        $term2 = [];
        $term3 = [];
        $rpts = [];
        $currentTerm = $termRepo->findCurrentTerm(new \Datetime(date('Y-m-d')));
        $reports = $reportRepo->findBy(
            ['uig' => $session->getUserInstrumentGrade()],
            ['id' => 'ASC'],
        );
        // $reportE = $reportRepo->findOneBy(
        //     array('teacher' => $session->getTeacher()->getTeacherdata(), 'term' => $currentTerm, 'student' => $session->getStudent()->getStudentdata(), 'uig' => $session->getUserInstrumentGrade()),
        //     array('id' => 'ASC'),
        // );

        if(count($reports) > 0) {

            foreach ($reports as $report) {
                $uig = $report->getUig();
                $reportTerm = $termRepo->findCurrentTerm($report->getAddedOn());

                if($report -> getAddedOn()->format('Y') != date('Y')) {
                    $past[$uig->getId()] = $report;
                } else {
                    if($reportTerm == "Term 1") {
                        $term1[$uig->getId()] = $report;
                    } elseif ($reportTerm == "Term 2") {
                        $term2[$uig->getId()] = $report;
                    } elseif ($reportTerm == "Term 3") {
                        $term3[$uig->getId()] = $report;
                    }
                }
                // $reportsuniq[$report->getUig()->getId().'-'.$reportTerm] = $report;
                // $all_reports[$reportTerm.'-'.$report->getId()] = $report;
            }

        }

        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true) {
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $rpts[] = $reportentry->getUig()->getId();
            }
        }

        $sessionsuniq = [];
        $term1List = [];
        $term2List = [];
        $term3List = [];
        $sentreportsList = [];
        $reportsList = [];

        $r = [];
        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true) {
                $sentreportsList[] = $reportentry->getUig()->getId();
            } else {
                $reportsList[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }

        }

        $ct = $termRepo->findOneByTermnumber($currentTerm);

        $termStart = $ct->getStartingOn();
        $termEnd = $ct->getEndingOn();
        $sessions = $sessionRepo->findForThisTermAsc($termStart, $termEnd, $session->getTeacher());


        foreach ($sessions as $key => $sess) {
            $uig = $sess->getUserInstrumentGrade();
            $firstSession = $sessionRepo->findOneBy(
                ['userInstrumentGrade' => $uig],
                ['id' => 'ASC']
            );
            $firstSessionDate = $firstSession->getStartingOn();
            $prevailingTerm = $termRepo->findCurrentTerm($firstSessionDate);

            if($prevailingTerm == "Term 1") {
                $term1List[$uig->getId()] = $firstSession;
            } elseif ($prevailingTerm == "Term 2") {
                $term2List[$uig->getId()] = $firstSession;
            } elseif ($prevailingTerm == "Term 3") {
                $term3List[$uig->getId()] = $firstSession;
            }
            $sessionsuniq[$firstSession->getUserInstrumentGrade()->getId().'-'.$prevailingTerm] = $firstSession;

        }

        return $this->render('report/report_view_view.html.twig', [
            'reports' => $past,
            'reportE' => $past,
            'sessions' => $sessionsuniq,
            'r' => $r,
            'rpts' => $rpts,
            'term1' => $term1,
            'term2' => $term2,
            'term3' => $term3,
            'terms' => ['Term 1' => $term1, 'Term 2' => $term2, 'Term 3' => $term3, 'Past' => $past],
            'termsList' => ['Term 1' => $term1List, 'Term 2' => $term2List, 'Term 3' => $term3List],
            'sentreports' => $sentreports,
            'session' => $session,
            'currentTerm' => $currentTerm,
        ]);

    }

    #[Route(path: '/student/report/list', name: 'student-report-list')]
    public function studentsReportList(SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $student = $this->getUser();
        $sessionsuniq = [];

        $sessions = $sessionRepo->findBy(
            ['student' => $student],
            ['id' => 'ASC']
        );

        foreach ($sessions as $key => $sess) {
            $sessionsuniq[$sess->getUserInstrumentGrade()->getId()] = $sess;
        }

        return $this->render('student/reportlist.html.twig', [
            'controller_name' => 'StudentController',
            'sessions' => $sessionsuniq,
        ]);
    }

    #[Route(path: '/student/test', name: 'student-test')]
    public function test(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        return $this->render('student/test.html.twig', [
            'controller_name' => 'StudentController',
        ]);
    }

    #[Route(path: '/student/uigs', name: 'student-uigs')]
    public function showUigs(UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        $studentData = $this->getUser()->getStudentdata();
        $uigsForThisStudent = $uigRepo->findBy(
            ['studentUserData' => $studentData],
            ['id' => 'DESC']
        );
        return $this->render('student/student_uigs.html.twig', [
            'uigs' => $uigsForThisStudent,
        ]);
    }

    #[Route(path: '/student/payments/list/{uig_id}', name: 'student-payments-list')]
    public function paymentsList(UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');

        $uig = $uigRepo->findOneById($uig_id);
        $student = $this->getUser();
        $payments = $student->getPayments();
        return $this->render('student/payments.html.twig', [
            'student' => $student,
            'uig' => $uig,
            'payments' => $payments,
        ]);
    }

    #[Route(path: '/student/delete/one/{student_id}', name: 'student_delete', methods: ['GET'])]
    public function deleteStudent(UserRepository $userRepo, $student_id): Response
    {
        $student = $userRepo->findOneById($student_id);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($student);
        $entityManager->flush();

        $this->addFlash(
            'success',
            $student->getFullname() . " has been deleted from the database.",
        );

        return $this->redirectToRoute('assign_access_roles', ['_fragment' => 'students-list']);
    }

    #[Route(path: '/students/uig/list', name: 'student-students-list')]
    public function studentsList(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');

        $students = $userRepo->findByUsertype('student');
        $uigs = $uigRepo->findUIGForStudent($this->getUser()->getStudentdata());
        return $this->render('student/studentslist.html.twig', [
            'controller_name' => 'StudentController',
            'students' => $students,
            'uigs' => $uigs,
        ]);
    }


}
