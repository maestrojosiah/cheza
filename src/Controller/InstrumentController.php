<?php

namespace App\Controller;

use App\Entity\Instrument;
use App\Form\InstrumentType;
use App\Repository\InstrumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/instrument')]
class InstrumentController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'instrument_index', methods: ['GET'])]
    public function index(InstrumentRepository $instrumentRepository): Response
    {
        $data = [];
        $instruments = $instrumentRepository->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );

        $courses = $instrumentRepository->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepository->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        return $this->render('instrument/index.html.twig', [
            'data' => $data,
        ]);
    }

    #[Route(path: '/new', name: 'instrument_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $instrument = new Instrument();
        $form = $this->createForm(InstrumentType::class, $instrument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();
            $category = $form->get('category')->getData();
            $instrument_title = $form->get('name')->getData();
            $trimmed_title = str_replace(" ", "_", (string) $instrument_title);
            $originalName = $image->getClientOriginalName();
            ;
            $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
            $image->move($filepath, $originalName);
            $simple_filepath = "/img/instruments/$category/$trimmed_title/";
            $instrument->setImage($simple_filepath . $originalName);

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($instrument);
            $entityManager->flush();

            return $this->redirectToRoute('instrument_index');
        }

        return $this->render('instrument/new.html.twig', [
            'instrument' => $instrument,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'instrument_show', methods: ['GET'])]
    public function show(Instrument $instrument): Response
    {
        return $this->render('instrument/show.html.twig', [
            'instrument' => $instrument,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'instrument_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Instrument $instrument): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(InstrumentType::class, $instrument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();
            $category = $form->get('category')->getData();
            $instrument_title = $form->get('name')->getData();
            $trimmed_title = str_replace(" ", "_", (string) $instrument_title);
            $originalName = $image->getClientOriginalName();
            ;
            $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
            $image->move($filepath, $originalName);
            $simple_filepath = "/img/instruments/$category/$trimmed_title/";
            $instrument->setImage($simple_filepath . $originalName);

            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('instrument_index');
        }

        return $this->render('instrument/edit.html.twig', [
            'instrument' => $instrument,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'instrument_delete', methods: ['DELETE'])]
    public function delete(Request $request, Instrument $instrument): Response
    {
        if ($this->isCsrfTokenValid('delete'.$instrument->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($instrument);
            $entityManager->flush();
        }

        return $this->redirectToRoute('instrument_index');
    }
}
