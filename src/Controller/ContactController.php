<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/contact')]
class ContactController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/list/for/group/{group_id}', name: 'contact_index', methods: ['GET'])]
    public function index(ContactRepository $contactRepository, GroupRepository $groupRepo, $group_id): Response
    {
        $group = $groupRepo->findOneById($group_id);
        $contacts = $contactRepository->findByContactgroup($group);
        return $this->render('contact/index.html.twig', [
            'contacts' => $contacts,
            'group' => $group,
        ]);
    }

    #[Route(path: '/new/{group_id}', name: 'contact_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GroupRepository $groupRepo, $group_id): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        $group = $groupRepo->findOneById($group_id);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('contact_index', ['group_id' => $group->getId()]);
        }

        return $this->render('contact/new.html.twig', [
            'contact' => $contact,
            'group' => $group,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'contact_show', methods: ['GET'])]
    public function show(Contact $contact): Response
    {
        return $this->render('contact/show.html.twig', [
            'contact' => $contact,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'contact_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Contact $contact): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        $group = $contact->getContactgroup();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('contact_index', ['group_id' => $group->getId()]);
        }

        return $this->render('contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'contact_delete', methods: ['DELETE'])]
    public function delete(Request $request, Contact $contact): Response
    {
        $group = $contact->getContactgroup();
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contact_index', ['group_id' => $group->getId()]);
    }
}
