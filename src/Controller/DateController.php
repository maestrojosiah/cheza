<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Payment;
use App\Form\SessionType;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use App\Repository\PackageRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\UserInstrumentGradeRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Mailer;
use App\Repository\InstrumentRepository;
use App\Repository\PaymentRepository;
use App\Repository\TermRepository;
use Twig\Environment;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/date')]
class DateController extends AbstractController
{
    public function __construct(private readonly InstrumentRepository $instrumentRepo, private readonly PackageRepository $packageRepo, private readonly SchoolTimeRepository $scheduleRepo, private readonly SessionRepository $sessionRepo, private readonly UserRepository $userRepo)
    {
    }

    public function save($myEntityClass): void
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($myEntityClass);
        $entityManager->flush();
    }

    #[Route(path: '/find/all/free/slots/{contact}', name: 'find_free_slots')]
    public function findFreeSlots(Request $request, $contact)
    {
        $weekday = $request->request->get('weekday');

        if($contact == 0) {
            $contact = "0720 962 288";
            $schedules = $this->findSchedulesFor($weekday);
        } else {
            $teacher = $this->userRepo->findOneById($contact);
            $contact = $teacher->getTeacherdata()->getPhone();
            $schedules = $this->findSchedulesForTeacher($weekday, $teacher);
        }
        $package_id = $request->request->get('package');
        $package = $this->packageRepo->findOneById($package_id);

        $instrument_id = $request->request->get('instrument');
        $instrument = $this->instrumentRepo->findOneById($instrument_id);

        $arr = [];

        $scheduleList = $this->iterateSchedules($schedules, $instrument);
        foreach ($scheduleList as $key => $slot) {
            if ($this->slotFitsPackage($slot, $package)) {
                $arr[$key] = $slot;
            } else {
                // $arr[] = $key . ' - ' . $slot . ' - doesnt fit ';
            }
        }
        $html = $this->createResultsHtml($arr, $package, $contact);
        // arr = 7200-02:15 pm-03:30 pm;
        return new JsonResponse($html);
    }

    #[Route(path: '/get/disabled/days', name: 'get_disabled_days')]
    public function getDisabledDaysString(Request $request)
    {

        $weekday = $request->request->get('weekday');

        $weekdayNumber = date('w', strtotime($weekday));
        $str = "";
        for ($i = 0; $i < 7; $i++) {
            if($i == $weekdayNumber) {
                // do nothing
            } else {
                $str .= $i;
                if($i < 6) {
                    $str .= ",";
                }
            }
        }

        return new JsonResponse($str);

    }

    public function sortDates($arr)
    {
        usort($arr, fn($a, $b) => strtotime((string) $a) - strtotime((string) $b));

        return $arr;
    }

    #[Route(path: '/check/if/email/exists', name: 'check_email')]
    public function checkIfEmailExists(Request $request)
    {

        $emailAddress = $request->request->get('emailAddress');
        $data = [];
        $user = $this->userRepo->findFirstByEmail($emailAddress);
        if($user) {
            $usertype = $user->getUsertype();
            $fullname = $user->getFullname();
            $data['fullname'] = $fullname;
            if($usertype == 'admin') {
                $phone = "";
            } elseif ($usertype == 'student') {
                $phone = $user->getStudentdata()->getPhone();
                $age = $user->getStudentdata()->getAge();
                $gender = $user->getStudentdata()->getSex();
            } elseif ($usertype == 'teacher') {
                $phone = $user->getTeacherdata()->getPhone();
                $age = $user->getTeacherdata()->getAge();
                $gender = $user->getTeacherdata()->getSex();
            }
            $data['phone'] = $phone;
            $data['age'] = $age;
            $data['gender'] = $gender;
            $data['id'] = $user->getId();
        } else {
            $data['phone'] = '';
        }

        return new JsonResponse($data);

    }

    public function findSchedulesFor($day)
    {
        $schedules = $this->scheduleRepo->findBy(
            ['day' => $day],
            ['title' => 'ASC']
        );
        return $schedules;
    }

    public function findSchedulesForTeacher($day, $teacher)
    {
        $schedules = $this->scheduleRepo->findBy(
            ['day' => $day, 'teacher' => $teacher],
            ['title' => 'ASC']
        );
        return $schedules;
    }

    public function iterateSchedules($schedules, $instrument)
    {
        $allSlots = [];

        foreach ($schedules as $key => $schedule) {
            $day = $schedule->getDay();
            $currentDate = new \Datetime("$day this week");

            $begin = $schedule->getBeginAt()->setDate($currentDate->format('Y'), $currentDate->format('m'), $currentDate->format('d'));
            $end = $schedule->getEndAt()->setDate($currentDate->format('Y'), $currentDate->format('m'), $currentDate->format('d'));

            $teacherTeachesInstrument = $this->teacherTeachesInstrument($schedule->getTeacher(), $instrument);

            if($teacherTeachesInstrument == true) {

                $sessions = $this->getLessonsWithinSchedule($begin, $end, $schedule->getTeacher());

                $slots = $this->getSlots($begin, $sessions, $end);

                $allSlots[$schedule->getTeacher()."-".$schedule->getId()."-".$schedule->getTeacher()->getId()] = $slots;

            }

        }
        return $this->sortSlotsByTime($allSlots);
    }

    public function sortSlotsByTime($slotArrays)
    {
        $sortedSlots = [];
        foreach ($slotArrays as $key => $slots) {
            foreach ($slots as $k => $slot) {
                $startingFrom = explode("-", (string) $slot)[1];
                $sortedSlots[strtotime($startingFrom).'-'.$key] = $slot;
            }
        }

        ksort($sortedSlots);
        return $sortedSlots;
    }

    public function slotFitsPackage($slot, $package)
    {
        $packageDuration = explode(' ', (string) $package->getDuration())[0] * 60; // in seconds
        if($slot >= $packageDuration) {
            return true;
        } else {
            return false;
        }
    }

    public function teacherTeachesInstrument($teacher, $instrument)
    {
        $uigs = $teacher->getTeacherdata()->getUserinstruments();
        $instrumentArray = [];
        foreach ($uigs as $key => $uig) {
            $instrumentArray[] = $uig->getInstrument()->getName();
        }
        if(in_array($instrument, $instrumentArray)) {
            return true;
        } else {
            return false;
        }

    }

    public function createResultsHtml($array, $package, $contact)
    {
        $count = count($array);
        if($count > 0) {

            $html = '<h5>Available Slots</h5>';
            $packageDuration = explode(' ', (string) $package->getDuration())[0] * 60;

            foreach ($array as $key => $value) {
                $teacher = $key;
                $teacherId = explode("-", (string) $teacher)[3];
                $teacher_name = explode("-", (string) $teacher)[1];
                $teacherConcatName = str_replace(" ", "", (string) $teacher);
                $numberOfSeconds = explode("-", (string) $value)[0];
                $startingFrom = explode("-", (string) $value)[1];
                $endingAt = explode("-", (string) $value)[2];
                $adjustedEnd = strtotime($endingAt) - $packageDuration;
                $endingAtAdj = date('H:i', $adjustedEnd);
                $hrsMin = gmdate("H:i", $numberOfSeconds);
                $options = $this->makeTimeSelectOptions($startingFrom, $endingAtAdj, $teacherConcatName);
                $html .=
                '<div class="col-sm-6 mb-2">' .
                    '<div class="card" style="border-color:#198754 !important">' .
                        '<div class="card-body" style="#198754 !important">' .
                            '<h5 class="card-title"><small>' . $teacher_name . ' : '  . $startingFrom . ' to ' . $endingAt . '</small></h5>' .
                            '<p class="card-text">' . 'Select starting time below</b> time below</p>' .
                            '<input type="hidden" id="teacher_'.$teacherConcatName.'" value="' .$teacherId. '">' .
                            $options .
                            '<button type="button" id="book_'.$teacherConcatName.'" class="btn mb-1 btn-primary">Select This</button>' .
                        '</div>' .
                    '</div>' .
                '</div>';
            }

            return $html;

        } else {
            return '<div class="alert alert-danger" style="color: #856404;background-color: #fff3cd;border-color: #ffeeba;" role="alert">
            Sorry, there are no slots available for the selected options. 
            <strong>Try changing the package to lesser time or select a different day of the week.</strong>
            <i>If you can\'t find a slot, call ' . $contact . ' for assistance </div>';
        }

    }

    public function makeTimeSelectOptions($start, $end, $teacher)
    {

        // $start = "00:00"; //you can write here 00:00:00 but not need to it
        // $end = "23:30";

        $tStart = strtotime((string) $start);
        $tEnd = strtotime((string) $end);
        $tNow = $tStart;
        $html = '<select name="'.$teacher.'" id="option_'.$teacher.'" class="form-select mb-2 mt-2" style="margin-right:5px" >';
        while($tNow <= $tEnd) {
            $html .= '<option value="'.date("H:i", $tNow).'">'.date("H:i", $tNow).'</option>';
            $tNow = strtotime('+30 minutes', $tNow);
        }
        $html .= '</select>';
        return $html;
    }

    public function getSlots($beginningOfSchedule, $sessions, $endOfSchedule)
    {
        $slots = [];
        // between beginning of schedule and beginning of first session
        // - check if instrument selected is taught by teacher before getting slots
        if(count($sessions) > 0) {
            // between end of last session and end of schedule
            $endOfLastSession = clone end($sessions)->getEndAt();
            $lastSlot = $this->getIntervalInSeconds(end($sessions)->getEndAt(), $endOfSchedule);
            $firstSlot = $this->getIntervalInSeconds($beginningOfSchedule, reset($sessions)->getBeginAt());
            // end of previous, remove first array element and get its end
            $firstSession = array_shift($sessions);
            $endOfPrevious = $firstSession->getEndAt();
            $beginningOfFirstSession = $firstSession->getBeginAt();
            $slots[] = $firstSlot.'-'.$beginningOfSchedule->format('h:i a').'-'.$beginningOfFirstSession->format('h:i a');
            // iterate sessions
            foreach ($sessions as $key => $session) {
                // between end of each session and beginning of next session
                $startOfThis = $session->getBeginAt();
                $nextSlot = $this->getIntervalInSeconds($endOfPrevious, $startOfThis);
                $slots[] = $nextSlot.'-'.$endOfPrevious->format('h:i a').'-'.$startOfThis->format('h:i a');
                $endOfPrevious = $session->getEndAt();
            }
            $slots[] = $lastSlot.'-'.$endOfLastSession->format('h:i a').'-'.$endOfSchedule->format('h:i a');
        // if no sessions, return all schedule
        } else {
            $allSchedule = $this->getIntervalInSeconds($beginningOfSchedule, $endOfSchedule);
            $slots[] = $allSchedule.'-'.$beginningOfSchedule->format('h:i a').'-'.$endOfSchedule->format('h:i a');
            // return false;
        }

        return $slots;

    }

    public function getIntervalInSeconds($startTime, $endTime)
    {
        $diff = $endTime->getTimestamp() - $startTime->getTimestamp();
        return $diff;
    }

    public function getLessonsWithinSchedule($scheduleStart, $scheduleEnd, $teacher)
    {
        // $sessions = $this->rawQuery("SELECT * from session WHERE begin_at BETWEEN :arg1 AND :arg2 ORDER BY begin_at ASC", $scheduleStart->format('Y-m-d H:i:s'),  $scheduleEnd->format('Y-m-d H:i:s'));
        $sessions = $this->sessionRepo->findSessionsBetween($scheduleStart, $scheduleEnd, $teacher);
        return $sessions;
    }

    public function rawQuery($query, $arg1, $arg2)
    {

        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = $query.";";

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('arg1', $arg1);
        $statement->bindValue('arg2', $arg2);
        $statement->execute();

        $result = $statement->fetchAll();
        return $result;
    }
}
