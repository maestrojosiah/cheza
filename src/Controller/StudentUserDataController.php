<?php

namespace App\Controller;

use App\Entity\StudentUserData;
use App\Form\StudentUserDataType;
use App\Repository\StudentUserDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/student/user/data')]
class StudentUserDataController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'student_user_data_index', methods: ['GET'])]
    public function index(StudentUserDataRepository $studentUserDataRepository): Response
    {
        return $this->render('student_user_data/index.html.twig', [
            'student_user_datas' => $studentUserDataRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'student_user_data_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $studentUserDatum = new StudentUserData();
        $form = $this->createForm(StudentUserDataType::class, $studentUserDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($studentUserDatum);
            $entityManager->flush();

            return $this->redirectToRoute('student_user_data_index');
        }

        return $this->render('student_user_data/new.html.twig', [
            'student_user_datum' => $studentUserDatum,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'student_user_data_show', methods: ['GET'])]
    public function show(StudentUserData $studentUserDatum): Response
    {
        return $this->render('student_user_data/show.html.twig', [
            'student_user_datum' => $studentUserDatum,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'student_user_data_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, StudentUserData $studentUserDatum): Response
    {
        $form = $this->createForm(StudentUserDataType::class, $studentUserDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('student_user_data_index');
        }

        return $this->render('student_user_data/edit.html.twig', [
            'student_user_datum' => $studentUserDatum,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'student_user_data_delete', methods: ['DELETE'])]
    public function delete(Request $request, StudentUserData $studentUserDatum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$studentUserDatum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($studentUserDatum);
            $entityManager->flush();
        }

        return $this->redirectToRoute('student_user_data_index');
    }
}
