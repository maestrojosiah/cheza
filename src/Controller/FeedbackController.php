<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Repository\FeedbackRepository;
use App\Repository\WorkshopRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Mailer;
use App\Service\SendSms;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\ShortenUrl;

#[Route(path: '/feedback')]
class FeedbackController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'feedback_index', methods: ['GET'])]
    public function index(FeedbackRepository $feedbackRepository): Response
    {
        return $this->render('feedback/index.html.twig', [
            'feedback' => $feedbackRepository->findAll(),
        ]);
    }

    #[Route(path: '/new/{contact_id}', name: 'feedback_new', methods: ['GET', 'POST'])]
    public function new(Request $request, WorkshopRepository $contactRepo, Mailer $mailer, SendSms $sendSms, $contact_id): Response
    {
        $c = explode("^", (string) $contact_id)[1];
        $contact = $contactRepo->findOneById($c);
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $feedback->setContact($contact);
            $entityManager->persist($feedback);
            $entityManager->flush();

            $phonenumber = $this->fullNumber($contact->getPhone());

            $maildata = ['f_name' => $contact->getFname(), 'document' => $contact->getWshop()->getDocument()];
            $mailer->sendEmail($maildata, $contact->getEmail(), "Thank you for your feedback", "thankyou.html.twig", "necessary", "personal_email_from_office");

            $msg = "Hi " . $contact->getFname() . ". Thank you for your feedback. We appreciate that you spared a moment for us.";
            $sendSms->quickSend($phonenumber, $contact->getFname(), "Thank you for your feedback", $msg, 'necessary', 'personal_sms_from_office', 'nomail');

            return $this->redirectToRoute('thankyou', ['msg' => "Your feedback is important to us. Download your document below. <br /><br /><a class='btn btn-success' href='https://chezamusicschool.co.ke/" . $contact->getWshop()->getDocument() . "'>Download " . explode('/', (string) $contact->getWshop()->getDocument())[1] . "</a> "]);

        }

        return $this->render('feedback/new.html.twig', [
            'contact' => $contact,
            'feedback' => $feedback,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'feedback_show', methods: ['GET'])]
    public function show(Feedback $feedback): Response
    {
        return $this->render('feedback/show.html.twig', [
            'feedback' => $feedback,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'feedback_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Feedback $feedback): Response
    {
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('feedback_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('feedback/edit.html.twig', [
            'feedback' => $feedback,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'feedback_delete', methods: ['POST'])]
    public function delete(Request $request, Feedback $feedback): Response
    {
        if ($this->isCsrfTokenValid('delete'.$feedback->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($feedback);
            $entityManager->flush();
        }

        return $this->redirectToRoute('feedback_index', [], Response::HTTP_SEE_OTHER);
    }

    public function fullNumber($num)
    {
        $number = "";
        if(strlen((string) $num) == 10) {
            $number = preg_replace('/^0/', '+254', (string) $num);
        } elseif(strlen((string) $num) == 13) {
            $number = $num;
        } else {
            $number = $num;
        }
        return $number;
    }

    #[Route(path: '/admin/send/feedback/prompt', name: 'send_feedback_prompt')]
    public function sendFeedbackPrompt(
        WorkshopRepository $contactRepo,
        Request $request,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl
    ): Response {

        $contact_id = $request->request->get('contact_id');
        $contact = $contactRepo->findOneById($contact_id);
        $phonenumber = $this->fullNumber($contact->getPhone());

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/feedback/new/'.$contact->getEmail().'^', $contact->getId(), 'Leave a feedback');
        $message = "Thank you for attending our workshop. We would like to know how we did. Kindly leave us a quick feedback using this link: " . $urlShort;

        $sendSms->quickSend($phonenumber, $contact->getFname(), 'Leave a feedback', $message, 'necessary', 'personal_sms_from_office', 'nomail');

        $mailer->sendEmail(['name' => $contact->getFname(), 'message' => $message, 'additional_info' => 'Your certificate is ready for collection.'], $contact->getEmail(), $contact->getFname().' - Your Workshop Feedback', 'general.html.twig', 'necessary', 'personal_email_from_office');
        // send text with short url to pdf of invoice
        return new JsonResponse($contact->getFname());

    }


}
