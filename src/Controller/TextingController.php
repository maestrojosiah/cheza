<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\AttachmentRepository;
use App\Repository\GroupRepository;
use App\Entity\Communication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SendSms;
use App\Service\Mailer;

class TextingController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/communication/email/texting/send', name: 'communicate')]
    public function index(SendSms $sendSms, Request $request, Mailer $mailer, UserRepository $userRepo, AttachmentRepository $attachRepo)
    {
        $isSent = null;
        $tosendsms = $request->request->get('tosendsms');
        $tosendemail = $request->request->get('tosendemail');
        $sent_to = $request->request->get('sent_to');
        $subject = $request->request->get('subject');
        $msg = $request->request->get('msg');
        $phone_to = $request->request->get('phone_to');
        $sent_to_contact = "";
        $sentStatus = "";
        $deliveredStatus = "N/A";
        $error = "";
        $type = "";
        $res = [];
        $reportres = [];
        $messageId = "";
        // instantiate communication
        $communication = new Communication();
        // time now
        $time_now = new \Datetime();
        // set sent at
        $communication->setSentAt($time_now);

        $resp = "";
        $data = "";

        $emailAddress = empty($request->request->get('mail_to')) ? 'nomail' : $request->request->get('mail_to');


        if(null != $request->request->get('phone_to') && $tosendsms == "true") {

            $isSent = $sendSms->quickSend($phone_to, $sent_to, $subject, $msg, 'communication', 'personal_sms_from_office', $emailAddress);

        }

        if(null != $request->request->get('mail_to') && $tosendemail == "true") {

            $mail_to = $request->request->get('mail_to');
            $attachment = $request->request->get('attachment');
            $msg = $request->request->get('msg');
            $subject = $request->request->get('subject');
            $user = $userRepo->findOneByEmail($mail_to);
            $maildata = ['msg' => $msg, 'user' => $user];
            if($attachment == "") {
                if($mailer->sendEmailMessage($maildata, $mail_to, $subject, "communication.html.twig", "communication", "personal_email_from_office")) {
                    $sentStatus .= ' EMAIL_SENT';
                } else {
                    $sentStatus .= ' EMAIL_NOT_SENT';
                }
            } else {
                $attch = $attachRepo->findOneById($attachment);
                if($mailer->sendEmailWithLinkedAttachment($maildata, $mail_to, $subject, "communication.html.twig", "communication", "personal_email_from_office", $this->getParameter('attachment_files').$attch->getFile(), $attch->getFilename())) {
                    $sentStatus .= ' EMAIL_SeNT';
                } else {
                    $sentStatus .= ' EMAIL_NOT_SeNT';
                }
            }
            $resp .= ", $sentStatus";

            if($sent_to == "") {
                $sent_to = $mail_to;
            }
            $type .= "MAIL";
            $sent_to_contact .= " " . $mail_to . " ";
        }

        $communication->setSentTo($sent_to);
        $communication->setSendToContact($sent_to_contact);
        $communication->setMessage($msg);
        $communication->setType($type);
        $communication->setSubject($subject);
        $communication->setSentStatus($sentStatus);
        $communication->setDeliveredStatus($deliveredStatus);
        $communication->setMessageId($messageId);
        $communication->setOpenedStatus('N/A');
        $communication->setDeliveredAt(new \Datetime('1970/01/01'));
        $communication->setAdmin($this->getUser());

        $this->save($communication);

        return new JsonResponse($isSent);
    }

    #[Route(path: '/communication/quick/message/send', name: 'quickmessage')]
    public function quickSend(SendSms $sendSms, Request $request)
    {
        $name = $request->request->get('name');
        $phone_to = $request->request->get('phone_to');
        $subject = $request->request->get('subject');
        $msg = $request->request->get('msg');

        $sendSms->quickSend($phone_to, $name, $subject, $msg, 'necessary', 'personal_sms_from_office', 'nomail');

        return new JsonResponse($msg);
    }

    #[Route(path: '/communication/bulk/message/send/{group_id}', name: 'bulkmessage')]
    public function bulkSend(GroupRepository $groupRepo, SendSms $sendSms, Request $request, $group_id)
    {
        $group = $groupRepo->findOneById($group_id);
        $contacts = $group->getContacts();
        $contactsStr = "";
        foreach ($contacts as $c) {
            $contactsStr .= $c->getNumber().",";
        }
        $name = $group->getTitle();
        $phone_to = substr($contactsStr, 0, -1);
        $subject = "Bulk message to " . $name;
        $msg = $request->request->get('msg');

        $sendSms->quickSend($phone_to, $name, $subject, $msg, 'necessary', 'personal_sms_from_office', 'nomail');

        return new JsonResponse($msg);
    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    #[Route(path: '/communication/get/message/log/{para}/{id}', name: 'getlogs')]
    public function getLogs(SendSms $sendSms, $para, $id)
    {

        $reportres = [];
        $today = date("Y-m-d");
        $dataL = "userid=Muske2022&password=Eirwmr-33@hp&uuid=$id&fromdate=$today&todate=$today&output=json";

        try {
            $reportresponse = $sendSms->getLogs($id, $today, $today);
            // $reportresponse = "{\"response\":{\"api\":\"send\",\"action\":\"status\",\"status\":\"success\",\"msg\":\"success\",\"code\":\"200\",\"count\":4,\"report_statusList\":[{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"jlZQFnKswD8lC0P\",\"mobileNo\":\"254711476249\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198800131\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"j4vR94BlyvZIzDi\",\"mobileNo\":\"254705285959\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198767404\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"gpCsviHLlhnn8oO\",\"mobileNo\":\"254736600033\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"11\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"0\",\"Status\":\"SUBMITTED\",\"Cause\":\"SubmittedtoOperator\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"InProcess\"}},{\"status\":{\"uuId\":\"1171387500624170825\",\"msgId\":\"y3LcaiJFxG4G0zK\",\"mobileNo\":\"254711832933\",\"senderName\":\"MUSKE-MUSIC\",\"text\":\"Testingthemessagingsystem.Ignore\",\"msgType\":\"text\",\"length\":\"36\",\"cost\":\"1\",\"globalErrorCode\":\"0\",\"submitTime\":\"1646198756563\",\"deliveryTime\":\"1646198800442\",\"Status\":\"DELIVERED\",\"Cause\":\"Delivered\",\"ChannelName\":\"API\",\"SubmittedTime\":\"March2,20228:25AM\",\"DeliveredTime\":\"March2,20228:26AM\"}}]}}";
            $reportres[] = $reportresponse;
            $reportres = $sendSms->jsonToAssocArray($reportresponse);
            // $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
            // $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');
        } catch (HttpException $ex) {
            echo $ex;
        }

        $table = $sendSms->arrayToTable($reportres['response']['report_statusList']);
        return new JsonResponse($table);

    }



}
