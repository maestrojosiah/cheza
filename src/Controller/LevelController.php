<?php

namespace App\Controller;

use App\Entity\Level;
use App\Form\LevelType;
use App\Repository\LevelRepository;
use App\Repository\MyKeyRepository;
use App\Repository\InstrumentRepository;
use App\Repository\ExerciseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/level')]
class LevelController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/for/{instrument}/{exercise}/{key}', name: 'level_index', methods: ['GET'])]
    public function index(
        LevelRepository $levelRepository,
        MyKeyRepository $myKeyRepo,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        $instrument = null,
        $exercise = null,
        $key = null
    ): Response {
        $key = $myKeyRepo->findOneById($key);
        $instrument = $instrumentRepo->findOneById($instrument);
        $exercise = $exerciseRepo->findOneById($exercise);

        return $this->redirectToRoute('level_index_updated', [
            'instrument' => $instrument->getName(),
            'exercise' => $exercise->getType(),
            'key' => $key->getName()
        ], 301);

    }

    #[Route(path: '/show/{instrument}/{exercise}/{key}', name: 'level_index_updated', methods: ['GET'])]
    public function indexWithText(
        LevelRepository $levelRepository,
        MyKeyRepository $myKeyRepo,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        $instrument = null,
        $exercise = null,
        $key = null
    ): Response {
        $key = $myKeyRepo->findOneByName($key);
        $instrument = $instrumentRepo->findOneByName($instrument);
        $exercise = $exerciseRepo->findOneByType($exercise);

        return $this->render('level/index.html.twig', [
            'levels' => $levelRepository->findAll(),
            'key' => $key,
            'instrument' => $instrument,
            'exercise' => $exercise,
        ]);
    }

    #[Route(path: '/list', name: 'level_list', methods: ['GET'])]
    public function list(LevelRepository $levelRepository): Response
    {

        return $this->render('level/list.html.twig', [
            'levels' => $levelRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'level_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $level = new Level();
        $form = $this->createForm(LevelType::class, $level);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($level);
            $entityManager->flush();

            return $this->redirectToRoute('level_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('level/new.html.twig', [
            'level' => $level,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'level_show', methods: ['GET'])]
    public function show(Level $level): Response
    {
        return $this->render('level/show.html.twig', [
            'level' => $level,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'level_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Level $level): Response
    {
        $form = $this->createForm(LevelType::class, $level);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('level_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('level/edit.html.twig', [
            'level' => $level,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'level_delete', methods: ['POST'])]
    public function delete(Request $request, Level $level): Response
    {
        if ($this->isCsrfTokenValid('delete'.$level->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($level);
            $entityManager->flush();
        }

        return $this->redirectToRoute('level_list', [], Response::HTTP_SEE_OTHER);
    }
}
