<?php

namespace App\Controller;

use App\Service\MusicApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MusicAssessmentController extends AbstractController
{
    private $musicApiService;

    public function __construct(MusicApiService $musicApiService)
    {
        $this->musicApiService = $musicApiService;
    }

    #[Route('/submit-assessment', name: 'submit_assessment', methods: ['POST'])]
    public function submitAssessment(): JsonResponse
    {
        $data = [
            'student' => 'RB11123',
            'term' => 1,
            'year' => 2024,
            'payload' => [
                [
                    'category' => 1,
                    'description' => 'This report assesses Nathan\'s progress in learning the Piano...',
                ],
                // Additional payload items as needed
            ],
        ];

        try {
            $response = $this->musicApiService->submitAssessment($data);
            return new JsonResponse($response);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Failed to submit assessment'], 500);
        }
    }
}
