<?php

namespace App\Controller;

use App\Entity\SimpleReport;
use App\Form\SimpleReportType;
use App\Repository\SimpleReportRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/simple/report')]
class SimpleReportController extends AbstractController
{
    #[Route('/for/{term}/{year}/{user}', name: 'app_simple_report_index', methods: ['GET'])]
    public function index(SimpleReportRepository $simpleReportRepository, UserRepository $userRepo, $term, $year, $user): Response
    {
        $user = $userRepo->findOneById($user);
        $simpleReports = $simpleReportRepository->findBy(
            ['term' => $term, 'year' => $year, 'student' => $user],
            ['id' => 'DESC'],
        );
        return $this->render('simple_report/index.html.twig', [
            'simple_reports' => $simpleReports,
            'student' => $user,
        ]);
    }

    #[Route('/list/for/{term}/{year}/{user}', name: 'app_simple_report_list', methods: ['GET'])]
    public function list(SimpleReportRepository $simpleReportRepository, UserRepository $userRepo, $term, $year, $user): Response
    {
        $user = $userRepo->findOneById($user);
        $simpleReports = $simpleReportRepository->findBy(
            ['term' => $term, 'year' => $year, 'student' => $user],
            ['id' => 'DESC'],
        );
        return $this->render('simple_report/list.html.twig', [
            'simple_reports' => $simpleReports,
            'student' => $user,
        ]);
    }

    #[Route('/new', name: 'app_simple_report_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SimpleReportRepository $simpleReportRepository, UserRepository $userRepo): Response
    {
        $simpleReport = new SimpleReport();
        $form = $this->createForm(SimpleReportType::class, $simpleReport);
        $form->handleRequest($request);

        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['name'])) {
            $name = $_GET['name'];
        } else {
            $name = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['t'])) {
            $teacher = $_GET['t'];
        } else {
            $teacher = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['u'])) {
            $student = $_GET['u'];
        } else {
            $student = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['instrument'])) {
            $instrument = $_GET['instrument'];
        } else {
            $instrument = "";
        }

        $studentUser = $userRepo->findOneById($student);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $simpleReportRepository->save($simpleReport, true);

        //     return $this->redirectToRoute('app_simple_report_index', [], Response::HTTP_SEE_OTHER);
        // }

        return $this->render('simple_report/new.html.twig', [
            'simple_report' => $simpleReport,
            'teacher_id' => $teacher,
            'student_id' => $student,
            'student' => $studentUser,
            'name' => $name,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_simple_report_show', methods: ['GET'])]
    public function show(SimpleReport $simpleReport): Response
    {
        return $this->render('simple_report/show.html.twig', [
            'simple_report' => $simpleReport,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_simple_report_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SimpleReport $simpleReport, SimpleReportRepository $simpleReportRepository): Response
    {
        $form = $this->createForm(SimpleReportType::class, $simpleReport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $simpleReportRepository->save($simpleReport, true);

            return $this->redirectToRoute('app_simple_report_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('simple_report/edit.html.twig', [
            'simple_report' => $simpleReport,
            'form' => $form,
            'teacher' => $simpleReport->getTeacher(),
            'student' => $simpleReport->getStudent(),
            'name' => $simpleReport->getStudent()->getFullname(),
        ]);
    }

    #[Route('/{id}', name: 'app_simple_report_delete', methods: ['POST'])]
    public function delete(Request $request, SimpleReport $simpleReport, SimpleReportRepository $simpleReportRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$simpleReport->getId(), $request->request->get('_token'))) {
            $simpleReportRepository->remove($simpleReport, true);
        }

        return $this->redirectToRoute('app_simple_report_index', ['term' => $simpleReport->getTerm(), 'year' => $simpleReport->getYear(), 'user' => $simpleReport->getStudent()->getId()], Response::HTTP_SEE_OTHER);
    }
}
