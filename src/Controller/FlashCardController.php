<?php

namespace App\Controller;

use App\Entity\FlashCard;
use App\Form\FlashCardType;
use App\Repository\FlashCardRepository;
use App\Repository\InstrumentRepository;
use App\Repository\ExerciseRepository;
use App\Repository\MyKeyRepository;
use App\Repository\LevelRepository;
use App\Repository\SettingsRepository;
use App\Repository\UserSettingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/flash/card')]
class FlashCardController extends AbstractController
{
    public function __construct(private readonly Pdf $pdf, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }

    #[Route(path: '/start', name: 'flash_card_start', methods: ['GET'])]
    public function start(
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo
    ): Response {
        $instruments = [];
        $exercises = $exerciseRepo->findAll();
        foreach ($exercises as $key => $exercise) {
            if(count($exercise->getFlashCards()) > 0) {
                $instruments[] = $exercise->getInstrument();
            }
        }

        return $this->render('flash_card/start.html.twig', [
            'instruments' => array_unique($instruments),
        ]);
    }

    #[Route(path: '/list', name: 'flash_card_list', methods: ['GET'])]
    public function list(FlashCardRepository $flashCardRepository, Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $limit = 50;
    
        $paginator = $flashCardRepository->findPaginated($page, $limit);
    
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $limit);
    
        // Determine the range of pages to display
        $maxPagesToShow = 10;
        $startPage = max(1, $page - intval($maxPagesToShow / 2));
        $endPage = min($pagesCount, $page + intval($maxPagesToShow / 2));
        
        if ($endPage - $startPage < $maxPagesToShow) {
            if ($startPage == 1) {
                $endPage = min($startPage + $maxPagesToShow - 1, $pagesCount);
            } else {
                $startPage = max(1, $endPage - $maxPagesToShow + 1);
            }
        }
    
        return $this->render('flash_card/list.html.twig', [
            'flash_cards' => $paginator,
            'currentPage' => $page,
            'pagesCount' => $pagesCount,
            'startPage' => $startPage,
            'endPage' => $endPage,
        ]);
    }
    
    // #[Route(path: '/list', name: 'flash_card_list', methods: ['GET'])]
    // public function list(FlashCardRepository $flashCardRepository): Response
    // {

    //     return $this->render('flash_card/list.html.twig', [
    //         // 'flash_cards' => $flashCardRepository->findLast30(),
    //         'flash_cards' => $flashCardRepository->findAll(),
    //     ]);
    // }

    #[Route(path: '/save/card/description', name: 'save_card_description')]
    public function saveCard(Request $request, FlashCardRepository $fcRepo): Response
    {

        $msg = "msg";
        $fc_id = $request->request->get('fc_id');
        $description = $request->request->get('description');

        $flashcard = $fcRepo->findOneById($fc_id);

        $flashcard->setDescription($description);

        $this->save($flashcard);
        $msg = "success";

        return new JsonResponse($msg);

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


    #[Route(path: '/images/{instrument}/{exercise}/{key}/{level}', name: 'flashcardimages', methods: ['GET'])]
    public function images(
        FlashCardRepository $flashCardRepository,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        MyKeyRepository $myKeyRepo,
        LevelRepository $levelRepo,
        $instrument,
        $exercise,
        $key,
        $level
    ): Response {

        $instrument = $instrumentRepo->findOneById($instrument);
        $exercise = $exerciseRepo->findOneById($exercise);
        $key = $myKeyRepo->findOneById($key);
        $level = $levelRepo->findOneById($level);

        return $this->redirectToRoute('flashcardimages_updated', [
            'instrument' => $instrument->getName(),
            'exercise' => $exercise->getType(),
            'key' => $key->getName(),
            'level' => $level->getName()
        ], 301);

    }

    #[Route(path: '/download/{instrument}/{exercise}/{key}/{level}', name: 'flashcardimages_updated', methods: ['GET'])]
    public function imagesWithText(
        FlashCardRepository $flashCardRepository,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        MyKeyRepository $myKeyRepo,
        LevelRepository $levelRepo,
        $instrument,
        $exercise,
        $key,
        $level
    ): Response {

        $instrument = $instrumentRepo->findOneByName($instrument);
        $exercise = $exerciseRepo->findOneByType($exercise);
        $key = $myKeyRepo->findOneByName($key);
        $level = $levelRepo->findOneByName($level);

        $filename = str_replace(" ", "", $instrument->getName() . $exercise->getType() . $key->getName() . $level->getName());
        $cards = $flashCardRepository->findBy(
            ['exercise' => $exercise, 'mykey' => $key, 'level' => $level],
            ['id' => 'ASC']
        );

        $arr = [
            'cards' => $cards,
            'instrument' => $instrument,
            'exercise' => $exercise,
            'key' => $key,
            'level' => $level,
        ];

        // return $this->render('flash_card/images.html.twig', [
        // ]);
        $this->toPdf($filename, "flash_card/images.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;

    }


    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . '.pdf');

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html, ['footer-html' => $footer]), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        //     )
        // );

    }

    #[Route(path: '/for/{instrument}/{exercise}/{key}/{level}', name: 'flash_card_index', methods: ['GET'])]
    public function index(
        FlashCardRepository $flashCardRepository,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        MyKeyRepository $myKeyRepo,
        LevelRepository $levelRepo,
        SettingsRepository $settingsRepo,
        UserSettingsRepository $uSetRepo,
        $instrument,
        $exercise,
        $key,
        $level
    ): Response {
        $instrument = $instrumentRepo->findOneById($instrument);
        $exercise = $exerciseRepo->findOneById($exercise);
        $key = $myKeyRepo->findOneById($key);
        $level = $levelRepo->findOneById($level);

        return $this->redirectToRoute('flash_card_index_updated', [
            'instrument' => $instrument->getName(),
            'exercise' => $exercise->getType(),
            'key' => $key->getName(),
            'level' => $level->getName()
        ], 301);
        
    }

    #[Route(path: '/show/{instrument}/{exercise}/{key}/{level}', name: 'flash_card_index_updated', methods: ['GET'])]
    public function indexWithText(
        FlashCardRepository $flashCardRepository,
        InstrumentRepository $instrumentRepo,
        ExerciseRepository $exerciseRepo,
        MyKeyRepository $myKeyRepo,
        LevelRepository $levelRepo,
        SettingsRepository $settingsRepo,
        UserSettingsRepository $uSetRepo,
        $instrument,
        $exercise,
        $key,
        $level
    ): Response {
        $instrument = $instrumentRepo->findOneByName($instrument);
        $exercise = $exerciseRepo->findOneByType($exercise);
        $key = $myKeyRepo->findOneByName($key);
        $level = $levelRepo->findOneByName($level);
        $flash_cards = $flashCardRepository->findBy(
            ['exercise' => $exercise, 'mykey' => $key, 'level' => $level],
            ['id' => 'ASC']
        );
        $settings = null;
        $settingArr = null;
        if(null !== $this->getUser()) {
            $settings = $settingsRepo->findBy(
                ['category' => 'cards'],
                ['id' => 'asc']
            );
            foreach ($settings as $setting) {
                $thisSetting = $uSetRepo->findOneBySetting($setting);
                if(null == $thisSetting) {
                    $thisSetting = $setting->getDefaultval();
                } else {
                    $thisSetting = $thisSetting->getValue();
                }
                $thisSetting = $thisSetting == 'on' ? 'true' : $thisSetting;
                $thisSetting = $thisSetting == 'off' ? 'false' : $thisSetting;
                $settingArr[$setting->getDescription()] = $thisSetting;
            }
        }
        return $this->render('flash_card/index.html.twig', [
            'flash_cards' => $flash_cards,
            'instrument' => $instrument,
            'exercise' => $exercise,
            'settings' => $settings,
            'settingsArr' => $settingArr,
            'key' => $key,
            'level' => $level,
        ]);
    }

    #[Route(path: '/new', name: 'flash_card_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SluggerInterface $slugger, MyKeyRepository $keyRepo): Response
    {
        $flashCard = new FlashCard();
        $form = $this->createForm(FlashCardType::class, $flashCard);
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {

            /** @var UploadedFile $cardImage */
            $cardImages = $form->get('image')->getData();

            // this condition is needed because the 'flash_cards' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($cardImages) {
                foreach ($cardImages as $cardImage) {
                    $keyNMode = $keyRepo->findOneBy(
                        ['name' => $form->get('mykey')->getData()->getName(), 'mode' => $form->get('mode')->getData()],
                        ['id' => 'DESC']
                    );
                    $flashCard = new FlashCard();
                    $flashCard->setExercise($form->get('exercise')->getData());
                    $flashCard->setMykey($keyNMode);
                    $flashCard->setMode($form->get('mode')->getData());
                    $flashCard->setLevel($form->get('level')->getData());
                    $flashCard->setDescription($form->get('description')->getData());
                    // echo "<pre>";
                    // var_dump($cardImage);
                    $originalFilename = pathinfo((string) $cardImage->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$cardImage->guessExtension();

                    // Move the file to the directory where flash_cards are stored
                    try {
                        $cardImage->move(
                            $this->getParameter('flash_cards_folder'),
                            $newFilename
                        );
                    } catch (FileException) {
                        // ... handle exception if something happens during file upload
                    }

                    // updates the 'cardImagename' property to store the PDF file name
                    // instead of its contents
                    $flashCard->setImage($newFilename);

                    $entityManager = $this->managerRegistry->getManager();
                    $entityManager->persist($flashCard);
                    $entityManager->flush();

                }
            }

            return $this->redirectToRoute('flash_card_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('flash_card/new.html.twig', [
            'flash_card' => $flashCard,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'flash_card_show', methods: ['GET'])]
    public function show(FlashCard $flashCard): Response
    {
        return $this->render('flash_card/show.html.twig', [
            'flash_card' => $flashCard,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'flash_card_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, FlashCard $flashCard): Response
    {
        $form = $this->createForm(FlashCardType::class, $flashCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('flash_card_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('flash_card/edit.html.twig', [
            'flash_card' => $flashCard,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'flash_card_delete', methods: ['POST'])]
    public function delete(Request $request, FlashCard $flashCard): Response
    {
        if ($this->isCsrfTokenValid('delete'.$flashCard->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($flashCard);
            $entityManager->flush();
        }

        return $this->redirectToRoute('flash_card_list', [], Response::HTTP_SEE_OTHER);
    }
}
