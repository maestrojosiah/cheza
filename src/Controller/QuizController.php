<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Form\QuizType;
use App\Repository\ChoiceRepository;
use App\Repository\LessonRepository;
use App\Repository\QuizRepository;
use App\Repository\TestRepository;
use App\Service\Mailer;
use App\Service\SendSms;
use App\Service\ShortenUrl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;
use Twig\Environment;

#[Route(path: '/quiz')]
class QuizController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[IsGranted("ROLE_ADMIN")]
    #[Route(path: '/', name: 'quiz_index', methods: ['GET'])]
    public function index(Request $request, QuizRepository $quizRepository): Response
    {
        $lessonId = $request->query->get('lesson');
        
        if ($lessonId) {
            $quizzes = $quizRepository->findBy(['lesson' => $lessonId]);
        } else {
            $quizzes = $quizRepository->findAll();
        }
    
        return $this->render('quiz/index.html.twig', [
            'quizzes' => $quizzes,
        ]);
    }
    

    #[Route(path: '/for/lesson/{lesson_id}', name: 'quiz_pub_index', methods: ['GET'])]
    public function public_quiz(QuizRepository $quizRepository, LessonRepository $lessonRepo, $lesson_id): RedirectResponse
    {
        $lesson = $lessonRepo->findOneById($lesson_id);
        $slug = $lesson->getSlug();
        return $this->redirectToRoute('public_quiz_page', ['grade' => $lesson->getGrade(), 'slug' => $slug], 301);
    }

    #[Route(path: '/level/{grade}/{slug}', name: 'public_quiz_page', methods: ['GET'])]
    public function public_quiz_new(QuizRepository $quizRepository, LessonRepository $lessonRepo, $slug, $grade): Response
    {
        $lesson = $lessonRepo->FindBy(
            ['slug' => $slug, 'grade' => $grade], 
            ['id' => 'DESC']
        );
        $quizzes = $quizRepository->findByLesson($lesson);
        date_default_timezone_set('Africa/Nairobi');
        $timer = new \Datetime();
        $timer = $timer->modify("+ 1 hour");

        return $this->render('quiz/public_show.html.twig', [
            'lesson' => $lesson[0],
            'timer' => $timer,
            'grade' => $grade,
            'quizzes' => $quizzes,
        ]);
    }

    #[Route(path: '/form/processor', name: 'quiz_form', methods: ['POST'])]
    public function quiz_form(QuizRepository $quizRepository, TestRepository $testRepo, LessonRepository $lessonRepo, ChoiceRepository $choiceRepo, SendSms $sendSms, Mailer $mailer, ShortenUrl $shortenUrl, Environment $twig): Response
    {

        $lesson = $lessonRepo->findOneById($_POST['lesson']);
        $fullname = $_POST['fullname'];
        $email = $_POST['email'];

        $test = $testRepo->findOneById($_POST['test']);
        $test->setComplete(true);
        $this->save($test);

        $quizzes = [];
        $choices = [];
        $correctAnswers = 0;
        $correctAnswer = [];
        foreach ($_POST as $key => $value) {

            if(is_numeric($key)) {
                $quiz_id = $key;
                $choice_id = $value;
                $quiz = $quizRepository->findOneById($quiz_id);
                $choice = $choiceRepo->findOneById($choice_id);
                $quizzes[$quiz_id] = $quiz;
                $choices[$quiz_id] = $choice;
                // if choice is correct, add to correct answers
                if(null != $choice && $choice->isIsCorrect() == 1) {
                    $correctAnswers++;
                }
                $allChoicesForThisQuiz = $quiz->getChoices();
                foreach ($allChoicesForThisQuiz as $ch) {
                    if($ch->isIsCorrect() == true) {
                        $correctAnswer[$quiz_id] = $ch;
                    }
                }

            }
        }

        $numberOfQuizzes = count($quizzes);
        $percentage = round(($correctAnswers / $numberOfQuizzes) * 100);

        $html = $twig->render('pdf/results.html.twig', [
            'percentage' => $percentage,
            'quizzes' => $quizzes,
            'lesson' => $lesson,
            'fullname' => $fullname,
            'choices' => $choices,
            'correctAnswer' => $correctAnswer,
        ]);

        // var_dump([$email, $fullname, $phone]);
        // die();

        $mailer->sendEmailWithAttachmentToNonUser(['fullname' => $fullname, 'percentage' => $percentage], $email, $fullname.'s Score in '.$lesson->getName(), $html, 'results.html.twig', $email . '-score', 'necessary', 'personal_email_from_office');
        $mailer->sendEmailWithAttachmentToNonUser(['fullname' => $fullname, 'percentage' => $percentage], 'chezamusicschool@gmail.com', $fullname.'s Score in '.$lesson->getName(), $html, 'results.html.twig', $email . '-score', 'necessary', 'personal_email_from_office');
        // $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/invoice/', $payment->getId(), 'Download Invoice');
        // $sendSms->quickSend($phone, $fullname, 'Test Complete', "You have scored $percentage. Check your email for your score details", 'necessary', 'personal_sms_from_office', 'nomail');
        // $sendSms->quickSend('0705285959', "Admin", 'Test Complete', "$fullname has scored $percentage. Check your email for the score details", 'necessary', 'personal_sms_from_office', 'nomail');
        $this->addFlash(
            'success',
            'The invoice was sent successfully.'
        );

        // foreach ($scores as $key => $score) {
        //     # code...
        // }

        return $this->render('quiz/results.html.twig', [
            'percentage' => $percentage,
            'quizzes' => $quizzes,
            'lesson' => $lesson,
            'fullname' => $fullname,
            'choices' => $choices,
            'correctAnswer' => $correctAnswer,
        ]);

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route(path: '/new', name: 'quiz_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $quiz = new Quiz();
        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('image')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('quiz_files'),
                        $newFilename
                    );
                } catch (FileException) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $quiz->setImage($newFilename);
            }

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($quiz);
            $entityManager->flush();

            return $this->redirectToRoute('quiz_index', ['lesson' => $quiz->getLesson()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('quiz/new.html.twig', [
            'quiz' => $quiz,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route(path: '/{id}', name: 'quiz_show', methods: ['GET'])]
    public function show(Quiz $quiz): Response
    {
        return $this->render('quiz/show.html.twig', [
            'quiz' => $quiz,
        ]);
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route(path: '/{id}/edit', name: 'quiz_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Quiz $quiz, SluggerInterface $slugger): Response
    {
        $lessonId = $request->query->get('lesson');
        
        if ($lessonId) {
            // do nothing
        } else {
            $lessonId = $quiz->getLesson()->getId();
        }

        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('image')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('quiz_files'),
                        $newFilename
                    );
                } catch (FileException) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $quiz->setImage($newFilename);
            }

            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('quiz_index', ['lesson' => $lessonId], Response::HTTP_SEE_OTHER);
        }

        return $this->render('quiz/edit.html.twig', [
            'quiz' => $quiz,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route(path: '/{id}', name: 'quiz_delete', methods: ['POST'])]
    public function delete(Request $request, Quiz $quiz): Response
    {
        $lessonId = $request->query->get('lesson');
        if ($lessonId) {
            // do nothing
        } else {
            $lessonId = $quiz->getLesson()->getId();
        }

        if ($this->isCsrfTokenValid('delete'.$quiz->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($quiz);
            $entityManager->flush();
        }

        return $this->redirectToRoute('quiz_index', ['lesson' => $quiz->getLesson()->getId()], Response::HTTP_SEE_OTHER);
    }
}
