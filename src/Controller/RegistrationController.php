<?php

namespace App\Controller;

use App\Entity\StudentUserData;
use App\Entity\TeacherUserData;
use App\Entity\User;
use App\Entity\UserInstrumentGrade;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Sonata\SeoBundle\Seo\SeoPageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SendSms;
use App\Entity\Communication;
use App\Repository\ConfigurationRepository;
use App\Repository\NavigationRepository;
use App\Repository\PageRepository;
use App\Repository\WebPhotosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/accounts')]
class RegistrationController extends AbstractController
{
    public function __construct(private readonly EmailVerifier $emailVerifier, private readonly SendSms $sendSms, private readonly UserRepository $usr, private readonly ConfigurationRepository $config, private readonly NavigationRepository $nav, private readonly WebPhotosRepository $photoRepo, private readonly PageRepository $pageRepo)
    {
    }

    public function getGlobalVars($blogCount = 3, $courseCount = 3)
    {

        $data = [];
        $navData = $this->nav->findAll();
        $configData = $this->config->findAll();
        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['configData'] = $configData ? $configData[0] : null;
        $glVars['navData'] = $navData;
        $glVars['thisUrl'] = $url['path'];

        return $glVars;

    }

    public function getAllPagePhotos($page)
    {
        $photos = [];
        foreach ($page->getSections() as $key => $section) {
            $sectionImages = $this->photoRepo->findBy(
                ['section' => $section],
                ['id' => 'ASC']
            );
            $photos[$section->getName()] = $sectionImages;

        }
        $globalPage = $this->pageRepo->findOneByName('global');

        if($globalPage) {

            foreach ($globalPage->getSections() as $key => $section) {
                $sectionImages = $this->photoRepo->findBy(
                    ['section' => $section],
                    ['id' => 'ASC']
                );
                $photos[$section->getName()] = $sectionImages;

            }

        }

        return $photos;
    }


    #[Route(path: '/register/{usertype}', name: 'app_register')]
    public function register(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        $usertype,
        InstrumentRepository $instrumentRepo,
        Mailer $mailer,
        UserRepository $userRepo,
        SeoPageInterface $seoPage,
        InstrumentGradeRepository $instrumentGradeRepo,
        PageRepository $pageRepo,
        ValidatorInterface $validator,
        SendSms $sendSms,
        EntityManagerInterface $entityManager
    ): Response {
        $userdata = null;
        $phone = null;
        $user = new User();
        $users = $userRepo->findAll();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $instrument = "";
        $page = $pageRepo->findOneByName('global');

        $glVars = $this->getGlobalVars();
        $pagePhotos = $this->getAllPagePhotos($page);
        $data = [];
        // get the instrument from url if available
        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $query = explode("=", (string) $_SERVER['QUERY_STRING'])[1];
        } else {
            $query = "";
        }

        if($usertype == 'student') {
            $instruments = $instrumentRepo->findBy(
                ['category' => 'instrument'],
                ['id' => 'ASC']
            );
            $courses = $instrumentRepo->findBy(
                ['category' => 'course'],
                ['id' => 'ASC']
            );
            $traditional = $instrumentRepo->findBy(
                ['category' => 'traditional'],
                ['id' => 'ASC']
            );
            $data['instruments'] = $instruments;
            $data['courses'] = $courses;
            $data['traditional'] = $traditional;

            $userdata = new StudentUserData();

            $instrumentGrades = $instrumentGradeRepo->findAll();
            $data['instrumentGrades'] = $instrumentGrades;

        }


        if($usertype == 'teacher') {
            $userdata = new TeacherUserData();
        }

        $data['usertype'] = $usertype;

        if ($form->isSubmitted() && $form->isValid()) {
            if(isset($_POST['myusername']) && !empty($_POST['myusername'])){
                return $this->redirect('https://example.com');
            }
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setActive(false);
            $user->setUsertype($usertype);
            if(count($users) == 0 && $usertype == 'admin') {
                $user->setRoles(['ROLE_ADMIN']);
            }

            $entityManager->persist($user);

            if(isset($_POST['instrument']) && $usertype == 'student') {
                $instrument = $instrumentRepo->findOneById($_POST['instrument']);
                $phone = $_POST['phone'];
                $userInstrument = new UserInstrumentGrade();
                $userInstrument->setStudentUserData($userdata);
                $userInstrument->setGrade($instrumentGradeRepo->findOneById($_POST['grade']));
                $userInstrument->setInstrument($instrument);
                $entityManager->persist($userInstrument);

                $userdata->addUserInstruments($userInstrument);
                $userdata->setPhone($phone);
                $entityManager->persist($userdata);

                $user->setStudentdata($userdata);
                $entityManager->persist($user);

            } elseif ($usertype == 'teacher') {
                $user->setTeacherdata($userdata);
                $entityManager->persist($userdata);
                $entityManager->persist($user);
            }

            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $data = ['student' => $user, 'instrument' => $instrument];
            if ($usertype == 'student') {
                $mailer->sendEmail($data, $user->getEmail(), "Welcome to Cheza Music School", "welcome.html.twig", "necessary", "welcome_message");
            } elseif ($usertype == 'teacher') {
                $mailer->sendEmail($data, $user->getEmail(), "Welcome to Cheza Music School", "welcome_teacher.html.twig", "necessary", "welcome_message");
            }
            // do anything else you need here, like send an email
            // send email to alert admins of a new student
            $admins = $userRepo->findByUsertype('admin');
            if ($user->getUsertype() == 'student') {
                foreach ($admins as $admin) {
                    $maildata = ['student' => $user];
                    $mailer->sendEmail($maildata, $admin->getEmail(), "New Student Registration Alert", "new_student.html.twig", "necessary", "registration_successful");
                }

            }
            $msg = "Your registration has been received. We will call you as soon as possible.";
            $sendSms->quickSend($phone, $user->getFullname(), 'New Registration', $msg, 'necessary', 'personal_sms_from_office', 'nomail');
            $msg2 = "New registration. Name: " . $user->getFullname() . ", phone: ". $phone . ", instrument: " . $instrument;
            $sendSms->quickSend('0711832933', $user->getFullname(), 'New Registration', $msg2, 'necessary', 'personal_sms_from_office', 'nomail');


            return $this->redirectToRoute('welcome_page', ['user_id' => $user->getId()]);
        }

        $seoPage
            ->setTitle(ucfirst((string) $usertype) . ' Registration')
            ->addMeta('name', 'keywords', 'Learn your favorite instrument. Join Cheza Music Today')
            ->addMeta('name', 'title', 'Join Cheza Music School Today')
            ->addMeta('name', 'description', 'Join Cheza Music School today and learn from professional musicians at reasonable rates.')
            ->addMeta('property', 'og:title', 'Join Cheza Music School Today')
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:image', "https://chezamusicschool.co.ke/adm/assets/images/cheza_logo_sm.jpg")
            ->addMeta('property', 'og:url', 'https://chezamusicschool.co.ke/accounts/register/'.$usertype)
            ->addMeta('property', 'og:description', 'Join Cheza Music School today and learn from professional musicians at reasonable rates.')
        ;

        $errors = $validator->validate($user);

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form,
            'data' => $data,
            'query' => $query,
            'errors' => $errors,
            'sonata' => true,
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos],
        ]);
    }

    #[Route(path: '/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_login');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('app_login');
    }

}
