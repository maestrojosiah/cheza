<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InstrumentRepository;
use App\Repository\BlogRepository;
use App\Repository\UserRepository;

class IndexController extends AbstractController
{
    #[Route(path: '/index', name: 'index')]
    public function index(InstrumentRepository $instrumentRepo, BlogRepository $postRepo, UserRepository $userRepo): Response
    {
        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $studentscount = $userRepo->countUsersOfType('student');
        $teacherscount = $userRepo->countUsersOfType('teacher');

        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $data['posts'] = $postRepo->findThreeLatestPosts();
        $data['studentscount'] = $studentscount;
        $data['teacherscount'] = $teacherscount;
        $data['programs'] = count($instruments) + count($courses) + count($traditional);

        return $this->render('index/index.html.twig', [
            'data' => $data,
        ]);
    }
}
