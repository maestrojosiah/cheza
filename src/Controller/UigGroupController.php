<?php

namespace App\Controller;

use App\Entity\UigGroup;
use App\Form\UigGroupType;
use App\Repository\TermRepository;
use App\Repository\UigGroupRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/uig/group')]
class UigGroupController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/list/for/uig/{user_id}', name: 'uig_group_index', methods: ['GET'])]
    public function index(UigGroupRepository $uigGroupRepository, $user_id, UserRepository $userRepo, TermRepository $termRepo): Response
    {
        $user = $userRepo->findOneById($user_id);
        $terms = $termRepo->findAll();

        return $this->render('uig_group/index.html.twig', [
            'uig_groups' => $uigGroupRepository->findByUser($user),
            'terms' => $terms,
            'user' => $user,
        ]);
    }

    #[Route(path: '/new/{user_id}', name: 'uig_group_new', methods: ['GET', 'POST'])]
    public function new(Request $request, $user_id, UserRepository $userRepo): Response
    {
        $user = $userRepo->findOneById($user_id);
        $uigGroup = new UigGroup();
        $form = $this->createForm(UigGroupType::class, $uigGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uigGroup->setUser($user);

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($uigGroup);
            $entityManager->flush();

            return $this->redirectToRoute('uig_group_new', ['user_id' => $user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('uig_group/new.html.twig', [
            'user' => $user,
            'uig_group' => $uigGroup,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'uig_group_show', methods: ['GET'])]
    public function show(UigGroup $uigGroup): Response
    {
        return $this->render('uig_group/show.html.twig', [
            'uig_group' => $uigGroup,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'uig_group_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UigGroup $uigGroup): Response
    {
        $form = $this->createForm(UigGroupType::class, $uigGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('uig_group_index', ['user_id' => $uigGroup->getUser()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('uig_group/edit.html.twig', [
            'uig_group' => $uigGroup,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'uig_group_delete', methods: ['POST'])]
    public function delete(Request $request, UigGroup $uigGroup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$uigGroup->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($uigGroup);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uig_group_index', ['user_id' => $uigGroup->getUser()->getId()], Response::HTTP_SEE_OTHER);
    }
}
