<?php

namespace App\Controller;

use App\Entity\Section;
use App\Form\SectionType;
use App\Repository\SectionRepository;
use App\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/section')]
class SectionController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/new/order/save', name: 'save_section_list_order')]
    public function saveOrder(SectionRepository $sectionRepo, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $page_id = $request->request->get('page_id');
        $orderStr = $request->request->get('new_order');
        $index = 1;
        foreach (explode(",", $orderStr) as $sec) {
            $section_id = explode('-', $sec)[1];
            $section = $sectionRepo->findOneById($section_id);
            $section->setPosition($index);
            $this->save($section);
            $index++;

        }
        return new JsonResponse("saved new order");

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


    #[Route(path: '/', name: 'section_index', methods: ['GET'])]
    public function index(SectionRepository $sectionRepository, PageRepository $pageRepo): Response
    {
        // get the instrument from url if available
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $query = explode("=", (string) $_SERVER['QUERY_STRING'])[1];
            $page = $pageRepo->findOneById($query);
        } else {
            $query = "";
        }
        $pages = $pageRepo->findAll();
        if ($query != "") {
            $sections = $sectionRepository->findBy(
                ['page' => $page],
                ['position' => 'ASC'],
            );
        } else {
            $sections = $sectionRepository->findBy(
                [],
                ['position' => 'DESC']
            );
        }
        return $this->render('section/index.html.twig', [
            'page_id' => $query,
            'pages' => $pages,
            'sections' => $sections,
        ]);
    }

    #[Route(path: '/new', name: 'section_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $section = new Section();
        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $section->setLastUpdated(new \Datetime());
            $entityManager->persist($section);
            $entityManager->flush();

            return $this->redirectToRoute('section_index');
        }

        return $this->render('section/new.html.twig', [
            'section' => $section,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'section_show', methods: ['GET'])]
    public function show(Section $section): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('section/show.html.twig', [
            'section' => $section,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'section_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Section $section): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $section->setLastUpdated(new \Datetime());
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('section_index');
        }

        return $this->render('section/edit.html.twig', [
            'section' => $section,
            'form' => $form,
        ]);
    }
    //  #[Route(path: '/save/edit/section', name: 'update_section_edit', methods: ['GET', 'POST'])]
    //     public function saveEdit(Request $request, PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    //     {
    //         $this->denyAccessUnlessGranted('ROLE_ADMIN');

    //         $section_id = $request->request->get('section_id');
    //         $name = $request->request->get('name');
    //         $position = $request->request->get('position');
    //         $page_id = $request->request->get('page');
    //         $visible = $request->request->get('visible');
    //         $typograpy = $request->request->get('typography');



    //         // $page = $pageRepo->findOneById($page_id);
    //         // $section = $sectionRepo->findOneById($section_id);

    //         // $section->setName($name);
    //         // $section->setPosition($position);
    //         // $section->setPage($page);
    //         // $section->setVisible($visible);
    //         // $section->setTypography($typography);

    //         // $this->save($section);
    //         return new JsonResponse($_POST);
    //     }


    #[Route(path: '/{id}', name: 'section_delete', methods: ['DELETE'])]
    public function delete(Request $request, Section $section): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$section->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($section);
            $entityManager->flush();
        }

        return $this->redirectToRoute('section_index');
    }
}
