<?php

namespace App\Controller;

use App\Entity\Reporting;
use App\Form\ReportingType;
use App\Repository\ReportingRepository;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;
use Knp\Snappy\Image;
use App\Service\Mailer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SendSms;
use App\Service\ShortenUrl;

#[Route(path: '/reporting')]
class ReportingController extends AbstractController
{
    public function __construct(private readonly Pdf $pdf, private readonly Image $img, private readonly Environment $twig, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }

    #[Route(path: '/for/teacher/{teacher_id}', name: 'reporting_index', methods: ['GET'])]
    public function index(ReportingRepository $reportingRepository, UserRepository $userRepo, $teacher_id): Response
    {
        $teacher = $userRepo->findOneById($teacher_id);
        return $this->render('reporting/index.html.twig', [
            'reportings' => $reportingRepository->findByTeacher($teacher),
            'teacher' => $teacher
        ]);
    }

    #[Route(path: '/new', name: 'reporting_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $reporting = new Reporting();
        $form = $this->createForm(ReportingType::class, $reporting);
        $form->handleRequest($request);

        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['name'])) {
            $name = $_GET['name'];
        } else {
            $name = "Pupil";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['age'])) {
            $age = $_GET['age'];
        } else {
            $age = "Over/Under 18";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['term'])) {
            $term = $_GET['term'];
        } else {
            $term = "Term";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['s'])) {
            $sex = $_GET['s'];
        } else {
            $sex = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['t'])) {
            $teacher_id = $_GET['t'];
        } else {
            $teacher_id = null;
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['instrument'])) {
            $instrument = $_GET['instrument'];
        } else {
            $instrument = "Instrument";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['u'])) {
            $user_id = $_GET['u'];
        } else {
            $user_id = null;
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($reporting);
            $entityManager->flush();

            return $this->redirectToRoute('reporting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('reporting/new.html.twig', [
            'reporting' => $reporting,
            'name' => $name,
            'age' => $age,
            'term' => $term,
            'sex' => $sex,
            'teacher_id' => $teacher_id,
            'instrument' => $instrument,
            'user_id' => $user_id,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'reporting_show', methods: ['GET'])]
    public function show(Reporting $reporting): Response
    {
        return $this->render('reporting/show.html.twig', [
            'reporting' => $reporting,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'reporting_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Reporting $reporting): Response
    {
        $form = $this->createForm(ReportingType::class, $reporting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('reporting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('reporting/edit.html.twig', [
            'reporting' => $reporting,
            'name' => $reporting->getName(),
            'age' => $reporting->getAge(),
            'term' => $reporting->getTerm(),
            'sex' => $reporting->getSex(),
            'teacher_id' => $reporting->getTeacher()->getId(),
            'instrument' => $reporting->getInstrument(),
            'user_id' => $reporting->getUser()->getId(),
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'reporting_delete', methods: ['POST'])]
    public function delete(Request $request, Reporting $reporting): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reporting->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($reporting);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reporting_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/ajax/save/new', name: 'save_new_report')]
    public function newReportAjaxSave(
        ReportRepository $reportRepo,
        Request $request,
        UserRepository $userRepo,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig
    ): Response {
        $user_id = $request->request->get('user_id');
        $name = $request->request->get('name');
        $term = $request->request->get('term');
        $instrument = $request->request->get('instrument');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $teacher_id = $request->request->get('teacher_id');
        $performed = $request->request->get('performed');
        $learning_goals = $request->request->get('goals');
        $progress_assessment = $request->request->get('progress');
        $strengths = $request->request->get('strengths');
        $weaknesses = $request->request->get('weaknesses');
        $recommendations = $request->request->get('recommend');
        $practice_habits = $request->request->get('practice');
        $learning_level = $request->request->get('pupil_level');

        $user = $userRepo->findOneById($user_id);

        if($sex == 'male') {
            $hisHer = 'his';
            $himHer = 'him';
        } elseif ($sex == 'female') {
            $hisHer = 'her';
            $himHer = 'her';
        } else {
            $hisHer = "their";
            $himHer = "them";
        }

        $summary_string = "This report has evaluated " . $name . "'s progress in learning " . $instrument  . ", including " . $hisHer . " learning goals, progress assessment, strengths and weaknesses, recommendations for continued progress, and overall assessment. The instructor is pleased with " . $name . "'s progress and looks forward to continuing to work with " . $himHer . " in the future.";

        $reporting = new Reporting();


        $student = $userRepo->findOneById($user_id);
        $teacher = $userRepo->findOneById($teacher_id);

        $reporting->setUser($user);

        $reporting->setName($name);
        $reporting->setTerm($term);
        $reporting->setAddedOn(new \DateTimeImmutable());
        $reporting->setInstrument($instrument);
        $reporting->setAge($age);
        $reporting->setPerformed($performed);
        $reporting->setLearningGoals($learning_goals);
        $reporting->setSex($sex);
        $reporting->setTeacher($teacher);
        $reporting->setProgressAssessment($progress_assessment);
        $reporting->setStrengths($strengths);
        $reporting->setWeaknesses($weaknesses);
        $reporting->setRecommendations($recommendations);
        $reporting->setLearningLevel($learning_level);
        $reporting->setPracticeHabits($practice_habits);
        $reporting->setSummary($summary_string);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($reporting);
        $entityManager->flush();

        $html = $twig->render('pdf/student_report.html.twig', [
            'reporting' => $reporting,
        ]);

        // $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $reporting->getId(), 'Download Lesson Plan');
        // $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Lesson Plan', 'Your Lesson plan has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());
        // $sendSms->quickSend($teacher->getTeacherdata()->getPhone(), $teacher->getFullname(), 'Lesson Plan Sent', $student . '\'s Lesson plan has been sent, and a copy has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $teacher->getEmail());

        // $mailer->sendEmailWithAttachment(['student' => $student, 'lesson_plan' => $lessonPlan], $student->getEmail(), $student->getFullname().' - Your Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_student.html.twig', $student->getFullname().'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        // $mailer->sendEmailWithAttachment(['teacher' => $teacher, 'student' => $student, 'lesson_plan' => $lessonPlan], $teacher->getEmail(), $student . '\'s Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_teacher.html.twig', $student->getFullname().'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        // $this->addFlash(
        //     'success',
        //     'The lesson plan was sent successfully.',
        // );
        // // send text with short url to pdf of invoice
        return new JsonResponse($teacher->getFullname());

    }

    #[Route(path: '/ajax/edit/entry', name: 'ajax_edit_report')]
    public function editReportAjaxSave(
        ReportingRepository $reportingRepo,
        Request $request,
        UserRepository $userRepo,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig
    ): Response {
        $reporting_id = $request->request->get('reporting_id');
        $user_id = $request->request->get('user_id');
        $name = $request->request->get('name');
        $term = $request->request->get('term');
        $instrument = $request->request->get('instrument');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $teacher_id = $request->request->get('teacher_id');
        $performed = $request->request->get('performed');
        $learning_goals = $request->request->get('goals');
        $progress_assessment = $request->request->get('progress');
        $strengths = $request->request->get('strengths');
        $weaknesses = $request->request->get('weaknesses');
        $recommendations = $request->request->get('recommend');
        $practice_habits = $request->request->get('practice');
        $learning_level = $request->request->get('pupil_level');

        $reporting = $reportingRepo->findOneById($reporting_id);

        if($sex == 'male') {
            $hisHer = 'his';
            $himHer = 'him';
        } elseif ($sex == 'female') {
            $hisHer = 'her';
            $himHer = 'her';
        } else {
            $hisHer = "their";
            $himHer = "them";
        }

        $summary_string = "This report has evaluated " . $name . "'s progress in learning " . $instrument  . ", including " . $hisHer . " learning goals, progress assessment, strengths and weaknesses, recommendations for continued progress, and overall assessment. The instructor is pleased with " . $name . "'s progress and looks forward to continuing to work with " . $himHer . " in the future.";


        $student = $userRepo->findOneById($user_id);
        $teacher = $userRepo->findOneById($teacher_id);

        $reporting->setUser($student);

        $reporting->setName($name);
        $reporting->setTerm($term);
        $reporting->setAddedOn(new \DateTimeImmutable());
        $reporting->setInstrument($instrument);
        $reporting->setAge($age);
        $reporting->setPerformed($performed);
        $reporting->setLearningGoals($learning_goals);
        $reporting->setSex($sex);
        $reporting->setTeacher($teacher);
        $reporting->setProgressAssessment($progress_assessment);
        $reporting->setStrengths($strengths);
        $reporting->setWeaknesses($weaknesses);
        $reporting->setRecommendations($recommendations);
        $reporting->setLearningLevel($learning_level);
        $reporting->setPracticeHabits($practice_habits);
        $reporting->setSummary($summary_string);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($reporting);
        $entityManager->flush();

        $html = $twig->render('pdf/student_report.html.twig', [
            'reporting' => $reporting,
        ]);

        // $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $lessonPlan->getId(), 'Download Lesson Plan');
        // $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Lesson Plan', 'Your Lesson plan has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());
        // $sendSms->quickSend($teacher->getTeacherdata()->getPhone(), $teacher->getFullname(), 'Lesson Plan Sent', $student . '\'s Lesson plan has been sent, and a copy has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $teacher->getEmail());

        // $mailer->sendEmailWithAttachment(['student' => $student, 'lesson_plan' => $lessonPlan], $student->getEmail(), $student->getFullname().' - Your Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_student.html.twig', $student->getFullname().'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        // $mailer->sendEmailWithAttachment(['teacher' => $teacher, 'student' => $student, 'lesson_plan' => $lessonPlan], $teacher->getEmail(), $student . '\'s Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_teacher.html.twig', $student->getFullname().'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        // $this->addFlash(
        //     'success',
        //     'The lesson plan was sent successfully.',
        // );
        // // send text with short url to pdf of invoice
        return new JsonResponse($teacher->getFullname());

    }

    // #[Route(path: '/admin/send/reporting', name: 'send_reporting')]
    // public function sendLessonPlan(
    //     ReportingRepository $reportingRepo,
    //     Request $request,
    //     Mailer $mailer,
    //     SendSms $sendSms,
    //     ShortenUrl $shortenUrl,
    //     Environment $twig
    // ): Response {

    //     $session = null;
    //     $reportingId = $request->request->get('reporting_id');
    //     $reporting = $reportingRepo->findOneById($reportingId);

    //     $student = $reporting->getUser();
    //     $html = $twig->render('pdf/student_report.html.twig', [
    //         'reporting' => $reporting,
    //     ]);

    //     $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $reporting->getId(), 'Download Report');
    //     $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Report', 'Your Report has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());

    //     $mailer->sendEmailWithAttachment(['student' => $student, 'lesson_plan' => $reporting], $student->getEmail(), $student->getFullname().' Your Updated Report for '.$reporting->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_student.html.twig', $student->getFullname().'-reporting-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
    //     $this->addFlash(
    //         'success',
    //         'The invoice was sent successfully.'
    //     );
    //     // send text with short url to pdf of invoice
    //     return new JsonResponse('test');

    // }

    #[Route(path: '/show/pdf/for/{id}', name: 'reporting_show_pdf', methods: ['GET'])]
    public function showPdf(ReportingRepository $reportingRepo, $id): Response
    {


        $reporting = $reportingRepo->findOneById($id);
        $arr = [
            'reporting' => $reporting,
        ];

        $this->toPdf("report", "pdf/student_report.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;


    }

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . '.pdf');

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }


}
