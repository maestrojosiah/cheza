<?php

namespace App\Controller;

use App\Entity\NavChild;
use App\Form\NavChildType;
use App\Repository\NavChildRepository;
use App\Repository\NavigationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/nav/child')]
class NavChildController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'nav_child_index', methods: ['GET'])]
    public function index(NavChildRepository $navChildRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('nav_child/index.html.twig', [
            'nav_children' => $navChildRepository->findAll(),
        ]);
    }

    #[Route(path: '/new/for/{navigation_id}', name: 'nav_child_new', methods: ['GET', 'POST'])]
    public function new(Request $request, NavigationRepository $navRepo, $navigation_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $navChild = new NavChild();
        $navigation = $navRepo->findOneById($navigation_id);
        $form = $this->createForm(NavChildType::class, $navChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $navChild->setNavigation($navigation);
            $entityManager->persist($navChild);
            $entityManager->flush();

            return $this->redirectToRoute('nav_child_index');
        }

        return $this->render('nav_child/new.html.twig', [
            'nav_child' => $navChild,
            'navigation' => $navigation,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'nav_child_show', methods: ['GET'])]
    public function show(NavChild $navChild): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('nav_child/show.html.twig', [
            'nav_child' => $navChild,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'nav_child_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, NavChild $navChild): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(NavChildType::class, $navChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('nav_child_index');
        }

        return $this->render('nav_child/edit.html.twig', [
            'nav_child' => $navChild,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'nav_child_delete', methods: ['DELETE'])]
    public function delete(Request $request, NavChild $navChild): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$navChild->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($navChild);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nav_child_index');
    }
}
