<?php

namespace App\Controller;

use App\Entity\Report;
use App\Form\ReportType;
use App\Repository\ReportRepository;
use App\Repository\SessionRepository;
use App\Repository\TermRepository;
use App\Repository\UserRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\StudentUserDataRepository;
use App\Repository\TeacherUserDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;
use Knp\Snappy\Image;
use App\Service\Mailer;
use App\Repository\PaymentRepository;
use App\Service\CalculateFees;

#[Route(path: '/report')]
class ReportController extends AbstractController
{
    public function __construct(private readonly Pdf $pdf, private readonly Image $img, private readonly Environment $twig, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'report_index', methods: ['GET'])]
    public function index(ReportRepository $reportRepository): Response
    {
        return $this->render('report/index.html.twig', [
            'reports' => $reportRepository->findAll(),
        ]);
    }

    #[Route(path: '/new/{session_id}', name: 'report_new', methods: ['GET', 'POST'])]
    public function new(SessionRepository $sessionRepo, TermRepository $termRepo, ReportRepository $reportRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $past = [];
        $sentreports = [];
        $term1 = [];
        $term2 = [];
        $term3 = [];
        $rpts = [];
        $test = [];
        $currentTerm = $termRepo->findCurrentTerm(new \Datetime(date('Y-m-d')));
        $reports = $reportRepo->findBy(
            ['uig' => $session->getUserInstrumentGrade()],
            ['id' => 'ASC']
        );
        // $reportE = $reportRepo->findOneBy(
        //     array('teacher' => $session->getTeacher()->getTeacherdata(), 'term' => $currentTerm, 'student' => $session->getStudent()->getStudentdata(), 'uig' => $session->getUserInstrumentGrade()),
        //     array('id' => 'ASC'),
        // );

        if(count($reports) > 0) {

            foreach ($reports as $report) {
                $uig = $report->getUig();
                $reportTerm = $termRepo->findCurrentTerm($session->getStartingOn());
                $test[] = $session->getStartingOn()->format('d/m/Y');

                if($report -> getAddedOn()->format('Y') != date('Y')) {
                    $past[$uig->getId()] = $report;
                } else {
                    if($reportTerm == "Term 1") {
                        $term1[$uig->getId()] = $report;
                    } elseif ($reportTerm == "Term 2") {
                        $term2[$uig->getId()] = $report;
                    } elseif ($reportTerm == "Term 3") {
                        $term3[$uig->getId()] = $report;
                    }
                }
                // $reportsuniq[$report->getUig()->getId().'-'.$reportTerm] = $report;
                // $all_reports[$reportTerm.'-'.$report->getId()] = $report;
            }

        }

        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true) {
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $rpts[] = $reportentry->getUig()->getId();
            }
        }

        $sessionsuniq = [];
        $term1List = [];
        $term2List = [];
        $term3List = [];
        $sentreportsList = [];
        $reportsList = [];

        $r = [];
        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true) {
                $sentreportsList[] = $reportentry->getUig()->getId();
            } else {
                $reportsList[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }

        }

        $ct = $termRepo->findOneByTermnumber($currentTerm);

        $termStart = $ct->getStartingOn();
        $termEnd = $ct->getEndingOn();
        $sessions = $sessionRepo->findForThisTermAsc($termStart, $termEnd, $session->getTeacher());


        foreach ($sessions as $key => $sess) {
            $uig = $sess->getUserInstrumentGrade();
            $firstSession = $sessionRepo->findOneBy(
                ['userInstrumentGrade' => $uig],
                ['id' => 'ASC']
            );
            $firstSessionDate = $firstSession->getStartingOn();
            $prevailingTerm = $termRepo->findCurrentTerm($firstSessionDate);

            if($prevailingTerm == "Term 1") {
                $term1List[$uig->getId()] = $firstSession;
            } elseif ($prevailingTerm == "Term 2") {
                $term2List[$uig->getId()] = $firstSession;
            } elseif ($prevailingTerm == "Term 3") {
                $term3List[$uig->getId()] = $firstSession;
            }
            $sessionsuniq[$firstSession->getUserInstrumentGrade()->getId().'-'.$prevailingTerm] = $firstSession;

        }

        return $this->render('report/new.html.twig', [
            'reports' => $past,
            'reportE' => $past,
            'sessions' => $sessionsuniq,
            'r' => $r,
            'rpts' => $rpts,
            'term1' => $term1,
            'test' => $test,
            'term2' => $term2,
            'term3' => $term3,
            'terms' => ['Term 1' => $term1, 'Term 2' => $term2, 'Term 3' => $term3, 'Past' => $past],
            'termsList' => ['Term 1' => $term1List, 'Term 2' => $term2List, 'Term 3' => $term3List],
            'sentreports' => $sentreports,
            'session' => $session,
            'currentTerm' => $currentTerm,
        ]);
    }

    #[Route(path: '/{id}', name: 'report_show', methods: ['GET'])]
    public function show(Report $report, TermRepository $termRepo, SessionRepository $sessionRepo): Response
    {

        // $currentTerm = $termRepo->findCurrentTerm($report->getAddedOn());
        $uig = $report->getUig();
        $firstSession = $sessionRepo->findOneBy(
            ['userInstrumentGrade' => $uig],
            ['id' => 'ASC']
        );
        $currentTerm = $termRepo->findCurrentTerm($firstSession->getStartingOn());
        $ct = $termRepo->findOneByTermnumber($currentTerm);

        if($report->getUig()->getSiblingname() != null and $report->getUig()->getSiblingname() != "") {
            $fullname = $report->getUig()->getSiblingname();
        } else {
            $fullname = $report->getStudent()->getStudent()->getFullname();
        }

        $arr = [
            'report' => $report,
            'currentTerm' => $currentTerm,
            'ct' => $ct,
            'fullname' => $fullname,
        ];

        $this->toPdf($fullname."_report", "pdf/student_report.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;


    }

    #[Route(path: '/{id}/edit', name: 'report_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Report $report): Response
    {
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('report_index');
        }

        return $this->render('report/edit.html.twig', [
            'report' => $report,
            'form' => $form,
        ]);
    }

    #[Route(path: '/admin/pdf/invoice/{payment_id}', name: 'pdf_std_invoice')]
    public function pdfInvoice(TermRepository $termRepo, PaymentRepository $paymentRepo, CalculateFees $feesCalculator, Mailer $mailer, Environment $twig, $payment_id): Response
    {

        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $student = $payment->getUser();

        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        $html = $twig->render('pdf/student_invoice.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);

        $arr = [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ];

        $this->toPdf($student->getFullname().'-invoice-'.$payment->getDoneOn()->format('d/m/Y'), "pdf/student_invoice.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;

    }

    #[Route(path: '/admin/pdf/student/invoice/{user_id}', name: 'pdf_student_invoice')]
    public function pdfStudentInvoice(UserRepository $userRepo, Environment $twig, $user_id): Response
    {

        $student = $userRepo->findOneById($user_id);
        $studentData = $student->getStudentdata();
        // $dateTimeFrom = new \Datetime($starting_from." 00:00:00");
        // $dateTimeTo = new \Datetime($to." 23:59:59");

        $uigs = $studentData->getUserInstruments();
        $uigsWithBalance = [];
        $uigsWithNegativeBalance = [];
        $studentdata = $student->getStudentdata();
        $usertype = $student->getUsertype();

        $bal = 0;
        $instruments = [];
        // echo $student;
        // echo "<br/>";

        foreach ($uigs as $key => $uig) {

            $payments = $uig->getPayments();
            $uig_bal = 0;

            if((is_countable($payments) ? count($payments) : 0) > 0 && $studentdata != null && $usertype = "student") {

                foreach ($payments as $key => $payment) {

                    $instruments[] = $uig->getInstrument();
                    $bal = $payment->getType() == "inv" ? $bal + $payment->getAmount() : $bal - $payment->getAmount();
                    $uig_bal = $payment->getType() == "inv" ? $uig_bal + $payment->getAmount() : $uig_bal - $payment->getAmount();
                    // print_r($instruments);
                    // echo  "<br/>";
                    // print_r($payment->getAmount() . ": " . $bal. " : " . $payment->getType() . "<br />");

                }

            }

            if($uig_bal > 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();

            }
            if($uig_bal < 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithNegativeBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();

            }
            // echo "<br/>Bal: " . $bal;

        }

        $html = $twig->render('pdf/user_invoice.html.twig', [
            'student' => $student,
            'instruments' => array_unique($instruments),
            'studentData' => $studentData,
            'balance' => $bal,
            'uigsWithBalanceChunked' => array_chunk($uigsWithBalance, 10, true),
            'uigsWithNegativeBalance' => $uigsWithNegativeBalance,
        ]);

        $arr = [
            'student' => $student,
            'instruments' => array_unique($instruments),
            'studentData' => $studentData,
            'balance' => $bal,
            'uigsWithBalanceChunked' => array_chunk($uigsWithBalance, 10, true),
            'uigsWithNegativeBalance' => $uigsWithNegativeBalance,
        ];

        $this->toPdf($student->getFullname().'-invoice-'.date('d/m/Y'), "pdf/user_invoice.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;

    }

    #[Route(path: '/admin/pdf/receipt/{payment_id}', name: 'pdf_receipt')]
    public function pdfreceipt(TermRepository $termRepo, PaymentRepository $paymentRepo, CalculateFees $feesCalculator, Environment $twig, $payment_id): Response
    {

        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $student = $payment->getUser();

        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $student = $payment->getUser();
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        $html = $twig->render('pdf/student_receipt.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);

        $arr = [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ];

        $this->toPdf($student->getFullname().'-receipt-'.$payment->getDoneOn()->format('d/m/Y'), "pdf/student_receipt.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;

    }

    #[Route(path: '/admin/pdf/students/balances/show', name: 'show_balances')]
    public function showBalances(UserRepository $studentRepo, Environment $twig): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $students = $studentRepo->findByActive(1);

        return $this->render('admin/balances.html.twig', [
            'students' => $students,
        ]);

    }

    #[Route(path: '/admin/pdf/students/balances/export', name: 'export_balances')]
    public function exportBalances(UserRepository $studentRepo, Environment $twig): Response
    {

        $students = $studentRepo->findByActive(1);
        // $payment = $paymentRepo->findOneById($payment_id);
        // $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        // $student = $payment->getUser();
        $html = $twig->render('pdf/export_balances.html.twig', [
            'students' => $students,
        ]);

        $arr = [
            'students' => $students,
        ];

        $this->toPdf('Balances-'.date('d/m/Y'), "pdf/export_balances.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;


    }


    #[Route(path: '/teacher/save/student/report', name: 'save_student_report')]
    public function saveStudentReport(Request $request, TermRepository $termRepo, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, ReportRepository $reportRepo): Response
    {
        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $student = $userRepo->findOneById($request->request->get('student_id'));
        $uig = $uigRepo->findOneById($request->request->get('uig_id'));
        $term = $termRepo->findOneById($request->request->get('term_id'));
        $rep = $request->request->get('report_id');

        if($rep != 0) {
            $report = $reportRepo->findOneById($rep);
        } else {
            $report = new Report();
        }


        if(!$report) {

        }
        $test = [];
        $data = $request->request->all();
        foreach ($data as $field => $d) {
            $test[] = $field;
            if (!str_contains($field, '_id')) {
                $cmd = "set".$field;
                $report->{$cmd}($d);
            }
        }
        $report->setTeacher($teacher->getTeacherdata());
        $report->setStudent($student->getStudentdata());
        $report->setAddedOn(new \Datetime(date("Y-m-d")));
        $report->setUig($uig);

        $this->save($report);

        return new JsonResponse($test);

        // return new JsonResponse("success");
    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . '.pdf');

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }

    public function toImage($filename, $path = '', $array = []): Response
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        //Generate pdf with the retrieved HTML
        return new Response(
            $this->img->getOutputFromHtml($html),
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            ['Content-Type'          => 'image/jpg', 'Content-Disposition'   => 'inline; filename='.$filename.'.jpg']
        );
    }

}
