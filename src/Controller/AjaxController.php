<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Notes;
use App\Entity\StudentUserData;
use App\Entity\Payment;
use App\Entity\UserSettings;
use App\Entity\Template;
use App\Entity\Comment;
use App\Entity\CommentReply;
use App\Entity\UserInstrumentGrade;
use App\Entity\SchoolTime;
use App\Entity\DeductionPayment;
use App\Entity\SimpleReport;
use App\Entity\PracticeSessions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\InstrumentRepository;
use App\Repository\SessionRepository;
use App\Repository\PackageRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PaymentRepository;
use App\Repository\SimpleReportRepository;
use App\Repository\SettingsRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\TemplateRepository;
use App\Repository\UserRepository;
use App\Repository\UserSettingsRepository;
use App\Repository\TermRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\AttachmentRepository;
use App\Repository\CommentRepository;
use App\Repository\BlogRepository;
use App\Repository\NotesRepository;
use App\Repository\AssignmentRepository;
use App\Repository\PracticeSessionsRepository;
use App\Service\Mailer;
use App\Service\FeesNotify;
use Twig\Environment;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Service\SendSms;
use App\Service\ShortenUrl;
use App\Repository\ConfigurationRepository;
use App\Repository\WebPhotosRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\CourseRepository;
use App\Entity\PhotoUpload;
use App\Entity\Score;
use App\Entity\TeacherInvoice;
use App\Entity\Test;
use App\Repository\ChoiceRepository;
use App\Repository\LessonRepository;
use App\Repository\QuizRepository;
use App\Repository\ReportingRepository;
use App\Repository\TestRepository;
use App\Service\CalculateFees;
use App\Service\InvoiceService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AjaxController extends AbstractController
{
    public function __construct(private readonly Pdf $snappy_pdf, private readonly Mailer $mailer, private readonly FeesNotify $feesNotify, private readonly SendSms $sendSms, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry, private ShortenUrl $shortenUrl)
    {
    }

    #[Route(path: '/contact/form/send/message', name: 'send_message_contact_form', methods: ['POST'])]
    public function messageFromContactForm(Request $request, SendSms $sendSms, Mailer $mailer, ConfigurationRepository $configRepo, UserRepository $userRepo): Response
    {
        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $phone = $request->request->get('phone');
        $message = $request->request->get('message');

        $config = $configRepo->findAll();
        $adminEmail1 = $config[0]->getSiteEmail();
        $adminEmail2 = $config[0]->getEmailAlt();
        $adminEmail3 = 'chezamusicschool@gmail.com';
        $admin_emails = [ $adminEmail1, $adminEmail2, $adminEmail3 ];


        if(empty($message)) {
            // do nothing
        } else {
            $message .= "<br />";
            $message .= "Name: $name, Email: $email, Phone number: $phone.";
            $mail_msg = $message;
            $text_msg = "New message from contact form. Check your email. Name: $name, Email: $email, Phone number: $phone";
            $subject = "Message from Contact Form";
            $user = ['fullname' => 'Admin'];

            foreach ($admin_emails as $mail) {

                $maildata = ['msg' => $message, 'user' => $user];
                $mailer->sendEmail($maildata, $mail, $subject, "communication.html.twig", "necessary", "personal_email_from_office");
            }

            $sendSms->quickSend('0711832933', $name, $subject, $text_msg, 'necessary', 'personal_sms_from_office', 'nomail');

        }

        return new JsonResponse([$message, $adminEmail1, $adminEmail2, $adminEmail3]);
    }


    #[Route(path: '/ajax', name: 'ajax')]
    public function index(): Response
    {
        return $this->render('ajax/index.html.twig', [
            'controller_name' => 'AjaxController',
        ]);
    }

    #[Route(path: '/save/user/about', name: 'save_about')]
    public function saveabout(Request $request, UserRepository $userRepo): Response
    {
        $userdata = null;
        $teacher_id = $request->request->get('teacher_id');

        $user = $userRepo->findOneById($teacher_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
        }

        $about = $request->request->get('about');

        $userdata->setAbout($about);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }

    #[Route(path: '/save/simple/report', name: 'save_short_report')]
    public function saveSimpleReport(Request $request, UserRepository $userRepo): Response
    {
        $userdata = null;
        $teacher_id = $request->request->get('teacher_id');
        $student_id = $request->request->get('student_id');
        $full_report = $request->request->get('short_report');
        $background = $request->request->get('background');
        $term = $request->request->get('term');
        $year = $request->request->get('year');
        $name = $request->request->get('name');

        $teacher = $userRepo->findOneById($teacher_id);
        $student = $userRepo->findOneById($student_id);
        $now = new \Datetime();

        $simpleReport = new SimpleReport();
        $simpleReport->setTeacher($teacher);
        $simpleReport->setStudent($student);
        $simpleReport->setFullReport($full_report);
        $simpleReport->setAddedOn($now);
        $simpleReport->setBackground($background);
        $simpleReport->setTerm($term);
        $simpleReport->setName($name);
        $simpleReport->setYear($year);


        $this->save($simpleReport);


        return new JsonResponse($userdata);

    }

    #[Route(path: '/edit/simple/report', name: 'edit_short_report')]
    public function editSimpleReport(Request $request, SimpleReportRepository $simpleReportRepo, UserRepository $userRepo): Response
    {
        $simple_report_id = $request->request->get('simple_report_id');
        $simpleReport = $simpleReportRepo->findOneById($simple_report_id);
        $teacher_id = $request->request->get('teacher_id');
        $student_id = $request->request->get('student_id');
        $full_report = $request->request->get('short_report');
        $background = $request->request->get('background');
        $term = $request->request->get('term');
        $year = $request->request->get('year');

        $teacher = $userRepo->findOneById($teacher_id);
        $student = $userRepo->findOneById($student_id);
        $now = new \Datetime();

        $simpleReport->setTeacher($teacher);
        $simpleReport->setStudent($student);
        $simpleReport->setFullReport($full_report);
        $simpleReport->setAddedOn($now);
        $simpleReport->setBackground($background);
        $simpleReport->setTerm($term);
        $simpleReport->setYear($year);


        $this->save($simpleReport);


        return new JsonResponse("");

    }

    #[Route(path: '/start/test/music/theory', name: 'start_test')]
    public function startTest(Request $request, LessonRepository $lessonRepo, TestRepository $testRepo): Response
    {
        $data = [];
        $lesson_id = $request->request->get('lesson');
        $test_id = $request->request->get('test');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        // $phone = $request->request->get('phone');

        $lesson = $lessonRepo->findOneById($lesson_id);
        $test = $lessonRepo->findOneById($test_id);
        $now = new \Datetime();
        $thisTime = new \Datetime();
        $end = $thisTime->modify("+ 1 hour");

        // check if there is incomplete test by this mail of phone number
        // for the same lesson
        $incompleteTest = $testRepo->findBy(
            ['lesson' => $lesson, 'complete' => false, 'email' => $email],
            ['id' => 'DESC'],
            1
        );

        // if there is an incomplete test
        if(count($incompleteTest) > 0) {

            $test = $incompleteTest[0];
            $scores = $test->getScores();
            $scoreItems = [];
            $numberOfScores = count($scores);
            foreach ($scores as $key => $score) {
                $scoreItems[] = ['quiz' => $score->getQuiz()->getId(), 'choice' => $score->getChoice()->getId()];
            }
            $data['scoresCount'] = $numberOfScores;
            $data['scoreItems'] = $scoreItems;

        } else {

            $test = new Test();
            $test->setLesson($lesson);
            $test->setExaminee($fullname);
            $test->setEmail($email);
            $test->setPhone("");
            $test->setBeginAt($now);
            $test->setEndAt($end);
            $test->setComplete(false);

            $this->save($test);
            $data['scoresCount'] = 0;
            $data['scoreItems'] = [];

        }
        $data['test'] = $test->getId();

        return new JsonResponse($data);

    }

    #[Route(path: '/record/score/music/theory', name: 'record_score')]
    public function record_score(Request $request, TestRepository $testRepo, QuizRepository $quizRepo, ChoiceRepository $choiceRepo): Response
    {
        $choice_id = $request->request->get('choice_id');
        $quiz_id = $request->request->get('quiz_id');
        $test_id = $request->request->get('test');

        $test = $testRepo->findOneById($test_id);
        $quiz = $quizRepo->findOneById($quiz_id);
        $choice = $choiceRepo->findOneById($choice_id);

        $score = new Score();
        $score->setChoice($choice);
        $score->setQuiz($quiz);
        $score->setTest($test);

        $this->save($score);

        return new JsonResponse($choice->getId());

    }

    #[Route(path: '/save/user/conditions', name: 'save_conditions')]
    public function saveconditions(Request $request, UserRepository $userRepo): Response
    {
        $userdata = null;
        $teacher_id = $request->request->get('teacher_id');

        $user = $userRepo->findOneById($teacher_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
        }

        $conditions = $request->request->get('conditions');

        $userdata->setConditions($conditions);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }

    #[Route('/upload/profile/picture', name: 'save_profile_picture')]
    public function saveFile(UserRepository $userRepo)
    {
        $file = $_POST;
        $path_to_save = 'profile_img_directory';
        $thisfiletype = $file['thisfiletype'];
        $teacher_id = $file['teacher_id'];
    
        if($file) {
            $data = $_POST['image'];
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
    
            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $short_profile_img_directory = $this->getParameter('short_profile_img_directory').time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
    
            // Save the image to a temporary file
            $temp_file = tempnam(sys_get_temp_dir(), 'profile_');
            file_put_contents($temp_file, $data);
    
            // Now you can use getimagesize on the temporary file
            $image_info = getimagesize($temp_file);
    
            // Move the temporary file to the desired location
            rename($temp_file, $current_photo_path);
    
            // Set file permissions to 644
            chmod($current_photo_path, 0644);
    
            $user = $userRepo->findOneById($teacher_id);
    
            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $user->getPhoto();
            if(is_file($existing_img) && file_exists($existing_img)) {
                unlink($existing_img);
            }
    
            $user->setPhoto($short_profile_img_directory);
            $this->save($user);
            return new JsonResponse($short_profile_img_directory);
        }
    }
    
    /**
     * @return User|null
     */
    protected function getUser(): ?UserInterface
    {
        return parent::getUser();
    }

    #[Route(path: '/resized/photo/upload/{photo_id}', name: 'upload_resized_photo')]
    /** @var User $user */
    public function saveCroppedFile(WebPhotosRepository $webPhotosRepo, $photo_id)
    {
        $file = $_POST;
        $path_to_save = 'web_photos';
        $thisfiletype = $file['thisfiletype'];
        $toPage = $file['toPage'];
        $photo = $webPhotosRepo->findOneById($photo_id);

        if($file) {

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $short_web_photo_directory = $this->getParameter('short_web_photo_directory').time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $data = $_POST['image'];
            // $image_array_1 = explode(";", $data);
            // $image_array_2 = explode(",", $image_array_1[1]);
            // $data = base64_decode($image_array_2[1]);
            $destination = $this->compressImage($data, $current_photo_path, 75);
            // file_put_contents($current_photo_path, $data);

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $photo->getUrl();
            if(is_file($existing_img) && file_exists($existing_img)) {
                unlink($existing_img);
            }

            $photo->setUrl($short_web_photo_directory);
            $photo->setThumbnail($short_web_photo_directory);
            $this->save($photo);
            return new JsonResponse(['short' => $short_web_photo_directory, 'url' => $toPage, 'destination' => $destination]);

        }
        // return new JsonResponse("success");
    }

    #[Route(path: '/resize/crop/and/replace/{url}', name: 'resize_crop_and_overwrite')]
    public function resizeCropAndReplaceImage(BlogRepository $blogRepo, CourseRepository $courseRepo, WebPhotosRepository $webPhotosRepo, $url)
    {
        $photo = null;
        $blog = null;
        $course = null;
        $file = $_POST;
        $path_to_save = 'web_photos';
        $thisfiletype = $file['thisfiletype'];
        $id = $file['thisId'];
        $toPage = $file['toPage'];
        $entity = $file['entity'];
        if($entity == 'blog') {
            $blog = $blogRepo->findOneById($id);
            $photo = $blog->getImage();
        }
        if($entity == 'course') {
            $course = $courseRepo->findOneById($id);
            $photo = $course->getImage();
        }

        if($file) {

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $short_web_photo_directory = $this->getParameter('short_web_photo_directory').time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $data = $_POST['image'];
            // $image_array_1 = explode(";", $data);
            // $image_array_2 = explode(",", $image_array_1[1]);
            // $data = base64_decode($image_array_2[1]);
            $destination = $this->compressImage($data, $current_photo_path, 75);
            // file_put_contents($current_photo_path, $data);
            $partsOfUrl = explode('/', (string) $destination);
            $imageName = array_pop($partsOfUrl);

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete. 'site/images/' . $photo;
            if(is_file($existing_img) && file_exists($existing_img)) {
                unlink($existing_img);
            }

            if($entity == 'blog') {
                $blog->setImage($imageName);
                $this->save($blog);
            }
            if($entity == 'course') {
                $course->setImage($imageName);
                $this->save($course);
            }

            return new JsonResponse(['short' => $short_web_photo_directory, 'url' => $toPage, 'destination' => $destination, 'existing' => $existing_img]);

        }
        // return new JsonResponse("success");
    }

    #[Route(path: '/photo/uploader/for/future/', name: 'upload_photo_for_future_use')]
    public function savePhotoFile()
    {
        $file = $_POST;
        $path_to_save = 'uploads';
        $thisfiletype = $file['thisfiletype'];
        $photoUpload = new PhotoUpload();
        if($file) {

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $short_web_photo_directory = $this->getParameter('short_uploads_directory').time().$this->getUser()->getId().'.'.explode("/", (string) $thisfiletype)[1];
            $data = $_POST['image'];
            // $image_array_1 = explode(";", $data);
            // $image_array_2 = explode(",", $image_array_1[1]);
            // $data = base64_decode($image_array_2[1]);
            $destination = $this->compressImage($data, $current_photo_path, 75);
            // file_put_contents($current_photo_path, $data);

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $photoUpload->getUrl();
            if(is_file($existing_img) && file_exists($existing_img)) {
                unlink($existing_img);
            }

            $photoUpload->setUrl($short_web_photo_directory);
            $photoUpload->setName($short_web_photo_directory);
            $photoUpload->setUser($this->getUser());
            $this->save($photoUpload);
            return new JsonResponse(['short' => $short_web_photo_directory, 'destination' => $destination]);

        }
        // return new JsonResponse("success");
    }

    public function compressImage($source, $destination, $quality)
    {
        // Get image info
        $imgInfo = getimagesize($source);
        $mime = $imgInfo['mime'];

        $image = match ($mime) {
            'image/jpeg' => imagecreatefromjpeg($source),
            'image/png' => imagecreatefrompng($source),
            'image/gif' => imagecreatefromgif($source),
            default => imagecreatefromjpeg($source),
        };

        // Save image
        imagejpeg($image, $destination, $quality);

        // Return compressed image
        return $destination;
    }

    #[Route(path: '/upload/website/photo', name: 'upload_web_photo')]
    public function saveWebImage(WebPhotosRepository $webPhotosRepo)
    {
        $file = $_FILES;
        $post = $_POST;
        $path_to_save = 'web_photos';
        $imageId = $_POST['imageId'];
        $toPage = $_POST['toPage'];
        $image = $webPhotosRepo->findOneById($imageId);
        $partsOfUrl = explode('/', (string) $image->getUrl());
        $imageName = array_pop($partsOfUrl);
        $path = implode("/", $partsOfUrl);
        $msg = "";
        $resizeImageUrl = "";

        $target_dir = $this->getParameter('web_photos');
        $short_target_dir = $this->getParameter('short_web_photo_directory');
        // $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $target_file = $target_dir . $imageName;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo((string) $target_file, PATHINFO_EXTENSION));

        $newpath = $short_target_dir.basename((string) $_FILES["image"]["name"]);
        $newfilepath = str_replace("//", "/", (string) ($target_dir . basename((string) $_FILES["image"]["name"])));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            unlink($target_file);
            $msg .= "Current file deleted.";
            $uploadOk = 1;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 1_000_000) {
            $msg .= "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
            $msg .= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $msg .= "Sorry, your file was not uploaded.";
            $resizeImageUrl = "#";
        // if everything is ok, try to upload file
        } else {

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $newfilepath)) {
                $msg .= "The file ". htmlspecialchars(basename((string) $_FILES["image"]["name"])). " has been uploaded.";
                $image->setUrl($newpath);
                $image->setThumbnail($newpath);
                $this->save($image);
                // url to redirect to after upload
                $resizeImageUrl = $this->generateUrl('resize_uploaded_web_photo', ['photo_id' => $image->getId(), 'toPage' => $toPage ], UrlGeneratorInterface::ABSOLUTE_URL);
            // $resizeImageUrl = "#";
            } else {
                $msg .= "Sorry, there was an error uploading your file.";
                $resizeImageUrl = "#";
            }
        }

        return new JsonResponse(['src' => $newpath, 'url' => $resizeImageUrl, 'msg' => $msg, 'target' => $target_file]);
    }

    #[Route(path: '/give/duration/from/package/id', name: 'check_duration')]
    public function checkDuration(Request $request, PackageRepository $packageRepo)
    {
        $package_id = $request->request->get('package_id');
        $package = $packageRepo->findOneById($package_id);
        $duration = $package->getDuration();

        return new JsonResponse($duration);
    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }
    #[Route(path: '/save/user/bio', name: 'savebio')]
    public function savebio(UserRepository $userRepo, Request $request): Response
    {
        $userdata = null;
        // $user = $this->getUser();
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
        }

        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');

        $user->setFullname($fullname);
        if($user->getUsertype() != 'admin') {
            $userdata->setPhone($phone);
            $user->setEmail($email);
            if($age) {
                $userdata->setAge($age);
            }
            $userdata->setSex($sex);

        }

        $this->save($user);
        if($user->getUsertype() != 'admin') {
            $this->save($userdata);
        }


        return new JsonResponse($_POST);

    }

    #[Route(path: '/save/student/from/admin', name: 'savestudentfromadmin')]
    public function savestudentfromadmin(Request $request, InstrumentRepository $instrumentRepo, UserPasswordHasherInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $user = new User();
        $userdata = new StudentUserData();
        $entityManager = $this->managerRegistry->getManager();

        $instruments = $_POST['instrument']; // array
        $grades = $_POST['grade']; // array
        $branch = $request->request->get('branch');
        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinfullname');
        $kinphone = $request->request->get('kinphone');
        $kinrship = $request->request->get('kinrship');
        $lessonmode = $request->request->get('lessonmode');
        $siblingname = $request->request->get('sibling_name');

        $user->setPassword(
            $passwordEncoder->hashPassword(
                $user,
                $phone
            )
        );

        $user->setActive(false);
        $user->setFullname($fullname);
        $user->setEmail($email);
        $user->setUsertype("student");
        $entityManager->persist($user);

        $userdata->setPhone($phone);
        $userdata->setAge($age);
        $userdata->setSex($sex);
        $userdata->setBranch($branch);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinrship($kinrship);
        $userdata->setLessonmode($lessonmode);
        $entityManager->persist($userdata);

        foreach ($instruments as $key => $ins) {
            $instrument = $instrumentRepo->findOneById($ins);
            $userInstrument = new UserInstrumentGrade();
            $userInstrument->setStudentUserData($userdata);
            $userInstrument->setGrade($instrumentGradeRepo->findOneById($grades[$key]));
            $userInstrument->setInstrument($instrument);
            $userInstrument->setSiblingname($siblingname);
            $entityManager->persist($userInstrument);

            $userdata->addUserInstruments($userInstrument);
            $entityManager->persist($userdata);
        }

        $user->setStudentdata($userdata);
        $entityManager->persist($user);

        // activate student
        $active = $user->getActive();
        $uigActives = $user->getStudentdata()->getUserInstruments();

        $activeText = "";
        if($active) {
            $user->setActive(false);
            foreach ($uigActives as $uigActive) {
                $uigActive->setActive(false);
                $entityManager->persist($uigActive);
            }
            $activeText = "Inactive";
        } else {
            $user->setActive(true);
            foreach ($uigActives as $uigActive) {
                $uigActive->setActive(true);
                $entityManager->persist($uigActive);
            }
            $activeText = "Active";
        }


        $entityManager->persist($user);

        $entityManager->flush();

        $this->addFlash(
            'success',
            'An account was created for ' . $user->getFullname() . ". You can assign them a teacher from here.",
        );

        return $this->redirectToRoute('admin-teachers-list');
        // return $this->render('default/test.html.twig', [
        //     'test' => $userdata,
        // ]);

        // return new JsonResponse($userdata);

    }

    #[Route(path: '/edit/student/from/admin', name: 'editstudentfromadmin')]
    public function editstudentfromadmin(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordHasherInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->managerRegistry->getManager();

        $instruments = $_POST['instrument']; // array
        $grades = $_POST['grade']; // array
        $branch = $request->request->get('branch');
        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinfullname');
        $kinphone = $request->request->get('kinphone');
        $kinrship = $request->request->get('kinrship');
        $lessonmode = $request->request->get('lessonmode');
        $siblingname = $request->request->get('sibling_name');

        $user->setFullname($fullname);
        $user->setEmail($email);
        $entityManager->persist($user);

        $userdata->setPhone($phone);
        $userdata->setAge($age);
        $userdata->setSex($sex);
        $userdata->setBranch($branch);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinrship($kinrship);
        $userdata->setLessonmode($lessonmode);
        $entityManager->persist($userdata);
        $userInstruments = $user->getStudentdata()->getUserInstruments();
        $idarrayuig = [];
        $test = [];

        foreach($userInstruments as $uig) {
            $idarrayuig[] = $uig->getInstrument()->getId();
        }

        foreach ($instruments as $key => $ins) {

            $instrument = $instrumentRepo->findOneById($ins);

            if (!in_array($ins, $idarrayuig)) {
                $test[] = 'not in array';
                $userInstrument = new UserInstrumentGrade();
                $userInstrument->setStudentUserData($userdata);
                $userInstrument->setGrade($instrumentGradeRepo->findOneById($grades[$key]));
                $userInstrument->setInstrument($instrument);
                $userInstrument->setSiblingname($siblingname);
                $entityManager->persist($userInstrument);

                $userdata->addUserInstruments($userInstrument);
                $entityManager->persist($userdata);
            } else {
                $test[] = 'in array';
                $userInstrument = $uigRepo->findOneByInstrument($instrument);
                $userInstrument->setSiblingname($siblingname);
                $entityManager->persist($userInstrument);
            }

        }

        $user->setStudentdata($userdata);
        $entityManager->persist($user);

        // activate student
        $uigActives = $user->getStudentdata()->getUserInstruments();

        foreach ($uigActives as $uigActive) {
            $uigActive->setActive(true);
            $entityManager->persist($uigActive);
        }

        $entityManager->flush();

        $this->addFlash(
            'success',
            $user->getFullname() . "'s account was successfully edited.",
        );

        return $this->redirectToRoute('admin-teachers-list');

        // return new JsonResponse($test);

    }

    #[Route(path: '/add/sibling/from/admin', name: 'addsiblingfromadmin')]
    public function addSiblingUig(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordHasherInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->managerRegistry->getManager();

        $instrument_id = $request->request->get('myinstrument');
        $grade_id = $request->request->get('mygrade');
        $siblingname = $request->request->get('siblingname');

        $instrument = $instrumentRepo->findOneById($instrument_id);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($instrumentGradeRepo->findOneById($grade_id));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $userInstrument->setSiblingname($siblingname);
        $entityManager->persist($userInstrument);

        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $entityManager->flush();

        $this->addFlash(
            'success',
            'A sibling account was created for ' . $user->getFullname() . ". You can assign them a teacher from here.",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($siblingname);

    }

    #[Route(path: '/add/term/from/admin', name: 'addtermfromadmin')]
    public function addTermUig(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordHasherInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->managerRegistry->getManager();

        $instrument_id = $request->request->get('myinstrument');
        $grade_id = $request->request->get('mygrade');

        $instrument = $instrumentRepo->findOneById($instrument_id);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($instrumentGradeRepo->findOneById($grade_id));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $entityManager->persist($userInstrument);

        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $entityManager->flush();

        $this->addFlash(
            'success',
            $user->getFullname() . " Can now be assigned more sessions",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($grade_id);

    }

    #[Route(path: '/save/user/nextofkin', name: 'savenextofkin')]
    public function savenextofkin(UserRepository $userRepo, Request $request): Response
    {
        $userdata = null;
        // $user = $this->getUser();
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
        }

        $kinphone = $request->request->get('kinphone');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinfullname');
        $kinrship = $request->request->get('kinrship');

        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinrship($kinrship);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }

    #[Route(path: '/save/user/academic', name: 'saveacademic')]
    public function saveacademic(UserRepository $userRepo, Request $request): Response
    {
        $userdata = null;
        // $user = $this->getUser();
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
            $resumelnk = $request->request->get('resumelnk');
            $userdata->setResumelnk($resumelnk);
        }


        $branch = $request->request->get('branch');
        $lessonmode = $request->request->get('lessonmode');

        $userdata->setLessonmode($lessonmode);
        $userdata->setBranch($branch);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }

    #[Route(path: '/save/student/note', name: 'savestudentnote')]
    public function savenote(Request $request, UserInstrumentGradeRepository $uigRepo, NotesRepository $notesRepo): Response
    {

        $data = [];
        $uig_id = $request->request->get('uig_id');
        $message = $request->request->get('note_for_'.$uig_id);

        $uig = $uigRepo->findOneById($uig_id);
        $student = $uig->getStudentUserData()->getStudent();
        $note = new Notes();
        $note->setMessage($message);
        $note->setUser($student);
        $note->setCreatedOn(new \DateTime());
        $note->setUig($uig);

        $this->save($note);

        $notes = $notesRepo->findBy(
            ['uig' => $uig],
            ['id' => 'desc'],
        );

        $data['noteslength'] = count($notes);

        return new JsonResponse($data);

    }

    #[Route(path: '/view/student/notes', name: 'viewstudentnotes')]
    public function viewnotes(Request $request, UserInstrumentGradeRepository $uigRepo, NotesRepository $notesRepo): Response
    {

        $data = [];
        $uig_id = $request->request->get('uig_id');
        $message = "<div>";

        $uig = $uigRepo->findOneById($uig_id);
        $student = $uig->getStudentUserData()->getStudent()->getFullname();
        $notes = $notesRepo->findBy(
            ['uig' => $uig],
            ['id' => 'DESC'],
            20
        );
        foreach ($notes as $note) {
            $message .= $note->getCreatedOn()->format('jS M Y') . "<br />";
            $message .= $note->getMessage() . "<hr />";
        }
        // $this->save($note);

        $message .= "</div>";
        $data['message'] = nl2br($message);
        $data['student'] = $student;
        $data['uig'] = $this->getUigString($uig);


        return new JsonResponse($data);

    }

    #[Route(path: '/view/student/user/notes', name: 'view_user_notes')]
    public function viewUserNotes(Request $request, UserRepository $userRepo, NotesRepository $notesRepo): Response
    {

        $data = [];
        $user_id = $request->request->get('user_id');
        $message = "<div>";

        $user = $userRepo->findOneById($user_id);
        $student = $user->getFullname();
        $notes = $notesRepo->findBy(
            ['user' => $user],
            ['id' => 'DESC'],
            20
        );
        foreach ($notes as $note) {
            $message .= "<h3>" . $this->getUigString($note->getUig()) . "</h3>";
            $message .= $note->getCreatedOn()->format('jS M Y') . "<br />";
            $message .= $note->getMessage() . "<hr />";
        }
        // $this->save($note);

        $message .= "</div>";
        $data['message'] = nl2br($message);
        $data['student'] = $student;


        return new JsonResponse($data);

    }

    public function getUigString($uig)
    {
        if($uig->getStudentUserData() !== null && $uig->getStudentUserData()->getStudent() !== null && ($uig->getSiblingname() == null || $uig->getSiblingname() == "")) {
            return $uig->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $uig->getInstrument()->getName();//' . ' ( ' . $uig->getGrade()->getName() . ' ) ';
        } elseif($uig->getSiblingname() != null && $uig->getSiblingname() != "") {
            return $uig->getSiblingname() . ' - ' .  $uig->getInstrument()->getName();// . ' ( ' . $uig->getGrade()->getName() . ' ) ';
        } else {
            return $uig->getInstrument()->getName();
        }

    }
    #[Route(path: '/calendar/change/date', name: 'calendar_change_date')]
    public function calendarChangeDate(Request $request, SessionRepository $sessionRepo): Response
    {

        $year = $request->request->get('year');
        $month = $request->request->get('month');
        $day = $request->request->get('date');
        $url = $request->request->get('url');
        $hour = $request->request->get('hour');
        $minute = $request->request->get('minute');
        $id = explode("/", $url)[3];

        $session = $sessionRepo->findOneById($id);
        // $startinghour = $session->getBeginAt()->format('H');
        // $startingminute = $session->getBeginAt()->format('i');
        // $endinghour = $session->getEndAt()->format('H');
        // $endingminute = $session->getEndAt()->format('i');
        $duration = "";
        switch ($session->getPackage()->getDuration()) {
            case '60 Minutes':
                $duration = "+1 hour";
                break;
            case '45 Minutes':
                $duration = "+45 minutes";
                break;
            case '30 Minutes':
                $duration = "+30 minutes";
                break;
        }

        $newdate = new \DateTime();
        $newdate->setDate($year, $month + 1, $day);
        $newdate->setTime($hour, $minute);

        $varenddate = clone $newdate;
        $endinghr = $varenddate->modify($duration);

        $session->setBeginAt($newdate);
        $session->setEndAt($endinghr);

        $startingOn = clone $newdate;
        $session->setStartingOn($startingOn);

        $this->save($session);

        return new JsonResponse($duration);

    }

    #[Route(path: '/validity/check/session/schedule', name: 'check_schedule')]
    public function checkValidityOfDateAndTime(Request $request, SessionRepository $sessionRepo, UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo)
    {

        $dayDateTimeEnd = null;
        // get available days in schedules for this teacher as intervals
        // get all the lessons for this teacher in intervals
        // if teacher is not available on the given day, return
        // if teacher is available on given day but not available on the given time, return
        // if teacher has lesson on given day and time, return
        $dayDateStart = new \Datetime($request->request->get('startingOn'));
        $dayDateEnd = clone $dayDateStart;


        $dayInt = $dayDateStart->format('N');
        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $schedules = $teacher->getSchoolTimes();
        $responses = [];
        $message = "";
        $target = "";

        $arr = [];
        foreach ($schedules as $key => $schedule) {
            $arr[] = $schedule->getJsdaycode();
        }
        $thisDay = (int)$dayInt - 1;

        $availableSchedules = array_unique($arr);

        //if teacher is not available on the day
        if(!in_array($thisDay, $availableSchedules)) {
            $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available on " . $dayDateStart->format('l') . "s</div>";
            $target = "feedback-startingOn";
            $responses[] = ['target' => $target, 'message' => $message];
        } else {
            $message = "<div class='alert alert-success'>" . $teacher->getFullname() . " is available on " . $dayDateStart->format('l') . "s</div>";
            $target = "feedback-startingOn";
            $responses[] = ['target' => $target, 'message' => $message];
        }

        // $sessions = $teacher->getTeachersessions();
        $sessions = $sessionRepo->findFutureLessons($dayDateStart, $teacher);

        if($request->request->get('beginAt')) {
            $hourBegin = explode(":", $request->request->get('beginAt'))[0];
            $minuteBegin = explode(":", $request->request->get('beginAt'))[1];
            $dayDateTimeBegin = $dayDateStart->setTime($hourBegin, $minuteBegin);
            $dayDateTimeBeginStr = $hourBegin.$minuteBegin;
        }
        if($request->request->get('endAt')) {
            $hourEnd = explode(":", $request->request->get('endAt'))[0];
            $minuteEnd = explode(":", $request->request->get('endAt'))[1];
            $dayDateTimeEnd = $dayDateEnd->setTime($hourEnd, $minuteEnd);
            $dayDateTimeEndStr = $hourEnd.$minuteEnd;
        }
        $test = null;
        // echo 'test';
        //if teacher has not arrived
        if(isset($dayDateTimeBegin)) {
            $theDaySelected = $dayDateTimeBegin->format('l');
            $matchingDay = $schoolTimeRepo->findOneBy(
                ['day' => $theDaySelected, 'teacher' => $teacher],
                ['id' => 'ASC'],
            );

            if ($matchingDay) {
                $teacherArrivesAt = $matchingDay->getBeginAt();
                $teacherDepartingTime = $matchingDay->getEndAt();

                if($dayDateTimeBegin->format('Hi') < $teacherArrivesAt->format('Hi')) {
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available before ".$teacherArrivesAt->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-beginAt";
                    $responses[] = ['target' => $target, 'message' => $message];
                } elseif ($dayDateTimeBegin->format('Hi') > $teacherDepartingTime->format('Hi')) {
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available after " .$teacherDepartingTime->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-beginAt";
                    $responses[] = ['target' => $target, 'message' => $message];
                } elseif ($dayDateTimeEnd->format('Hi') > $teacherDepartingTime->format('Hi')) {
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available after " .$teacherDepartingTime->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-endAt";
                    $responses[] = ['target' => $target, 'message' => $message];
                } else {
                    foreach ($sessions as $key => $session) {
                        if(isset($dayDateTimeBegin) && $dayDateTimeBegin->format('l') == $session->getBeginAt()->format('l')) {
                            // if comes after existing lesson and diff btw both beginnings is less than existing lesson duration
                            // or beginning is less than beginning of existing lesson and end is greater than beginning of existing lesson
                            $comesAfterExistingLesson = $dayDateTimeBegin->format('Hi') > $session->getBeginAt()->format('Hi');
                            $comesSameAsExistingLesson = $dayDateTimeBegin->format('Hi') == $session->getBeginAt()->format('Hi');
                            $intervalBetweenBeginnings = abs($dayDateTimeBegin->format('Hi') - $session->getBeginAt()->format('Hi')) - 40;
                            $existingLessonDifference = ($session->getEndAt()->format('Hi') - $session->getBeginAt()->format('Hi')) - 40;

                            $comesBeforeExistingLesson = $dayDateTimeBegin->format('Hi') < $session->getBeginAt()->format('Hi');

                            if($comesAfterExistingLesson && $intervalBetweenBeginnings < $existingLessonDifference) {
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be on another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-beginAt";
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } elseif ($comesBeforeExistingLesson && $dayDateTimeEndStr > $session->getBeginAt()->format('Hi')) {
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be on another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-endAt";
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } elseif ($comesSameAsExistingLesson) {
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be starting another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-beginAt";
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } else {
                                $message = "<div class='alert alert-success'> Looks good </div>";
                                $target = "feedback-beginAt";
                                $responses[] = ['target' => $target, 'message' => $message];
                                $message2 = "<div class='alert alert-success'> Looks good </div>";
                                $target2 = "feedback-endAt";
                                $responses[] = ['target' => $target2, 'message' => $message2];
                            }

                        // echo $session->getTeacher()->getFullname();

                        } else {
                            $message = "<div class='alert alert-success'> Looks good </div>";
                            $target = "feedback-beginAt";
                            $responses[] = ['target' => $target, 'message' => $message];
                            $message2 = "<div class='alert alert-success'> Looks good </div>";
                            $target2 = "feedback-endAt";
                            $responses[] = ['target' => $target2, 'message' => $message2];
                        }

                    }

                }

            }
        }

        return new JsonResponse($responses);

    }

    #[Route(path: '/calendar/add/schedule', name: 'add_schedule')]
    public function addSchedule(Request $request, SchoolTimeRepository $schoolTimeRepo, UserRepository $userRepo): Response
    {

        $schoolTime = null;
        $dayName = null;
        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $data = json_decode($request->request->get('data'), null, 512, JSON_THROW_ON_ERROR);
        $dbdata = $schoolTimeRepo->findByTeacher($teacher);

        $dt = [];
        $dbdt = [];

        foreach ($dbdata as $key => $value) {
            $dbdt[$value->getJsdaycode()."-".$key] = $value->getBeginAt()->format('H:i') . '-' . $value->getEndAt()->format('H:i'). '-' .$value->getJsdaycode();
        }


        $lastperiod = "";

        foreach ($data as $key => $singleday) {
            $schoolTime = new SchoolTime();
            $dayIndex = $singleday->day;
            $periods = $singleday->periods;

            foreach ($periods as $key => $period) {
                $dt[] = $period->start .'-'. $period->end . '-' . $dayIndex;
                $lastperiod = $period->start .'-'. $period->end . '-' . $dayIndex;
            }

        }


        // add to database.
        // split the string $lastperiod

        $diff = array_diff($dt, $dbdt);

        $periodStart = explode("-", reset($diff))[0];
        $periodEnd = explode("-", reset($diff))[1];
        $whichDay = explode("-", reset($diff))[2];

        $datetime = new \Datetime();
        $starttime = clone $datetime;
        $starttime->setTime(explode(":", $periodStart)[0], explode(":", $periodStart)[1]);

        $endtime = clone $datetime;
        $endtime->setTime(explode(":", $periodEnd)[0], explode(":", $periodEnd)[1]);

        switch ($whichDay) {
            case 0:
                $dayName = "Monday";
                break;
            case 1:
                $dayName = "Tuesday";
                break;
            case 2:
                $dayName = "Wednesday";
                break;
            case 3:
                $dayName = "Thursday";
                break;
            case 4:
                $dayName = "Friday";
                break;
            case 5:
                $dayName = "Saturday";
                break;
            case 6:
                $dayName = "Sunday";
                break;

        }

        $schoolTime->setBeginAt($starttime);
        $schoolTime->setEndAt($endtime);
        $schoolTime->setDay($dayName);
        $schoolTime->setJsdaycode($whichDay);
        $schoolTime->setTeacher($teacher);
        $schoolTime->setTitle($whichDay."-".$periodStart."-".$periodEnd);

        $this->save($schoolTime);

        return new JsonResponse($dayName . " " . $periodStart . " - " . $periodEnd);

    }

    #[Route(path: '/calendar/remove/schedule', name: 'remove_schedule')]
    public function removeSchedule(Request $request, SchoolTimeRepository $schoolTimeRepo): Response
    {

        $title = $request->request->get('title');
        $schedule = $schoolTimeRepo->findOneByTitle($title);
        $this->delete($schedule);

        return new JsonResponse($title);

    }

    #[Route(path: '/sessionuig/change/instrument', name: 'change_instrument')]
    public function changeInstrument(Request $request, UserInstrumentGradeRepository $uigRepo, InstrumentRepository $instrumentRepo): Response
    {

        $entryId = $request->request->get('entryId');
        $newInstrument = $request->request->get('newInstrument');

        $uig = $uigRepo->findOneById($entryId);
        $instrument = $instrumentRepo->findOneById($newInstrument);

        $uig->setInstrument($instrument);
        $this->save($uig);

        return new JsonResponse($uig->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $uig->getInstrument()->getName() . ' ( ' . $uig->getGrade()->getName() . ' ) ');

    }

    #[Route(path: '/sessionuig/change/grade', name: 'change_grade')]
    public function changeGrade(Request $request, UserInstrumentGradeRepository $uigRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {

        $entryId = $request->request->get('entryId');
        $newGrade = $request->request->get('newGrade');

        $uig = $uigRepo->findOneById($entryId);
        $grade = $instrumentGradeRepo->findOneById($newGrade);

        $uig->setGrade($grade);
        $this->save($uig);

        return new JsonResponse($uig->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $uig->getInstrument()->getName() . ' ( ' . $uig->getGrade()->getName() . ' ) ');

    }

    #[Route(path: '/sessionuig/change/teacher', name: 'change_teacher')]
    public function changTeacher(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $teacherRepo): Response
    {

        $entityManager = $this->managerRegistry->getManager();
        $entryId = $request->request->get('entryId');
        $newTeacher = $request->request->get('newTeacher');
        $teacher = $teacherRepo->findOneById($newTeacher);

        $uig = $uigRepo->findOneById($entryId);
        $sessions = $uig->getSessions();
        foreach ($sessions as $session) {
            $session->setTeacher($teacher);
            $entityManager->persist($session);
        }


        $entityManager->flush();

        return new JsonResponse();

    }

    #[Route(path: '/sessionuig/change/package', name: 'change_package')]
    public function changePackage(Request $request, UserInstrumentGradeRepository $uigRepo, PackageRepository $packageRepo): Response
    {

        $entityManager = $this->managerRegistry->getManager();
        $entryId = $request->request->get('entryId');
        $newPackage = $request->request->get('newPackage');
        $package = $packageRepo->findOneById($newPackage);

        $uig = $uigRepo->findOneById($entryId);
        $sessions = $uig->getSessions();
        foreach ($sessions as $session) {
            $session->setPackage($package);
            $entityManager->persist($session);
        }


        $entityManager->flush();

        return new JsonResponse();

    }

    #[Route(path: '/sessionuig/change/siblingname', name: 'change_siblingname')]
    public function changeSiblingname(Request $request, UserInstrumentGradeRepository $uigRepo): Response
    {

        $siblingname = $request->request->get('siblingname');
        $uigid = $request->request->get('uigid');

        $uig = $uigRepo->findOneById($uigid);
        $uig->setSiblingname($siblingname);
        $this->save($uig);
        return new JsonResponse($uigid);

    }

    #[Route(path: '/sessionuig/change/time', name: 'change_time')]
    public function changeTime(Request $request, SessionRepository $sessionRepo): Response
    {
        // get starttime from form vars
        $startTime = $request->request->get('startTime');
        // get name of day of clicked date
        $startTimeSessDay = $request->request->get('startTimeSessDay');

        // manipulate the entries
        $sthr = explode(":", $startTime)[0];
        $stmin = explode(":", $startTime)[1];

        // do the same for endtime
        $endTime = $request->request->get('endTime');
        $ethr = explode(":", $endTime)[0];
        $etmin = explode(":", $endTime)[1];

        // get the id of this particular session
        $session = $sessionRepo->findOneById($request->request->get('session'));

        // get the uig associated with this session
        $uig = $session->getUserInstrumentGrade();

        // get all the sessions that belong with this session
        $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);

        $count = 0;
        $test = [];
        foreach ($sessionsForThisUIG as $session) {
            // check to see if the session in iteration is of same day as this startTimeSessDay
            if($session->getStartingOn()->format('l') == $startTimeSessDay) {
                $beginAt = $session->getBeginAt();
                $endAt = $session->getEndAt();

                $newStartTime = clone $beginAt;
                $newEndTime = clone $endAt;
                $newStartTime->setTime($sthr, $stmin);
                $newEndTime->setTime($ethr, $etmin);

                $session->setBeginAt($newStartTime);
                $session->setEndAt($newEndTime);

                $this->save($session);
                $count += 0;

            }
            $test[] = $session->getStartingOn()->format('l');
        }

        return new JsonResponse($test);

    }

    #[Route(path: '/sessionuig/change/times', name: 'change_times')]
    public function changeTimes(Request $request, SessionRepository $sessionRepo): Response
    {
        // get starttime from form vars
        $startTime = $request->request->get('startTime');
        // get date from form vars
        $sessionDate = $request->request->get('sessionDate');
        $sessionDatetime = new \Datetime($sessionDate);
        // get name of day of clicked date
        $sessid = $request->request->get('sessid');

        // manipulate the entries
        $sthr = explode(":", $startTime)[0];
        $stmin = explode(":", $startTime)[1];

        // do the same for endtime
        $endTime = $request->request->get('endTime');
        $ethr = explode(":", $endTime)[0];
        $etmin = explode(":", $endTime)[1];

        // do the same for date
        $stday = explode("-", $sessionDate)[0];
        $stmon = explode("-", $sessionDate)[1];
        $styr = explode("-", $sessionDate)[2];

        // get the id of this particular session
        $session = $sessionRepo->findOneById($sessid);

        $beginAt = $session->getBeginAt();
        $endAt = $session->getEndAt();
        $date = $session->getStartingOn();

        $newStartTime = clone $beginAt;
        $newEndTime = clone $endAt;
        $newDate = clone $sessionDatetime;
        $newStartTime->setTime($sthr, $stmin);
        $newEndTime->setTime($ethr, $etmin);
        $newStartTime->setDate($stday, $stmon, $styr);
        $newEndTime->setDate($stday, $stmon, $styr);


        $session->setBeginAt($newStartTime);
        $session->setEndAt($newEndTime);
        $session->setStartingOn($newDate);

        $this->save($session);


        return new JsonResponse($session->getId());

    }

    #[Route(path: '/sessionuig/change/dates', name: 'change_dates')]
    public function changeDates(Request $request, SessionRepository $sessionRepo, TermRepository $termRepo): Response
    {

        $test = [];
        // $test = [];
        $days = [];
        // the new date string
        $newdate = $request->request->get('newdate');
        $halftermandholidays = $request->request->get('halftermandholidays');
        // the new datetime
        $newDateTime = new \DateTime($newdate);
        // find the session
        $session = $sessionRepo->findOneById($request->request->get('session'));
        $numberofsessionsperweek = $request->request->get('daysLength');
        // find the uig for this session
        $uig = $session->getUserInstrumentGrade();
        // find the sessions that go along with this session
        $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);
        // $test = $newDateTime->format('l Y-m-d');
        $entityManager = $this->managerRegistry->getManager();

        $currentTerm = $termRepo->findOneByTermnumber($termRepo->findCurrentTermArr($newDateTime));
        $termnumber = $currentTerm->getTermnumber();
        $halftermstart = $currentTerm->getHalftermstart();
        $halftermend = $currentTerm->getHalftermend();
        $holidaystart = $currentTerm->getHolidaystart();
        $holidayend = $currentTerm->getHolidayend();
        $days = $_POST['days'];
        $daysperweek = is_countable($days) ? count($days) : 0;
        $iterations = (is_countable($sessionsForThisUIG) ? count($sessionsForThisUIG) : 0) / $daysperweek;
        $multipliedby = (is_countable($sessionsForThisUIG) ? count($sessionsForThisUIG) : 0) / $session->getPackage()->getNoOfSessions();
        // $multipliedby = count($sessionsForThisUIG) / 12;
        $startingDate = $newDateTime;
        $otherdate = clone $startingDate;
        $counter = 1;
        $test[] = [$halftermstart, $halftermend];
        for ($i = 0; $i < $iterations; $i++) {
            foreach ($days as $key => $day) {
                if($counter <= (12 * $multipliedby)) {
                    $halftermbegins = date('Y-m-d', strtotime((string) $halftermstart->format('m/d/Y')));
                    $halftermends = date('Y-m-d', strtotime((string) $halftermend->format('m/d/Y')));
                    $holidaybegins = date('Y-m-d', strtotime((string) $holidaystart->format('m/d/Y')));
                    $holidayends = date('Y-m-d', strtotime((string) $holidayend->format('m/d/Y')));

                    if($counter > 1) {
                        $otherdate->modify("next $day");
                        $test[] = 'said next '.$day;

                    } elseif($counter == 1) {
                        $otherdate->modify("$day");
                        $test[] = 'said this '.$day;

                    }
                    $otherdatestr = date('Y-m-d', strtotime($otherdate->format('Y-m-d')));
                    // $test[] = $otherdatestr .'.'.$halftermbegins . '.'.$halftermends;

                    if (($otherdatestr >= $halftermbegins) && ($otherdatestr <= $halftermends && $halftermandholidays == "skip")) {
                        $test[] = 'halfterm: '.$otherdate->format('l Y-m-d');
                        $iterations++;
                    } elseif (($otherdatestr > $holidaybegins) && ($otherdatestr < $holidayends && $halftermandholidays == "skip")) {
                        $test[] = 'holiday: '.$otherdate->format('l, Y-m-d');
                        $iterations++;
                    } else {
                        $sess = $sessionsForThisUIG[$counter - 1];

                        $begin = clone $sess->getBeginAt();
                        $begin->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                        $end = clone $sess->getEndAt();
                        $end->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                        $startdate = clone $sess->getStartingOn();
                        $startdate->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));

                        $sess->setBeginAt($begin);
                        $sess->setEndAt($end);
                        $sess->setStartingOn($startdate);
                        $entityManager->persist($sess);

                        $test[] = $otherdate->format('l, Y-m-d');
                        $counter++;
                    }


                }

            }
        }
        $entityManager->flush();

        return new JsonResponse($currentTerm);

    }

    #[Route(path: '/sessionuig/organize/all/dates', name: 'organize_all_dates')]
    public function organizeAllDates(UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo, TermRepository $termRepo): Response
    {
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        set_time_limit(300);
        $test = [];

        $sessions = [];
        foreach ($uigRepo->findAll() as $uigkey => $uig) {
            foreach($uig->getSessions() as $session) {
                $sessions[$uig->getId()] = $session;
            }
        }
        foreach ($sessions as $uigid => $session) {
            // $test[] = $session;
            $days = [];
            // the new date string
            // $newdate = $request->request->get('newdate');
            // the new datetime
            // find the uig for this session
            $uig = $session->getUserInstrumentGrade();
            // find the sessions that go along with this session
            $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);
            $newDateTime = $sessionsForThisUIG[0]->getStartingOn();
            // $test[] = $newDateTime->format('l, Y-m-d');

            foreach ($sessionsForThisUIG as $s) {
                $days[$s->getStartingOn()->format('l')] = $s->getStartingOn()->format('l');
            }
            // $test = $newDateTime->format('l Y-m-d');
            $entityManager = $this->managerRegistry->getManager();

            $currentTerm = $termRepo->findOneByTermnumber($termRepo->findCurrentTermArr($newDateTime));
            $halftermstart = $currentTerm->getHalftermstart();
            $halftermend = $currentTerm->getHalftermend();
            $holidaystart = $currentTerm->getHolidaystart();
            $holidayend = $currentTerm->getHolidayend();
            $daysperweek = count($days);
            $iterations = 12 / $daysperweek;
            $startingDate = $newDateTime;
            $otherdate = clone $startingDate;
            $counter = 0;
            // $test[] = [$halftermstart, $halftermend];
            for ($i = 0; $i < $iterations; $i++) {
                foreach ($days as $key => $day) {
                    if($counter <= 11) {
                        $otherdatestr = date('Y-m-d', strtotime((string) $otherdate->format('Y-m-d')));
                        $halftermbegins = date('Y-m-d', strtotime((string) $halftermstart->format('m/d/Y')));
                        $halftermends = date('Y-m-d', strtotime((string) $halftermend->format('m/d/Y')));
                        $holidaybegins = date('Y-m-d', strtotime((string) $holidaystart->format('m/d/Y')));
                        $holidayends = date('Y-m-d', strtotime((string) $holidayend->format('m/d/Y')));

                        if (($otherdatestr >= $halftermbegins) && ($otherdatestr <= $halftermends)) {
                            $test[] = 'halfterm: '.$otherdate->format('l Y-m-d');
                            $iterations++;
                        } elseif (($otherdatestr > $holidaybegins) && ($otherdatestr < $holidayends)) {
                            $test[] = 'holiday: '.$otherdate->format('l, Y-m-d');
                            $iterations++;
                        } else {
                            if(isset($sessionsForThisUIG[$counter])) {
                                $sess = $sessionsForThisUIG[$counter];

                                $begin = clone $sess->getBeginAt();
                                $begin->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                                $end = clone $sess->getEndAt();
                                $end->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                                $startdate = clone $sess->getStartingOn();
                                $startdate->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));

                                $sess->setBeginAt($begin);
                                $sess->setEndAt($end);
                                $sess->setStartingOn($startdate);
                                $entityManager->persist($sess);

                                $test[] = $session->getStudent().'-'.$otherdate->format('l, Y-m-d');
                                $counter++;

                            }
                        }



                        $otherdate->modify("next $day");

                    }

                }
            }

            $entityManager->flush();
        }

        // return new JsonResponse($test);
        return $this->render('default/test2.html.twig', [
            'statements' => $test,
        ]);


    }

    #[Route(path: '/teacher/attendance/manage', name: 'manage_attendance')]
    public function manageAttendance(Request $request, InvoiceService $invoiceService, SessionRepository $sessionRepo, UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo): Response
    {

        $handle = $request->request->get('handle');
        $uigId = explode("-", $handle)[1];
        $sessionId = explode("-", $handle)[2];

        // $userInstrumentGrade = $userInstrumentGradeRepo->findOneById($uigId);
        $session = $sessionRepo->findOneById($sessionId);
        $saved = $session->getDone() == true ? true : false;
        $return = '';
        $modify = '';
        $student = $session->getStudent();

        if($saved) {
            $session->setDone(false);
            $dt = $session->getStartingOn();
            $return = "<small>" .$dt->format('M jS') . " <br /> <i class='fa fa-close text-warning'></i></small>";
            $modify = '-';
        } else {
            if (stripos((string) $student->getFullname(), 'school') !== false || stripos((string) $student->getFullname(), 'church') !== false){
                $session->setDone(true);
                $dt = $session->getStartingOn();
                $return = "<small>" . $dt->format('M jS') . " <br /> <i class='fa fa-check text-success'></i></small>";
                $modify = '+';
            } else {


                $admin = $userRepo->findOneByEmail("chezamusicschool@gmail.com");
                $admins = [
                    $userRepo->findOneByEmail("oumabenjah018@gmail.com"),
                    $userRepo->findOneByEmail("chezamusicschool@gmail.com")
                ];
    
                // get package
                $package = $session->getPackage();
                // get price per session
                $pricePerSession = $package->getCostPerLesson();
                // get uid and see how many are done
                $uig = $uigRepo->findOneById($uigId);
                // number of sessions
                $sessions = $uig->getSessions();
                // count all sessions
                $number_of_sessions = count($sessions);
                // calculate done lessons
                $doneLessons = 0;
                foreach ($sessions as $key => $s) {
                    if ($s->getDone() == true) {
                        $doneLessons++;
                    } 
                }
                $current_lesson = $doneLessons + 1;
                $payments = $uig->getPayments();
                $cashPaid = 0;
                foreach ($payments as $key => $payment) {
                    if($payment->getType() == 'pmt'){
                        $cashPaid += $payment->getAmount();
                    }
                }
    
                if(null == $session->getLessonPlan()){
                    $session->setDone(false);
                    $dt = $session->getStartingOn();
                    $return = "<small>" .$dt->format('M jS') . " <br /> <i class='fa fa-close text-danger'></i></small>";
                    $modify = '-';
                    
                    $this->sendSms->quickSend($session->getTeacher()->getTeacherdata()->getPhone(), $session->getTeacher()->getFullname(), 'Hey ' . $session->getTeacher() . '! You didn\'t lesson plan for that', 'Hey ' . $session->getTeacher() . ' ! You did not lesson plan for that lesson that you marked as done. That is against the Cheza culture', 'necessary', 'personal_sms_from_office', $session->getTeacher()->getEmail());
                    $this->mailer->sendEmail(['user' => $session->getTeacher(), 'msg' => "You just marked a lesson as done, without having done a lesson plan before. To reinforce the culture, you will not be able to invoice for the lesson until you update it to indicate what you taught. Thanks for understanding"], $session->getTeacher()->getEmail(), 'Unable To Mark A Lesson As Attended.' , "communication.html.twig", "necessary", "personal_email_from_office");
                    $this->mailer->sendEmail(['user' => $admin, 'msg' => $session->getTeacher() . " just attempted to mark a lesson as done, without having done a lesson plan for it."], $admin->getEmail(), 'Attempt to Mark Attendance Without Lesson Plan' , "communication.html.twig", "necessary", "personal_email_from_office");
    
                } else {
                    $session->setDone(true);
                    $dt = $session->getStartingOn();
                    $return = "<small>" . $dt->format('M jS') . " <br /> <i class='fa fa-check text-success'></i></small>";
                    $modify = '+';    
                    // $this->sendSms->quickSend($session->getTeacher()->getTeacherdata()->getPhone(), $session->getTeacher()->getFullname(), 'Nice One!', 'Hello There! that was lesson planned. Nice job!', 'necessary', 'personal_sms_from_office', $session->getTeacher()->getEmail());
                    // $this->mailer->sendEmail(['user' => $session->getTeacher(), 'msg' => "Good job! we found a lesson plan for the session that you marked as done. Keep up the good job"], $session->getTeacher()->getEmail(), 'Awesome! You lesson planned!' , "communication.html.twig", "necessary", "personal_email_from_office");
                }
                $student = $session->getStudent();
                // if not a school or church
                if (stripos((string) $student->getFullname(), 'school') === false && stripos((string) $student->getFullname(), 'church') === false){
                    $this->feesNotify->notifyIfPaidFor($current_lesson, $pricePerSession, $number_of_sessions, $doneLessons, $cashPaid, $session->getTeacher(), $admins, $session->getStudent(), $session->getUserInstrumentGrade(), "attended");
                } else {
                    // do nothing
                }
    
                
            }


        }

        $this->save($session);
        $amt = $invoiceService->calculateCommission($session);

        return new JsonResponse([$return, $amt, $modify]);

    }

    #[Route(path: '/admin/attendance/manage', name: 'manage_attendance_admin')]
    public function manageAttendanceAdmin(Request $request, InvoiceService $invoiceService, SessionRepository $sessionRepo, UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo): Response
    {

        $handle = $request->request->get('handle');
        $sessionId = explode("-", $handle)[2];

        // $userInstrumentGrade = $userInstrumentGradeRepo->findOneById($uigId);
        $session = $sessionRepo->findOneById($sessionId);
        $saved = $session->getDone() == true ? true : false;
        $return = '';
        $modify = '';

        if($saved) {
            $session->setDone(false);
            $dt = $session->getStartingOn();
            $return = "<small>" .$dt->format('M jS') . " <br /> <i class='fa fa-close text-warning'></i></small>";
            $modify = '-';
        } else {
            $session->setDone(true);
            $dt = $session->getStartingOn();
            $return = "<small>" . $dt->format('M jS') . " <br /> <i class='fa fa-check text-success'></i></small>";
            $modify = '+';

        }

        $this->save($session);
        
        $amt = $invoiceService->calculateCommission($session);

        return new JsonResponse([$return, $amt, $modify]);

    }
    
    #[Route(path: '/teacher/submit/invoice', name: 'submit_invoice')]
    public function submitInvoice(UserRepository $userRepo, InvoiceService $invoiceService, UserInstrumentGradeRepository $userInstrumentGradeRepo, PackageRepository $packageRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {

        $uigs = $request->request->get('nonEmptyUigs');
        $teacher_id = $request->request->get('teacher_id');

        $userInstrumentGradesIds = json_decode($uigs, null, 512, JSON_THROW_ON_ERROR);
        $em = $this->managerRegistry->getManager();
        $teacher = $userRepo->find($teacher_id);
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }

        // Fetch UserInstrumentGrades and their sessions
        $userInstrumentGrades = [];
        foreach ($userInstrumentGradesIds as $uigId) {
            $user_instrument_grade_id = explode(":", $uigId)[0];
            
            $uig = $userInstrumentGradeRepo->findOneById($user_instrument_grade_id);
            if ($uig) {
                $userInstrumentGrades[] = $uig;
            }
        }
    
        
        $teacherData = $teacher->getTeacherdata();
        if ($teacherData && $teacherData->getPercentage() !== null) {
            $percentage = (float)$teacherData->getPercentage();
        } else {
            $percentage = 0.7;
        }
        // $percentage = $teacherData->getPercentage() ?? 0.7; // Default percentage if not set
        $deductions = array_reduce($teacherData->getDeductions()->toArray(), fn($sum, $d) => $sum + $d->getToDeduct(), 0);
    
        $invoiceData = [];
        $totalInvoiceAmount = 0;
        $nonEmptyUigs = []; // This is where we will store the grouped session data
    
        foreach ($userInstrumentGrades as $uig) {
            $sessionGroups = [];
            foreach ($uig->getSessions() as $session) {
                if ($session->getDone() && !$session->getInvoiced()) {
                    $package = $session->getPackage();
                    $pricePerSession = $package->getPrice() / $package->getNoOfSessions();
                    // $uigPercentage = $uig->getPercentage() ?? $percentage;
                    if ($uig && $uig->getPercentage() !== null && !empty($uig->getPercentage())) {
                        $uigPercentage = (float)$uigPercentage = $uig->getPercentage();
                    } else {
                        $uigPercentage = $percentage;
                    }
                    $sessionCommission = $invoiceService->calculateCommission($session);
    
                    $totalInvoiceAmount += $sessionCommission;
                    
                    $session->setInvoiced(true);
                    $this->save($session);
                    // Group sessions by userInstrumentGradeId and packageId
                    $sessionGroups[] = [
                        'packageId' => $package->getId(),
                        'pricePerSession' => $pricePerSession,
                        'uigPercentage' => $uigPercentage,
                        'sessionCommission' => $sessionCommission,
                    ];
    
                    // Building nonEmptyUigs
                    $nonEmptyUigs[$uig->getId()][] = $uig->getId() . '-' . $package->getId(); // same as in the original code
                }
            }
            if (!empty($sessionGroups)) {
                $invoiceData[] = [
                    'userInstrumentGradeId' => $uig->getId(),
                    'studentName' => $uig->getStudentUserData()->getStudent()->getFullName(), 
                    'noOfSessions' => count($sessionGroups),
                    'sessions' => $sessionGroups,
                ];
            }
        }

        $html = $twig->render('pdf/invoice.html.twig', [
            'teacher' => $teacher,
            'userInstrumentGrades' => $userInstrumentGrades,
            'inv' => $totalInvoiceAmount,
            'ded' => $deductions,
            'invoiceData' => $invoiceData,
            'nonEmptyUigs' => $userInstrumentGrades,
        ]);

        $invoice = new TeacherInvoice();
        $invoice->setTeacher($teacher);
        $invoice->setAmount($totalInvoiceAmount);
        $invoice->setUrl("url");
        $invoice->setDateSubmitted(new \DateTimeImmutable());
        $invoice->setUigs($uigs);
        $invoice->setDeduction($deductions);
        $this->save($invoice);

        $urlShort = $this->shortenUrl->new('https://chezamusicschool.co.ke/teacher/pdf/invoice/', $invoice->getId(), 'Download Invoice');

        $invoice->setUrl($urlShort);
        $this->save($invoice);

        // change email to go to finance / accounts email
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], 'chezamusicschool@gmail.com', $teacher->getFullname().' Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice', 'necessary', 'personal_email_from_office');
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], 'oumabenjah018@gmail.com', $teacher->getFullname().' Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice', 'necessary', 'personal_email_from_office');
        // $mailer->sendEmailWithAttachment(['teacher' => $teacher], 'vessijohn@gmail.com', $teacher->getFullname().' Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice', 'necessary', 'personal_email_from_office');
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], $teacher->getEmail(), $teacher->getFullname().' Copy of Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice', 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'Your invoice of ' . $totalInvoiceAmount . " was sent successfully.",
        );
        foreach ($teacher->getTeacherdata()->getDeductions() as $deduction) {
            // $this->delete($deduction);
            $dedPayment = new DeductionPayment();
            if(null !== $deduction->getBalance()) {
                $amt = $deduction->getBalance();
            } else {
                $amt = $deduction->getAmount(); // 10000
            }
            $bal = $amt - $deductions;

            $ded = $deduction->getToDeduct(); // 4000
            $deduction->setBalance($bal); // 10000 - 4000
            $deduction->setToDeduct($bal);
            $this->save($deduction);

            if($bal <= 0) {
                $dedPayment->setCleared(true); // false
            } else {
                $dedPayment->setCleared(false);
            }
            $dedPayment->setPaidAmt($ded);
            $dedPayment->setTeacher($teacher);
            $dedPayment->setBalance($bal);
            $dedPayment->setPaidOn(new \Datetime());
            $dedPayment->setDeduction($deduction);
            $this->save($dedPayment);
        }

        return new JsonResponse($teacher->getId());

    }

    #[Route(path: '/admin/send/statement', name: 'send_statement')]
    public function sendStatement(UserInstrumentGradeRepository $userInstrumentGradeRepo, TermRepository $termRepo, PaymentRepository $paymentRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {


        $dateFrom = $request->request->get('dateFrom');
        $dateTimeFrom = new \Datetime($dateFrom." 00:00:00");
        $dateTo = $request->request->get('dateTo');
        $dateTimeTo = new \Datetime($dateTo." 23:59:59");
        $uig_ids = explode("-", $request->request->get('uig'));
        $uig_ids = array_unique($uig_ids);
        $uigs = [];
        foreach ($uig_ids as $uigid) {
            $u = $userInstrumentGradeRepo->findOneById($uigid);
            $uigs[] = $u;
            $insts[] = $u->getInstrument()->getName();

        }


        $dates = [];
        $student = $uigs[0]->getStudentUserData()->getStudent();
        $payments = $paymentRepo->findAllWithinRange($dateTimeFrom, $dateTimeTo, $student);
        $totalInvoicesBD = $paymentRepo->findSumOfTypeForUserBeforeDate("inv", $dateTimeFrom, $student)['totalAmount'];
        $totalPaymentsBD = $paymentRepo->findSumOfTypeForUserBeforeDate("pmt", $dateTimeFrom, $student)['totalAmount'];
        $totalInvoices = $paymentRepo->findSumOfTypeForUser("inv", $student)['totalAmount'];
        $totalPayments = $paymentRepo->findSumOfTypeForUser("pmt", $student)['totalAmount'];
        $dateTimeFrom->setDate(date('Y'), $dateTimeFrom->format('m'), $dateTimeFrom->format('d'));
        $currentTerm = $termRepo->findOneByTermnumber($termRepo->findCurrentTerm($dateTimeFrom));
        $lastDayOfPreviousTerm = $currentTerm->getStartingOn()->modify("yesterday");
        $balanceCF = $totalInvoicesBD - $totalPaymentsBD;
        $currentBalance = $totalInvoices - $totalPayments;
        // $oneToThirdyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '30', '0', $student)['totalAmount'];
        // $thirtyToSixtyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '60', '31', $student)['totalAmount'];
        // $sixtyToNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '90', '61', $student)['totalAmount'];
        // $moreThenNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '100000', '91', $student)['totalAmount'];
        // $oneToThirdyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '30', '0', $student)['totalAmount'];
        // $thirtyToSixtyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '60', '31', $student)['totalAmount'];
        // $sixtyToNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '90', '61', $student)['totalAmount'];
        // $moreThenNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '100000', '91', $student)['totalAmount'];

        $html = $twig->render('pdf/statement.html.twig', [
            'student' => $student,
            'payments' => $payments,
            'uigs' => $uigs,
            'balanceCF' => $balanceCF,
            'currentBalance' => $currentBalance,
            'lastDayOfPreviousTerm' => $lastDayOfPreviousTerm,
            'instruments' => array_unique($insts),
            // 'oneToThirdyDaysDue' => $oneToThirdyDaysDue,
            // 'thirtyToSixtyDaysDue' => $thirtyToSixtyDaysDue,
            // 'sixtyToNinetyDaysDue' => $sixtyToNinetyDaysDue,
            // 'moreThenNinetyDaysDue' => $moreThenNinetyDaysDue,
            // 'oneToThirdyDaysPaid' => $oneToThirdyDaysPaid,
            // 'thirtyToSixtyDaysPaid' => $thirtyToSixtyDaysPaid,
            // 'sixtyToNinetyDaysPaid' => $sixtyToNinetyDaysPaid,
            // 'moreThenNinetyDaysPaid' => $moreThenNinetyDaysPaid,
        ]);

        $mailer->sendEmailWithAttachment(['student' => $student], $student->getEmail(), $student->getFullname().' Your Fee Statement', $html, 'statement.html.twig', $student->getFullname().'-statement', 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The statement was sent successfully.',
        );
        // send text with short url to  pdf of statement
        return new JsonResponse($uig_ids);

    }

    #[Route(path: '/admin/send/invoice', name: 'send_invoice')]
    public function sendInvoice(
        TermRepository $termRepo,
        PaymentRepository $paymentRepo,
        Request $request,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig, 
        CalculateFees $feesCalculator
    ): Response {

        $payment_id = $request->request->get('payment_id');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $student = $payment->getUser();
        
        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $student = $payment->getUser();
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        $html = $twig->render('pdf/student_invoice.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,

        ]);

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/invoice/', $payment->getId(), 'Download Invoice');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Invoice', 'Your invoice has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());

        $mailer->sendEmailWithAttachment(['student' => $student, 'payment' => $payment], $student->getEmail(), $student->getFullname().'\'s Invoice dated '.$payment->getDoneOn()->format('d/m/Y'), $html, 'invoice_student.html.twig', $student->getFullname().'-invoice-'.$payment->getDoneOn()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The invoice was sent successfully.',
        );
        // send text with short url to pdf of invoice
        return new JsonResponse('test');

    }

    #[Route(path: '/admin/send/user/invoice', name: 'send_user_invoice')]
    public function sendUserInvoice(
        TermRepository $termRepo,
        UserRepository $userRepo,
        Request $request,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig
    ): Response {

        $user_id = $request->request->get('user_id');
        $student = $userRepo->findOneById($user_id);
        $studentData = $student->getStudentdata();
        // $dateTimeFrom = new \Datetime($starting_from." 00:00:00");
        // $dateTimeTo = new \Datetime($to." 23:59:59");

        $uigs = $studentData->getUserInstruments();
        $uigsWithBalance = [];
        $uigsWithNegativeBalance = [];
        $studentdata = $student->getStudentdata();
        $usertype = $student->getUsertype();

        $bal = 0;
        $instruments = [];
        // echo $student;
        // echo "<br/>";

        foreach ($uigs as $key => $uig) {

            $payments = $uig->getPayments();
            $uig_bal = 0;

            if((is_countable($payments) ? count($payments) : 0) > 0 && $studentdata != null && $usertype = "student") {

                foreach ($payments as $key => $payment) {

                    $instruments[] = $uig->getInstrument();
                    $bal = $payment->getType() == "inv" ? $bal + $payment->getAmount() : $bal - $payment->getAmount();
                    $uig_bal = $payment->getType() == "inv" ? $uig_bal + $payment->getAmount() : $uig_bal - $payment->getAmount();
                    // print_r($instruments);
                    // echo  "<br/>";
                    // print_r($payment->getAmount() . ": " . $bal. " : " . $payment->getType() . "<br />");

                }

            }

            if($uig_bal > 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();

            }
            if($uig_bal < 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithNegativeBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();

            }
            // echo "<br/>Bal: " . $bal;

        }

        $html = $twig->render('pdf/user_invoice.html.twig', [
            'student' => $student,
            'instruments' => array_unique($instruments),
            'studentData' => $studentData,
            'balance' => $bal,
            'uigsWithBalanceChunked' => array_chunk($uigsWithBalance, 10, true),
            'uigsWithNegativeBalance' => $uigsWithNegativeBalance,
        ]);

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/student/invoice/', $student->getId(), 'Download Invoice');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Invoice', 'Your invoice has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());

        $mailer->sendEmailWithAttachment(['student' => $student], $student->getEmail(), $student->getFullname().'\'s Invoice dated '.date('d/m/Y'), $html, 'invoice_user.html.twig', $student->getFullname().'-invoice-'.date('d/m/Y'), 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The invoice was sent successfully.',
        );
        // send text with short url to pdf of invoice
        return new JsonResponse('test');

    }

    #[Route(path: '/admin/send/receipt', name: 'send_receipt')]
    public function sendReceipt(
        TermRepository $termRepo,
        PaymentRepository $paymentRepo,
        Request $request,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig,
        CalculateFees $feesCalculator
    ): Response {

        $payment_id = $request->request->get('payment_id');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $student = $payment->getUser();
        
        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $student = $payment->getUser();
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        $html = $twig->render('pdf/student_receipt.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/receipt/', $payment->getId(), 'Download Receipt');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Download Receipt', 'Your receipt has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());

        $mailer->sendEmailWithAttachment(['student' => $student, 'payment' => $payment], $student->getEmail(), $student->getFullname().' Your Receipt dated '.$payment->getDoneOn()->format('d/m/Y'), $html, 'receipt_student.html.twig', $student->getFullname().'-receipt-'.$payment->getDoneOn()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The receipt was sent successfully.',
        );
        // send text with short url to pdf of receipt
        return new JsonResponse('test');

    }

    #[Route(path: '/send/message/from/payments/page', name: 'send_message_from_payments_page', methods: ['GET', 'POST'])]
    public function sendMessageFromPaymentsPage(Request $request, SendSms $sendSms, Mailer $mailer, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo): Response
    {
        $student = $userRepo->findOneById($request->request->get('student_id')); // 2
        $uig = $uigRepo->findOneById($request->request->get('uig_id'));
        $balance = $request->request->get('balance');
        $short = $student->getShortUrls()[0];

        $msg = "Dear " . $student->getFullname() . ", 
        This is a friendly reminder that there is a remaining balance for " . explode("-", (string) $uig)[1] . " course of KSh" . $balance . "  
        We kindly request that you settle this balance at your earliest convenience to ensure that we can be able to pay the teachers and continue providing you with our services.
        ";
        if(null !== $short) {
            $msg .= " As a courtesy, we would like to remind you that we offer several payment options, including online payment: https://chez.co.ke/" . $short->getShort() . ". or by Mpesa Till No 9810765
            ";
        } else {
            $msg .= ". As a courtesy, we would like to remind you that we offer several payment options, including Mpesa Till No 9810765.
            ";
        }
        $msg .= " If you have any questions or concerns about your account, please do not hesitate to contact us on 0711 832 933.
        
        Thank you for your prompt attention to this matter. We value your business and appreciate your cooperation.
        
        <br>Best regards,
        <br>Ouma Benjah,
        <br>Finance.";
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), "Tuition Fee Balance Reminder", $msg, 'necessary', 'personal_sms_from_office', $student->getEmail());
        $mailer->sendEmail(['user' => $student, 'msg' => $msg], $student->getEmail(), 'Tuition Fee Balance Reminder', "communication.html.twig", "necessary", "personal_email_from_office");

        $this->addFlash(
            'success',
            'The message was sent successfully:<br/>.' . $msg,
        );

        return new JsonResponse($msg);
    }

    #[Route(path: '/send/reminder/for/fees', name: 'send_reminder_for_payments', methods: ['GET', 'POST'])]
    public function sendFeeReminder(Request $request, SendSms $sendSms, Mailer $mailer, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo): Response
    {
        $student = $userRepo->findOneById($request->request->get('student_id')); // 2
        $uig = $uigRepo->findOneById($request->request->get('uig_id'));
        $balance = $request->request->get('balance');
        $teacher = $request->request->get('teacher');
        $email_content = $request->request->get('email_content');
        $unpaid_lessons = $request->request->get('unpaid_lessons');
        $short = $student->getShortUrls()[0];
        $msg = "Dear $student, Please check your email for an important payment information. Thank you.";

        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), "Important: Payment Reminder for Your Planned Music Lesson", $msg, 'necessary', 'personal_sms_from_office', $student->getEmail());
        $mailer->sendEmail(['user' => $student, 'msg' => $email_content], $student->getEmail(), 'Important: Payment Reminder for Your Planned Music Lesson', "communication.html.twig", "necessary", "personal_email_from_office");

        return new JsonResponse([$student->getFullname(), $uig->getId(), $balance, $email_content]);
    }

    #[Route(path: '/homepage/contact/form', name: 'contactformsend')]
    public function contactForm(Request $request, Mailer $mailer, Environment $twig, UserRepository $userRepo): Response
    {

        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $subject = $request->request->get('subject');
        $message = $request->request->get('message');
        $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $message];

        $admins = $userRepo->findByUsertype('admin');
        foreach ($admins as $admin) {
            $mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig", "necessary", "contact_form");
        }
        $mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig", "necessary", "contact_form");


        $this->addFlash(
            'success',
            'The message was sent successfully.',
        );
        return new JsonResponse('test');

    }

    #[Route(path: '/admin/send/report', name: 'send_report')]
    public function sendReport(
        TermRepository $termRepo,
        ReportingRepository $reportingRepo,
        Request $request,
        SendSms $sendSms,
        SessionRepository $sessionRepo,
        ShortenUrl $shortenUrl,
        Mailer $mailer,
        Environment $twig
    ): Response {

        $report_id = $request->request->get('report_id');
        // $report_id = 5;
        $report = $reportingRepo->findOneById($report_id);

        $fullname = $report->getName();
        $html = $twig->render('pdf/student_report.html.twig', [
            'reporting' => $report,
            'currentTerm' => $report->getTerm(),
            'fullname' => $fullname,
        ]);

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/reporting/show/pdf/for/', $report->getId(), 'Download Report');
        $sendSms->quickSend($report->getUser()->getStudentdata()->getPhone(), $fullname, 'Download Report', 'Your report has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $report->getUser()->getEmail());

        $mailer->sendEmailWithAttachment(['report' => $report, 'fullname' => $fullname, 'currentTerm' => $report->getTerm()], $report->getUser()->getEmail(), $fullname."'s Report for " . $report->getTerm(), $html, 'report_student.html.twig', $fullname.'-report-'.$report->getTerm(), 'necessary', 'personal_email_from_office');
        // $this->addFlash(
        //     'success',
        //     'The report was sent successfully.',
        // );
        $report->setSent(true);
        $this->save($report);

        return new JsonResponse($report->getUser()->getFullname());
        // return $this->render('default/test2.html.twig', [
        //     'statements' => $report,
        // ]);

    }

    #[Route(path: '/student/add/payment', name: 'add_payment')]
    public function addPayment(UserInstrumentGradeRepository $userInstrumentGradeRepo, 
        Request $request, 
        Mailer $mailer, 
        ShortenUrl $shortenUrl, 
        SendSms $sendSms, 
        TermRepository $termRepo, 
        Environment $twig,
        CalculateFees $feesCalculator
    ): Response
    {
        $uigid = $request->request->get('uigid');
        $amount = $request->request->get('amount'); // 1000
        $receipt_number = $request->request->get('receipt_number');
        $uig = $userInstrumentGradeRepo->findOneById($uigid);
        $student = $uig->getStudentUserData()->getStudent();
        // $statement = $connection->prepare('SELECT * FROM tbl WHERE col = ?');
        // $result = $statement->executeQuery();

        // $rows = $result->fetchAllAssociative();

        $em = $this->managerRegistry->getManager();
        $RAW_QUERY = 'SELECT SUM(amount) AS totalamount FROM payment where payment.uig_id = :thisuig and payment.type = :pmt ;';
        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('thisuig', $uig->getId());
        $statement->bindValue('pmt', "pmt");
        $result = $statement->executeQuery();

        $sumOfPayments = $result->fetchAllAssociative();
        $allpaymentsmade = $sumOfPayments[0]['totalamount'];// 18000

        $totalpaid = $allpaymentsmade + $amount; // 18000 + 1000 = 19000
        $uig->setPayment($totalpaid);
        $this->save($uig);


        $uigName = explode(' - ', $uig)[0];
        
        $fullfee = $uig->getSessions()[0]->getPackage()->getPrice(); // 24000
        $onelessonfee = $fullfee / $uig->getSessions()[0]->getPackage()->getNoOfSessions(); // 2000
        $sessions = is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0; //12
        $all_sessions = $uig->getSessions();
        $realfee = $onelessonfee * $sessions; // 24000
        $balance = $realfee - $totalpaid; // 24000 - 19000 = 5000
        $firstSession = $all_sessions[0];
        $package = $firstSession->getPackage();
        $pricePerSession = $package->getCostPerLesson();
        $number_of_sessions = count($all_sessions);

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($all_sessions, $uig, $pricePerSession);

        $newPayment = new Payment();
        $newPayment->setAmount($amount);
        $newPayment->setDoneOn(new \Datetime("now"));
        $newPayment->setUser($student);
        $newPayment->setDocNumber($receipt_number);
        $newPayment->setType("pmt");
        $newPayment->setUig($uig);
        $newPayment->setBalance($balance);
        $this->save($newPayment);

        $payment = $newPayment;
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $html = $twig->render('pdf/student_receipt.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);

        $maildata = ['amount' => $amount, 'newbal' => $balance, 'student' => $student];
        //$mailer->sendEmailMessage($maildata, $student->getEmail(), "Payment Received", "payment_received.html.twig", "necessary", "payment_received");
        $this->addFlash(
            'success',
            'Payment of ' . $amount . " for " . $student->getFullname() . " has been made. Balance is now " . $balance,
        );

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/receipt/', $payment->getId(), 'Download Receipt');
        $msg = "Hi " . $student->getFullname() . ". Your payment of Kes " . $amount . " for " . $newPayment->getUig() . " Has been received. Your balance is now " . $balance . ". Download receipt using this link: " . $urlShort . " Thank you for choosing us.";
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), "Payment Received", $msg, 'necessary', 'personal_sms_from_office', $student->getEmail());
        $mailer->sendEmailWithAttachment(['student' => $student, 'payment' => $payment], $student->getEmail(), $student->getFullname().' Your Receipt dated '.$payment->getDoneOn()->format('d/m/Y'), $html, 'receipt_student.html.twig', $student->getFullname().'-receipt-'.$payment->getDoneOn()->format('d/m/Y'), 'necessary', 'personal_email_from_office');

        return new JsonResponse($balance);

    }

    #[Route(path: '/student/edit/payment', name: 'edit_payment')]
    public function editPayment(PaymentRepository $paymentRepo, Request $request, Mailer $mailer): Response
    {
        $pmtid = $request->request->get('pmtid');
        $amount = $request->request->get('amount');
        $docNo = $request->request->get('docNo');
        $editedBalance = $request->request->get('uigBal');
        $paymentDate = $request->request->get('paymentDate');
        $payment = $paymentRepo->findOneById($pmtid); // the entry to be edited

        $uig = $payment->getUig(); // get the instrument paid for
        $fullfee = $uig->getSessions()[0]->getPackage()->getPrice();
        $onelessonfee = $fullfee / 12;
        $sessions = is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0;
        $realfee = $onelessonfee * $sessions; // actual fee to pay


        $payment->setAmount($amount); // set the new amount
        $payment->setBalance($editedBalance);// set the balance
        $payment->setDocNumber($docNo);
        $payment->setDoneOn(new \Datetime($paymentDate));
        $this->save($payment); // save the edited entry

        // $em = $this->getDoctrine()->getManager();
        // $RAW_QUERY = 'SELECT SUM(amount) AS totalamount FROM payment where payment.uig_id = :thisuig;';

        // $statement = $em->getConnection()->prepare($RAW_QUERY);
        // // Set parameters
        // $statement->bindValue('thisuig', $uig->getId());
        // $statement->execute();

        // $sumOfPayments = $statement->fetchAll();


        // $totalpaid = $sumOfPayments[0]['totalamount']; // all the money paid updated and correct

        // $balance = $realfee - $totalpaid;

        // $payment->setBalance($balance);
        // $this->save($payment);

        $this->addFlash(
            'success',
            'Payment for '. $uig->getInstrument() .' was successfully edited',
        );

        return new JsonResponse($paymentDate);

    }

    #[Route(path: '/teacher/print/form/for/{uig_id}', name: 'print_form')]
    public function printFormTest(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {

        $uig = $uigRepo->findOneById($uig_id);
        $student = $userRepo->findOneById($uig->getSessions()[0]->getStudent());
        $this->toPdf($student->getFullname()."-Registration-form", 'pdf/reg_form.html.twig', ['student' => $student, 'uig' => $uig]);

        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;
        // return $this->render('pdf/student_form.html.twig', [
        //     'student' => $student,
        // ]);

    }

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . '.pdf');

        //Generate pdf with the retrieved HTML
        // return new Response( $this->snappy_pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }


    #[Route(path: '/add/user/instrument', name: 'addinstrument')]
    public function addinstrument(UserRepository $userRepo, Request $request, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $gradeRepo): Response
    {
        $userdata = null;
        // $user = $this->getUser();
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);

        if($user->getUsertype() == 'student') {
            $userdata = $user->getStudentdata();
        } elseif ($user->getUsertype() == 'teacher') {
            $userdata = $user->getTeacherdata();
        }

        $ins = $request->request->get('instrument');
        $grade = $request->request->get('grade');

        $instrument = $instrumentRepo->findOneById($ins);
        $grade = $gradeRepo->findOneById($grade);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setInstrument($instrument);

        if($user->getUsertype() == 'student') {
            $userInstrument->setStudentUserData($userdata);

            // activate uig if student is activated
            $active = $user->getActive();
            $entityManager = $this->managerRegistry->getManager();

            if($active) {
                $userInstrument->setActive(true);
                $entityManager->persist($userInstrument);
            }

        } elseif ($user->getUsertype() == 'teacher') {
            $userInstrument->setTeacherUserData($userdata);
        }

        $userInstrument->setGrade($grade);

        $this->save($userInstrument);
        $html = '
        <button type="button" id="uigid-' . $userInstrument->getId() . '" class="btn btn-secondary">
            '. $userInstrument->getInstrument()->getName() . ' ' . $userInstrument->getGrade()->getName() . '&nbsp;&nbsp;&nbsp;<span id="delete-'. $userInstrument->getId() .'" class="badge badge-danger">x</span>
        </button>                                                    
        ';


        return new JsonResponse($html);

    }

    #[Route(path: '/remove/user/instrument', name: 'removeinstrument')]
    public function removeinstrument(Request $request, UserInstrumentGradeRepository $instrumentGradeRepo): Response
    {

        $uig = $request->request->get('uigid');

        $uiGrade = $instrumentGradeRepo->findOneById($uig);
        $this->delete($uiGrade);

        return new JsonResponse($uig);

    }

    #[Route(path: '/admin/communications/delete/template/', name: 'delete_template')]
    public function deleteTemplate(Request $request, TemplateRepository $templateRepo): Response
    {

        $template_id = $request->request->get('template_id');

        $template = $templateRepo->findOneById($template_id);
        $this->delete($template);

        return new JsonResponse($template->getId());

    }

    #[Route(path: '/communications/get/template/message', name: 'get_template_message')]
    public function getTemplateMessage(Request $request, TemplateRepository $templateRepo): Response
    {

        $template_id = $request->request->get('template_id');

        $template = $templateRepo->findOneById($template_id);

        return new JsonResponse($template->getMessage());

    }

    #[Route(path: '/user/done/practice/today', name: 'done_practice_today')]
    public function donePracticeToday(Request $request, AssignmentRepository $assignmentRepo, PracticeSessionsRepository $practiceRepo): Response
    {
        $assignmentId = $request->request->get('assignmentId');

        $assignment = $assignmentRepo->findOneById($assignmentId);
        $user = $this->getUser();
        $today = new \Datetime();

        $practiceSession = new PracticeSessions();

        $practiceSession->setAssignment($assignment);
        $practiceSession->setStudent($user);
        $practiceSession->setDoneOn($today);

        $this->save($practiceSession);

        return new JsonResponse("Session Saved");

    }

    #[Route(path: '/user/settings/change/setting', name: 'change_setting')]
    public function changeSetting(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $setting_id = $request->request->get('setting_id');
        $state = $request->request->get('state');
        $setting = $settingsRepo->findOneById($setting_id);
        $user = $this->getUser();
        $userSetting = $userSettingsRepo->findOneBy(
            ['user' => $user, 'setting' => $setting],
            ['id' => 'ASC'],
        );
        if(empty($userSetting)) {
            $userSetting = new UserSettings();
            $userSetting->setUser($user);
        }

        $userSetting->setSetting($setting);
        $userSetting->setValue($state);
        $this->save($userSetting);

        return new JsonResponse("Setting Saved");

    }

    #[Route(path: '/user/settings/save/preference', name: 'save_preference')]
    public function savePreference(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $setting_id = $request->request->get('setting_id');
        $preference = $request->request->get('preference');
        $setting = $settingsRepo->findOneById($setting_id);
        $user = $this->getUser();
        $userSetting = $userSettingsRepo->findOneBy(
            ['user' => $user, 'setting' => $setting],
            ['id' => 'ASC'],
        );
        if(empty($userSetting)) {
            $userSetting = new UserSettings();
            $userSetting->setUser($user);
        }

        $userSetting->setSetting($setting);
        $userSetting->setValue($preference);
        $this->save($userSetting);

        return new JsonResponse("Setting Saved");

    }

    #[Route(path: '/admin/save/message/template', name: 'save_template')]
    public function saveTemplate(Request $request, AttachmentRepository $attachRepo): Response
    {

        $msg = $request->request->get('msg');
        $type = $request->request->get('type');
        $title = $request->request->get('title');
        if($request->request->get('attachment') != "") {
            $attachment = $attachRepo->findOneById($request->request->get('attachment'));
            $attachmentFile = $attachment->getFilename();
        } else {
            $attachmentFile = null;
            $attachment = null;
        }

        $data = [];

        $template = new Template();

        $template->setMessage($msg);
        $template->setType($type);
        $template->setTitle($title);
        $template->setAttachment($attachment);

        $this->save($template);
        $data['msg'] = $template->getMessage();
        $data['title'] = $template->getTitle();
        $data['type'] = $template->getType();
        $data['id'] = $template->getId();
        $data['attachment'] = $attachmentFile;
        return new JsonResponse($data);

    }

    #[Route(path: '/test/test/test', name: 'multitesting')]
    public function testingmultiplethings(PaymentRepository $paymentRepo, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo): Response
    {

        $arr = ["src/Controller/AdminController.php", "src/Controller/AjaxController.php", "src/Controller/AssignmentController.php", "src/Controller/SessionController.php", "src/Controller/StudentController.php", "src/Controller/TeacherController.php", "templates/pdf/invoice.html.twig", "templates/session/show.html.twig", "templates/teacher/send_invoice.html.twig", "templates/user_instrument_grade/show.html.twig", "src/Service/InvoiceService.php"];

        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a) {
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }

        return $this->render('default/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

    #[Route(path: '/sessionuig/change/term}', name: 'change_term', methods: ['GET', 'POST'])]
    public function changeTerm(Request $request, UserInstrumentGradeRepository $uigRepo, TermRepository $termRepo ): Response
    {        

        $entityManager = $this->managerRegistry->getManager();
        $entryId = $request->request->get('entryId');
        $newTerm = $request->request->get('newTerm');
        $term = $termRepo->findOneById($newTerm);

        $uig = $uigRepo->findOneById($entryId);
      
        $uig->setTerm($term);
        $entityManager->persist($uig);  
        $entityManager->flush();
        
        return new JsonResponse($term->getTermnumber());

    }

    #[Route(path: '/payment/delete/one/{payment_id}', name: 'payment_delete_one', methods: ['GET'])]
    public function deleteOne(PaymentRepository $paymentRepo, $payment_id): Response
    {
        $payment = $paymentRepo->findOneById($payment_id);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($payment);
        $entityManager->flush();
        // return $this->redirectToRoute('user_instrument_grade_show');
        return $this->redirectToRoute('admin-payments-list', ['uig_id' => $payment->getUig()->getId()]);
    }

    #[Route(path: '/communications/template/save/edit', name: 'edit_template')]
    public function editTemplate(Request $request, TemplateRepository $templateRepo): Response
    {
        $template_id = $request->request->get('template_id');
        $message = $request->request->get('msg');
        $title = $request->request->get('title');
        $type = $request->request->get('type');

        $template = $templateRepo->findOneById($template_id);

        $template->setType($type);
        $template->setMessage($message);
        $template->setTitle($title);
        $this->save($template);

        return new JsonResponse($template->getType());
    }

    #[Route(path: '/save/user/comment', name: 'savecomment')]
    public function saveComment(Request $request, BlogRepository $postRepo, SendSms $sendSms): Response
    {
        $dt = [];
        $email = $request->request->get('email');
        $name = $request->request->get('name');
        $content = $request->request->get('comment');
        $post = $postRepo->findOneById($request->request->get('post_id'));
        $comment = new Comment();
        $comment->setContent($content);
        $comment->setCreatedAt(new \DateTime());

        $this->save($comment);
        // send email to concerned parties
        $dt['name'] = $name;
        $dt['content'] = $content;
        $dt['time'] = $comment->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();

        $sendSms->quickSend('0711832933', $name, 'New comment to blog', 'New comment to blog : ' . $content, 'necessary', 'personal_sms_from_office', 'nomail');

        return new JsonResponse($dt);

    }

    #[Route(path: '/save/comment/reply', name: 'save_reply')]
    public function saveReply(Request $request, CommentRepository $commentRepo, SendSms $sendSms): Response
    {
        $dt = [];
        $r_email = $request->request->get('r_email');
        $r_name = $request->request->get('r_name');
        $content = $request->request->get('reply');
        $comment = $commentRepo->findOneById($request->request->get('comment_id'));
        $reply = new CommentReply();
        $reply->setContent($content);
        $reply->setCreatedAt(new \DateTime());
        $reply->setComment($comment);

        $this->save($reply);
        // send email to concerned parties
        $dt['name'] = $r_name;
        $dt['content'] = $content;
        $dt['time'] = $reply->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();

        $sendSms->quickSend('0711832933', $r_name, 'New reply to blog comment', 'New reply to blog comment: ' . $content, 'necessary', 'personal_sms_from_office', 'nomail');

        return new JsonResponse($dt);

    }



}
