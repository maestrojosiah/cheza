<?php

namespace App\Controller;

use App\Entity\SchoolTime;
use App\Form\SchoolTimeType;
use App\Repository\SchoolTimeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Detection\MobileDetect;

#[Route(path: '/school/time')]
class SchoolTimeController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/calendar/view/{teacher_id}', name: 'teacher_schooltime_calendar_view', methods: ['GET'])]
    public function calendarView(UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo, $teacher_id): Response
    {
        $teacher = $userRepo->findOneById($teacher_id);
        $periods = $schoolTimeRepo->findByTeacher($teacher);

        return $this->render('school_time/calendar_view.html.twig', [
            'teacher' => $teacher,
            'periods' => $periods,
        ]);
    }

    #[Route(path: '/calendar/edit/{teacher_id}', name: 'teacher_schooltime_calendar_edit')]
    public function calendarEdit(Request $request, UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo, $teacher_id): Response
    {
        $teacher = $userRepo->findOneById($teacher_id);
        $periods = $schoolTimeRepo->findByTeacher($teacher);
        $detect = new MobileDetect();
        if($detect->isMobile()) {
            $schoolTime = new SchoolTime();
            $form = $this->createForm(SchoolTimeType::class, $schoolTime);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $teacher = $userRepo->findOneById($teacher_id);
                $dayCode = explode("-", (string) $form->get('day')->getData())[1];
                $day = explode("-", (string) $form->get('day')->getData())[0];
                $beginAt = $form->get('beginAt')->getData();
                $endAt = $form->get('endAt')->getData();
                $title = $dayCode.'-'.$beginAt->format('H:i').'-'.$endAt->format('H:i');

                $schoolTime->setDay($day);
                $schoolTime->setJsdaycode($dayCode);
                $schoolTime->setTeacher($teacher);
                $schoolTime->setBeginAt($beginAt);
                $schoolTime->setEndAt($endAt);
                $schoolTime->setTitle($title);

                $entityManager = $this->managerRegistry->getManager();
                $entityManager->persist($schoolTime);
                $entityManager->flush();

                return $this->redirectToRoute($this->getUser()->getUsertype().'_session_calendar');
            }

            return $this->render('school_time/new.html.twig', [
                'school_time' => $schoolTime,
                'form' => $form,
                'periods' => $periods,
            ]);

        } else {

            return $this->render('school_time/calendar_edit.html.twig', [
                'teacher' => $teacher,
                'periods' => $periods,
            ]);

        }
    }

    #[Route(path: '/', name: 'school_time_index', methods: ['GET'])]
    public function index(SchoolTimeRepository $schoolTimeRepository): Response
    {
        if($this->getUser()->getUsertype() == 'admin') {
            $schedules = $schoolTimeRepository->findAll();
        } else {
            $schedules = $schoolTimeRepository->findBy(
                ['teacher' => $this->getUser()],
                ['id' => 'DESC']
            );
        }
        return $this->render('school_time/index.html.twig', [
            'school_times' => $schedules,
        ]);
    }

    #[Route(path: '/new', name: 'school_time_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $schoolTime = new SchoolTime();
        $form = $this->createForm(SchoolTimeType::class, $schoolTime);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dayCode = $form->get('day')->getData();
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($schoolTime);
            $entityManager->flush();

            return $this->redirectToRoute('school_time_index');
        }

        return $this->render('school_time/new.html.twig', [
            'school_time' => $schoolTime,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'school_time_show', methods: ['GET'])]
    public function show(SchoolTime $schoolTime): Response
    {
        return $this->render('school_time/show.html.twig', [
            'school_time' => $schoolTime,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'school_time_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SchoolTime $schoolTime): Response
    {
        $form = $this->createForm(SchoolTimeType::class, $schoolTime);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('school_time_index');
        }

        return $this->render('school_time/edit.html.twig', [
            'school_time' => $schoolTime,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'school_time_delete', methods: ['DELETE'])]
    public function delete(Request $request, SchoolTime $schoolTime): Response
    {
        if ($this->isCsrfTokenValid('delete'.$schoolTime->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($schoolTime);
            $entityManager->flush();
        }

        return $this->redirectToRoute('school_time_index');
    }
}
