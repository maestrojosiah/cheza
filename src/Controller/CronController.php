<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Repository\SessionRepository;
use App\Repository\SettingsRepository;
use App\Repository\AssignmentRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Service\Mailer;
use Twig\Environment;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use App\Repository\UserSettingsRepository;
use App\Service\SendSms;
use App\Service\ShortenUrl;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CronController extends AbstractController
{
    public function __construct(private readonly Mailer $mailer, private readonly Pdf $pdf, private readonly Environment $twig, private readonly UserSettingsRepository $usrSt, private readonly UserRepository $userRepo, private readonly SessionRepository $sessionRepo, private readonly UserInstrumentGradeRepository $uigRepo, private readonly ShortenUrl $shortenUrl, private readonly SendSms $sendSms)
    {
    }

    #[Route(path: '/cron/sendreminder/to/teachers/{category}/{emailtype}', name: 'cron')]
    public function index($category, $emailtype, SendSms $sendSms): Response
    {

        $test = [];
        $tl = [];

        $teachers = $this->userRepo->findBy(
            ['usertype' => 'teacher'],
            ['id' => 'ASC']
        );

        foreach ($teachers as $teacher) {

            $settings = [];
            $emailtypes = [];
            $send = false;
            $todayslessons = $this->sessionRepo->findTodaysLessons($teacher);
            $tl[] = $todayslessons;
            $lessonsStr = "Hi " . $teacher . ". <br />";
            $lessonsStr .= "You have the following lessons today: <br />";
            $lessonsStr .= "<table>";
            foreach ($todayslessons as $key => $lesson) {
                $lessonsStr .= "<tr>";
                $lessonsStr .= "<td>";
                $lessonsStr .= $lesson->getBeginAt()->format('h:i A') . " - ";
                $lessonsStr .= "</td>";
                $lessonsStr .= "<td>";
                $lessonsStr .= $lesson->getUserInstrumentGrade() . "<br> ";
                $lessonsStr .= "</td>";
                $lessonsStr .= "</tr>";
            }
            $lessonsStr .= "</table>";

            // if((is_countable($todayslessons) ? count($todayslessons) : 0) > 0) {
            //     if($emailtype == 'todays_lessons_sms') {
            //         $smsSent = $sendSms->quickSend($teacher->getTeacherdata()->getPhone(), $teacher->getFullname(), 'Lessons Reminder', $lessonsStr, $category, $emailtype, $teacher->getEmail());
            //     }
            //     if($emailtype == 'todays_lessons_email') {
            //         $maildata = ['todayslessons' => $todayslessons, 'user' => $teacher];
            //         $this->mailer->sendEmailMessage($maildata, $teacher->getEmail(), "Today's Lessons", "cron.html.twig", $category, $emailtype);
            //     }
            // } else {
            //     $test[] = 'nope';
            // }

        }
        return $this->render('cron/index.html.twig', [
            'test' => $test,
            'todayslessons' => $tl,
        ]);
    }

    #[Route(path: '/cron/lessonplan/sendreminder/to/teachers/{category}/{emailtype}', name: 'cron')]
    public function lessonPlanReminders($category, $emailtype): Response
    {

        $test = [];
        $tl = [];

        $teachers = $this->userRepo->findBy(
            ['usertype' => 'teacher'],
            ['id' => 'ASC']
        );

        // remind to add a new lesson plan
        // define the threshold date as two days from now
        $thresholdDate = (new \DateTime())->modify('+3 days');

        foreach ($teachers as $teacher) {

            $futureLessons = $this->sessionRepo->findFutureLessons(new \Datetime(), $teacher);
            // $test[$teacher->getFullname()] = $futureLessons;

            // loop through the array and check if each DateTime object is less than two days away
            // foreach ($futureLessons as $lesson) {
            //     $message = "";

            //     if ($lesson->getBeginAt() < $thresholdDate && null == $lesson->getLessonPlan()) {

            //         $urlShort = $this->shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/new/for/', $lesson->getId(), 'Lesson Planning Is Fun');

            //         $message = "Hello " . $lesson->getTeacher()->getFullname() . ". ";
            //         $message .= "You have " . $lesson->getUserInstrumentGrade() . "'s lesson on " . $lesson->getBeginAt()->format('l, jS F Y') . " at " . $lesson->getBeginAt()->format('H:i') . ". ";
            //         $message .= "Please prepare a lesson plan here: " . $urlShort . ". It's easy and fun!";
            //         $this->sendSms->quickSend($lesson->getTeacher()->getTeacherdata()->getPhone(), $lesson->getTeacher()->getFullname(), 'Lesson Planning Is Fun', $message, $category, $emailtype, 'nomail');
            //         $maildata = ['teacher' => $lesson->getTeacher(), 'student' => $lesson->getStudent(), 'url' => $urlShort, 'lesson' => $lesson];
            //         $this->mailer->sendEmail($maildata, $lesson->getTeacher()->getEmail(), "Lesson Planning Is Fun", "cron_lesson_plan.html.twig", $category, $emailtype);
            //         $test[] = $message."<br/>";

            //     }
            // }

        }

        // remind to add update lesson plan
        // define the threshold date as two days before now
        $thresholdDate = (new \DateTime())->modify('-3 days');

        // foreach ($teachers as $teacher) {

        //     $pastLessons = $this->sessionRepo->findPastLessons(new \Datetime(), $teacher);
        //     // $test[$teacher->getFullname()] = $pastLessons;

        //     // loop through the array and check if each DateTime object is more than two days away
        //     foreach ($pastLessons as $lesson) {
        //         $message = "";

        //         if ($lesson->getBeginAt() > $thresholdDate && null != $lesson->getLessonPlan()) {
        //             if($lesson->getLessonPlan()->getAssessment() == "" || $lesson->getLessonPlan()->getNextlesson() == "") {
        //                 $urlShort = $this->shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/new/for/', $lesson->getId(), 'Finalize your lesson plan');
        //                 $message = "Hello " . $lesson->getTeacher()->getFullname() . ". ";
        //                 $message .= "You did " . $lesson->getUserInstrumentGrade() . "'s lesson on " . $lesson->getBeginAt()->format('l, jS F Y') . " at " . $lesson->getBeginAt()->format('H:i') . ". ";
        //                 $message .= "Please update the lesson plan to include assessment and priorities for next lesson: " . $urlShort . ". It's easy and fun!";
        //                 $this->sendSms->quickSend($lesson->getTeacher()->getTeacherdata()->getPhone(), $lesson->getTeacher()->getFullname(), 'Finalize your lesson plan', $message, $category, $emailtype, 'nomail');
        //                 $maildata = ['teacher' => $lesson->getTeacher(), 'student' => $lesson->getStudent(), 'url' => $urlShort, 'lesson' => $lesson];
        //                 $this->mailer->sendEmail($maildata, $lesson->getTeacher()->getEmail(), "Finalize your lesson plan", "cron_lessonplan_update.html.twig", $category, $emailtype);
        //                 $test[] = $message;

        //             }

        //         }
        //     }

        // }

        return $this->render('cron/index.html.twig', [
            'test' => $test,
            'todayslessons' => $tl,
        ]);
    }

    #[Route(path: '/cron/sendreminder/to/students/{category}/{emailtype}', name: 'cron_students')]
    public function studentsLessonReminder($category, $emailtype, SendSms $sendSms): Response
    {

        $test = [];
        $tl = [];
        $smsSent = false;
        $emailSent = false;

        $students = $this->userRepo->findBy(
            ['usertype' => 'student'],
            ['id' => 'ASC']
        );

        // foreach ($students as $student) {

        //     $settings = [];
        //     $emailtypes = [];
        //     $send = false;
        //     $todayslessons = $this->sessionRepo->findTodaysLessonsStudents($student);
        //     $tl[] = $todayslessons;
        //     $lessonsStr = "Hi " . $student . ". <br />";
        //     $lessonsStr .= "You have the following lessons today: <br />";
        //     $lessonsStr .= "<table>";
        //     foreach ($todayslessons as $key => $lesson) {
        //         $lessonsStr .= "<tr>";
        //         $lessonsStr .= "<td>";
        //         $lessonsStr .= $lesson->getBeginAt()->format('h:i A') . " - ";
        //         $lessonsStr .= "</td>";
        //         $lessonsStr .= "<td>";
        //         $lessonsStr .= $lesson->getTeacher()->getFullname() . "<br> ";
        //         $lessonsStr .= "</td>";
        //         $lessonsStr .= "</tr>";
        //     }
        //     $lessonsStr .= "</table>";

        //     if((is_countable($todayslessons) ? count($todayslessons) : 0) > 0) {
        //         if($emailtype == 'todays_lessons_sms') {
        //             $smsSent = $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Lesson Reminder', $lessonsStr, $category, $emailtype, $student->getEmail());
        //         }
        //         if($emailtype == 'todays_lessons_email') {
        //             $maildata = ['todayslessons' => $todayslessons, 'user' => $student];
        //             $emailSent = $this->mailer->sendEmailMessage($maildata, $student->getEmail(), "Today's Lessons", "cron_student.html.twig", $category, $emailtype);
        //         }
        //     } else {
        //         $test[] = 'nope';
        //     }

        // }
        return $this->render('cron/student_reminder.html.twig', [
            'test' => $test,
            'todayslessons' => $tl,
            'sent' => [$smsSent, $emailSent],
        ]);
    }

    #[Route(path: '/cronjob/sendreminder/to/student/{category}/{emailtype}', name: 'cron_student_reminder')]
    public function remindStudent($category, $emailtype, SettingsRepository $settingsRepo, SendSms $sendSms, ShortenUrl $shortenUrl, Mailer $mailer, AssignmentRepository $assignmentRepo): Response
    {

        $smsSent = false;
        $emailSent = false;

        $test = [];
        $tl = [];
        $studentsWithAssignments = [];

        $allStudents = $this->userRepo->findAll();

        foreach ($allStudents as $student) {
            $assignments = $student->getAssignments();
            if(count($assignments) > 0) {
                $studentsWithAssignments[] = $student;
            }

        }

        $statement = [];
        // foreach ($studentsWithAssignments as $key => $student) {

        //     // get last assignment
        //     $assignment = $assignmentRepo->getLastAssignment($student);

        //     date_default_timezone_set('Africa/Nairobi');

        //     $reminder_time = $assignment->getReminder()->format('H:i:s');
        //     $time_now = date('H:i:s');

        //     $remindertime = new \DateTime($reminder_time);
        //     $timenow = new \DateTime($time_now);

        //     $interval = $remindertime->diff($timenow);

        //     if($reminder_time > $time_now) {
        //         // reminder is still in future
        //     } else {
        //         // reminder is past
        //         $hours = $interval->format('%h');
        //         $minutes = $interval->format('%i');
        //         // echo $hours;
        //         // echo $minutes;
        //         if($hours == 0 && $minutes < 30 && $assignment->getKnob() == 1) {
        //             $urlShort = 'https://chezamusicschool.co.ke/db';
        //             $message = 'Please remember to do your practice. Your assignment is well described in your portal: '. $urlShort ;
        //             $smsSent = $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Practice Reminder', $message, $category, $emailtype, $student->getEmail());
        //             $maildata = ['student' => $student, 'assignment' => $assignment->getAssignment(), 'attachment' => $assignment->getAttachment(), 'link_to_dashboard' => $urlShort , 'added_on' => $assignment->getAddedOn()->format('jS M Y')];
        //             $emailSent = $mailer->sendEmailMessage($maildata, $student->getEmail(), ' Your current assignment added on '.$assignment->getAddedOn()->format('jS M Y'), 'practice_reminder.html.twig', $category, $emailtype);

        //         }

        //     }

        // }
        return $this->render('cron/remind_student.html.twig', [
            'test' => [$smsSent, $emailSent],
        ]);
    }

    #[Route(path: '/cronjob/send/fee/reminder/to/student/{category}/{emailtype}', name: 'cron_student_fee_reminder')]
    public function remindStudentFee($category, $emailtype, SendSms $sendSms, ShortenUrl $shortenUrl, Mailer $mailer): Response
    {

        $smsSent = false;
        $emailSent = false;

        $test = [];
        $tl = [];
        $studentsWithBalance = [];

        $allStudents = $this->userRepo->findByActive(1);

        foreach ($allStudents as $student) {
            $payments = $student->getPayments();
            $studentdata = $student->getStudentdata();
            $usertype = $student->getUsertype();

            $bal = 0;
            $instruments = [];

            if((is_countable($payments) ? count($payments) : 0) > 0 && $studentdata != null && $usertype = "student") {
                $prev_bal = 0;
                foreach ($payments as $key => $payment) {
                    $instruments[] = $payment->getUig()->getInstrument();
                    $bal = $payment->getType() == "inv" ? $bal + $payment->getAmount() : $bal - $payment->getAmount();
                }
            }

            if($bal > 0 && (stripos((string) $student->getFullname(), 'school') === false && stripos((string) $student->getFullname(), 'church') === false)) {

                $short = $student->getShortUrls()[0];
                $instruments = implode(", ", array_unique($instruments));

                if(null == $short) {
                    $shortUrl = "";
                } else {
                    $shortUrl = $short->getShort();
                }

                // print_r($student->getFullname()) . "<br/>";

                // print_r($student . " -> " . $bal . "<br/>");
                // print_r($student . " -> " . count(array_unique($uigs)). "<br>");

                $sms = "Friendly reminder: Remaining balance of KSh " . $bal . " for " . $instruments . " course. Please settle at your earliest convenience to ensure our teachers are paid in time and services continue. If payment plan in place, disregard. 
";
                if(null !== $short) {
                    $sms .= " Pay online: chez.co.ke/" . $shortUrl . " or Mpesa Till No 9810765. Questions? Call 0711 832 933. Thank you for your cooperation and valued business.";
                } else {
                    $sms .= " Pay using Mpesa Till No 9810765. Questions? Call 0711 832 933. Thank you for your cooperation and valued business.";
                }$sms .= "
Best regards,
Ouma Benjah,
Finance.";

                // if($emailtype == 'personal_sms_from_office') {
                //     // if($student->getEmail() == "chezamusicschool@gmail.com"){
                //     // print_r($student->getEmail());
                //     $smsSent = $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Tuition Fee Balance Reminder', $sms, $category, $emailtype, $student->getEmail());

                //     // }
                // }

                // if($emailtype == 'personal_email_from_office') {
                //     $maildata = ['instruments' => $instruments, 'user' => $student, 'balance' => $bal, 'short' => $shortUrl];
                //     // if($student->getEmail() == "chezamusicschool@gmail.com"){
                //     $emailSent = $this->mailer->sendEmailMessage($maildata, $student->getEmail(), "Tuition Fee Balance Reminder", "cron_fee_balances.html.twig", $category, $emailtype);

                //     // }
                // }


            }


        }


        return $this->render('cron/remind_fee_student.html.twig', [
            'test' => [$smsSent, $emailSent],
        ]);
    }

}
