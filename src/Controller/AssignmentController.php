<?php

namespace App\Controller;

use App\Entity\Assignment;
use App\Entity\Attachment;
use App\Form\AssignmentType;
use App\Form\AttachmentType;
use App\Repository\AssignmentRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/assignment')]
class AssignmentController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/{teacher_id}', name: 'assignment_index', methods: ['GET'])]
    public function index(AssignmentRepository $assignmentRepository, UserRepository $userRepo, $teacher_id): Response
    {
        $teacher = $userRepo->findOneById($teacher_id);
        $query = "";
        $student = null;

        // get the student_Id from url if available
        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $query = explode("=", (string) $_SERVER['QUERY_STRING'])[1];
            $student = $userRepo->findOneById($query);
            $assignments = $assignmentRepository->findBy(
                ['teacher' => $teacher, 'student' => $student],
                ['id' => 'DESC']
            );
        } else {
            $query = "";
            $assignments = $assignmentRepository->findAllDesc($teacher);
        }


        return $this->render('assignment/index.html.twig', [
            'query' => $query,
            'teacher' => $teacher,
            'student' => $student,
            'assignments' => $assignments,
        ]);
    }

    #[Route(path: '/student/{student_id}', name: 'assignment_index_student', methods: ['GET'])]
    public function indexStudent(AssignmentRepository $assignmentRepository, UserRepository $userRepo, $student_id): Response
    {
        $student = $userRepo->findOneById($student_id);

        $assignments = $assignmentRepository->findBy(
            ['student' => $student],
            ['id' => 'DESC']
        );


        return $this->render('assignment/index_student.html.twig', [
            'student' => $student,
            'assignments' => $assignments,
        ]);
    }

    #[Route(path: '/new/for/{student_id}/{teacher_id}', name: 'assignment_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserRepository $userRepo, SluggerInterface $slugger, $student_id, $teacher_id): Response
    {
        $assignment = new Assignment();
        $student = $userRepo->findOneById($student_id);
        $now = new \Datetime();
        $modify = false;
        $teacher = $userRepo->findOneById($teacher_id);
        $attachment = new Attachment();
        $formAtt = $this->createForm(AttachmentType::class, $attachment);
        $formAtt->handleRequest($request);
        $assignmentDescription = "";

        $form = $this->createForm(AssignmentType::class, $assignment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->managerRegistry->getManager();
            $assignment->setStudent($student);
            $assignment->setAddedOn($now);
            $assignment->setAssessment("later");
            $assignment->setTeacher($teacher);
            $entityManager->persist($assignment);
            $entityManager->flush();

            if($assignment->getknob() == 1) {

                $em = $this->managerRegistry->getManager();
                // turn all other reminders off if this reminder is on
                $RAW_QUERY = "UPDATE assignment SET knob = 0 where student_id = :student_id AND teacher_id = :teacher_id AND id != :this_id;";
                $em = $this->managerRegistry->getManager();
                if (!$em instanceof EntityManager) {
                    throw new \Exception('Not an EntityManager instance');
                }
        
                $statement = $em->getConnection()->prepare($RAW_QUERY);
                // Set parameters
                $statement->bindValue('student_id', $student_id);
                $statement->bindValue('teacher_id', $teacher_id);
                $statement->bindValue('this_id', $assignment->getId());
                $statement->executeQuery();

            }

            return $this->redirectToRoute('assignment_index', ['teacher_id' => $teacher->getId(), 'student_id' => $student->getId()], Response::HTTP_SEE_OTHER);
        }


        if ($formAtt->isSubmitted() && $formAtt->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $formAtt->get('file')->getData();
            $assignmentDescription = $_POST['assignmentDescription'];

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo((string) $uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('attachment_files'),
                        $newFilename
                    );
                } catch (FileException) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $attachment->setFile($newFilename);
            }

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($attachment);
            $entityManager->flush();

            return $this->redirectToRoute('assignment_new', ['student_id' => $student->getId(), 'teacher_id' => $teacher->getId(), 'doc' => $attachment->getId()]);
        }

        return $this->render('assignment/new.html.twig', [
            'assignment' => $assignment,
            'attachment' => $attachment,
            'modify' => $modify,
            'teacher' => $teacher,
            'student' => $student,
            'form' => $form,
            'formAtt' => $formAtt,
        ]);
    }

    #[Route(path: '/show/for/{id}', name: 'assignment_show', methods: ['GET'])]
    public function show(Assignment $assignment): Response
    {
        $teacher = $this->getUser();

        return $this->render('assignment/show.html.twig', [
            'assignment' => $assignment,
            'teacher' => $teacher,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'assignment_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Assignment $assignment, SluggerInterface $slugger): Response
    {
        $teacher = $assignment->getTeacher();
        $student = $assignment->getStudent();

        $form = $this->createForm(AssignmentType::class, $assignment);
        $form->handleRequest($request);

        $attachment = new Attachment();
        $formAtt = $this->createForm(AttachmentType::class, $attachment);
        $formAtt->handleRequest($request);
        $assignmentDescription = "";

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            if($assignment->getknob() == 1) {

                $em = $this->managerRegistry->getManager();
                // turn all other reminders off if this reminder is on
                $RAW_QUERY = "UPDATE assignment SET knob = 0 where student_id = :student_id AND teacher_id = :teacher_id AND id != :this_id;";
                if (!$em instanceof EntityManager) {
                    throw new \Exception('Not an EntityManager instance');
                }
                $statement = $em->getConnection()->prepare($RAW_QUERY);
                // Set parameters
                $statement->bindValue('student_id', $assignment->getStudent()->getId());
                $statement->bindValue('teacher_id', $assignment->getTeacher()->getId());
                $statement->bindValue('this_id', $assignment->getId());
                $statement->executeQuery();

            }

            return $this->redirectToRoute('assignment_index', ['teacher_id' => $assignment->getTeacher()->getId(), 'student_id' => $assignment->getStudent()->getId()], Response::HTTP_SEE_OTHER);
        }

        $modify = true;

        if ($formAtt->isSubmitted() && $formAtt->isValid()) {

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $formAtt->get('file')->getData();
            $assignmentDescription = $_POST['assignmentDescription'];

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo((string) $uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('attachment_files'),
                        $newFilename
                    );
                } catch (FileException) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $attachment->setFile($newFilename);
            }

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($attachment);
            $entityManager->flush();

            return $this->redirectToRoute('assignment_edit', ['id' => $assignment->getId(),'doc' => $attachment->getId()]);
        }

        return $this->render('assignment/edit.html.twig', [
            'modify' => $modify,
            'teacher' => $teacher,
            'student' => $student,
            'assignment' => $assignment,
            'form' => $form,
            'formAtt' => $formAtt,
            'attachment' => $attachment,
        ]);
    }

    #[Route(path: '/{id}', name: 'assignment_delete', methods: ['POST'])]
    public function delete(Request $request, Assignment $assignment): Response
    {
        $teacher = $assignment->getTeacher();
        if ($this->isCsrfTokenValid('delete'.$assignment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($assignment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('assignment_index', ['teacher_id' => $teacher->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/user/assignment/change/reminder/setting', name: 'change_assignment_state')]
    public function changeReminderSetting(Request $request, AssignmentRepository $assignmentRepo): Response
    {
        $assignment_id = $request->request->get('assignment_id');
        $state = $request->request->get('state');
        $stateBool = $state == 'on' ? 1 : 0;
        $assignment = $assignmentRepo->findOneById($assignment_id);

        $assignment->setKnob($stateBool);
        $this->save($assignment);

        return new JsonResponse("Reminder Setting Saved");

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


}
