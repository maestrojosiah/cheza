<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Callback;
use App\Entity\StudentUserData;
use App\Entity\User;
use App\Entity\UserInstrumentGrade;
use App\Repository\CallbackRepository;
use App\Repository\CourseRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\InstrumentRepository;
use App\Repository\UserRepository;
use App\Service\MPesaManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Service\Mailer;
use App\Service\SendSms;

class MPesaController extends AbstractController
{
    public function __construct(private readonly MPesaManager $mpesa, private readonly CallbackRepository $cbRepo, private readonly InstrumentRepository $instrumentRepo, private readonly UserPasswordHasherInterface $passwordEncoder, private readonly InstrumentGradeRepository $instrumentGradeRepo, private readonly CourseRepository $courseRepo, private readonly UserRepository $userRepo, private readonly SendSms $sendSms, private readonly Mailer $mailer, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }

    #[Route(path: '/m/pesa', name: 'm_pesa')]
    public function index(): Response
    {
        return $this->render('m_pesa/index.html.twig', [
            'controller_name' => 'MPesaController',
        ]);
    }

    #[Route(path: '/m/pesa/auth', name: 'm_pesa_auth')]
    public function auth(): Response
    {
        $response = $this->mpesa->auth();
        return new JsonResponse($response);
    }

    #[Route(path: '/m/pesa/simulate', name: 'm_pesa_simulate')]
    public function pushSimulate(Request $request): Response
    {
        $amount = $request->request->get('amount');
        $number = $request->request->get('number');
        $reference = $request->request->get('reference');

        [$status, $res] = $this->mpesa->pushSimulate($amount, $number, $reference);
        return new JsonResponse(['status' => $status, 'response' => $res]);
    }

    #[Route(path: '/make/online/payment/push', name: 'make_payment_from_website')]
    public function stkPush(Request $request)
    {

        $amount = (int) trim($request->request->get('amount'));
        // $amount = 1;
        $number = trim($request->request->get('number'));
        $reference = $request->request->get('reference');
        $description = $request->request->get('description');

        [$status, $res] = $this->mpesa->pushSTK($amount, $number, $reference, $description);
        return new JsonResponse(['status' => $status, 'response' => $res]);
        // return new JsonResponse([$amount, $number, $reference, $description]);
    }

    #[Route(path: '/checkout/submit/payment', name: 'checkout_submit_payment')]
    public function submitPayment(InstrumentRepository $instrumentRepo): \Symfony\Component\HttpFoundation\RedirectResponse
    {

        $data = [];
        // echo "<pre>";
        // print_r($_POST);
        // die();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // get the form data

            // $amount = (int) trim($request->request->get('amount'));
            $course_id = $_POST['instrument'];
            $weekday = $_POST['weekday'];
            $residence = $_POST['residence'];
            $startingdate = $_POST['startingdate'];
            $userexists = $_POST['userexists'];
            $request_id = $_POST['request_id'];
            $teacher_id = $_POST['teacher_id'];
            $regEmail = $_POST['regEmail'];
            $fullname = $_POST['fullname'];
            $regPhone = $_POST['regPhone'];
            $message = $_POST['message'];
            $gender = $_POST['gender'];
            $age = $_POST['age'];
            $parentname = $_POST['parentname'];
            $numbertocharge = $_POST['numbertocharge'];
            $amount = $_POST['amount'];

            $courseEntity = $this->courseRepo->findOneById($course_id);
            if($courseEntity) {
                $instrumentName = $courseEntity->getName();
            } else {
                $instrumentName = $course_id;
            }

            $data['instrument'] = $instrumentName;
            $data['instrument_id'] = $course_id;
            $data['weekday'] = $weekday;
            $data['residence'] = $residence;
            $data['startingdate'] = $startingdate;
            $data['userexists'] = $userexists;
            $data['request_id'] = $request_id;
            $data['teacher_id'] = $teacher_id;
            $data['regEmail'] = $regEmail;
            $data['fullname'] = $fullname;
            $data['regPhone'] = $regPhone;
            $data['message'] = $message;
            $data['gender'] = $gender;
            $data['age'] = $age;
            $data['parentname'] = $parentname;
            $data['numbertocharge'] = $numbertocharge;
            $data['amount'] = $amount;

            $reference = "CHEZA-$instrumentName";
            $description = "CHEZA-$instrumentName";

            $this->sendSms->quickSend('0705285959', 'Online potential', 'Attempting to register: ' . $fullname, "Details. Name: $fullname, Phone: $numbertocharge, Instrument: $instrumentName", 'necessary', 'personal_sms_from_office', 'nomail');

            // $amount = 1;

            // list($status, $res) = ["pending", "ws_CO_21122022220452114705285959"];
            [$status, $res] = $this->mpesa->pushSTK($amount, $numbertocharge, $reference, $description);

            return $this->redirectToRoute('checkout_confirm_payment', [
                'data' => json_encode($data, JSON_THROW_ON_ERROR),
                'status' => $status,
                'request_id' => $res,
            ]);

        }
    }

    #[Route(path: '/checkout/confirm/payment/{data}/{status}/{request_id}', name: 'checkout_confirm_payment')]
    public function confirmPayment($data, $status, $request_id): Response
    {

        // var_dump(json_decode($data, true));
        // die();
        return $this->render('default/payment_page.html.twig', [
            'data' => json_decode((string) $data, true, 512, JSON_THROW_ON_ERROR),
            'status' => $status,
            'request_id' => $request_id,
        ]);


    }

    #[Route(path: '/checkout/complete/payment', name: 'checkout_complete_payment')]
    public function completePayment(): \Symfony\Component\HttpFoundation\Response
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $fullname = $_POST['fullname'];
            $phone = $_POST['regPhone'];
            $course_id = $_POST['instrument_id'];
            $email = $_POST['regEmail'];
            $age = $_POST['age'];
            $sex = $_POST['gender'];
            $instrument = $_POST['instrument'];
            $request_id = $_POST['request_id'];
            $parentname = $_POST['parentname'];
            $message = $_POST['message'];
            $amount = $_POST['amount'];
            $residence = $_POST['residence'];
            $startingdate = $_POST['startingdate'];
            $weekday = $_POST['weekday'];
            $purpose = "CHEZA-$instrument";

            $courseEntity = $this->courseRepo->findOneById($course_id);
            if($courseEntity) {
                $instrumentName = $courseEntity->getName();
            } else {
                $instrumentName = $course_id;
            }

            [$status, $res] = $this->mpesa->checkStatus($request_id, $purpose);

            if($status == 'success') {
                $paymentStatus = "successful";

                // create new user if not exists
                $userExists = $this->userExists($email, 'student');

                // save the student details
                if($userExists) {
                    $student = $this->userRepo->findFirstByEmail($email, 'student');
                    $uig = $this->newUigForExistingStudent($student, $courseEntity, 'Grade 1');
                } else {
                    // list($student, $uig) = $this->saveStudent($instrument, $phone, $fullname, $email, $age, $gender, $password );
                    $student = $this->saveNewStudentAndUIG($courseEntity, $phone, $fullname, $email, $age, $sex, $parentname);
                }

                $this->followUp($request_id);
                $subject = "New Registration";
                $message = "Name: $fullname, Email: $email, Phone: $phone, Instrument: $instrumentName, Age: $age, Gender: $sex, Weekday: $weekday,
                            Message: $message, Amount paid: $amount, Starting date: $startingdate, Residence: $residence";
                $this->sendSms->quickSend('0705285959', 'Online client', 'Message From Registration ' . $fullname, "New Registration. Name: $fullname, Phone: $phone, Instrument: $instrumentName, Residence: $residence", 'necessary', 'personal_sms_from_office', 'nomail');
                $admins = $this->userRepo->findByUsertype('admin');
                foreach ($admins as $admin) {
                    $maildata = ['msg' => $message, 'user' => $admin];
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "communication.html.twig", "necessary", "personal_email_from_office");
                }

            } else {
                $paymentStatus = "not successful";
            }

            // var_dump($paymentStatus);
            // die();
            return $this->render('default/status.html.twig', [
                'status' => $status,
                'paymentStatus' => $paymentStatus,
                'request_id' => $request_id,
                'fullname' => $fullname,
                'phone' => $phone,
            ]);


        }


    }

    public function userExists($emailAddress, $usertype)
    {

        $data = [];
        $user = $this->userRepo->findFirstByEmail($emailAddress, $usertype);
        if($user) {
            return true;
        } else {
            return false;
        }

    }

    public function newUigForExistingStudent($student, $course, $gradeName)
    {

        $user = $student;
        $userdata = $user->getStudentdata();
        $entityManager = $this->managerRegistry->getManager();
        $instrument = $this->instrumentRepo->findOneById($course->getInstrument()->getId());

        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($this->instrumentGradeRepo->findOneByName($gradeName));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $entityManager->persist($userInstrument);

        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $entityManager->flush();

        return $userInstrument;

    }

    public function saveNewStudentAndUIG($course, $phone, $fullname, $email, $age, $sex, $parentname)
    {
        $user = new User();
        $userdata = new StudentUserData();
        $entityManager = $this->managerRegistry->getManager();

        $grade = 1; // array
        $branch = "main";
        $lessonmode = "physical";

        if($parentname == "") {

            $kinemail = "null";
            $kinfullname = "null";
            $kinphone = "null";
            $kinrship = "null";

        } else {

            $kinemail = $email;
            $kinfullname = $parentname;
            $kinphone = $phone;
            $kinrship = "parent";

        }

        $user->setPassword(
            $this->passwordEncoder->hashPassword(
                $user,
                $phone
            )
        );

        $user->setActive(true);
        $user->setFullname($fullname);
        $user->setEmail($email);
        $user->setUsertype("student");
        $entityManager->persist($user);

        $userdata->setPhone($phone);
        $userdata->setAge($age);
        $userdata->setSex($sex);
        $userdata->setBranch($branch);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinrship($kinrship);
        $userdata->setLessonmode($lessonmode);
        $entityManager->persist($userdata);

        $instrument = $this->instrumentRepo->findOneById($course->getInstrument()->getId());
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($this->instrumentGradeRepo->findOneById(1));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $entityManager->persist($userInstrument);


        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $user->setStudentdata($userdata);
        $entityManager->persist($user);

        // activate student
        $active = $user->getActive();
        $uigActives = $user->getStudentdata()->getUserInstruments();

        $entityManager->persist($user);

        $entityManager->flush();

        $this->addFlash(
            'success',
            'An account was created for ' . $user->getFullname(),
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return $user;

    }


    #[Route(path: '/pmt/stk_push/check/status', name: 'leepahnapush_status')]
    public function checkStatus(Request $request)
    {

        $request_id = $request->request->get('request_id');
        $purpose = $request->request->get('purpose');

        [$status, $res] = $this->mpesa->checkStatus($request_id, $purpose);
        if($status == 'success') {
            $this->followUp($request_id);
        }
        return new JsonResponse(['status' => $status, 'response' => $res]);


    }

    public function followUp($request_id)
    {

        $callback = $this->cbRepo->findOneByCheckoutRequestID($request_id);

        $json = $callback->getCallbackmetadata();

        $Amount = $this->mpesa->getVar($json, 'Amount', 3);
        $MpesaReceiptNumber = $this->mpesa->getVar($json, 'MpesaReceiptNumber', 3);
        $TransactionDate = $this->mpesa->getVar($json, 'TransactionDate', 3);
        $PhoneNumber = $this->mpesa->getVar($json, 'PhoneNumber', 3);

        $callback->setMpesaReceiptNumber($MpesaReceiptNumber);
        $callback->setTransactionDate($TransactionDate);
        $callback->setAmount($Amount);
        $callback->setPhoneNumber($PhoneNumber);
        $this->save($callback);

        return new JsonResponse('true');

    }

    #[Route(path: '/touch/script/pmt', name: 'callback')]
    public function callBack()
    {

        if($json = json_decode(file_get_contents("php://input"), true, 512, JSON_THROW_ON_ERROR)) {
            $CheckoutRequestID = $this->mpesa->getVar($json, 'CheckoutRequestID', 2);
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);
        } else {
            $json = $_POST;
            $CheckoutRequestID = $this->mpesa->getVar($json, 'CheckoutRequestID', 2);
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);
        }

        return new JsonResponse('true');

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


}
