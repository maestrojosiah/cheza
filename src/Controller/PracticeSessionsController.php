<?php

namespace App\Controller;

use App\Entity\PracticeSessions;
use App\Form\PracticeSessionsType;
use App\Repository\PracticeSessionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/practice/sessions')]
class PracticeSessionsController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'practice_sessions_index', methods: ['GET'])]
    public function index(PracticeSessionsRepository $practiceSessionsRepository): Response
    {
        return $this->render('practice_sessions/index.html.twig', [
            'practice_sessions' => $practiceSessionsRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'practice_sessions_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $practiceSession = new PracticeSessions();
        $form = $this->createForm(PracticeSessionsType::class, $practiceSession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($practiceSession);
            $entityManager->flush();

            return $this->redirectToRoute('practice_sessions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('practice_sessions/new.html.twig', [
            'practice_session' => $practiceSession,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'practice_sessions_show', methods: ['GET'])]
    public function show(PracticeSessions $practiceSession): Response
    {
        return $this->render('practice_sessions/show.html.twig', [
            'practice_session' => $practiceSession,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'practice_sessions_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, PracticeSessions $practiceSession): Response
    {
        $form = $this->createForm(PracticeSessionsType::class, $practiceSession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('practice_sessions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('practice_sessions/edit.html.twig', [
            'practice_session' => $practiceSession,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'practice_sessions_delete', methods: ['POST'])]
    public function delete(Request $request, PracticeSessions $practiceSession): Response
    {
        if ($this->isCsrfTokenValid('delete'.$practiceSession->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($practiceSession);
            $entityManager->flush();
        }

        return $this->redirectToRoute('practice_sessions_index', [], Response::HTTP_SEE_OTHER);
    }
}
