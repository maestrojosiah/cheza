<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Repository\GroupRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PackageRepository;
use App\Repository\PaymentRepository;
use App\Repository\SessionRepository;
use App\Repository\TemplateRepository;
use App\Repository\ContactRepository;
use App\Repository\TermRepository;
use App\Repository\CommunicationRepository;
use App\Repository\AttachmentRepository;
use App\Service\CalculateFees;
use App\Service\InvoiceService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/admin')]
class AdminController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'admin')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('admin/profile.html.twig', [
            'admin' => $this->getUser(),
        ]);
    }

    #[Route(path: '/list/active/students', name: 'admin_list_active_students')]
    public function activeStudents(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('admin/active_students_list.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    #[Route(path: '/complete/your/profile', name: 'admin_complete_profile')]
    public function completeprofile(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('admin/complete_profile.html.twig', [
            'admin' => $this->getUser(),
        ]);
    }

    #[Route(path: '/communications/sms/email/{template_id}', defaults: ['template_id' => null], name: 'admin-communications')]
    public function communications(TemplateRepository $templateRepo, AttachmentRepository $attachRepo, CommunicationRepository $commRepository, GroupRepository $groupRepo, $template_id = null): Response
    {
        if(null !== $template_id) {
            $template = $templateRepo->findOneById($template_id);
        } else {
            $template = null;
        }
        $groups = $groupRepo->findAll();
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $communications = $commRepository->findHistory();
        $attachments = $attachRepo->findAll();
        $templates = $templateRepo->findAll();

        return $this->render('admin/communication.html.twig', [
            'admin' => $this->getUser(),
            'templates' => $templates,
            'template' => $template,
            'groups' => $groups,
            'communications' => $communications,
            'attachments' => $attachments,
        ]);
    }

    #[Route(path: '/communications/notify/sms/{send_to}/{session_id}', name: 'admin-communications-notify')]
    public function communicationsFromLink(
        UserRepository $userRepo,
        SessionRepository $sessionRepo,
        TemplateRepository $templateRepo,
        AttachmentRepository $attachRepo,
        CommunicationRepository $commRepository,
        GroupRepository $groupRepo,
        $session_id,
        $send_to = null,
        $template_id = null
    ): Response {
        if(null !== $template_id) {
            $template = $templateRepo->findOneById($template_id);
        } else {
            $template = null;
        }
        $user = $userRepo->findOneById($send_to);
        $groups = $groupRepo->findAll();
        $session = $sessionRepo->findOneById($session_id);
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $communications = $commRepository->findHistory();
        $attachments = $attachRepo->findAll();
        $templates = $templateRepo->findAll();
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $days[$beginAt->format('H:i').'-'.$endAt->format('H:i').'-'.$beginAt->format('l')] = $beginAt->format('l');
        }

        $who = "";
        if(strlen((string) $session->getUserInstrumentGrade()->getSiblingname()) > 0) {
            $who = "Parent's";
        } else {
            $who = $session->getStudent()->getStudentdata()->getSex() == 'male' ? 'His' : 'Her';
        }
        $daysString = "";
        foreach($days as $key => $day) {
            $daysString .= ucfirst((string) $day)."s";
            $daysString .= " from ";
            if($key) {
                $daysString .= date('H:i', strtotime(explode("-", $key)[0]));
            }
            $daysString .= " to ";
            if($key) {
                $daysString .= date('H:i', strtotime(explode("-", $key)[1]));
            }
            $daysString .= ", ";


        }

        if($user->getUsertype() == 'teacher') {
            $msg = "Dear ".$session->getTeacher()->getFullname() . ".Please note you have a new " . $session->getUserInstrumentGrade()->getInstrument()->getName()." student on ".
            $daysString . "First lesson: " . $session->getUserInstrumentGrade()->getSessions()[0]->getStartingOn()->format('l, jS F Y').". ". $who . " name is " .
            $session->getStudent()->getFullname() . " ,tel: ". $session->getStudent()->getStudentData()->getPhone().".Regards.";
        } else {
            $msg = "Dear ". $session->getStudent()->getFullname() . ", Thank you for choosing the Cheza for your music lessons, your " . $session->getUserInstrumentGrade()->getInstrument()->getName() . " lessons will be on " .$daysString . "First lesson: " . $session->getUserInstrumentGrade()->getSessions()[0]->getStartingOn()->format('l, jS F Y') . ". We have assigned you Teacher ".
            $session->getTeacher()->getFullname() . ". phone no is " . $session->getTeacher()->getTeacherData()->getPhone() . ". they use room [?]. Payment can be made Via Mpesa Paybill Business no: Account No: Name of student." .
            " Regards, 
		  John Kungu ";
        }

        return $this->render('admin/communication_link.html.twig', [
            'admin' => $this->getUser(),
            'templates' => $templates,
            'template' => $template,
            'session' => $session,
            'user' => $user,
            'msg' => $msg,
            'days' => array_unique($days),
            'groups' => $groups,
            'communications' => $communications,
            'attachments' => $attachments,
        ]);
    }

    #[Route(path: '/communications/template/edit/{template_id}', name: 'edit_template_page')]
    public function editTemplate(TemplateRepository $templateRepo, $template_id): Response
    {

        $template = $templateRepo->findOneById($template_id);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $templates = $templateRepo->findAll();

        return $this->render('admin/edit_template.html.twig', [
            'template' => $template,
        ]);
    }

    #[Route(path: '/dashboard', name: 'admin-dashboard')]
    public function dashboard(UserRepository $userRepo, PackageRepository $packageRepo, ContactRepository $contactRepo, InstrumentRepository $instrumentRepo, SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = [];
        $insarr = [];
        $durationarr = [];
        $contactscount = $contactRepo->countAll();
        $studentscount = $userRepo->countUsersOfType('student');
        $teacherscount = $userRepo->countUsersOfType('teacher');
        $countinactive = $userRepo->countActive(false);
        $instruments = $instrumentRepo->findAll();
        $uigs = $uigRepo->findUigInstruments();
        $packages = $packageRepo->findAll();
        $instrumentscount = 0;
        $sessions = $sessionRepo->findAll();
        $data['countinactive'] = $countinactive;
        $data['studentscount'] = $studentscount;
        $data['teacherscount'] = $teacherscount;
        $data['instruments'] = $instruments;
        $data['sessions'] = $sessions;
        $data['contactscount'] = $contactscount;
        $data['uigs'] = $uigs;
        $teachers = $userRepo->findBy(
            ['usertype' => 'teacher'],
            ['id' => 'ASC'],
            8,
        );
        $admins = $userRepo->findBy(
            ['usertype' => 'admin'],
            ['id' => 'ASC'],
            4,
        );
        $students = $userRepo->findBy(
            ['usertype' => 'student'],
            ['id' => 'DESC'],
            8,
        );
        $data['teachers'] = $teachers;
        $data['students'] = $students;
        $data['admins'] = $admins;
        foreach ($uigs as $key => $uig) {
            $inscount = $uigRepo->countForThisInstrument($uig->getInstrument());
            $insarr[$uig->getInstrument()->getName()] = $inscount."-".$uig->getInstrument()->getId();
        }
        foreach ($packages as $key => $package) {
            $durationarr[$package->getName()] = $sessionRepo->countForThisPackage($package)."-".$package->getDuration().$package->getId();
        }

        // Extract the numeric values from the array
        $numericValues = array_map(function ($value) {
            return explode('-', $value)[0];
        }, $insarr);

        $instrumentscount += array_sum($numericValues);
        $data['instrumentscount'] = $instrumentscount;
        $data['insarr'] = $insarr;
        $data['durationarr'] = $durationarr;
        return $this->render('admin/dashboard.html.twig', [
            'data' => $data,
        ]);
    }

    #[Route(path: '/register/new/student', name: 'student-registration-admin')]
    public function studentReg(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('admin/student_reg.html.twig', [
            'student' => $this->getUser(),
            'data' => $data,
        ]);
    }

    #[Route(path: '/register/edit/student/{studentid}', name: 'student-edit-admin')]
    public function studentEdit(UserRepository $userRepo, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo, $studentid): Response
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $student = $userRepo->findOneById($studentid);
        $uigs = $student->getStudentdata()->getUserInstruments();
        $myInstruments = [];
        $myGrades = [];
        foreach ($uigs as $uig) {
            $myInstruments[] = $uig->getInstrument()->getId();
            $myGrades[] = $uig->getInstrument()->getId().'-'.$uig->getGrade()->getId();
        }

        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $data['myInstruments'] = $myInstruments;
        $data['myGrades'] = $myGrades;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('admin/student_edit.html.twig', [
            'student' => $student,
            'data' => $data,
        ]);
    }

    #[Route(path: '/view/teacher/calendar/{teacher_id}', name: 'admin_session_calendar', methods: ['GET'])]
    public function calendar(UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo, $teacher_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $teacher = $userRepo->findOneById($teacher_id);
        $schedules = $schoolTimeRepo->findByTeacher($teacher);
        return $this->render('admin/calendar.html.twig', [
            'teacher_id' => $teacher_id,
            'teacher' => $teacher,
            'schedules' => $schedules,
        ]);
    }

    #[Route(path: '/view/full/teachers/calendar', name: 'admin_full_calendar', methods: ['GET'])]
    public function fullCalendar(UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $schedules = $schoolTimeRepo->findAll();
        return $this->render('admin/full-calendar.html.twig', [
            'schedules' => $schedules,
        ]);
    }

    #[Route(path: '/teacher/profile/{teacher_id}', name: 'teacher_profile_admin')]
    public function teacherProfile(UserRepository $userRepo, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo, $teacher_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $teacher = $userRepo->findOneById($teacher_id);
        $data = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('teacher/profile.html.twig', [
            'teacher' => $teacher,
            'data' => $data,
        ]);
    }

    #[Route(path: '/teachers/list', name: 'admin-teachers-list')]
    public function teachersList(UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $hourly = [];
        $fourtyfive = [];
        $thirty = [];
        $listofteachers = [];
        $teachers = $userRepo->findBy(
            ['usertype' => 'teacher', 'active' => 1],
            ['id' => 'DESC']
        );
        foreach ($teachers as $teacher) {
            foreach ($teacher->getTeachersessions() as $session) {
                if($session->getDone() == false) {
                    if($session->getPackage()->getDuration() == '60 Minutes') {
                        $hourly[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    } elseif ($session->getPackage()->getDuration() == '45 Minutes') {
                        $fourtyfive[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    } elseif ($session->getPackage()->getDuration() == '30 Minutes') {
                        $thirty[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    }
                }
            }
            $listofteachers[$teacher->getId()] = ['hourly' => count($hourly), 'fourtyfive' => count($fourtyfive), 'thirty' => count($thirty), 'user' => $teacher];
            $hourly = [];
            $fourtyfive = [];
            $thirty = [];
        }

        return $this->render('admin/teacherslist.html.twig', [
            'controller_name' => 'StudentController',
            'teachers' => $teachers,
            'hourly' => $hourly,
            'fourtyfive' => $fourtyfive,
            'thirty' => $thirty,
            'listofteachers' => $listofteachers,
        ]);
    }

    #[Route(path: '/students/list', name: 'admin-students-list')]
    public function studentsList(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $students = $userRepo->findByUsertype('student');
        $uigs = $uigRepo->findUIGForStudents();
        return $this->render('admin/studentslist.html.twig', [
            'controller_name' => 'StudentController',
            'students' => $students,
            'uigs' => $uigs,
        ]);
    }

    #[Route(path: '/students/details/{student_id}', name: 'student_details_admin')]
    public function studentsDetails(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, $student_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $student = $userRepo->findOneById($student_id);
        // $test = $uigRepo->findAllJoinedToPayment();
        return $this->render('admin/student_details.html.twig', [
            'student' => $student,
        ]);
    }

    #[Route('/fetch/student/details', name: 'fetch_student_details', methods: ['GET'])]
    public function fetchStudentDetails(UserRepository $userRepo, Request $request): Response
    {
        $studentId = $request->query->get('id');
        
        // Fetch student details (replace with your entity and logic)
        $student = $userRepo->find($studentId);

        if (!$student) {
            return new Response('Student not found', 404);
        }

        // Render a partial template with the student details
        return $this->render('admin/partial_student_details.html.twig', [
            'student' => $student,
        ]);
    }


    #[Route(path: '/change/teacher/percentage/{teacher_id}', name: 'admin-change-percentage')]
    public function changePercentage(UserRepository $userRepo, $teacher_id): never
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $teacher = $userRepo->findOneById($teacher_id);
        $teacherdata = $teacher->getTeacherdata();

        $percentage = $_POST['percentage'];
        $teacherdata->setPercentage($percentage);
        $em = $this->managerRegistry->getManager();
        $em->persist($teacherdata);
        $em->flush();

        echo "<pre>";
        echo "Success";
        die();
        // $uigs = $uigRepo->findUIGForStudents();
        // return $this->render('admin/studentslist.html.twig', [
        //     'controller_name' => 'StudentController',
        //     'students' => $students,
        //     'uigs' => $uigs,
        // ]);
    }

    #[Route(path: '/change/uig/percentage/{uig_id}', name: 'admin-change-uig-percentage')]
    public function changeUigPercentage(UserInstrumentGradeRepository $uigRepo, $uig_id): never
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $uig = $uigRepo->findOneById($uig_id);

        $percentage = $_POST['percentage'];
        $uig->setPercentage($percentage);
        $em = $this->managerRegistry->getManager();
        $em->persist($uig);
        $em->flush();

        echo "<pre>";
        echo "Success";
        print_r($uig->getId());
        print_r($uig->getPercentage());
        die();

    }

    #[Route(path: '/teachers/invoices/sum', name: 'teacher_sinvoices_sum', methods: ['GET'])]
    public function teachersInvoices(InvoiceService $invoiceService, UserRepository $userRepo, SessionRepository $sessionRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // find users that are teachers
        $teachers = $userRepo->findByUsertype('teacher');

        // filter to only those who have sessions
        $teachersWithSessions = [];
        foreach ($teachers as $key => $teacher) {
            if((is_countable($teacher->getTeachersessions()) ? count($teacher->getTeachersessions()) : 0) > 0) {
                $teachersWithSessions[] = $teacher;
            }
        }

        $t = [];
        $tSum = 0;
        // iterate through the teachers with sessions and do the maths for each
        foreach ($teachersWithSessions as $key => $tws) {
            // get the sessions that are done and not invoiced
            $doneButNotInvoicedSessions = $sessionRepo->findBy(
                ['teacher' => $tws, 'invoiced' => null, 'done' => 1],
                ['id' => 'ASC']
            );
            // start calculation
            $sum = 0;
            foreach ($doneButNotInvoicedSessions as $key => $dnis) {
                $commission = $invoiceService->calculateCommission($dnis);
                $sum += $commission;
                $tSum += $commission;
            }
            $t[$tws->getFullname().'-'.$tws->getId()] = $sum;
        }


        return $this->render('admin/teachers_invoices.html.twig', [
            'invList' => $t,
            'total' => $tSum,
        ]);

    }


    #[Route(path: '/payments/list/{uig_id}', name: 'admin-payments-list')]
    public function paymentsList(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $uig = $uigRepo->findOneById($uig_id);
        $student = $uig->getStudentUserData()->getStudent();
        $payments = $student->getPayments();
        $uigPayments = $uig->getPayments();
        return $this->render('admin/payments.html.twig', [
            'student' => $student,
            'uig' => $uig,
            'payments' => $payments,
            'uigPayments' => $uigPayments,
        ]);
    }

    #[Route(path: '/remind/parent/prepay/{studentId}/{uigId}/{notif_type}', name: 'admin-remind-parent')]
    public function remindParentPayment(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, CalculateFees $feesCalculator, $studentId, $uigId, $notif_type): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $uig = $uigRepo->findOneById($uigId);
        $uigName = explode(' - ', $uig)[0];
        $student = $userRepo->findOneById($studentId);
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();
        
        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        return $this->render('admin/remind_about_payments.html.twig', [
            'notif_type' => $notif_type,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);
    }


    #[Route(path: '/security/role', name: 'assign_access_roles')]
    public function assign_admin_role(UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $admins = $userRepo->findBy(
            ['usertype' => 'admin'],
            ['id' => 'DESC']
        );

        $students = $userRepo->findBy(
            ['usertype' => 'student'],
            ['id' => 'DESC']
        );

        $teachers = $userRepo->findBy(
            ['usertype' => 'teacher'],
            ['id' => 'DESC']
        );

        // $admins = $userRepo->findByUsertype('admin');
        // $students = $userRepo->findByUsertype('student');
        // $teachers = $userRepo->findByUsertype('teacher');
        return $this->render('admin/roles.html.twig', [
            'admins' => $admins,
            'students' => $students,
            'teachers' => $teachers,
        ]);
    }

    #[Route(path: '/admin/security/now/role/add/new/{usertype}/{user_id}/{roletype}', name: 'add_role')]
    public function assign_new_role(UserRepository $userRepo, $usertype, $user_id, $roletype): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $has_role = $user->hasRole($user->getRoles(), $roletype);
        if($has_role) {
            $user->setRoles([]);
        } else {
            $roles = [];
            $roles[] = $roletype;
            $user->setRoles([$roletype]);
        }

        $em = $this->managerRegistry->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('assign_access_roles');
    }

    #[Route(path: '/admin/security/now/activate/{user_id}', name: 'toggle_activate')]
    public function toggleStudentActivate(UserRepository $userRepo, $user_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $active = $user->getActive();
        if($user->getUsertype() == 'student') {
            $uigActives = $user->getStudentdata()->getUserInstruments();
        } else {
            $uigActives = $user->getTeacherdata()->getUserInstruments();
        }

        $em = $this->managerRegistry->getManager();

        $activeText = "";
        if($active) {
            $user->setActive(false);
            $user->setIsVerified(false);
            foreach ($uigActives as $uigActive) {
                $uigActive->setActive(false);
                $em->persist($uigActive);
            }
            $activeText = "Inactive";
        } else {
            $user->setActive(true);
            $user->setIsVerified(true);
            foreach ($uigActives as $uigActive) {
                $uigActive->setActive(true);
                $em->persist($uigActive);
            }
            $activeText = "Active";
        }


        $em->persist($user);

        $em->flush();
        return $this->redirectToRoute('assign_access_roles');
        // return $this->render('default/test.html.twig', [
        //     'test' => $uigActive,
        // ]);
    }

    #[Route(path: '/admin/statement/preview', name: 'preview_statement', methods: ['GET'])]
    public function previewStatement(UserInstrumentGradeRepository $userInstrumentGradeRepo, PaymentRepository $paymentRepo, TermRepository $termRepo): Response
    {
        $dateTimeFrom = null;
        $dateTimeTo = null;
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $dateFrom = explode("=", explode("&", (string) $_SERVER['QUERY_STRING'])[0])[1];
            $dateTimeFrom = new \Datetime($dateFrom." 00:00:00");
            $dateTo = explode("=", explode("&", (string) $_SERVER['QUERY_STRING'])[1])[1];
            $dateTimeTo = new \Datetime($dateTo." 23:59:59");
            $uig_ids = explode("-", explode("=", explode("&", (string) $_SERVER['QUERY_STRING'])[2])[1]);
            $uig_ids = array_unique($uig_ids);
            $uigs = [];
            $insts = [];
            foreach ($uig_ids as $uigid) {
                $u = $userInstrumentGradeRepo->findOneById($uigid);
                $uigs[] = $u;
                $insts[] = $u->getInstrument()->getName();
            }

        } else {
            $dateFrom = "";
            $dateTo = "";
            $uigs = null;
            $insts = [];
        }
        $dates = [];
        $student = $uigs[0]->getStudentUserData()->getStudent();
        $payments = $paymentRepo->findAllWithinRange($dateTimeFrom, $dateTimeTo, $student);
        $totalInvoicesBD = $paymentRepo->findSumOfTypeForUserBeforeDate("inv", $dateTimeFrom, $student)['totalAmount'];
        $totalPaymentsBD = $paymentRepo->findSumOfTypeForUserBeforeDate("pmt", $dateTimeFrom, $student)['totalAmount'];
        $totalInvoices = $paymentRepo->findSumOfTypeForUser("inv", $student)['totalAmount'];
        $totalPayments = $paymentRepo->findSumOfTypeForUser("pmt", $student)['totalAmount'];
        $dateTimeFrom->setDate(date('Y'), $dateTimeFrom->format('m'), $dateTimeFrom->format('d'));
        $currentTerm = $termRepo->findOneByTermnumber($termRepo->findCurrentTerm($dateTimeFrom));
        if(null !== $currentTerm) {
            $lastDayOfPreviousTerm = $currentTerm->getStartingOn()->modify("yesterday");
        } else {
            $today = new \Datetime();
            $currentTerm = $termRepo->findOneByTermnumber($termRepo->findCurrentTerm($today));
            $lastDayOfPreviousTerm = $currentTerm->getStartingOn();
        }
        $balanceCF = $totalInvoicesBD - $totalPaymentsBD;
        $currentBalance = $totalInvoices - $totalPayments;

        return $this->render('admin/preview_statement.html.twig', [
            'student' => $student,
            'payments' => $payments,
            'uigs' => $uigs,
            'balanceCF' => $balanceCF,
            'currentBalance' => $currentBalance,
            'lastDayOfPreviousTerm' => $lastDayOfPreviousTerm,
            'instruments' => array_unique($insts),
        ]);
    }

    #[Route(path: '/admin/invoice/preview/{payment_id}', name: 'preview_invoice', methods: ['GET'])]
    public function previewInvoice(TermRepository $termRepo, PaymentRepository $paymentRepo, CalculateFees $feesCalculator, $payment_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());

        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $student = $payment->getUser();
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);

        return $this->render('admin/preview_invoice.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);
    }

    #[Route(path: '/admin/student/invoice/preview/{user_id}', name: 'preview_invoice_user', methods: ['GET'])]
    public function previewUserInvoice(UserRepository $userRepo, $user_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $student = $userRepo->findOneById($user_id);
        $studentData = $student->getStudentdata();
        // $dateTimeFrom = new \Datetime($starting_from." 00:00:00");
        // $dateTimeTo = new \Datetime($to." 23:59:59");

        $uigs = $studentData->getUserInstruments();
        $uigsWithBalance = [];
        $uigsWithNegativeBalance = [];
        $studentdata = $student->getStudentdata();
        $usertype = $student->getUsertype();

        $bal = 0;
        $instruments = [];
        // echo $student;
        // echo "<br/>";

        foreach ($uigs as $key => $uig) {

            $payments = $uig->getPayments();
            $uig_bal = 0;

            if((is_countable($payments) ? count($payments) : 0) > 0 && $studentdata != null && $usertype = "student") {

                foreach ($payments as $key => $payment) {

                    $instruments[] = $uig->getInstrument();
                    $bal = $payment->getType() == "inv" ? $bal + $payment->getAmount() : $bal - $payment->getAmount();
                    $uig_bal = $payment->getType() == "inv" ? $uig_bal + $payment->getAmount() : $uig_bal - $payment->getAmount();
                    // print_r($instruments);
                    // echo  "<br/>";
                    // print_r($payment->getAmount() . ": " . $bal. " : " . $payment->getType() . "<br />");

                }

            }

            if($uig_bal > 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();

            }
            if($uig_bal < 0 && (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0) > 0) {
                $uigsWithNegativeBalance[$uig->getId()] = $uig_bal . '|' . $uig . '|' . $uig->getSessions()[0]->getTeacher() . '|' . (is_countable($uig->getSessions()) ? count($uig->getSessions()) : 0)  . '|' . $uig->getSessions()[0]->getPackage()->getCostPerLesson();
            }
            // echo "<br/>Bal: " . $bal;

        }
        // echo "<br/>";
        // print_r($uigsWithBalance);
        // die();
        // if($bal > 0 && stripos($student->getFullname(), 'school') === false ){

        //     $short = $student->getShortUrls()[0];
        //     $instruments = implode(", ", array_unique($instruments));

        //     if(null == $short){
        //         $shortUrl = "";
        //     } else {
        //         $shortUrl = $short->getShort();
        //     }


        // }



        // foreach ($studentData->getUserInstruments() as $key => $uig) {
        //     // if uig has balance add to list
        // }

        return $this->render('admin/preview_student_invoice.html.twig', [
            'student' => $student,
            'instruments' => array_unique($instruments),
            'studentData' => $studentData,
            'balance' => $bal,
            'uigsWithBalance' => $uigsWithBalance,
            'uigsWithNegativeBalance' => $uigsWithNegativeBalance,
        ]);
    }

    #[Route(path: '/admin/receipt/preview/{payment_id}', name: 'preview_receipt', methods: ['GET'])]
    public function previewReceipt(TermRepository $termRepo, PaymentRepository $paymentRepo, CalculateFees $feesCalculator, $payment_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());

        $uig = $payment->getUig();
        $uigName = explode(' - ', $uig)[0];
        $student = $payment->getUser();
        $sessions = $uig->getSessions();
        $firstSession = $sessions[0];
        
        // get package
        $package = $firstSession->getPackage();
        
        // get price per session
        $pricePerSession = $package->getCostPerLesson();

        // count all sessions
        $number_of_sessions = count($sessions);

        list($cashPaid, $unpaid_lessons, $unpaid_sessions, $total_amount, $partial_payment_info) = $feesCalculator->calculateBalances($sessions, $uig, $pricePerSession);
        
        return $this->render('admin/preview_receipt.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
            'student' => $student,
            'uigName' => $uigName,
            'teacher' => $firstSession->getTeacher(),
            'cashPaid' => $cashPaid,
            'unpaid_lessons' => $unpaid_lessons,
            'unpaid_sessions' => $unpaid_sessions,
            'total_amount' => $total_amount,
            'uig' => $uig,
            'partial_payment_info' => $partial_payment_info,
        ]);
    }

    #[Route(path: '/admin/find/user/from/search', name: 'findthisuser')]
    public function findThisUser(Request $request, UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);
        $data = [];
        $data['email'] = $user->getEmail();
        $data['fullname'] = $user->getFullname();
        if($user->getUsertype() == 'student') {
            $data['phone'] = $user->getStudentdata()->getPhone();
        } elseif($user->getUsertype() == 'teacher') {
            $data['phone'] = $user->getTeacherdata()->getPhone();
        } else {
            $data['phone'] = "Not provided";
        }

        return new JsonResponse($data);
    }

    #[Route(path: '/admin/teacher/students/list/{teacher_id}', name: 'admin-teacher-students-list')]
    public function reportStudentsList(TermRepository $termRepo, SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, $teacher_id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $teacher = $userRepo->findOneById($teacher_id);
        $sessionsuniq = [];
        $term1 = [];
        $term2 = [];
        $term3 = [];
        $sentreports = [];
        $reports = [];

        $r = [];
        foreach ($teacher->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $reports[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }
            
        }

        $currentTerm = $termRepo->findCurrentTermArr(new \Datetime());
        $ct = $termRepo->findOneByTermnumber($currentTerm);
        $terms = $termRepo->findAll();
        $uigsForThisTeacher = $uigRepo->findBy(
            array('teacher' => $teacher),
        );

        // $termStart = $ct->getStartingOn();
        // $termEnd = $ct->getEndingOn();
        // $sessions = $sessionRepo->findForThisTerm($termStart, $termEnd, $teacher);
        $termStart = $ct->getStartingOn();
        $termEnd = $ct->getEndingOn();
        $sessions = $teacher->getTeachersessions();



        foreach ($sessions as $key => $sess) {
            $uig = $sess->getUserInstrumentGrade();

            $firstSession = $sessionRepo->getOneWithThisYearDate($uig->getId());
            if (!empty($firstSession)) {
                $id = $firstSession[0]['id'];
                $s = $sessionRepo->find($id);
                $firstSessionDate = $s->getStartingOn();
                $prevailingTerm = $termRepo->findCurrentTerm($firstSessionDate);
                if($prevailingTerm == "Term 1" || $s->getUserInstrumentGrade()->getTerm() == "Term 1") {
                    $term1[$uig->getId()] = $s;
                } 
                
                if ($prevailingTerm == "Term 2" || $s->getUserInstrumentGrade()->getTerm() == "Term 2") {
                    $term2[$uig->getId()] = $s;
                } 
                
                if ($prevailingTerm == "Term 3" || $s->getUserInstrumentGrade()->getTerm() == "Term 3") {
                    $term3[$uig->getId()] = $s;
                }
                $sessionsuniq[$s->getUserInstrumentGrade()->getId().'-'.$prevailingTerm] = $s;
            }
            
        }
        return $this->render('teacher/studentslist.html.twig', [
            'sessions' => $sessionsuniq,
            'teacher' => $teacher,
            'sentreports' => $sentreports,
            'currentTerm' => $currentTerm,
            'reports' => $reports,
            'r' => $r,
            'terms' => ['Term 1' => $term1, 'Term 2' => $term2, 'Term 3' => $term3],
            'uigsForThisTeacher' => $uigsForThisTeacher,
            // 'terms' => ['Term 1' => $term1, 'Term 2' => $term2, 'Term 3' => $term3],

        ]);

    }

    #[Route(path: '/admin/cheza/search/user', name: 'ajax_search')]
    public function search(Request $request, UserRepository $userRepo): Response
    {

        $result = [];
        $requestString = $request->request->get('q');

        $entities =  $userRepo->findEntitiesByString($requestString);

        if(!$entities) {
            $result['entities']['error'] = "Name not found";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }

        return new JsonResponse(json_encode($result, JSON_THROW_ON_ERROR));

    }


    public function getRealEntities($entities)
    {

        $realEntities = [];
        foreach ($entities as $entity) {
            $realEntities[$entity->getId()] = $entity->getFullname();
        }

        return $realEntities;
    }

    #[Route(path: '/teacher/attendance/{teacher}', name: 'admin_student_attendance', methods: ['GET'])]
    public function attendance(InvoiceService $invoiceService, UserInstrumentGradeRepository $userInstrumentGradeRepo, UserRepository $userRepo, $teacher): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $teacher = $userRepo->findOneById($teacher);

        // $statement = $connection->prepare('SELECT * FROM tbl WHERE col = ?');
        // $result = $statement->executeQuery();

        // $rows = $result->fetchAllAssociative();

        $em = $this->managerRegistry->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';

        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters
        $statement->bindValue('thisteacher', $teacher->getId());
        $result = $statement->executeQuery();

        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $result->FetchAllAssociative();

        foreach ($userInstrumentGradesIds as $uigId) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session) {
                if($session->getDone() == true && $session->getInvoiced() == false) {
                    $commission = $invoiceService->calculateCommission($session);
                    $inv += $commission;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)) {
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }
        // dd($inv);
        $days = [];
        foreach ($userInstrumentGrades as $uig) {

            $uigsessions = $uig->getSessions();
            foreach ($uigsessions as $key => $sess) {
                $beginAt = $sess->getBeginAt();
                $endAt = $sess->getEndAt();
                $days[$uig->getId().'-'.$beginAt->format('l')] = $beginAt->format('l').' '.$beginAt->format('H:i').' - '.$endAt->format('H:i');
            }

        }

        return $this->render('teacher/attendance.html.twig', [
            'teacher' => $teacher,
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'days' => $days,
            'inv' => $inv,
        ]);
    }

    #[Route(path: '/teacher/invoice/confirm/{teacher_id}', name: 'admin_send_invoice_confirm', methods: ['GET'])]
    public function sendInvoiceConfirm(
        InvoiceService $invoiceService, 
        UserInstrumentGradeRepository $userInstrumentGradeRepo,
        UserRepository $userRepo,
        $teacher_id
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $teacher = $userRepo->find($teacher_id);
        if(!$teacher instanceof User){
            throw new \Exception('not user');
        }

        $em = $this->managerRegistry->getManager();
        if (!$em instanceof EntityManager) {
            throw new \Exception('Not an EntityManager instance');
        }
        // Fetch user instrument grade IDs
        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session WHERE session.teacher_id = :thisteacher;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->bindValue('thisteacher', $teacher->getId());
        $result = $statement->executeQuery();
        $userInstrumentGradesIds = $result->fetchAllAssociative();
    
        // Fetch UserInstrumentGrades and their sessions
        $userInstrumentGrades = [];
        foreach ($userInstrumentGradesIds as $uigId) {
            $uig = $userInstrumentGradeRepo->findOneById($uigId['user_instrument_grade_id']);
            if ($uig) {
                $userInstrumentGrades[] = $uig;
            }
        }
    
        $teacherData = $teacher->getTeacherdata();
        if ($teacherData && $teacherData->getPercentage() !== null) {
            $percentage = (float)$teacherData->getPercentage();
        } else {
            $percentage = 0.7;
        }
        // $percentage = $teacherData->getPercentage() ?? 0.7; // Default percentage if not set
        $deductions = array_reduce($teacherData->getDeductions()->toArray(), fn($sum, $d) => $sum + $d->getToDeduct(), 0);
    
        $invoiceData = [];
        $totalInvoiceAmount = 0;
        $nonEmptyUigs = []; // This is where we will store the grouped session data
    
        foreach ($userInstrumentGrades as $uig) {
            $sessionGroups = [];
            foreach ($uig->getSessions() as $session) {
                if ($session->getDone() && !$session->getInvoiced()) {
                    $package = $session->getPackage();
                    $pricePerSession = $package->getPrice() / $package->getNoOfSessions();
                    // $uigPercentage = $uig->getPercentage() ?? $percentage;
                    if ($uig && $uig->getPercentage() !== null && !empty($uig->getPercentage())) {
                        $uigPercentage = (float)$uigPercentage = $uig->getPercentage();
                    } else {
                        $uigPercentage = $percentage;
                    }
                    $sessionCommission = $invoiceService->calculateCommission($session);
    
                    $totalInvoiceAmount += $sessionCommission;
                    // Group sessions by userInstrumentGradeId and packageId
                    $sessionGroups[] = [
                        'packageId' => $package->getId(),
                        'pricePerSession' => $pricePerSession,
                        'uigPercentage' => $uigPercentage,
                        'sessionCommission' => $sessionCommission,
                    ];
    
                    // Building nonEmptyUigs
                    $nonEmptyUigs[$uig->getId()][] = $uig->getId() . '-' . $package->getId(); // same as in the original code
                }
            }
            if (!empty($sessionGroups)) {
                $invoiceData[] = [
                    'userInstrumentGradeId' => $uig->getId(),
                    'studentName' => $uig->getStudentUserData()->getStudent()->getFullName(), 
                    'noOfSessions' => count($sessionGroups),
                    'sessions' => $sessionGroups,
                ];
            }
        }
        // dd($invoiceData);

        return $this->render('teacher/send_invoice.html.twig', [
            'teacher' => $teacher,
            'invoiceData' => $invoiceData,
            'totalInvoiceAmount' => $totalInvoiceAmount,
            'ded' => $deductions,
            'nonEmptyUigs' => $nonEmptyUigs,  
            'inv' => $totalInvoiceAmount,
        ]);
    }
    

}
