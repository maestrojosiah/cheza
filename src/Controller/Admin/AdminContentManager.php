<?php

namespace App\Controller\Admin;

use App\Entity\Instrument;
use App\Entity\InstrumentGrade;
use App\Entity\Package;
use App\Entity\Term;
use App\Entity\Deductions;
use App\Entity\DeductionPayment;
use App\Form\InstrumentGradeType;
use App\Form\InstrumentType;
use App\Form\PackageType;
use App\Form\TermType;
use App\Form\DeductionsType;
use App\Form\DeductionPaymentType;
use App\Repository\InstrumentGradeRepository;
use App\Repository\InstrumentRepository;
use App\Repository\PackageRepository;
use App\Repository\TermRepository;
use App\Repository\DeductionsRepository;
use App\Repository\DeductionPaymentRepository;
use App\Repository\SchoolTimeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/content/management/area')]
class AdminContentManager extends AbstractController
{
    public function __construct(
        private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry,
        private readonly InstrumentGradeRepository $instrumentGradeRepo,
        private readonly InstrumentRepository $instrumentRepo,
        private readonly PackageRepository $packageRepo,
        private readonly DeductionsRepository $deductionsRepo,
        private readonly DeductionPaymentRepository $deducttionPmtRepo,
        private readonly TermRepository $termRepo,
    ) {
    }
    #[Route(path: '/index/{entity}', name: 'user_admin_index', methods: ['GET'])]
    public function index(
        InstrumentGradeRepository $grades,
        InstrumentRepository $instruments,
        PackageRepository $packages,
        TermRepository $terms,
        DeductionsRepository $deductions,
        DeductionPaymentRepository $deductionPayments,
        SchoolTimeRepository $schedules,
        $entity = ''
    ): Response {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $this->getUser();
        $title = ucfirst((string) $entity);

        switch ($entity) {
            case 'grades':
                $entity = $grades->findAll();
                break;
            case 'instruments':
                $entity = $instruments->findAll();
                break;
            case 'packages':
                $entity = $packages->findAll();
                break;
            case 'deductions':
                $entity = $deductions->findAll();
                break;
            case 'deductionPayments':
                $entity = $deductionPayments->findAll();
                break;
            case 'terms':
                $entity = $terms->findAll();
                break;
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/index.html.twig', [
            'user' => $user,
            'sidebar_menu' => $sidebar_menu,
            'entity' => $entity,
            'title' => $title,
        ]);
    }

    #[Route(path: '/edit/{entity_name}/{id}', name: 'admin_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, $entity_name, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $usable_entity_name = rtrim(ucfirst((string) $entity_name), "s");
        $user = $this->getUser();
        switch ($entity_name) {
            case 'grades':
                $entity = $this->instrumentGradeRepo->findOneById($id);
                $form = $this->createForm(InstrumentGradeType::class, $entity);
                break;
            case 'instruments':
                $entity = $this->instrumentRepo->findOneById($id);
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
            case 'packages':
                $entity = $this->packageRepo->findOneById($id);
                $form = $this->createForm(PackageType::class, $entity);
                break;
            case 'deductions':
                $entity = $this->deductionsRepo->findOneById($id);
                $form = $this->createForm(DeductionsType::class, $entity);
                break;
            case 'deductionPayments':
                $entity = $this->deducttionPmtRepo->findOneById($id);
                $form = $this->createForm(DeductionPaymentType::class, $entity);
                break;
            case 'terms':
                $entity = $this->termRepo->findOneById($id);
                $form = $this->createForm(TermType::class, $entity);
                break;
            default:
                $entity = $this->instrumentRepo->findOneById($id);
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($usable_entity_name == 'Instrument') {
                $image = $form->get('image')->getData();
                $category = $form->get('category')->getData();
                $instrument_title = $form->get('name')->getData();
                $trimmed_title = str_replace(" ", "_", (string) $instrument_title);
                $originalName = $image->getClientOriginalName();
                ;
                $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
                $image->move($filepath, $originalName);
                $simple_filepath = "/img/instruments/$category/$trimmed_title/";
                $entity->setImage($simple_filepath . $originalName);
            }
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/edit.html.twig', [
            'entity' => $entity,
            'user' => $user,
            'form' => $form,
            'usable_entity_name' => $usable_entity_name,
            'sidebar_menu' => $sidebar_menu,
        ]);
    }

    #[Route(path: '/add/{entity_name}', name: 'admin_add', methods: ['GET', 'POST'])]
    public function add(Request $request, $entity_name): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $usable_entity_name = rtrim(ucfirst((string) $entity_name), "s");
        $user = $this->getUser();
        switch ($entity_name) {
            case 'grades':
                $entity = new InstrumentGrade();
                $form = $this->createForm(InstrumentGradeType::class, $entity);
                break;
            case 'instruments':
                $entity = new Instrument();
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
            case 'packages':
                $entity = new Package();
                $form = $this->createForm(PackageType::class, $entity);
                break;
            case 'deductions':
                $entity = new Deductions();
                $form = $this->createForm(DeductionsType::class, $entity);
                break;
            case 'deductionPayments':
                $entity = new DeductionPayment();
                $form = $this->createForm(DeductionPaymentType::class, $entity);
                break;
            case 'terms':
                $entity = new Term();
                $form = $this->createForm(TermType::class, $entity);
                break;
            default:
                $entity = new Instrument();
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($usable_entity_name == 'Instrument') {
                $image = $form->get('image')->getData();
                $category = $form->get('category')->getData();
                $instrument_title = $form->get('name')->getData();
                $trimmed_title = str_replace(" ", "_", (string) $instrument_title);
                $originalName = $image->getClientOriginalName();
                ;
                $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
                $image->move($filepath, $originalName);
                $simple_filepath = "/img/instruments/$category/$trimmed_title/";
                $entity->setImage($simple_filepath . $originalName);
            }
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/add.html.twig', [
            'entity' => $entity,
            'user' => $user,
            'form' => $form,
            'usable_entity_name' => $usable_entity_name,
            'sidebar_menu' => $sidebar_menu,
        ]);
    }

    #[Route(path: '/delete/{entity_name}/{id}', name: 'admin_delete', methods: ['DELETE', 'GET', 'POST'])]
    public function delete(Request $request, $entity_name, $id): Response
    {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $entity = match ($entity_name) {
            'grades' => $this->instrumentGradeRepo->findOneById($id),
            'instruments' => $this->instrumentRepo->findOneById($id),
            'packages' => $this->packageRepo->findOneById($id),
            'deductions' => $this->deductionsRepo->findOneById($id),
            'deductionPayments' => $this->deducttionPmtRepo->findOneById($id),
            'terms' => $this->termRepo->findOneById($id),
            default => $this->instrumentRepo->findOneById($id),
        };

        if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();

        }

        return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
    }


}
