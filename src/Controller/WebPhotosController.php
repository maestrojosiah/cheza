<?php

namespace App\Controller;

use App\Entity\WebPhotos;
use App\Form\WebPhotosType;
use App\Repository\WebPhotosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/web/photos')]
class WebPhotosController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'web_photos_index', methods: ['GET'])]
    public function index(WebPhotosRepository $webPhotosRepository): Response
    {
        return $this->render('web_photos/index.html.twig', [
            'web_photos' => $webPhotosRepository->findBy(
                [],
                ['id' => 'DESC']
            ),
        ]);
    }

    #[Route(path: '/new', name: 'web_photos_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $webPhoto = new WebPhotos();
        $form = $this->createForm(WebPhotosType::class, $webPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($webPhoto);
            $entityManager->flush();

            return $this->redirectToRoute('web_photos_index');
        }

        return $this->render('web_photos/new.html.twig', [
            'web_photo' => $webPhoto,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'web_photos_show', methods: ['GET'])]
    public function show(WebPhotos $webPhoto): Response
    {
        return $this->render('web_photos/show.html.twig', [
            'web_photo' => $webPhoto,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'web_photos_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, WebPhotos $webPhoto): Response
    {
        $form = $this->createForm(WebPhotosType::class, $webPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('web_photos_index');
        }

        return $this->render('web_photos/edit.html.twig', [
            'web_photo' => $webPhoto,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'web_photos_delete', methods: ['DELETE'])]
    public function delete(Request $request, WebPhotos $webPhoto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$webPhoto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($webPhoto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('web_photos_index');
    }
}
