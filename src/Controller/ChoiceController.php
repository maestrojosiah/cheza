<?php

namespace App\Controller;

use App\Entity\Choice;
use App\Repository\ChoiceRepository;
use App\Repository\LessonRepository;
use App\Repository\QuizRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route('/choice')]
class ChoiceController extends AbstractController
{
    #[Route('/', name: 'choice_index', methods: ['GET'])]
    public function index(ChoiceRepository $choiceRepository): Response
    {
        $choices = $choiceRepository->findBy(
            [],
            ['id' => 'desc'],
            20
        );

        return $this->render('choice/index.html.twig', [
            'choices' => $choices
        ]);
    }

    #[Route('/new', name: 'choice_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ChoiceRepository $choiceRepository, QuizRepository $quizRepo, LessonRepository $lessonRepo): Response
    {
        $quizId = $request->query->get('quiz');
        $lessonId = $request->query->get('lesson');
        
        $quiz = $quizRepo->find($quizId);
        $lesson = $lessonRepo->find($lessonId);
    
        $choice = new Choice();
        $form = $this->createForm(ChoiceType::class, $choice, [
            'quiz' => $quiz, // Pass the quiz entity to the form
        ]);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $choiceRepository->save($choice, true);
    
            // Handle AJAX request
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => true]);
            }
    
            $url = $this->generateUrl('quiz_index', [
                'lesson' => $lessonId,
            ]) . '#' . $quiz->getId();
            
            return $this->redirect($url);
        }
    
        // Handle validation errors for AJAX requests
        if ($request->isXmlHttpRequest()) {
            $errors = [];
            foreach ($form->getErrors(true) as $error) {
                $errors[] = $error->getMessage();
            }
            return new JsonResponse(['success' => false, 'errors' => $errors]);
        }
    
        return $this->render('choice/new.html.twig', [
            'choice' => $choice,
            'quiz' => $quiz,
            'lesson' => $lesson,
            'form' => $form->createView(),
        ]);
    }
    
    #[Route('/{id}', name: 'choice_show', methods: ['GET'])]
    public function show(Choice $choice): Response
    {
        return $this->render('choice/show.html.twig', [
            'choice' => $choice,
        ]);
    }

    #[Route('/{id}/edit', name: 'choice_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Choice $choice, ChoiceRepository $choiceRepository): Response
    {
        $form = $this->createForm(ChoiceType::class, $choice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $choiceRepository->save($choice, true);

            return $this->redirectToRoute('choice_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('choice/edit.html.twig', [
            'choice' => $choice,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'choice_delete', methods: ['POST'])]
    public function delete(Request $request, Choice $choice, ChoiceRepository $choiceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$choice->getId(), $request->request->get('_token'))) {
            $choiceRepository->remove($choice, true);
        }

        return $this->redirectToRoute('choice_index', [], Response::HTTP_SEE_OTHER);
    }
}
