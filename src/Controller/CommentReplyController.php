<?php

namespace App\Controller;

use App\Entity\CommentReply;
use App\Form\CommentReplyType;
use App\Repository\CommentReplyRepository;
use App\Repository\CommentRepository;
use App\Repository\BlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/comment/reply')]
class CommentReplyController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/save/comment/reply', name: 'save_reply')]
    public function saveReply(Request $request, CommentRepository $commentRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $dt = [];
        $content = $request->request->get('reply');
        $comment = $commentRepo->findOneById($request->request->get('comment_id'));
        $reply = new CommentReply();
        $reply->setContent($content);
        $reply->setCreatedAt(new \DateTime());
        $reply->setUser($this->getUser());
        $reply->setComment($comment);

        $this->save($reply);
        // send email to concerned parties
        $dt['name'] = $this->getUser()->getFullname();
        $dt['content'] = $content;
        $dt['time'] = $reply->getCreatedAt()->format("M j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();
        return new JsonResponse($dt);

    }


    #[Route(path: '/', name: 'comment_reply_index', methods: ['GET'])]
    public function index(CommentReplyRepository $commentReplyRepository): Response
    {
        return $this->render('comment_reply/index.html.twig', [
            'comment_replies' => $commentReplyRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'comment_reply_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $commentReply = new CommentReply();
        $form = $this->createForm(CommentReplyType::class, $commentReply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($commentReply);
            $entityManager->flush();

            return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('comment_reply/new.html.twig', [
            'comment_reply' => $commentReply,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'comment_reply_show', methods: ['GET'])]
    public function show(CommentReply $commentReply): Response
    {
        return $this->render('comment_reply/show.html.twig', [
            'comment_reply' => $commentReply,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'comment_reply_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CommentReply $commentReply): Response
    {
        $form = $this->createForm(CommentReplyType::class, $commentReply);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('comment_reply/edit.html.twig', [
            'comment_reply' => $commentReply,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'comment_reply_delete', methods: ['POST'])]
    public function delete(Request $request, CommentReply $commentReply): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commentReply->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($commentReply);
            $entityManager->flush();
        }

        return $this->redirectToRoute('comment_reply_index', [], Response::HTTP_SEE_OTHER);
    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


}
