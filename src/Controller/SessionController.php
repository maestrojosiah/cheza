<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Payment;
use App\Form\SessionType;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use App\Repository\PackageRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\UserInstrumentGradeRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Mailer;
use App\Repository\InstrumentRepository;
use App\Repository\PaymentRepository;
use App\Repository\TermRepository;
use Twig\Environment;
use App\Service\SendSms;
use App\Service\ShortenUrl;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Route(path: '/admin/session')]
class SessionController extends AbstractController
{
    public function __construct(private readonly PaymentRepository $paymentRepo, private readonly TermRepository $termRepo, private readonly InstrumentRepository $instrumentRepo, private readonly UserPasswordHasherInterface $passwordEncoder, private readonly InstrumentGradeRepository $instrumentGradeRepo, private readonly UserInstrumentGradeRepository $uigRepo, private readonly UserRepository $userRepo, private readonly SendSms $sendSms, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }


    #[Route(path: '/calendar', name: 'adm_session_calendar', methods: ['GET'])]
    public function calendar(): Response
    {
        return $this->render('session/calendar.html.twig');
    }

    #[Route(path: '/', name: 'session_index', methods: ['GET'])]
    public function index(InstrumentRepository $instrumentRepo, SessionRepository $sessionRepository, UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $data = [];
        $groupedusers = $userInstrumentGradeRepo->findUIGForStudents();
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('session/index.html.twig', [
            'groupedusers' => $groupedusers,
            'data' => $data,
        ]);
    }

    #[Route(path: '/new', name: 'session_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        TermRepository $termRepo,
        Mailer $mailer,
        Environment $twig,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        PaymentRepository $paymentRepo,
        PackageRepository $packageRepo,
        UserInstrumentGradeRepository $userInstrumentGradeRepo,
        UserRepository $userRepo
    ): Response {

        $numberofsessionsperweek = null;
        $beginAt = null;
        $endAt = null;
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);


        // get the teacher id from url if available
        // teacher_id=11&date=2021-01-19&time=08:20
        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $query = $_GET['teacher_id'];
            $teacher = $userRepo->findOneById($query);
        } else {
            $query = "";
            $teacher = null;
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['date'])) {
            $date = $_GET['date'];
            $starttime = $_GET['time'];
            $firstPart = explode(":", (string) $_GET['time'])[0] + 1;
            $padded = sprintf("%02d", $firstPart);
            $endtime = $padded .":". explode(":", (string) $_GET['time'])[1];
        } else {
            $date = "";
            $starttime = "";
            $endtime = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['pkg'])) {
            // package
            $pkg = $_GET['pkg'];
            $package = $packageRepo->findOneById($pkg)->getId();
            $endtime = $_GET['etime'];
            $thisday = new \Datetime($date);
            $thisday = $thisday->format('l');
            $dateformat = new \Datetime($thisday . "this week");
            $date = $dateformat->format('Y-m-d');
        } else {
            $package = "";
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['percent'])) {
            $percentage = $_GET['percent'];
        } else {
            $percentage = 0.7;
        }
        if ($form->isSubmitted() && $form->isValid()) {

            // some declarations to use in the function
            $package = $form->get('package')->getData();
            $userinstrumentgrade = $form->get('userinstrumentgrade')->getData();
            $teacher = $form->get('teacher')->getData();
            $numberofsessions = $form->get('numberofsessions')->getData();
            $percentage = $request->request->get('percentage');
            $entityManager = $this->managerRegistry->getManager();
            $startingDate = $form->get('startingOn')->getData();
            $paidamt = $form->get('paid')->getData();

            if(isset($_POST['day'])) {
                $numberofsessionsperweek = is_countable($_POST['day']) ? count($_POST['day']) : 0;
            }

            // get price per lesson
            $package_price_per_term = $package->getPrice();
            $price_per_lesson = $package_price_per_term / $numberofsessions;

            // calculate the number of sessions per week
            // and weeks needed depending on class frequency
            $numberofweeks = $numberofsessions / $numberofsessionsperweek;

            // declare some defaults and more data
            $cleared = false;

            $userinstrumentgrade->setPayment($paidamt);
            $userinstrumentgrade->setPercentage($percentage);

            $entityManager->persist($userinstrumentgrade);
            $entityManager->flush();

            $student = $userinstrumentgrade->getStudentUserData()->getStudent();

            $countOfInvoices = $paymentRepo->countEntriesWithType("inv");
            $countOfPayments = $paymentRepo->countEntriesWithType("pmt");
            $nextInvoiceNumber = $countOfInvoices + 1;
            $nextReceiptNumber = $countOfPayments + 1;

            $days = $_POST['day'];
            $fees = $package->getPrice();
            $balance = $fees - $paidamt;

            $fullfee = $package->getPrice();
            $onelessonfee = $fullfee / $package->getNoOfSessions();
            $realfee = $onelessonfee * $numberofsessions;

            $invoice = new Payment();
            $invoice->setAmount($realfee);
            $invoice->setDoneOn(new \Datetime("now"));
            $invoice->setType("inv");
            $invoice->setDocNumber($nextInvoiceNumber);
            $invoice->setUser($student);
            $invoice->setUig($userinstrumentgrade);
            $invoice->setBalance($realfee);
            $this->save($invoice);

            $newPayment = new Payment();
            $newPayment->setAmount($paidamt);
            $newPayment->setDoneOn(new \Datetime("now"));
            $newPayment->setType("pmt");
            $newPayment->setDocNumber($nextReceiptNumber);
            $newPayment->setUser($student);
            $newPayment->setUig($userinstrumentgrade);
            $newPayment->setBalance($balance);
            $this->save($newPayment);

            $dates = [];
            $test = [];
            $daysofweek = [];
            $tosave = [];
            $today = date('Y-m-d H:i:s');
            $td = date('w');
            // $dd = new \Datetime("now");
            $dd = clone $startingDate;

            for ($i = 0; $i < $numberofweeks; $i++) {

                foreach ($days as $day) {
                    if($i == 0) {
                        $dd->modify($day. " this week");
                        $beginAt = $form->get('beginAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $endAt = $form->get('endAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $daysofweek[] = $dd->format('l');
                        $test[] = $dd->format('d');
                        if($dd->format('w') > $td) {
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson;
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if((is_countable($days) ? count($days) : 0) == 1) {
                                $dd->modify("$day next week");
                            }
                        } else {
                            $tosave[$dd->format('l, d-m')] = $dd;
                        }
                    } else {
                        $modDd = $dd->modify("this ".$day);
                        $dd->setTime("08", "55", "00");
                        $beginAt = $form->get('beginAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $endAt = $form->get('endAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $daysofweek[] = $dd->format('l');
                        // echo "compare today : " . strtotime($today) . " with dd : " . strtotime($dd->format("Y-m-d"));
                        // echo "<br />";
                        if(strtotime((string) $modDd->format('Y-m-d')) > strtotime($today)) {
                            // $numberofsessions -= 1;
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson;
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if((is_countable($days) ? count($days) : 0) == 1) {
                                $dd->modify("$day next week");
                            }
                        } else {
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson;
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if((is_countable($days) ? count($days) : 0) == 1) {
                                $dd->modify("$day next week");
                            }
                        }

                    }

                }
                $dd->modify(end($daysofweek));

            }
            foreach ($tosave as $key => $item) {
                $item->modify(current($days));
                next($days);
                $dates[] = $item->format('l, d-m');
                $paidamt -= $price_per_lesson;
                if($paidamt >= 0) {
                    $cleared = true;
                } else {
                    $cleared = false;
                }
                $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $item, $beginAt, $endAt, $cleared);
                $numberofsessions -= 1;
                if((is_countable($days) ? count($days) : 0) == 1) {
                    $dd->modify("$day next week");
                }
            }

            $maildata = ['student' => $student, 'payment' => $invoice, 'teacher' => $teacher, 'userinstrumentgrade' => $userinstrumentgrade, 'startingdate' => $startingDate->format('Y-m-d'), 'beginAt' => $beginAt->format('H:i'), 'endAt' => $endAt->format('H:i')];
            // $mailer->sendEmailMessage($maildata, $student->getEmail(), "Meet your teacher", "new_teacher.html.twig", "notifications", "new_teacher_email");
            // $mailer->sendEmailMessage($maildata, $teacher->getEmail(), "New Student Alert", "new_session.html.twig", "notifications", "new_student_email");
            // send invoice
            // $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/report/admin/pdf/invoice/', $invoice->getId(), 'Download Invoice');
            // $sendSms->quickSend($student->getStudentdata()->getPhone(), $student->getFullname(), 'Meet Teacher & Invoice', 'Download your invoice: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());
            // $currentTerm = $termRepo->findCurrentTerm($invoice->getDoneOn());
            // $html = $twig->render('pdf/student_invoice.html.twig', [
            //     'payment' => $invoice,
            //     'currentTerm' => $currentTerm,
            // ]);

            // $mailer->sendEmailWithAttachment($maildata, $student->getEmail(), ' Your Invoice dated '.$invoice->getDoneOn()->format('d/m/Y'), $html, 'invoice_student_initial.html.twig', $student->getFullname().'-invoice-'.$invoice->getDoneOn()->format('d/m/Y'), 'necessary', 'personal_email_from_office');

            // if ($numberofsessionsperweek > 1) {
            return $this->redirectToRoute('session_show', ['id' => $session->getId()]);
            // } else {
            // return $this->redirectToRoute('user_instrument_grade_show', ['id' => $userinstrumentgrade->getId()]);
            // }

        }

        return $this->render('session/new.html.twig', [
            'userInstrumentGradeRepo' => count($userInstrumentGradeRepo->findWithNoSessions()),
            'session' => $session,
            'query' => $query,
            'seldate' => $date,
            'seltime' => $starttime,
            'percentage' => $percentage,
            'seletime' => $endtime,
            'package' => $package,
            'teacher' => $teacher,
            'form' => $form,
        ]);
    }

    public function saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $movingdate, $beginAt, $endAt, $cleared)
    {
        $session = new Session();
        $session->setTeacher($teacher);
        $session->setUserInstrumentGrade($userinstrumentgrade);
        $session->setPackage($package);
        $session->setPaid($paidamt);
        $session->setStudent($student);
        $session->setNumberofsessions($numberofsessions);
        // if(isset($_POST['day'])){
        //     $comingday = $_POST['day'][$a];
        // } else {
        //     $comingday = $startingDate->format('l');
        // }
        $beginAt = $beginAt->setDate($movingdate->format('Y'), $movingdate->format('m'), $movingdate->format('d'));
        $endAt = $endAt->setDate($movingdate->format('Y'), $movingdate->format('m'), $movingdate->format('d'));
        $session->setBeginAt($beginAt);
        $session->setEndAt($endAt);
        $session->setStartingOn($movingdate);
        $session->setCleared(false);
        $this->save($session);
        return $session;
    }

    public function save($myEntityClass): void
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($myEntityClass);
        $entityManager->flush();
    }

    #[Route(path: '/send/info/without/registration', name: 'save_incomplete_registration', methods: ['GET', 'POST'])]
    public function sendInfoWithoutRegistration(Request $request, Mailer $mailer, PackageRepository $packageRepo, UserRepository $userRepo): Response
    {
        $package_id = $request->request->get('residence'); // 2
        $teacher_id = $request->request->get('teacherId'); // 2335
        $startingdate = $request->request->get('startingdate'); // 2022-06-28
        $email = $request->request->get('regEmail'); // jshbr7@gmail.com
        $phone = $request->request->get('regPhone'); // 0705285959
        $fullname = $request->request->get('fullname'); // Josiah Birai
        $instrument = $request->request->get('instrument'); // 1
        $age = $request->request->get('age'); // over_18
        $gender = $request->request->get('gender'); // male
        // $password = $request->request->get('passInput'); // 1234abcd
        $weekday = $request->request->get('weekday');
        $message = $request->request->get('message');
        $amount = $request->request->get('amount');
        $confirmationcode = $request->request->get('confirmationcode');

        $package = $packageRepo->findOneById($package_id);
        $packageName = $package->getName();
        $startingDate = new \Datetime($startingdate);
        $readableDate = $startingDate->format('d-m-Y');
        $instrument = $this->instrumentRepo->findOneById($instrument);
        $instName = $instrument->getName();
        // $admin = [];
        // $admin['fullname'] = 'Josiah';

        $subject = "Incomplete Registration Notice";
        $message = "Name: $fullname, Email: $email, Phone: $phone, Instrument: $instName, Age: $age, Gender: $gender, Weekday: $weekday
                    Message: $message, Amount in payment field: $amount, Package: $packageName, Starting date: $readableDate";
        $this->sendSms->quickSend('0705285959', 'Online client', 'Incomplete Registration', "Incomplete registration. Name: $fullname, Phone: $phone, Instrument: $instName", 'necessary', 'personal_sms_from_office', 'nomail');

        $admins = $userRepo->findByUsertype('admin');
        foreach ($admins as $admin) {
            $maildata = ['msg' => $message, 'user' => $admin];
            $mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "communication.html.twig", "necessary", "personal_email_from_office");
        }
        // $mailer->sendEmailMessage(['msg' => $message, 'user' => $admin], 'chezamusicschool@gmail.com', $subject, "communication.html.twig", "communication", "personal_email_from_office");

        $user = [];
        $user['fullname'] = $fullname;
        $msg = "We have noticed that you haven't completed your registration at the Cheza Music School website. Kindly call 0711832933 if you need any assistance.";
        $this->sendSms->quickSend($phone, 'online client', 'Incomplete Registration', "Sorry, It seems you want to learn $instName. Kindly call 0711832933 if you need any assistance.", 'necessary', 'personal_sms_from_office', 'nomail');
        $mailer->sendEmail(['user' => $user, 'msg' => $msg], $email, 'How Can We Assist You?', "communication.html.twig", "necessary", "personal_email_from_office");

        return new JsonResponse($message);
    }


    #[Route(path: '/{id}', name: 'session_show', methods: ['GET'])]
    public function show(UserRepository $userRepo, TermRepository $termRepo, Session $session, PackageRepository $packageRepo, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $data = [];
        // $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $packages = $packageRepo->findAll();
        $s = [];
        $instruments = $instrumentRepo->findBy(
            ['category' => 'instrument'],
            ['id' => 'ASC']
        );
        $courses = $instrumentRepo->findBy(
            ['category' => 'course'],
            ['id' => 'ASC']
        );
        $traditional = $instrumentRepo->findBy(
            ['category' => 'traditional'],
            ['id' => 'ASC']
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        $grades = $instrumentGradeRepo->findAll();
        $terms = $termRepo->findAll();
        $teachers = $userRepo->findBy(
            ['usertype' => 'teacher', 'active' => 1],
            ['id' => 'asc']
        );

        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $days[$beginAt->format('H:i').'-'.$endAt->format('H:i').'-'.$beginAt->format('l')] = $beginAt->format('l');
        }

        return $this->render('session/show.html.twig', [
            'packages' => $packages,
            'session' => $session,
            'data' => $data,
            'terms' => $terms,
            'teachers' => $teachers,
            'grades' => $grades,
            'days' => array_unique($days),
        ]);
    }

    #[Route(path: '/mark/as/invoiced/{session_id}', name: 'mark_as_invoiced', methods: ['GET'])]
    public function markAsInvoiced(SessionRepository $sessionRepo, $session_id): RedirectResponse
    {
        $session = $sessionRepo->findOneById($session_id);

        $session->setInvoiced(1);
        $this->save($session);

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/mark/as/not/invoiced/{session_id}', name: 'mark_as_not_invoiced', methods: ['GET'])]
    public function markAsNotInvoiced(SessionRepository $sessionRepo, $session_id): RedirectResponse
    {
        $session = $sessionRepo->findOneById($session_id);

        $session->setInvoiced(null);
        $this->save($session);

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/mark/as/attended/{session_id}', name: 'mark_as_attended', methods: ['GET'])]
    public function markAsAttended(SessionRepository $sessionRepo, $session_id): RedirectResponse
    {
        $session = $sessionRepo->findOneById($session_id);

        $session->setDone(1);
        $this->save($session);

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/mark/as/not/attended/{session_id}', name: 'mark_as_not_attended', methods: ['GET'])]
    public function markAsNotAttended(SessionRepository $sessionRepo, $session_id): RedirectResponse
    {
        $session = $sessionRepo->findOneById($session_id);

        $session->setDone(null);
        $this->save($session);

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/add/one/session/to/uig', name: 'add_one_session', methods: ['POST'])]
    public function addOneSession(SessionRepository $sessionRepo, PackageRepository $packageRepo,): RedirectResponse
    {

        // echo "<pre>";

        $teacher_id = $_POST["teacher_id"];
        $student_id = $_POST["student_id"];
        $package_id = $_POST["package_id"];
        $uig_id = $_POST["uig_id"];
        $no_of_sessions = $_POST["no_of_sessions"];
        $starting_on = $_POST["starting_on"];
        $starts_at = $_POST["starts_at"];
        $ends_at = $_POST["ends_at"];
        $paid = $_POST["paid"];
        $cleared = $_POST["cleared"];
        $done = $_POST["done"];
        $invoiced = $_POST["invoiced"];

        $session = new Session();

        $session->setTeacher($this->userRepo->findOneById($teacher_id));
        $session->setStudent($this->userRepo->findOneById($student_id));
        $session->setPackage($packageRepo->findOneById($package_id));
        $session->setUserInstrumentGrade($this->uigRepo->findOneById($uig_id));
        $session->setNumberofsessions($no_of_sessions);
        $session->setStartingOn(new \Datetime($starts_at));
        $session->setBeginAt(new \Datetime($starts_at));
        $session->setEndAt(new \Datetime($ends_at));
        $session->setPaid($paid);
        $session->setCleared($cleared);
        $session->setDone($done);
        $session->setInvoiced($invoiced);
              
        // var_dump($session);
        // die();
        $this->save($session);

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/edit/all/{sessid}', name: 'session_times', methods: ['GET'])]
    public function times(SessionRepository $sessionRepo, $sessid): Response
    {
        $session = $sessionRepo->findOneById($sessid);
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        $dates = [];

        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $invoiced = $sess->getInvoiced();
            $days[] = $beginAt->format('l');
            $dates[$sess->getId()] = [$beginAt, $endAt, $invoiced];
        }

        return $this->render('session/times.html.twig', [
            'session' => $session,
            'days' => array_unique($days),
            'alldays' => $dates,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'session_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Session $session, PackageRepository $packageRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo, UserRepository $userRepo): Response
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('session_index');
        }

        return $this->render('session/edit.html.twig', [
            'session' => $session,
            'form' => $form,
        ]);

    }

    #[Route(path: '/{id}', name: 'session_delete', methods: ['DELETE'])]
    public function delete(Request $request, Session $session): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$session->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    #[Route(path: '/session/delete/one/{session_id}', name: 'session_delete_one', methods: ['GET'])]
    public function deleteOne(SessionRepository $sessionRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($session);
        $entityManager->flush();
        // return $this->redirectToRoute('user_instrument_grade_show');
        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

}
