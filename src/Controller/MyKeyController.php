<?php

namespace App\Controller;

use App\Entity\MyKey;
use App\Form\MyKeyType;
use App\Repository\MyKeyRepository;
use App\Repository\InstrumentRepository;
use App\Repository\ExerciseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/my/key')]
class MyKeyController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/for/{instrument}/{exercise}', name: 'my_key_index', methods: ['GET'])]
    public function index(ExerciseRepository $exerciseRepo, InstrumentRepository $instrumentRepo, MyKeyRepository $myKeyRepository, $instrument = null, $exercise = null): Response
    {

        $exercise = $exerciseRepo->findOneById($exercise);
        $instrument = $instrumentRepo->findOneById($instrument);

        return $this->redirectToRoute('my_key_index_updated', [
            'instrument' => $instrument->getName(),
            'exercise' => $exercise->getType(),
        ], 301);

    }

    #[Route(path: '/show/{instrument}/{exercise}', name: 'my_key_index_updated', methods: ['GET'])]
    public function indexWithText(ExerciseRepository $exerciseRepo, InstrumentRepository $instrumentRepo, MyKeyRepository $myKeyRepository, $instrument = null, $exercise = null): Response
    {
        $keyswithcontent = [];
        $mixed_array = [];
        $has_mixed = false;
        $flashCards = $exerciseRepo->findOneByType($exercise)->getFlashCards();
        foreach ($flashCards as $key => $flashcard) {
            if($flashcard->getMyKey()->getName() == "Mixed") {
                $mixed_array[] = $flashcard->getMyKey();
            }
            $keyswithcontent[] = $flashcard->getMyKey();
        }
        if(count($mixed_array) > 0) {
            $has_mixed = true;
        }
        return $this->render('my_key/index.html.twig', [
            'my_keys' => array_unique($keyswithcontent),
            'instrument' => $instrumentRepo->findOneByName($instrument),
            'exercise' => $exerciseRepo->findOneByType($exercise),
            'has_mixed' => $has_mixed,
            'mixed_array' => $mixed_array,
        ]);
    }

    #[Route(path: '/list', name: 'my_key_list', methods: ['GET'])]
    public function list(MyKeyRepository $myKeyRepository): Response
    {

        return $this->render('my_key/list.html.twig', [
            'my_keys' => $myKeyRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'my_key_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $myKey = new MyKey();
        $form = $this->createForm(MyKeyType::class, $myKey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($myKey);
            $entityManager->flush();

            return $this->redirectToRoute('my_key_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('my_key/new.html.twig', [
            'my_key' => $myKey,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'my_key_show', methods: ['GET'])]
    public function show(MyKey $myKey): Response
    {
        return $this->render('my_key/show.html.twig', [
            'my_key' => $myKey,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'my_key_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, MyKey $myKey): Response
    {
        $form = $this->createForm(MyKeyType::class, $myKey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('my_key_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('my_key/edit.html.twig', [
            'my_key' => $myKey,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'my_key_delete', methods: ['POST'])]
    public function delete(Request $request, MyKey $myKey): Response
    {
        if ($this->isCsrfTokenValid('delete'.$myKey->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($myKey);
            $entityManager->flush();
        }

        return $this->redirectToRoute('my_key_list', [], Response::HTTP_SEE_OTHER);
    }
}
