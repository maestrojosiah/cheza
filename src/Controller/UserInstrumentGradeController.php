<?php

namespace App\Controller;

use App\Entity\UserInstrumentGrade;
use App\Form\UserInstrumentGradeType;
use App\Repository\SessionRepository;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/user/instrument/grade')]
class UserInstrumentGradeController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'user_instrument_grade_index', methods: ['GET'])]
    public function index(UserInstrumentGradeRepository $userInstrumentGradeRepository): Response
    {
        return $this->render('user_instrument_grade/index.html.twig', [
            'user_instrument_grades' => $userInstrumentGradeRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'user_instrument_grade_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $userInstrumentGrade = new UserInstrumentGrade();
        $form = $this->createForm(UserInstrumentGradeType::class, $userInstrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($userInstrumentGrade);
            $entityManager->flush();

            return $this->redirectToRoute('user_instrument_grade_index');
        }

        return $this->render('user_instrument_grade/new.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'user_instrument_grade_show', methods: ['GET'])]
    public function show(UserInstrumentGrade $userInstrumentGrade): Response
    {
        return $this->render('user_instrument_grade/show.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
        ]);
    }

    #[Route(path: '/delete/selected/sessions', name: 'delete_selected_sessions', methods: ['POST'])]
    public function deleteSelected(SessionRepository $sessionRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // Retrieve selected items
        $selectedItems = $_POST['item'];

        $uigId = null;

        // Perform deletion logic for each item
        foreach ($selectedItems as $itemId) {
            // Delete item from the database or perform the required deletion operation
            // Example: $this->getDoctrine()->getRepository(Item::class)->delete($itemId);
            $itemID = explode("-", (string) $itemId)[1];
            $session = $sessionRepository->findOneById($itemID);
            $this->managerRegistry->getManager()->remove($session);
            $uigId = $session->getUserInstrumentGrade()->getId();

        }
        $this->managerRegistry->getManager()->flush();
        // Redirect or display a success message
        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $uigId]);

    }


    #[Route(path: '/{id}/edit', name: 'user_instrument_grade_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserInstrumentGrade $userInstrumentGrade): Response
    {
        $form = $this->createForm(UserInstrumentGradeType::class, $userInstrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('user_instrument_grade_index');
        }

        return $this->render('user_instrument_grade/edit.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
            'form' => $form,
        ]);
    }

    #[Route(path: '/delete/sessions/for/{id}', name: 'user_instrument_grade_delete', methods: ['DELETE', 'POST'])]
    public function delete(Request $request, UserInstrumentGrade $userInstrumentGrade): Response
    {
        $teacher = null;
        if ($this->isCsrfTokenValid('delete'.$userInstrumentGrade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $teacher = null;
            foreach ($userInstrumentGrade->getSessions() as $session) {
                $entityManager->remove($session);
                $teacher = $session->getTeacher();
            }
            foreach ($userInstrumentGrade->getPayments() as $payment) {
                $entityManager->remove($payment);
            }
            // $entityManager->remove($userInstrumentGrade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_session_calendar', ['teacher_id' => $teacher->getId()]);
    }

    #[Route(path: '/delete/from/away/{id}', name: 'user_instrument_grade_delete_2', methods: ['DELETE', 'POST', 'GET'])]
    public function delete_2(Request $request, UserInstrumentGrade $userInstrumentGrade): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$userInstrumentGrade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $teacher = null;
            foreach ($userInstrumentGrade->getSessions() as $session) {
                $entityManager->remove($session);
                $teacher = $session->getTeacher();
            }
            foreach ($userInstrumentGrade->getPayments() as $payment) {
                $entityManager->remove($payment);
            }
            // $entityManager->remove($userInstrumentGrade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('show_balances');
    }

}
