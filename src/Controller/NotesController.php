<?php

namespace App\Controller;

use App\Entity\Notes;
use App\Form\NotesType;
use App\Repository\NotesRepository;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/notes')]
class NotesController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/list/notes/for/{uig}', name: 'notes_index', methods: ['GET'])]
    public function index(NotesRepository $notesRepository, $uig, UserInstrumentGradeRepository $uigRepo): Response
    {
        $userInsGr = $uigRepo->findOneById($uig);
        $notes = $notesRepository->findBy(
            ['uig' => $userInsGr],
            ['id' => 'DESC']
        );
        return $this->render('notes/index.html.twig', [
            'notes' => $notes,
            'student' => $userInsGr->getStudentUserData()->getStudent()->getFullname(),
        ]);
    }

    #[Route(path: '/new', name: 'notes_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $note = new Notes();
        $form = $this->createForm(NotesType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('notes_index', ['uig' => $note->getUig()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('notes/new.html.twig', [
            'note' => $note,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'notes_show', methods: ['GET'])]
    public function show(Notes $note): Response
    {
        return $this->render('notes/show.html.twig', [
            'note' => $note,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'notes_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Notes $note): Response
    {
        $form = $this->createForm(NotesType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('notes_index', ['uig' => $note->getUig()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('notes/edit.html.twig', [
            'note' => $note,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'notes_delete', methods: ['POST'])]
    public function delete(Request $request, Notes $note): Response
    {
        if ($this->isCsrfTokenValid('delete'.$note->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($note);
            $entityManager->flush();
        }

        return $this->redirectToRoute('notes_index', ['uig' => $note->getUig()->getId()], Response::HTTP_SEE_OTHER);
    }
}
