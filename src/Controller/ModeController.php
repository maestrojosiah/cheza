<?php

namespace App\Controller;

use App\Entity\Mode;
use App\Form\ModeType;
use App\Repository\ModeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/mode')]
class ModeController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'mode_index', methods: ['GET'])]
    public function index(ModeRepository $modeRepository): Response
    {
        return $this->render('mode/index.html.twig', [
            'modes' => $modeRepository->findAll(),
        ]);
    }

    #[Route(path: '/list', name: 'mode_list', methods: ['GET'])]
    public function list(ModeRepository $modeRepository): Response
    {

        return $this->render('mode/list.html.twig', [
            'modes' => $modeRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'mode_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $mode = new Mode();
        $form = $this->createForm(ModeType::class, $mode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($mode);
            $entityManager->flush();

            return $this->redirectToRoute('mode_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('mode/new.html.twig', [
            'mode' => $mode,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'mode_show', methods: ['GET'])]
    public function show(Mode $mode): Response
    {
        return $this->render('mode/show.html.twig', [
            'mode' => $mode,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'mode_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Mode $mode): Response
    {
        $form = $this->createForm(ModeType::class, $mode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('mode_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('mode/edit.html.twig', [
            'mode' => $mode,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'mode_delete', methods: ['POST'])]
    public function delete(Request $request, Mode $mode): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mode->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($mode);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mode_list', [], Response::HTTP_SEE_OTHER);
    }
}
