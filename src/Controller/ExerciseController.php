<?php

namespace App\Controller;

use App\Entity\Exercise;
use App\Form\ExerciseType;
use App\Repository\ExerciseRepository;
use App\Repository\InstrumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/exercise')]
class ExerciseController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/for/{instrument}', name: 'exercise_index', methods: ['GET'])]
    public function index(InstrumentRepository $instrumentRepo, ExerciseRepository $exerciseRepository, $instrument = null): Response
    {
        $instrument = $instrumentRepo->findOneById($instrument);
        return $this->redirectToRoute('exercise_index_updated', [
            'instrument' => $instrument->getName(),
        ], 301);

    }

    #[Route(path: '/show/{instrument}', name: 'exercise_index_updated', methods: ['GET'])]
    public function indexWithText(InstrumentRepository $instrumentRepo, ExerciseRepository $exerciseRepository, $instrument = null): Response
    {
        $instrument = $instrumentRepo->findOneByName($instrument);

        if (null == $instrument) {
            $exercises = $exerciseRepository->findAll();
        } else {
            $exercises = $exerciseRepository->findByInstrument($instrument);
        }

        return $this->render('exercise/index.html.twig', [
            'exercises' => $exercises,
            'instrument' => $instrument
        ]);
    }

    #[Route(path: '/list', name: 'exercise_list', methods: ['GET'])]
    public function list(ExerciseRepository $exerciseRepository): Response
    {

        return $this->render('exercise/list.html.twig', [
            'exercises' => $exerciseRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'exercise_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $exercise = new Exercise();
        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($exercise);
            $entityManager->flush();

            return $this->redirectToRoute('exercise_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('exercise/new.html.twig', [
            'exercise' => $exercise,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'exercise_show', methods: ['GET'])]
    public function show(Exercise $exercise): Response
    {
        return $this->render('exercise/show.html.twig', [
            'exercise' => $exercise,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'exercise_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Exercise $exercise): Response
    {
        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('exercise_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('exercise/edit.html.twig', [
            'exercise' => $exercise,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'exercise_delete', methods: ['POST'])]
    public function delete(Request $request, Exercise $exercise): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exercise->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($exercise);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exercise_list', [], Response::HTTP_SEE_OTHER);
    }
}
