<?php

namespace App\Controller;

use App\Entity\ShortUrl;
use App\Form\ShortUrlType;
use App\Repository\ShortUrlRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/short/url')]
class ShortUrlController extends AbstractController
{
    public $dictionary = "abcdeklmnfghjpqrstzABCuvwxyDEFGHNPQRJKLMSTUVW23456XYZ789";
    final public const ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
    final public const BASE = 51; // strlen(self::ALPHABET)

    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
        $this->dictionary = str_split((string) $this->dictionary);
    }

    #[Route(path: '/reverse/short/to/digit', name: 'reversefromshort', methods: ['POST'])]
    public function reverse(Request $request)
    {
        $short = $request->request->get('short');
        $i = 0;
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        $input = str_split($short);

        foreach($input as $char) {
            $pos = array_search($char, $this->dictionary);

            $i = $i * $base + $pos;
        }

        // return $i;
        return new JsonResponse($i);

    }


    #[Route(path: '/', name: 'short_url_index', methods: ['GET'])]
    public function index(ShortUrlRepository $shortUrlRepository): Response
    {
        $urls = $shortUrlRepository->findBy(
            [],
            ['id'=> 'DESC'],
            100
        );

        return $this->render('short_url/index.html.twig', [
            'short_urls' => $urls,
        ]);
    }

    #[Route(path: '/new', name: 'short_url_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ShortUrlRepository $shortRepo): Response
    {
        $shortUrl = new ShortUrl();
        $form = $this->createForm(ShortUrlType::class, $shortUrl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            // $url = $form->get('url')->getData();
            $short = $form->get('short')->getData();
            $digit = $_POST['digit'];
            if($short == 'auto') {
                $lastShortUrl = $shortRepo->findLastOne();
                if(null == $lastShortUrl) {
                    $newId = 1;
                } else {
                    $newId = $lastShortUrl->getId() + 1;
                }

                $short = $this->encode($newId);
                $digit = $newId;

            }
            $shortUrl->setShort($short);
            $shortUrl->setDigit($digit);
            // var_dump($newId);
            // die();
            $entityManager->persist($shortUrl);
            $entityManager->flush();

            return $this->redirectToRoute('short_url_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('short_url/new.html.twig', [
            'short_url' => $shortUrl,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'short_url_show', methods: ['GET'])]
    public function show(ShortUrl $shortUrl): Response
    {
        return $this->render('short_url/show.html.twig', [
            'short_url' => $shortUrl,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'short_url_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ShortUrl $shortUrl): Response
    {
        $form = $this->createForm(ShortUrlType::class, $shortUrl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();

            $short = $form->get('short')->getData();
            $digit = $_POST['digit'];

            $shortUrl->setShort($short);
            $shortUrl->setDigit($digit);
            // var_dump($newId);
            // die();
            $entityManager->persist($shortUrl);
            $entityManager->flush();

            return $this->redirectToRoute('short_url_index', [], Response::HTTP_SEE_OTHER);
        }



        return $this->render('short_url/edit.html.twig', [
            'short_url' => $shortUrl,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'short_url_delete', methods: ['POST'])]
    public function delete(Request $request, ShortUrl $shortUrl): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shortUrl->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($shortUrl);
            $entityManager->flush();
        }

        return $this->redirectToRoute('short_url_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/test/url/shortener', name: 'testurlshort', methods: ['GET'])]
    public function testurl(): Response
    {
        $int = 9;
        $encoded_int = static::encode2($int);
        $decoded = static::decode2($encoded_int);
        return $this->render('default/testurl.html.twig', [
            'test' => $encoded_int,
            'test2' => $decoded,
        ]);
    }

    public function encode($i)
    {
        if ($i == 0) {
            return $this->dictionary[0];
        }

        $result = [];
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        while ($i > 0) {
            $result[] = $this->dictionary[($i % $base)];
            $i = floor($i / $base);
        }

        $result = array_reverse($result);

        return join("", $result);
    }

    public function decode($input)
    {
        $i = 0;
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        $input = str_split((string) $input);

        foreach($input as $char) {
            $pos = array_search($char, $this->dictionary);

            $i = $i * $base + $pos;
        }

        return $i;
    }
    public static function encode2($num)
    {
        $str = '';

        while ($num > 0) {
            $str = self::ALPHABET[($num % self::BASE)] . $str;
            $num = (int) ($num / self::BASE);
        }

        return $str;
    }

    public static function decode2($str)
    {
        $num = 0;
        $len = strlen((string) $str);

        for ($i = 0; $i < $len; $i++) {
            $num = $num * self::BASE + strpos(self::ALPHABET, (string) $str[$i]);
        }

        return $num;
    }

}
