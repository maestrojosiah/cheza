<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Bijective extends AbstractController
{
    public $dictionary = "abcdeklmnfghjpqrstzABCuvwxyDEFGHNPQRJKLMSTUVW23456XYZ789";
    final public const ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
    final public const BASE = 51; // strlen(self::ALPHABET)

    public function __construct()
    {
        $this->dictionary = str_split((string) $this->dictionary);
    }


    #[Route(path: '/test/url/shortener', name: 'testurlshort', methods: ['GET'])]
    public function testurl(): Response
    {
        $int = 9;
        $encoded_int = static::encode2($int);
        $decoded = static::decode2($encoded_int);
        return $this->render('default/testurl.html.twig', [
            'test' => $encoded_int,
            'test2' => $decoded,
        ]);
    }

    public function encode($i)
    {
        if ($i == 0) {
            return $this->dictionary[0];
        }

        $result = [];
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        while ($i > 0) {
            $result[] = $this->dictionary[($i % $base)];
            $i = floor($i / $base);
        }

        $result = array_reverse($result);

        return join("", $result);
    }

    public function decode($input)
    {
        $i = 0;
        $base = is_countable($this->dictionary) ? count($this->dictionary) : 0;

        $input = str_split((string) $input);

        foreach($input as $char) {
            $pos = array_search($char, $this->dictionary);

            $i = $i * $base + $pos;
        }

        return $i;
    }
    public static function encode2($num)
    {
        $str = '';

        while ($num > 0) {
            $str = self::ALPHABET[($num % self::BASE)] . $str;
            $num = (int) ($num / self::BASE);
        }

        return $str;
    }

    public static function decode2($str)
    {
        $num = 0;
        $len = strlen((string) $str);

        for ($i = 0; $i < $len; $i++) {
            $num = $num * self::BASE + strpos(self::ALPHABET, (string) $str[$i]);
        }

        return $num;
    }

}
