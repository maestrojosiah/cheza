<?php

namespace App\Controller;

use App\Entity\GroupMember;
use App\Entity\MemberNotes;
use App\Form\GroupMemberType;
use App\Repository\GroupMemberRepository;
use App\Repository\MemberNotesRepository;
use App\Repository\UigGroupRepository;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

#[Route(path: '/groupmember')]
class GroupMemberController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/list/for/{uig_group_id}', name: 'group_member_index', methods: ['GET'])]
    public function index(GroupMemberRepository $groupMemberRepository, UigGroupRepository $uigGroupRepo, $uig_group_id): Response
    {
        $uig_group = $uigGroupRepo->findOneById($uig_group_id);
        $groupMembers = $groupMemberRepository->findByUigGroup($uig_group);
        return $this->render('group_member/index.html.twig', [
            'uig_group' => $uig_group,
            'group_members' => $groupMembers,
        ]);
    }

    #[Route(path: '/new/for/{uig_group_id}', name: 'group_member_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UigGroupRepository $uigGroupRepo, $uig_group_id): Response
    {
        $groupMember = new GroupMember();
        $uig_group = $uigGroupRepo->findOneById($uig_group_id);
        $groupMember->setUigGroup($uig_group);
        $form = $this->createForm(GroupMemberType::class, $groupMember);
        $form->handleRequest($request);
        $members = $uig_group->getGroupMembers();
        $count_members = is_countable($members) ? count($members) : 0;

        if($count_members == 30) {
            $this->addFlash(
                'danger',
                'This group is full. Create another UIG',
            );
            return $this->redirectToRoute('group_member_index', ['uig_group_id' => $uig_group->getId()], Response::HTTP_SEE_OTHER);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $groupMember->setUigGroup($uig_group);
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($groupMember);
            $entityManager->flush();

            return $this->redirectToRoute('group_member_index', ['uig_group_id' => $uig_group->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('group_member/new.html.twig', [
            'uig_group' => $uig_group,
            'group_member' => $groupMember,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'group_member_show', methods: ['GET'])]
    public function show(GroupMember $groupMember, MemberNotesRepository $mnRepo): Response
    {
        $memberNotes = $mnRepo->findByMember($groupMember);
        return $this->render('group_member/show.html.twig', [
            'group_member' => $groupMember,
            'member_notes' => $memberNotes,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'group_member_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, GroupMember $groupMember): Response
    {
        $form = $this->createForm(GroupMemberType::class, $groupMember);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('group_member_index', ['uig_group_id' => $groupMember->getUigGroup()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('group_member/edit.html.twig', [
            'group_member' => $groupMember,
            'uig_group' => $groupMember->getUigGroup(),
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'group_member_delete', methods: ['POST'])]
    public function delete(Request $request, GroupMember $groupMember): Response
    {
        if ($this->isCsrfTokenValid('delete'.$groupMember->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($groupMember);
            $entityManager->flush();
        }

        return $this->redirectToRoute('group_member_index', ['uig_group_id' => $groupMember->getUigGroup()->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/save/member/note', name: 'savemembernote')]
    public function savenote(Request $request, GroupMemberRepository $gmRepo, MemberNotesRepository $notesRepo): Response
    {

        $data = [];
        $member_id = $request->request->get('member_id');
        $message = $request->request->get('note_for_'.$member_id);

        $member = $gmRepo->findOneById($member_id);
        $uig = $member->getUigGroup();
        $note = new MemberNotes();
        $note->setMessage($message);
        $note->setMember($member);
        $note->setAddedOn(new \DateTime());

        $this->save($note);

        $notes = $notesRepo->findBy(
            ['member' => $member],
            ['id' => 'desc'],
        );

        $data['noteslength'] = count($notes);

        return new JsonResponse($data);

    }

    #[Route(path: '/edit/member/note', name: 'editmembernote')]
    public function editnote(Request $request, MemberNotesRepository $notesRepo): Response
    {

        $data = [];
        $note_id = $request->request->get('note_id');
        $editedmsg = $request->request->get('editedmsg');

        $note = $notesRepo->findOneById($note_id);
        $note->setMessage($editedmsg);

        $this->save($note);

        return new JsonResponse($editedmsg);

    }

    #[Route(path: '/view/member/notes', name: 'viewmembernotes')]
    public function viewnotes(Request $request, GroupMemberRepository $gmRepo, MemberNotesRepository $notesRepo): Response
    {

        $data = [];
        $member_id = $request->request->get('member_id');
        $message = "<div>";

        $member = $gmRepo->findOneById($member_id);
        $uig = $member->getUigGroup();
        $notes = $notesRepo->findBy(
            ['member' => $member],
            ['id' => 'DESC'],
            20
        );
        foreach ($notes as $note) {
            $message .= $note->getAddedOn()->format('jS M Y') . "<br />";
            $message .= $note->getMessage() . "<hr />";
        }
        // $this->save($note);

        $message .= "</div>";
        $data['message'] = $message;
        $data['member'] = $member->getFullname();


        return new JsonResponse($data);

    }

    #[Route(path: '/view/member/notes/for/{member_id}', name: 'Pgro')]
    public function viewnotesID(Request $request, GroupMemberRepository $gmRepo, MemberNotesRepository $notesRepo, $member_id): Response
    {

        $data = [];
        $message = "<div>";

        $member = $gmRepo->findOneById($member_id);
        $uig = $member->getUigGroup();
        $notes = $notesRepo->findBy(
            ['member' => $member],
            ['id' => 'DESC'],
            20
        );
        foreach ($notes as $note) {
            $message .= $note->getAddedOn()->format('jS M Y') . "<br />";
            $message .= $note->getMessage() . "<hr />";
        }
        // $this->save($note);

        $message .= "</div>";
        $data['message'] = $message;
        $data['member'] = $member->getFullname();


        return new JsonResponse($data);

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    #[Route(path: '/list/member/notes/for/{member}', name: 'member_notes_index', methods: ['GET'])]
    public function listNotes(MemberNotesRepository $notesRepository, $member, GroupMemberRepository $gmRepo): Response
    {
        $member = $gmRepo->findOneById($member);
        $notes = $notesRepository->findBy(
            ['member' => $member],
            ['id' => 'DESC']
        );
        return $this->render('member_notes/index.html.twig', [
            'member_notes' => $notes,
            'uig_group' => $member->getUigGroup(),
            'member' => $member,
        ]);
    }


}
