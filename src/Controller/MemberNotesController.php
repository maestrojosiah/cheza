<?php

namespace App\Controller;

use App\Entity\MemberNotes;
use App\Form\MemberNotesType;
use App\Repository\MemberNotesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/member/notes')]
class MemberNotesController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'member_notes_index_old', methods: ['GET'])]
    public function index(MemberNotesRepository $memberNotesRepository): Response
    {
        return $this->render('member_notes/index.html.twig', [
            'member_notes' => $memberNotesRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'member_notes_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $memberNote = new MemberNotes();
        $form = $this->createForm(MemberNotesType::class, $memberNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($memberNote);
            $entityManager->flush();

            return $this->redirectToRoute('member_notes_index', ['member' => $memberNote->getMember()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('member_notes/new.html.twig', [
            'member_note' => $memberNote,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'member_notes_show', methods: ['GET'])]
    public function show(MemberNotes $memberNote): Response
    {
        return $this->render('member_notes/show.html.twig', [
            'member_note' => $memberNote,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'member_notes_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, MemberNotes $memberNote): Response
    {
        $form = $this->createForm(MemberNotesType::class, $memberNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('member_notes_index', ['member' => $memberNote->getMember()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('member_notes/edit.html.twig', [
            'note' => $memberNote,
            'member_note' => $memberNote,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'member_notes_delete', methods: ['POST'])]
    public function delete(Request $request, MemberNotes $memberNote): Response
    {
        if ($this->isCsrfTokenValid('delete'.$memberNote->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($memberNote);
            $entityManager->flush();
        }

        return $this->redirectToRoute('member_notes_index', ['member' => $memberNote->getMember()->getId()], Response::HTTP_SEE_OTHER);
    }
}
