<?php

namespace App\Controller;

use App\Entity\TeacherUserData;
use App\Form\TeacherUserDataType;
use App\Repository\TeacherUserDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/teacher/user/data')]
class TeacherUserDataController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'teacher_user_data_index', methods: ['GET'])]
    public function index(TeacherUserDataRepository $teacherUserDataRepository): Response
    {
        return $this->render('teacher_user_data/index.html.twig', [
            'teacher_user_datas' => $teacherUserDataRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'teacher_user_data_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $teacherUserDatum = new TeacherUserData();
        $form = $this->createForm(TeacherUserDataType::class, $teacherUserDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($teacherUserDatum);
            $entityManager->flush();

            return $this->redirectToRoute('teacher_user_data_index');
        }

        return $this->render('teacher_user_data/new.html.twig', [
            'teacher_user_datum' => $teacherUserDatum,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'teacher_user_data_show', methods: ['GET'])]
    public function show(TeacherUserData $teacherUserDatum): Response
    {
        return $this->render('teacher_user_data/show.html.twig', [
            'teacher_user_datum' => $teacherUserDatum,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'teacher_user_data_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, TeacherUserData $teacherUserDatum): Response
    {
        $form = $this->createForm(TeacherUserDataType::class, $teacherUserDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('teacher_user_data_index');
        }

        return $this->render('teacher_user_data/edit.html.twig', [
            'teacher_user_datum' => $teacherUserDatum,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'teacher_user_data_delete', methods: ['DELETE'])]
    public function delete(Request $request, TeacherUserData $teacherUserDatum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$teacherUserDatum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($teacherUserDatum);
            $entityManager->flush();
        }

        return $this->redirectToRoute('teacher_user_data_index');
    }
}
