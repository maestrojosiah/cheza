<?php

namespace App\Controller;

use App\Repository\PackageRepository;
use App\Repository\TeacherInvoiceRepository;
use App\Repository\UserInstrumentGradeRepository;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;

class TeacherInvoiceController extends AbstractController
{
    #[Route('/teacher/invoice', name: 'app_teacher_invoice')]
    public function index(TeacherInvoiceRepository $invoiceRepo): Response
    {
        $invoices = $invoiceRepo->findBy(
            [],
            ['id'=> 'DESC'],
            50
        );
        return $this->render('teacher_invoice/index.html.twig', [
            'invoices' => $invoices,
        ]);
    }

    #[Route(path: '/teacher/pdf/invoice/{teacherInvoiceId}', name: 'pdf_teacher_invoice')]
    public function pdfTeacherInvoice(Environment $twig, PackageRepository $packageRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo, TeacherInvoiceRepository $teacherInvoiceRepo, $teacherInvoiceId): Response
    {
        $invoice = $teacherInvoiceRepo->find($teacherInvoiceId);

        $teacher = $invoice->getTeacher();
        $uigs = $invoice->getUigs();
        $inv = $invoice->getAmount();
        $ded = $invoice->getDeduction();
        $date = $invoice->getDateSubmitted();
        
        $userInstrumentGradesIds = json_decode($uigs, true);
        
        foreach ($userInstrumentGradesIds as $uigId) {
            $key = explode(":", (string) $uigId)[0];
            $val = explode(":", (string) $uigId)[1];
            $pack = explode(":", (string) $uigId)[2]; // 2030-3
            $packageId = explode("-", $pack)[1]; // 3
            $package = $packageRepo->findOneById($packageId);
            $packageDuration = $package->getDuration();
            // $userInstrumentGrades[$key] = $val;
            $userInstrumentGrades[$packageDuration.'-'.$uigId] = $userInstrumentGradeRepo->findOneById($key);
        }
        
        $arr = [
            'teacher' => $teacher,
            'userInstrumentGrades' => $userInstrumentGrades,
            'inv' => $inv,
            'ded' => $ded,
            'nonEmptyUigs' => $userInstrumentGrades,
        ];

        $this->toPdf($teacher->getFullname().'-invoice-'.$date->format('d/m/Y h:ia'), "pdf/invoice.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);

        return $response;

    }    

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);

        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("$filename.pdf");

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }


}
