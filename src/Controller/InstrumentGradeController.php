<?php

namespace App\Controller;

use App\Entity\InstrumentGrade;
use App\Form\InstrumentGradeType;
use App\Repository\InstrumentGradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/instrument-grade')]
class InstrumentGradeController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }
    #[Route(path: '/', name: 'instrument_grade_index', methods: ['GET'])]
    public function index(InstrumentGradeRepository $instrumentGradeRepository): Response
    {
        return $this->render('instrument_grade/index.html.twig', [
            'instrument_grades' => $instrumentGradeRepository->findAll(),
        ]);
    }

    #[Route(path: '/new', name: 'instrument_grade_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $instrumentGrade = new InstrumentGrade();
        $form = $this->createForm(InstrumentGradeType::class, $instrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($instrumentGrade);
            $entityManager->flush();

            return $this->redirectToRoute('instrument_grade_index');
        }

        return $this->render('instrument_grade/new.html.twig', [
            'instrument_grade' => $instrumentGrade,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'instrument_grade_show', methods: ['GET'])]
    public function show(InstrumentGrade $instrumentGrade): Response
    {
        return $this->render('instrument_grade/show.html.twig', [
            'instrument_grade' => $instrumentGrade,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'instrument_grade_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, InstrumentGrade $instrumentGrade): Response
    {
        $form = $this->createForm(InstrumentGradeType::class, $instrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('instrument_grade_index');
        }

        return $this->render('instrument_grade/edit.html.twig', [
            'instrument_grade' => $instrumentGrade,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'instrument_grade_delete', methods: ['DELETE'])]
    public function delete(Request $request, InstrumentGrade $instrumentGrade): Response
    {
        if ($this->isCsrfTokenValid('delete'.$instrumentGrade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($instrumentGrade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('instrument_grade_index');
    }
}
