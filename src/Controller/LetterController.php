<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Form\LetterType;
use App\Repository\LetterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;
use Knp\Snappy\Image;
use App\Service\Mailer;

#[Route(path: '/letter')]
class LetterController extends AbstractController
{
    public function __construct(private readonly Pdf $pdf, private readonly Image $img, private readonly Environment $twig, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }

    #[Route(path: '/', name: 'letter_index', methods: ['GET'])]
    public function index(LetterRepository $letterRepository): Response
    {
        $letters = $letterRepository->findDesc();
        return $this->render('letter/index.html.twig', [
            'letters' => $letters,
        ]);
    }

    #[Route(path: '/new', name: 'letter_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $letter = new Letter();
        $form = $this->createForm(LetterType::class, $letter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($letter);
            $entityManager->flush();

            return $this->redirectToRoute('letter_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('letter/new.html.twig', [
            'letter' => $letter,
            'modify' => false,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'letter_show', methods: ['GET'])]
    public function show(Letter $letter): Response
    {
        return $this->render('letter/show.html.twig', [
            'letter' => $letter,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'letter_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Letter $letter): Response
    {
        $form = $this->createForm(LetterType::class, $letter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();

            return $this->redirectToRoute('letter_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('letter/edit.html.twig', [
            'letter' => $letter,
            'modify' => true,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'letter_delete', methods: ['POST'])]
    public function delete(Request $request, Letter $letter): Response
    {
        if ($this->isCsrfTokenValid('delete'.$letter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($letter);
            $entityManager->flush();
        }

        return $this->redirectToRoute('letter_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/save/new/or/edit', name: 'letter_save')]
    public function saveLetter(Request $request, LetterRepository $letterRepo)
    {

        $text = $request->request->get('text');
        $modify = $request->request->get('modify');
        $letter_id = $request->request->get('letter_id');

        if($modify == true) {
            $letter = $letterRepo->findOneById($letter_id);
        } else {
            $letter = new Letter();
        }

        $letter->setText($text);
        $letter->setCreatedOn(new \Datetime());
        $this->save($letter);

        return new JsonResponse($letter->getId());

    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    #[Route(path: '/admin/pdf/letter/{letter_id}', name: 'pdf_letter')]
    public function pdfLetter(LetterRepository $letterRepo, Mailer $mailer, Environment $twig, $letter_id): Response
    {

        $letter = $letterRepo->findOneById($letter_id);

        $html = $twig->render('pdf/letter.html.twig', [
            'letter' => $letter,
        ]);

        $arr = [
            'letter' => $letter,
        ];

        $this->toPdf($letter->getId().'-letter-'.$letter->getCreatedOn()->format('d/m/Y'), "pdf/letter.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

        return $response;


    }

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename . '.pdf');

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }


}
