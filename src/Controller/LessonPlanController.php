<?php

namespace App\Controller;

use App\Entity\LessonPlan;
use App\Form\LessonPlanType;
use App\Repository\LessonPlanRepository;
use App\Repository\SessionRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;
use Knp\Snappy\Image;
use App\Service\Mailer;
use App\Service\FeesNotify;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SendSms;
use App\Service\ShortenUrl;

#[Route(path: '/lesson/plan')]
class LessonPlanController extends AbstractController
{
    public function __construct(private readonly Pdf $pdf, private readonly Image $img, private readonly Environment $twig, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry, private Mailer $mailer, private FeesNotify $feesNotify)
    {
    }

    #[Route(path: '/for/student/{uig_id}', name: 'lesson_plan_index', methods: ['GET'])]
    public function index(LessonPlanRepository $lessonPlanRepository, UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $uig = $uigRepo->findOneById($uig_id);
        $sessions = $uig->getSessions();
        $lessonPlans = [];
        foreach($sessions as $session) {
            if(null !== $session->getLessonPlan()) {
                $lessonPlans[] = $session->getLessonPlan();
            }

        }

        if(count($lessonPlans) < 1) {
            print_r('No lesson plans yet');
            die();
        }
        return $this->render('lesson_plan/index.html.twig', [
            'lesson_plans' => $lessonPlans,
            'student' => $lessonPlans[0]->getSession()->getStudent(),
        ]);
    }

    #[Route(path: '/new/for/{session_id}', name: 'lesson_plan_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo, LessonPlanRepository $lessonPlanRepo, $session_id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $session = $sessionRepo->findOneById($session_id);

        $lessonPlan = new LessonPlan();
        $uig = $session->getUserInstrumentGrade();
        $sessions = $uig->getSessions();

        $lessonPlan->setAddedOn($session->getBeginAt());

        $form = $this->createForm(LessonPlanType::class, $lessonPlan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lessonPlan->setSession($session);
            $lessonPlan->setStudent($session->getStudent());
            $lessonPlan->setAddedOn($session->getBeginAt());
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($lessonPlan);
            $entityManager->flush();

            return $this->redirectToRoute('lesson_plan_index', ['student_id' => $lessonPlan->getStudent()->getId()], Response::HTTP_SEE_OTHER);
        }

        $lessonPlanForThisSession = $lessonPlanRepo->findOneBySession($session);

        return $this->render('lesson_plan/new.html.twig', [
            'session' => $session,
            'sessions' => $sessions,
            'lessonPlanForThisSession' => $lessonPlanForThisSession,
            'lesson_plan' => $lessonPlan,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'lesson_plan_show', methods: ['GET'])]
    public function show(LessonPlan $lessonPlan): Response
    {
        return $this->render('lesson_plan/show.html.twig', [
            'lesson_plan' => $lessonPlan,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'lesson_plan_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, LessonPlan $lessonPlan): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $form = $this->createForm(LessonPlanType::class, $lessonPlan);
        $form->handleRequest($request);
        $session = $lessonPlan->getSession();

        if ($form->isSubmitted() && $form->isValid()) {
            $lessonPlan->setSession($session);
            $lessonPlan->setStudent($session->getStudent());
            $lessonPlan->setAddedOn($session->getBeginAt());

            // Persist and flush the new lesson plan to the database
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($lessonPlan); // Add this to persist the new entity
            $entityManager->flush();


            return $this->redirectToRoute('lesson_plan_index', ['uig_id' => $lessonPlan->getSession()->getUserInstrumentGrade()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('lesson_plan/edit.html.twig', [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
            'form' => $form,
        ]);
    }

    #[Route(path: '/new/without/wizard/{session_id}', name: 'lesson_plan_new_wo_wizard', methods: ['GET', 'POST'])]
    public function newWoWizard(Request $request, SessionRepository $sessionRepo, $session_id): Response
    {
        $lessonPlan = new LessonPlan();
        $session = $sessionRepo->find($session_id);
        $lessonPlan->setAddedOn($session->getBeginAt());

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $form = $this->createForm(LessonPlanType::class, $lessonPlan);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $lessonPlan->setSession($session);
            $lessonPlan->setStudent($session->getStudent());
            $lessonPlan->setAddedOn($session->getBeginAt());

            // Persist and flush the new lesson plan to the database
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($lessonPlan); // Add this to persist the new entity
            $entityManager->flush();

            return $this->redirectToRoute('lesson_plan_index', ['uig_id' => $lessonPlan->getSession()->getUserInstrumentGrade()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('lesson_plan/new_wo_wizard.html.twig', [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}', name: 'lesson_plan_delete', methods: ['POST'])]
    public function delete(Request $request, LessonPlan $lessonPlan): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        if ($this->isCsrfTokenValid('delete'.$lessonPlan->getId(), $request->request->get('_token'))) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($lessonPlan);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lesson_plan_index', ['student_id' => $lessonPlan->getStudent()->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route(path: '/teacher/pdf/lesson_plan/{lessonPlanId}', name: 'pdf_lesson_plan')]
    public function pdfLessonPlan(LessonPlanRepository $lessonPlanRepo, Mailer $mailer, Environment $twig, $lessonPlanId): Response
    {

        $lessonPlan = $lessonPlanRepo->findOneById($lessonPlanId);

        $session = $lessonPlan->getSession();

        $html = $twig->render('pdf/student_lesson_plan.html.twig', [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
        ]);

        $uigName = explode("-", (string) $session->getUserInstrumentGrade())[0];

        $arr = [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
        ];

        $this->toPdf($uigName.'-lessonPlan-'.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), "pdf/student_lesson_plan.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);

        return $response;

    }


    #[Route(path: '/teacher/pdf/test/dom/{lessonPlanId}', name: 'testdompdf')]
    public function testDomPdf(LessonPlanRepository $lessonPlanRepo, Mailer $mailer, Environment $twig, $lessonPlanId): Response
    {

        $lessonPlan = $lessonPlanRepo->findOneById($lessonPlanId);

        $session = $lessonPlan->getSession();

        $html = $twig->render('pdf/student_lesson_plan.html.twig', [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
        ]);

        $pdf = $this->mailPdfCreate($html);


        $arr = [
            'lesson_plan' => $lessonPlan,
            'session' => $session,
        ];

        $this->toPdf($lessonPlan->getStudent()->getFullname().'-lessonPlan-'.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), "pdf/student_lesson_plan.html.twig", $arr);
        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);

        return $response;

    }

    public function mailPdfCreate($html)
    {

        $pdf = null;
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream();
        return $pdf;

    }

    public function toPdf($filename, $path = '', $array = [])
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        $filename = str_replace(["/"," ",":"], "-", (string) $filename);

        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($html);
        // (Optional) Customize Dompdf options
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("$filename.pdf");

        //Generate pdf with the retrieved HTML
        // return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
        //     'Content-Type'          => 'application/pdf',
        //     'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        // )
        // );
    }

    #[Route(path: '/admin/send/lesson/plan', name: 'send_lesson_plan')]
    public function sendLessonPlan(
        LessonPlanRepository $lessonPlanRepo,
        Request $request,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig
    ): Response {

        $lessonPlanId = $request->request->get('lesson_plan_id');
        $lessonPlan = $lessonPlanRepo->findOneById($lessonPlanId);
        $session = $lessonPlan->getSession();

        $student = $lessonPlan->getStudent();
        $html = $twig->render('pdf/student_lesson_plan.html.twig', [
            'session' => $session,
            'lesson_plan' => $lessonPlan,
        ]);

        $uigName = explode("-", (string) $session->getUserInstrumentGrade())[0];

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $lessonPlan->getId(), 'Download Lesson Plan');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $uigName, 'Download Lesson Plan', 'Your Lesson plan has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());

        $mailer->sendEmailWithAttachment(['student' => $student, 'lesson_plan' => $lessonPlan], $student->getEmail(), $uigName.' Your Updated Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_student.html.twig', $uigName.'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The lesson was sent successfully.'
        );
        // send text with short url to pdf of invoice
        return new JsonResponse('test');

    }

    #[Route(path: '/ajax/save/new', name: 'new_lesson_plan')]
    public function newLessonPlan(
        LessonPlanRepository $lessonPlanRepo,
        Request $request,
        SessionRepository $sessionRepo,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig,
        UserRepository $userRepo
    ): Response {
        $new_stuff = $request->request->get('new_stuff');
        $finger_exercises = $request->request->get('finger_exercises');
        $previous_pieces = $request->request->get('previous_pieces');
        $current_piece = $request->request->get('current_piece');
        $techniques_reinforced = $request->request->get('techniques_reinforced');
        $extend_to_learn = $request->request->get('extend_to_learn');
        $print_out = $request->request->get('print_out');
        $assignment_for_student = $request->request->get('assignment_for_student');
        $modify = $request->request->get('modify');
        $sess_id = $request->request->get('session_id');

        $session = $sessionRepo->findOneById($sess_id);

        $activity_string = "";
        $activity_string .= "<h3>Warm-up</h3>";
        $activity_string .= "<p>" . nl2br($finger_exercises) . "</p>";
        $activity_string .= "<h3>Repertoire</h3>";
        $activity_string .= "<p>" . nl2br($previous_pieces) . "</p>";
        $activity_string .= "<h3>Creative</h3>";
        $activity_string .= "<p> Learn: " . nl2br($new_stuff) . "</p>";
        $activity_string .= "<h3>Current piece: " . $current_piece . "</h3>";
        $activity_string .= "<p> This piece will reinforce the following: ". $techniques_reinforced .". I intend to do " . $extend_to_learn . "</p>";
        $activity_string .= "<h3>Print outs</h3>";
        $activity_string .= "<p>" . $print_out . "</p>";
        $activity_string .= "<h3>Assignment</h3>";
        $activity_string .= "<p>" . nl2br($assignment_for_student) . "</p>";

        if($modify == 'false') {
            $lessonPlan = new LessonPlan();
            $lp = 'Lesson Plan';
        } else {
            $lessonPlan = $lessonPlanRepo->findOneBySession($session);
            $lp = 'Assessed Lesson Plan';
        }


        $lessonPlan->setAddedOn($session->getBeginAt());

        $lessonPlan->setSession($session);
        $lessonPlan->setStudent($session->getStudent());
        $lessonPlan->setAddedOn($session->getBeginAt());
        $lessonPlan->setWork($new_stuff);
        $lessonPlan->setActivities($activity_string);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($lessonPlan);
        $entityManager->flush();

        $student = $lessonPlan->getStudent();
        $teacher = $session->getTeacher();
        $html = $twig->render('pdf/student_lesson_plan.html.twig', [
            'session' => $session,
            'lesson_plan' => $lessonPlan,
        ]);

        $uigName = explode("-", (string) $session->getUserInstrumentGrade())[0];

        $admin = $userRepo->findOneByEmail("chezamusicschool@gmail.com");
        $admins = [
            $userRepo->findOneByEmail("oumabenjah018@gmail.com"),
            $userRepo->findOneByEmail("chezamusicschool@gmail.com")
        ];

        // get package
        $package = $session->getPackage();
        // get price per session
        $pricePerSession = $package->getCostPerLesson();
        // get uid and see how many are done
        $uig = $session->getUserInstrumentGrade();
        // number of sessions
        $sessions = $uig->getSessions();
        // count all sessions
        $number_of_sessions = count($sessions);
        // calculate done lessons
        $doneLessons = 0;
        foreach ($sessions as $key => $s) {
            if ($s->getDone() == true) {
                $doneLessons++;
            } 
        }
        $current_lesson = $doneLessons + 1;
        $payments = $uig->getPayments();
        $cashPaid = 0;
        foreach ($payments as $key => $payment) {
            if($payment->getType() == 'pmt'){
                $cashPaid += $payment->getAmount();
            }
        }



        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $lessonPlan->getId(), 'Download ' . $lp . '');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $uigName, 'Download ' . $lp, 'Your ' . $lp . ' has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());
        $sendSms->quickSend($teacher->getTeacherdata()->getPhone(), $teacher->getFullname(), '' . $lp . ' Sent', $uigName . '\'s ' . $lp . ' has been sent, and a copy has been sent to your email. Download using this link: '. $urlShort, 'necessary', 'personal_sms_from_office', $teacher->getEmail());

        $msg = "";
        try {
            // Send email to the student
            $mailer->sendEmailWithAttachment(
                ['student' => $student, 'lesson_plan' => $lessonPlan],
                $student->getEmail(),
                $session->getUserInstrumentGrade().' - Your ' . $lp . ' for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'),
                $html,
                'lesson_plan_student.html.twig',
                $uigName.'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'),
                'necessary',
                'personal_email_from_office'
            );
        } catch (\Exception $e) {
            // Log the error or handle it
            error_log('Failed to send email to student: ' . $e->getMessage());
            // You can add a flash message to notify about the error if needed
            $this->addFlash('error', 'Failed to send email to the student.');
            $msg .= "Failed to send email to the student";
        }
        
        try {
            // Send email to the teacher
            $mailer->sendEmailWithAttachment(
                ['teacher' => $teacher, 'student' => $student, 'lesson_plan' => $lessonPlan],
                $teacher->getEmail(),
                $uigName . '\'s ' . $lp . ' for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'),
                $html,
                'lesson_plan_teacher.html.twig',
                $uigName.'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'),
                'necessary',
                'personal_email_from_office'
            );
        } catch (\Exception $e) {
            // Log the error or handle it
            error_log('Failed to send email to teacher: ' . $e->getMessage());
            // You can add a flash message to notify about the error if needed
            $this->addFlash('error', 'Failed to send email to the teacher.');
            $msg .= "Failed to send email to the teacher";
        }
        
        $this->addFlash(
            'success',
            'The lesson plan was sent successfully.'
        );

        if (stripos((string) $student->getFullname(), 'school') === false && stripos((string) $student->getFullname(), 'church') === false){
            $this->feesNotify->notifyIfPaidFor($current_lesson, $pricePerSession, $number_of_sessions, $doneLessons, $cashPaid, $session->getTeacher(), $admins, $session->getStudent(), $session->getUserInstrumentGrade(), "prepared a lesson plan for");
        } else {
            // do nothing
        }

        // send text with short url to pdf of invoice
        return new JsonResponse($msg);

    }

    #[Route(path: '/ajax/save/edit', name: 'update_lesson_plan')]
    public function updateLessonPlan(
        LessonPlanRepository $lessonPlanRepo,
        Request $request,
        SessionRepository $sessionRepo,
        Mailer $mailer,
        SendSms $sendSms,
        ShortenUrl $shortenUrl,
        Environment $twig
    ): Response {
        $assessment = $request->request->get('assessment');
        $priorities = $request->request->get('priorities');
        $modify = $request->request->get('modify');
        $sess_id = $request->request->get('session_id');

        $session = $sessionRepo->findOneById($sess_id);

        $lessonPlan = $lessonPlanRepo->findOneBySession($session);


        $lessonPlan->setAssessment($assessment);
        $lessonPlan->setNextLesson($priorities);
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($lessonPlan);
        $entityManager->flush();

        $student = $lessonPlan->getStudent();
        $teacher = $session->getTeacher();
        $html = $twig->render('pdf/student_lesson_plan.html.twig', [
            'session' => $session,
            'lesson_plan' => $lessonPlan,
        ]);

        $uigName = explode("-", (string) $session->getUserInstrumentGrade())[0];

        $urlShort = $shortenUrl->new('https://chezamusicschool.co.ke/lesson/plan/teacher/pdf/lesson_plan/', $lessonPlan->getId(), 'Download Lesson Plan');
        $sendSms->quickSend($student->getStudentdata()->getPhone(), $uigName, 'Download Lesson Plan', 'Your updated lesson plan with assessment has been sent to your email. Download: '. $urlShort, 'necessary', 'personal_sms_from_office', $student->getEmail());
        $sendSms->quickSend($teacher->getTeacherdata()->getPhone(), $teacher->getFullname(), 'Lesson Plan Sent', $uigName . '\'s updated lesson plan has been sent. Download: '. $urlShort, 'necessary', 'personal_sms_from_office', $teacher->getEmail());

        $mailer->sendEmailWithAttachment(['student' => $student, 'lesson_plan' => $lessonPlan], $student->getEmail(), $session->getUserInstrumentGrade().' - Your Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_student.html.twig', $uigName.'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        $mailer->sendEmailWithAttachment(['teacher' => $teacher, 'student' => $student, 'lesson_plan' => $lessonPlan], $teacher->getEmail(), $uigName . '\'s Lesson Plan for '.$lessonPlan->getSession()->getBeginAt()->format('d/m/Y h:ia'), $html, 'lesson_plan_teacher.html.twig', $uigName.'-lessonPlan-'.$session->getBeginAt()->format('d/m/Y'), 'necessary', 'personal_email_from_office');
        $this->addFlash(
            'success',
            'The lesson plan was sent successfully.',
        );
        // send text with short url to pdf of invoice
        return new JsonResponse($sess_id);

    }

    #[Route(path: '/teacher/show/sessions/for/uig', name: 'show_uig_sessions')]
    public function showSessionsForUig(
        UserInstrumentGradeRepository $uigRepo,
        Request $request
    ) {
        $this_weeks_sessions = [];
        $other_sessions = [];
        $today = new \Datetime();
        $todayDt = $today->format('W');
    
        $uig_id = $request->request->get('uig_id');
        $uig = $uigRepo->findOneById($uig_id);
        foreach ($uig->getSessions() as $sess) {
            if($sess->getBeginAt()->format('W') == $todayDt) {
                $this_weeks_sessions[] = $sess;
            } else {
                $other_sessions[] = $sess;
            }
        }
    
        $html = "";
        $html .= "<div class='row'>";
        $html .= "<hr/><div class='col-12'><h4>This week</h4></div><hr/>";
        foreach ($this_weeks_sessions as $key => $session) {
            $html .= "<div class='col-6'>";
            if ($session->getBeginAt() >= $today) {
                $html .= "<a class='mb-1 btn btn-info btn-block' href='/lesson/plan/new/for/". $session->getId() . "'>" . $session->getBeginAt()->format('jS M \\a\\t h:ia') . "</a>";
            } else {
                $html .= "<span class='mb-1 btn btn-secondary btn-block'>" . $session->getBeginAt()->format('jS M \\a\\t h:ia') . "</span>";
            }
            $html .= "</div>";
        }
        $html .= "<hr/><div class='col-12'><h4>Other Sessions</h4></div><hr/>";
        foreach ($other_sessions as $key => $session) {
            $html .= "<div class='col-6'>";
            if ($session->getBeginAt() >= $today) {
                $html .= "<a class='mb-1 btn btn-info btn-block' href='/lesson/plan/new/for/". $session->getId() . "'>" . $session->getBeginAt()->format('jS M \\a\\t h:ia') . "</a>";
            } else {
                $html .= "<span class='mb-1 btn btn-secondary btn-block'>" . $session->getBeginAt()->format('jS M \\a\\t h:ia') . "</span>";
            }
            $html .= "</div>";
        }
        $html .= "</div>";
        return new JsonResponse($html);
    }


}
