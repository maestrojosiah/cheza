<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InstrumentRepository;
use App\Repository\BlogRepository;
use App\Repository\WorkshopRepository;
use App\Repository\WShopRepository;
use App\Entity\Workshop;
use App\Repository\UserRepository;
use Sonata\SeoBundle\Seo\SeoPageInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Mailer;
use App\Service\SendSms;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\ConfigurationRepository;
use App\Repository\NavigationRepository;
use App\Repository\WebPhotosRepository;
use App\Repository\CourseRepository;
use App\Entity\Course;
use App\Entity\User;
use App\Repository\BlogCategoryRepository;
use App\Repository\LessonRepository;
use Symfony\Component\Routing;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\Packages;
use Symfony\Bridge\Twig\Extension\AssetExtension;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use App\Repository\SectionRepository;
use App\Repository\PackageRepository;
use App\Repository\PageRepository;
use App\Repository\UserInstrumentGradeRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends AbstractController
{
    public function __construct(private readonly UserRepository $usr, private readonly ConfigurationRepository $config, private readonly NavigationRepository $nav, private readonly BlogRepository $blog, private readonly InstrumentRepository $instrumentRepo, private readonly SectionRepository $sectionRepo, private readonly PackageRepository $packageRepo, private readonly WebPhotosRepository $photoRepo, private readonly PageRepository $pageRepo, private readonly CourseRepository $courseRepo, private readonly BlogCategoryRepository $blogCategoryRepo, private readonly UserInstrumentGradeRepository $uigRepo, private readonly RouterInterface $router, private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }


    #[Route(path: 'blog/{title}/{id}', name: 'public_blog_show', methods: ['GET'])]
    public function publicBlogShow(BlogRepository $blogRepo, $id, PageRepository $pageRepo, CourseRepository $courseRepo): Response
    {
        $page = $pageRepo->findOneByName('global');
        $courses = $courseRepo->findAll();

        $glVars = $this->getGlobalVars();
        $pagePhotos = $this->getAllPagePhotos($page);

        return $this->render('blog/public_show.html.twig', [
            'blog' => $blogRepo->findOneById($id),
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos, 'courses' => $courses],
        ]);
    }

    #[Route(path: '/pay/online/std/{student_id}', name: 'payonline', methods: ['GET'])]
    public function pay(PageRepository $pageRepo, $student_id): Response
    {
        $page = $pageRepo->findOneByName('global');
        $student = $this->usr->findOneById($student_id);
        $payments = $student->getPayments();
        $studentData = $student->getStudentdata();
        $uigs = $this->uigRepo->findBy(
            ['studentUserData' => $studentData],
            ['id' => 'ASC']
        );

        $glVars = $this->getGlobalVars();
        $glVars['student'] = $student;
        $glVars['payments'] = $payments;
        $glVars['uigArr'] = $uigs;

        $pagePhotos = $this->getAllPagePhotos($page);

        return $this->render('default/pay.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos],
        ]);
    }

    #[Route(path: '/pay/for/{eventName}', name: 'payforevent', methods: ['GET'])]
    public function payForEvent(PageRepository $pageRepo, $eventName): Response
    {
        $page = $pageRepo->findOneByName('global');

        $glVars = $this->getGlobalVars();
        $glVars['eventName'] = $eventName;

        $pagePhotos = $this->getAllPagePhotos($page);

        return $this->render('default/pay_event.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos],
        ]);
    }

    #[Route(path: '/convenience/links', name: 'cheza_links', methods: ['GET'])]
    public function chezaLinks(): Response
    {

        return $this->render('default/cheza_links.html.twig', [
            'test' => 'test',
        ]);
    }

    #[Route(path: '/cheza/teacher/details/{name}/{id}', name: 'teacher_page', methods: ['GET'])]
    public function teacher(User $teacher, PageRepository $pageRepo): Response
    {
        $page = $pageRepo->findOneByName('global');

        $glVars = $this->getGlobalVars();
        $pagePhotos = $this->getAllPagePhotos($page);

        return $this->render('default/teacher.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos],
            'teacher' => $teacher,
        ]);
    }

    #[Route(path: '/music/theory/online/quiz/abrsm', name: 'abrsm_online_theory_quiz', methods: ['GET'])]
    public function theory_quiz(LessonRepository $lessonRepo, PageRepository $pageRepo, CourseRepository $courseRepo): Response
    {
        $page = $pageRepo->findOneByName('global');
        $courses = $courseRepo->findAll();


        $glVars = $this->getGlobalVars();
        $glVars['courses'] = $courses;
        $pagePhotos = $this->getAllPagePhotos($page);
        $lessons = $lessonRepo->findAllOrderedByGrade(); 
        $grades = [];
        foreach ($lessons as $key => $lesson) {
            $grades[$lesson->getGrade()] = $lesson->getGrade();
        }

        return $this->render('default/theory_quiz.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos, 'courses' => $courses],
            'lessons' => $lessons,
            'grades' => $grades,
        ]);
    }

    #[Route(path: '/music/theory/online/quiz/abrsm/{grade}', name: 'abrsm_online_theory_quiz_grade', methods: ['GET'])]
    public function theory_quiz_grade(LessonRepository $lessonRepo, PageRepository $pageRepo, CourseRepository $courseRepo, $grade ): Response
    {
        $page = $pageRepo->findOneByName('global');
        $courses = $courseRepo->findAll();


        $glVars = $this->getGlobalVars();
        $glVars['courses'] = $courses;
        $pagePhotos = $this->getAllPagePhotos($page);
        $lessons = $lessonRepo->findByGrade($grade); 

        return $this->render('default/theory_quiz_grade.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos, 'courses' => $courses],
            'lessons' => $lessons,
            'grade' => str_replace('-', ' ', $grade),
        ]);
    }

    #[Route(path: '/course/view/details/{name}/{id}', name: 'course_public_show', methods: ['GET'])]
    public function publicshow(Course $course, CourseRepository $courseRepo, PageRepository $pageRepo): Response
    {
        $page = $pageRepo->findOneByName('global');
        $courses = $courseRepo->findAll();

        $glVars = $this->getGlobalVars(3, 15);
        $glVars['courses'] = $courses;
        $pagePhotos = $this->getAllPagePhotos($page);
        $allInstruments = $this->instrumentRepo->findAll();

        // $glVars['allInstruments'] = $allInstruments;

        return $this->render('default/course.html.twig', [
            'global_vars' => ['vars' => $glVars, 'photos' => $pagePhotos, 'courses' => $courses],
            'course' => $course,
        ]);
    }

    #[Route(path: '/teachers/at/cheza/music/school', name: 'teachers')]
    public function teachers(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        // this page
        $page = $pageRepo->findOneByName('teachers');

        // html is now empty
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars();

        $pagePhotos = $this->getAllPagePhotos($page);
        $teachers = $this->usr->findBy(
            ['usertype' => 'teacher', 'active' => 1],
            ['fullname' => 'ASC']
        );
        $glVars['teachers'] = $teachers;

        $rendered = $this->generateTemplate($html, 'teachers.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/teachers.html.twig', [
            'global_vars' => $glVars,
            'html' => html_entity_decode((string) $html),
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }


    #[Route(path: '/news/from/cheza/music/school', name: 'news')]
    public function news(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {

        // this page
        $page = $pageRepo->findOneByName('blog');

        // html is now empty
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars(20);

        $pagePhotos = $this->getAllPagePhotos($page);

        $rendered = $this->generateTemplate($html, 'news.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/news.html.twig', [
            'global_vars' => $glVars,
            'html' => html_entity_decode((string) $html),
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    #[Route(path: '/contacts/for/cheza/music/school', name: 'contacts')]
    public function contacts(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {

        // this page
        $page = $pageRepo->findOneByName('contact');

        // html is now empty
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars();

        $pagePhotos = $this->getAllPagePhotos($page);

        $rendered = $this->generateTemplate($html, 'contact.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/contact.html.twig', [
            'global_vars' => $glVars,
            'html' => html_entity_decode((string) $html),
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    #[Route(path: '/about/cheza/classes', name: 'classes')]
    public function classes(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {

        // this page
        $page = $pageRepo->findOneByName('courses');

        // html is now empty
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars(3, 120);

        $pagePhotos = $this->getAllPagePhotos($page);

        $rendered = $this->generateTemplate($html, 'classes.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/classes.html.twig', [
            'global_vars' => $glVars,
            'html' => html_entity_decode((string) $html),
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    #[Route(path: '/about/cheza/music/school', name: 'about')]
    public function about(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        // this page
        $page = $pageRepo->findOneByName('about');

        // html is now empty
        $html = "";
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        // iterate sections to find content
        $html = $this->generateHtml($organizedSections);
        $glVars = $this->getGlobalVars();

        $pagePhotos = $this->getAllPagePhotos($page);

        $rendered = $this->generateTemplate($html, 'about.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/about.html.twig', [
            'global_vars' => $glVars,
            'html' => html_entity_decode((string) $html),
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    #[Route(path: '/', name: 'default')]
    public function index(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        if(count($this->config->findAll()) == 0) {
            return $this->redirectToRoute('configuration_new');
        }

        // this page
        $page = $pageRepo->findOneByName('homepage');
        $teachers = $this->usr->findTeachersByRand();

        // html is now empty
        $html = "";
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars(3, 15);
        $pagePhotos = $this->getAllPagePhotos($page);
        $glVars['teachers'] = $teachers;

        $rendered = $this->generateTemplate($html, 'index.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/index.html.twig', [
            'global_vars' => $glVars,
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    #[Route(path: '/all/global/templates', name: 'global')]
    public function global(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        // this page
        $page = $pageRepo->findOneByName('global');
        $teachers = $this->usr->findBy(
            ['usertype' => 'teacher', 'active' => 1],
            ['photo' => 'DESC']
        );

        // html is now empty
        $html = "";
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            ['page' => $page, 'visible' => 1],
            ['position' => 'ASC'],
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars();
        $pagePhotos = $this->getAllPagePhotos($page);
        $glVars['teachers'] = $teachers;

        $rendered = $this->generateTemplate($html, 'index.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/index.html.twig', [
            'global_vars' => $glVars,
            'rendered' => html_entity_decode((string) $rendered),
        ]);

    }

    public function generateHtml($organizedSections)
    {

        $html = "";

        // iterate sections to find content
        foreach ($organizedSections as $section) {

            // div start
            $bhtml = "<div class='sec' id='section_" . $section->getId() . "'>";
            // $bhtml .= "<button type='button' style='z-index:1000;' id='saveChanges_".$section->getId()."' class='save-changes-btn button'>Save changes</button>";
            if ($this->isGranted('ROLE_ADMIN')) {
                $bhtml .= "<a href='/section/".$section->getId()."/edit' style='z-index:1001;' id='edit_".$section->getId()."' target='_blank' class='save-changes-btn nicdark_background_none_hover nicdark_color_white_hover nicdark_border_2_solid_white_hover nicdark_border_2_solid_green nicdark_transition_all_08_ease nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_color_white nicdark_bg_green nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3'>Edit " . $section->getName() . "</a>";
            }

            // generate page forms for uploading images
            [$buildImgLinks, $photos] = $this->getImgForms($section->getWebPhotos());

            // main section text
            $bhtml .= $section->getTypography();

            // if is admin, then put in the forms for uploading images
            if ($this->isGranted('ROLE_ADMIN')) {
                $bhtml .= "<div class='attach-bottom'>" . $buildImgLinks . "</div>";
            }

            // div end
            $bhtml .= "</div>";

            // clear any floats
            $bhtml .= "<div style='clear:both;float:none;'></div>";

            // clean the code and add in the real images
            $html .= htmlspecialchars_decode($bhtml);

        }

        return $html;

    }
    public function getGlobalVars($blogCount = 3, $courseCount = 3)
    {

        $data = [];
        $allInstruments = $this->instrumentRepo->findAll();
        $courses = $this->courseRepo->findBy(
            [],
            ['type' => 'DESC'],
            $courseCount
        );
        $packages = $this->packageRepo->findAll();

        $configData = $this->config->findAll();
        $navData = $this->nav->findAll();
        $blogCategories = $this->blogCategoryRepo->findAll();
        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['configData'] = $configData ? $configData[0] : null;
        $glVars['navData'] = $navData;
        $glVars['thisUrl'] = $url['path'];
        $glVars['thisUser'] = $this->getUser();
        $glVars['blogCategories'] = $blogCategories;
        $glVars['blogs'] = $this->blog->findBy(
            [],
            ['published_at' => 'DESC'],
            $blogCount
        );
        $glVars['allInstruments'] = $allInstruments;
        $glVars['courses'] = $courses;
        $glVars['packages'] = $packages;

        return $glVars;

    }

    public function getAllPagePhotos($page)
    {
        $photos = [];
        foreach ($page->getSections() as $key => $section) {
            $sectionImages = $this->photoRepo->findBy(
                ['section' => $section],
                ['id' => 'ASC']
            );
            $photos[$section->getName()] = $sectionImages;

        }
        $globalPage = $this->pageRepo->findOneByName('global');

        if($globalPage) {

            foreach ($globalPage->getSections() as $key => $section) {
                $sectionImages = $this->photoRepo->findBy(
                    ['section' => $section],
                    ['id' => 'ASC']
                );
                $photos[$section->getName()] = $sectionImages;

            }

        }

        return $photos;
    }

    public function getImgForms($webPhotos)
    {

        $photos = [];
        $photoIds = [];

        $buildImgLinks = "";
        foreach($webPhotos as $wp) {
            $photos[] = $wp->getUrl();
            $photoIds[] = $wp->getId().'*'.$wp->getUrl();
            $currentPath = $_SERVER['REQUEST_URI'];
            $thumnail = $wp->getThumbnail();
            $url = $wp->getUrl();
            $id = $wp->getId();
            $buildImgLinks .= '<div style="width:85%;margin:auto;">';
            $buildImgLinks .= '<form name="photo" style="float:left;border:1px solid gray;margin-top:5px;margin-right:5px;padding:5px;" id="imageUploadForm-'.$id.'" enctype="multipart/form-data" action="/upload/website/photo" method="post">';
            $buildImgLinks .= '<input type="file" style="margin:5px;" hidden="hidden" id="ImageBrowse-'.$id.'" name="image" size=""/>';
            // $buildImgLinks .= '<input type="submit" name="upload" value="Upload" />';
            $buildImgLinks .= '<input type="hidden" name="imageId" value="'.$id.'" />';
            $buildImgLinks .= '<input type="hidden" name="toPage" value="'.$currentPath.'" />';
            $buildImgLinks .= '<img width="100" title="' . $url . '" style="border:#000; z-index:1;position: relative; border-width:2px; float:left; width:auto;" height="100px" src="'. $url .'" id="thumbnail-'.$id.'"/>';
            $buildImgLinks .= '</form>';
            $buildImgLinks .= '</div>';
        }

        return [$buildImgLinks, $photos];

    }

    public function getImages($html)
    {

        if (preg_match_all('/#%(.*?)%#/', (string) $html, $match)) {
            return $match;
        } else {
            return 'no images';
        }
    }

    public function replacePlaceholdersWithImages($photosForThisContent, $html)
    {
        $pattern = [];
        $replace = [];
        foreach ($photosForThisContent as $key => $imgP) {
            $pattern[] = '/#%(.*?)-'.$key.'%#/';
        }
        foreach ($photosForThisContent as $key => $photo) {
            $replace[] = $photo;
        }
        // $pattern = ['/#%(.*?)-1%#/','/#%(.*?)-2%#/','/#%(.*?)-3%#/'];
        // $replace = ["image-1","image-2","image-3"];
        // echo "<pre>";
        // print_r($pattern);
        // print_r($replace);
        return preg_replace($pattern, $replace, htmlentities((string) $html));
        // echo "</pre>";

    }


    #[Route(path: '/security/select', name: 'select_reg')]
    public function select_reg(): Response
    {
        return $this->render('default/select_reg.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    #[Route(path: '/testing/website/stuff', name: 'testing_stuff')]
    public function test(UserRepository $userRepo): Response
    {
        $url = "https://maps.googleapis.com/maps/api/place/details/json?key=Yourkey&placeid=ChIJoUKQbC0XLxgRg4zYV6RLEgk";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $res        = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
        $reviews    = $res['result']['reviews'];
        return $this->render('default/cropper.html.twig', [
            'student' => $reviews,
        ]);
    }

    #[Route(path: '/testing/website/stuff/again', name: 'testing_stuff_again')]
    public function testagain(UserRepository $userRepo): Response
    {
        $student = $userRepo->findOneById(5);
        return $this->render('default/cropperFromUrl.html.twig', [
            'student' => $student,
        ]);
    }

    #[Route(path: '/resize/website/photo/{photo_id}', name: 'resize_uploaded_web_photo')]
    public function replaceUploadedPhoto(WebPhotosRepository $photoRepo, $photo_id): Response
    {
        $photo = $photoRepo->findOneById($photo_id);
        return $this->render('default/cropperFromUrl.html.twig', [
            'photo' => $photo,
        ]);
    }

    #[Route(path: '/crop/resize/and/replace/{url}', name: 'crop_resize_replace')]
    public function resizeAndReplace(WebPhotosRepository $photoRepo, $url): Response
    {
        return $this->render('default/cropAndReplace.html.twig', [
            'url' => $url,
        ]);
    }

    public function checkConfigs(): RedirectResponse
    {
        // var_dump(count($this->config->findAll()));
        if(count($this->config->findAll()) == 0) {
            return $this->redirectToRoute('configuration_new');
        }
        // if(count($this->pageRepo->findAll()) == 0){
        //     return $this->redirectToRoute('page_new');
        // }

        // if(count($this->sectionRepo->findAll()) == 0){
        //     return $this->redirectToRoute('section_new');
        // }

    }

    public function generateTemplate($html, $template, $variables)
    {
        $loader = new \Twig\Loader\ArrayLoader([
            $template => $html,
            'header_global' => $this->sectionRepo->findOneByName('header_global') ? $this->sectionRepo->findOneByName('header_global')->getTypography() : null,
            'footer_global' => $this->sectionRepo->findOneByName('footer_global') ? $this->sectionRepo->findOneByName('footer_global')->getTypography() : null,
            'address_global' => $this->sectionRepo->findOneByName('address_global') ? $this->sectionRepo->findOneByName('address_global')->getTypography() : null,
            'contact-mini-form' => $this->sectionRepo->findOneByName('contact-mini-form') ? $this->sectionRepo->findOneByName('contact-mini-form')->getTypography() : null,
            'modal-mini-form' => $this->sectionRepo->findOneByName('modal-mini-form') ? $this->sectionRepo->findOneByName('modal-mini-form')->getTypography() : null,
            'pricing_global' => $this->sectionRepo->findOneByName('pricing_global') ? $this->sectionRepo->findOneByName('pricing_global')->getTypography() : null,
            'sidebar_right' => $this->sectionRepo->findOneByName('sidebar_right') ? $this->sectionRepo->findOneByName('sidebar_right')->getTypography() : null,
            'book_lessons' => $this->sectionRepo->findOneByName('book_lessons') ? $this->sectionRepo->findOneByName('book_lessons')->getTypography() : null,
            'start_learning' => $this->sectionRepo->findOneByName('start_learning') ? $this->sectionRepo->findOneByName('start_learning')->getTypography() : null,
            'contactform_global' => $this->sectionRepo->findOneByName('contactform_global') ? $this->sectionRepo->findOneByName('contactform_global')->getTypography() : null,
            'teacherinfo' => $this->sectionRepo->findOneByName('teacherinfo') ? $this->sectionRepo->findOneByName('teacherinfo')->getTypography() : null,
            'courseinfo' => $this->sectionRepo->findOneByName('courseinfo') ? $this->sectionRepo->findOneByName('courseinfo')->getTypography() : null,
            'bloginfo' => $this->sectionRepo->findOneByName('bloginfo') ? $this->sectionRepo->findOneByName('bloginfo')->getTypography() : null,
        ]);

        $twig = new \Twig\Environment($loader);
        $profile = new \Twig\Profiler\Profile();
        $context = new Routing\RequestContext();
        $router = $this->router;
        $routes = $router->getRouteCollection();

        $generator = new Routing\Generator\UrlGenerator($routes, $context);
        $versionStrategy = new StaticVersionStrategy('v1');
        $defaultPackage = new Package($versionStrategy);
        $namedPackages = [
            'img' => new UrlPackage('http://img.example.com/', $versionStrategy),
            'doc' => new PathPackage('/somewhere/deep/for/documents', $versionStrategy),
        ];

        $packages = new Packages($defaultPackage, $namedPackages);

        // this enables template_from_string function
        $twig->addExtension(new \Twig\Extension\StringLoaderExtension());
        // this enables debug
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        // this enables {{asset('somefile')}}
        $twig->addExtension(new AssetExtension($packages));
        // this enables {{path('someroute')}}
        $twig->addExtension(new RoutingExtension($generator));

        $rendered = $twig->render($template, ['global_vars' => $variables]);

        return $rendered;

    }

    #[Route(path: '/test/for/cheza/music/school', name: 'test')]
    public function testing(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        // this page
        $page = $pageRepo->findOneByName('about');

        // html is now empty
        $html = "<html><body><h1>Hello from test</h1></body></html>";

        // THIS WAS THE PROBLEMATIC PART
        $html .= '{{ include("header.html.twig") }}';

        $variables = ['this' => 'that'];

        // HERE IS THE SOLUTION -> ADD THE NEW TEMPLATE TO THE ARRAY LOADER
        $loader = new \Twig\Loader\ArrayLoader([
            'test.html.twig' => '{{ include(template_from_string(html)) }}',
            'header.html.twig' =>  "<html><body><h1>Hello from {{name}}</h1></body></html>",
        ]);

        $twig = new \Twig\Environment($loader);
        $profile = new \Twig\Profiler\Profile();
        $context = new Routing\RequestContext();
        $router = $this->router;
        $routes = $router->getRouteCollection();

        $generator = new Routing\Generator\UrlGenerator($routes, $context);
        $versionStrategy = new StaticVersionStrategy('v1');
        $defaultPackage = new Package($versionStrategy);
        $namedPackages = [
            'img' => new UrlPackage('http://img.example.com/', $versionStrategy),
            'doc' => new PathPackage('/somewhere/deep/for/documents', $versionStrategy),
        ];

        $packages = new Packages($defaultPackage, $namedPackages);

        // this enables template_from_string function
        $twig->addExtension(new \Twig\Extension\StringLoaderExtension());
        // this enables debug
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        // this enables {{asset('somefile')}}
        $twig->addExtension(new AssetExtension($packages));
        // this enables {{path('someroute')}}
        $twig->addExtension(new RoutingExtension($generator));

        $response = new Response();

        $response->setContent($twig->render('test.html.twig', ['html' => $html, 'name' => 'header']));
        $response->setStatusCode(Response::HTTP_OK);

        // sets a HTTP response header
        $response->headers->set('Content-Type', 'text/html');

        // return response
        return $response;

    }


    #[Route(path: '/security/welcome/{user_id}', name: 'welcome_page')]
    public function welcome(UserRepository $userRepo, $user_id): Response
    {
        $user = $userRepo->findOneById($user_id);
        return $this->render('default/welcome.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route(path: '/activity/register/{wshop_id}/{wshop_title}', name: 'activity')]
    public function activity(SeoPageInterface $seoPage, WShopRepository $wShopRepo, $wshop_id): Response
    {
        $wshop = $wShopRepo->findOneById($wshop_id);
        $captcha = $this->getParameter('captcha_key');
        return $this->render('default/activity.html.twig', [
            'wshop' => $wshop,
            'captcha_key' => $captcha,
        ]);

    }

    #[Route(path: '/activity/register/list/for/{wshop_id}', name: 'activity_list')]
    public function activity_list(WorkshopRepository $workshopRepo, WShopRepository $wShopRepo, $wshop_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $wshop = $wShopRepo->findOneById($wshop_id);
        $contacts = $workshopRepo->findByWshop($wshop);

        return $this->render('default/workshop_registration_list.html.twig', [
            'contacts' => $contacts,
        ]);

    }

    #[Route(path: '/activity/register/mail/list/{wshop_id}', name: 'activity_mail_list')]
    public function activity_mail_list(WorkshopRepository $workshopRepo, WShopRepository $wShopRepo, $wshop_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $wshop = $wShopRepo->findOneById($wshop_id);
        $contacts = $workshopRepo->findByWshop($wshop);

        return $this->render('default/workshop_mail_list.html.twig', [
            'contacts' => $contacts,
        ]);

    }

    #[Route(path: '/pages/privacy-policy', name: 'privacy_policy')]
    public function privacy_policy(): Response
    {
        return $this->render('default/privacy-policy.html.twig', [
            'test' => 'test',
        ]);

    }

    #[Route(path: '/new/activity/register/thankyou', name: 'thankyou')]
    public function thankyou(SeoPageInterface $seoPage): Response
    {
        // get the instrument from url if available
        if (isset($_SERVER['QUERY_STRING']) && $_GET) {
            $query = explode("=", (string) $_SERVER['QUERY_STRING'])[1];
        } else {
            $query = "";
        }

        return $this->render('default/thankyou.html.twig', [
            'msg' => urldecode($query),
        ]);

    }

    #[Route(path: '/new/activity/delete/contact', name: 'delete_contact')]
    public function deletecontact(WorkshopRepository $contactRepo, Request $request): Response
    {
        $contact_id = $request->request->get('contact_id');
        $contact = $contactRepo->findOneById($contact_id);
        $this->delete($contact);
        return new JsonResponse('deleted contact');
    }

    #[Route(path: '/new/activity/update/attended', name: 'update_attended')]
    public function updateAttended(WorkshopRepository $contactRepo, Request $request): Response
    {
        $contact_id = $request->request->get('contact_id');
        $contact = $contactRepo->findOneById($contact_id);
        $attended = $request->request->get('attended');
        $contact->setAttended($attended);
        $this->save($contact);
        return new JsonResponse('updated');
    }

    #[Route(path: '/new/activity/action/register', name: 'register_for_activity', methods: ['POST'])]
    public function register_for_activity(Mailer $mailer, SendSms $sendSms, WShopRepository $wShopRepo): Response
    {
        // check if reCaptcha has been validated by Google
        $secret = $this->getParameter('captcha_secret_key');
        $captchaId = $_POST['g-recaptcha-response'];

        //sends post request to the URL and tranforms response to JSON
        $responseCaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captchaId), null, 512, JSON_THROW_ON_ERROR);


        if($responseCaptcha->success == true) {

            $f_name = $_POST['f_name'];
            $l_name = $_POST['l_name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $extra = $_POST['extra'];
            $comment = $_POST['comment'];
            $wshop_id = $_POST['wshop_id'];

            $wshop = $wShopRepo->findOneById($wshop_id);

            $phonenumber = $this->fullNumber($phone);

            $workshop = new Workshop();
            $workshop->setFname($f_name);
            $workshop->setLname($l_name);
            $workshop->setEmail($email);
            $workshop->setPhone($phone);
            $workshop->setExtra($extra);
            $workshop->setComment($comment);
            $workshop->setWshop($wshop);

            $this->save($workshop);

            $msg = "Hello " . $f_name . ". Your Registration for " . $wshop->getTitle()  . " that is starting on / scheduled for " .  $wshop->getHappensOn()->format('jS F Y') . " is successful." . " We will communicate about other details on a separate message.";
            $mail_msg = "Your Registration for " . $wshop->getTitle()  . " that is starting on / scheduled for " .  $wshop->getHappensOn()->format('jS F Y') . " is successful." . " We will communicate about other details on a separate message.";
            $mail_msg2 = "New Registration for " . $wshop->getTitle()  . " that is starting on / scheduled for " .  $wshop->getHappensOn()->format('jS F Y') . " is successful." . " Please communicate about other details to $f_name $l_name, $phone, $email.";

            $maildata = ['user' => ['fullname' => $f_name . ' ' . $l_name], 'msg' => $mail_msg];
            $maildata2 = ['user' => ['fullname' => 'Admin'], 'msg' => $mail_msg2];
            $mailer->sendEmail($maildata, $email, "Welcome to " . $wshop->getTitle(), "communication.html.twig", "necessary", "personal_email_from_office");
            $mailer->sendEmail($maildata2, 'chezamusicschool@gmail.com', "New registration for " . $wshop->getTitle(), "communication.html.twig", "necessary", "personal_email_from_office");
            $sendSms->quickSend($phonenumber, $f_name, "Welcome to " . $wshop->getTitle(), $msg, 'necessary', 'personal_sms_from_office', 'nomail');

            return $this->redirectToRoute('thankyou', ['msg' => "<strong>Welcome to the " . $wshop->getTitle() . "</strong> We will communicate on the way forward."]);

        } else {
            var_dump('fail');
            die();
        }

    }

    // Define function to test
    public function _is_curl_installed()
    {
        if  (in_array('curl', get_loaded_extensions())) {
            return true;
        } else {
            return false;
        }
    }

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

    public function fullNumber($num)
    {
        $number = "";
        if(strlen((string) $num) == 10) {
            $number = preg_replace('/^0/', '+254', (string) $num);
        } elseif(strlen((string) $num) == 13) {
            $number = $num;
        } else {
            $number = $num;
        }
        return $number;
    }

}
